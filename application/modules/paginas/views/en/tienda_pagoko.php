<body class="woocommerce woocommerce-page kopa-subpage">
<div id="main-content">
	[menu]
		<header class="page-header have-disc-icon text-center">
			<div class="disc-bg"></div>
            <div class="mask"></div>

            <div class="page-header-bg-2 page-header-bg"></div>

            <div class="page-header-inner page-header-inner-1">

                <div class="wrapper">
                    
                    <h1 class="page-title">Pago procesado con éxito</h1>

                    <div class="breadcrumb clearfix">                    
                        <span itemtype="http://data-vocabulary.org/Breadcrumb" itemscope="">
                            <a href="<?= base_url() ?>" itemprop="url">
                                <span itemprop="title">Inicio</span>
                            </a>
                        </span>
                        <span>&nbsp;/&nbsp;</span>
                        <span itemtype="http://data-vocabulary.org/Breadcrumb" itemscope="">
                            <a href="<?= base_url() ?>" itemprop="url">
                                <span itemprop="title">Tienda</span>
                            </a>
                        </span>
                        <!-- <span>&nbsp;/&nbsp;</span> -->
                        <!-- <span itemtype="http://data-vocabulary.org/Breadcrumb" itemscope="" class="current-page"><span itemprop="title">Typography</span></span> -->
                    </div>
                    <!-- breadcrumb -->

                </div>
                <!-- wrapper -->

                </div>
                <!-- page-header-inner -->

                <div class="album-icon">
	                <div class="icon-inner-1">
	                    <span class="icon-inner-2"></span>
	                </div>
	                <span class="fa fa-music"></span>
	            </div>
            
        </header>
        <!-- page-header -->
		<div class="wrapper clearfix">
			<section class="elements-box mb-40">
				<div class="row">
					<div class="col-xs-12 col-sm-12">
			            <div class="element-title">
			                <h4>Resultado de pago</h4>
			                <p><i class="fa fa-times fa-5x" style="color:red"></i></p>
			                <p>¡Algo ha salido mal!</p>
			            </div>	
			            <div class="row">
			                <div class="col-md-12 col-sm-12 col-xs-12 mb-40">
			                    <p>Hemos intentado realizar tu pago pero no hemos podido realizarlo, las posibles causas pueden ser:</p>
		                        <ul>
		                            <li>Se ha cancelado la compra</li>
		                            <li>Se ha rechazado tu tarjeta de crédito por el banco</li>
		                            <li>Saldo insuficiente en tu tarjeta de crédito</li>
		                        </ul>                        
		                        <p>Llama al <a href="tel:+34902002068">902 002 068</a> y nuestras agentes del Mallorca Island Festival te ayudarán a solucionarlo.</p>
			                </div>
			                <!-- col-md-12 -->
			            </div>		            
		            </div>		            
				</div>
	            
	        </section>
	        </div>


			
	[footer]
</div>