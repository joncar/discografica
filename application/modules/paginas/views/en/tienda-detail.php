<body class="woocommerce woocommerce-page kopa-subpage">
	[menu]
	<div id="main-content">

        <header class="page-header have-disc-icon text-center">

        	<div class="mask"></div>

        	<div class="page-header-bg-2 page-header-bg"></div>

        	<div class="page-header-inner page-header-inner-1">

        		<div class="wrapper">
                
	                <h1 class="page-title">Shop Product</h1>

	                <div class="breadcrumb clearfix">                    
	                    <span itemtype="http://data-vocabulary.org/Breadcrumb" itemscope="">
	                        <a href="<?= base_url() ?>" itemprop="url">
	                            <span itemprop="title">Home</span>
	                        </a>
	                    </span>
	                    <span>&nbsp;/&nbsp;</span>
	                    <span itemtype="http://data-vocabulary.org/Breadcrumb" itemscope="">
	                        <a href="<?= base_url() ?>" itemprop="url">
	                            <span itemprop="title">Shop Categories</span>
	                        </a>
	                    </span>
	                    <span>&nbsp;/&nbsp;</span>
	                    <span itemtype="http://data-vocabulary.org/Breadcrumb" itemscope="" class="current-page"><span itemprop="title">Shop Product</span></span>
	                </div>
	                <!-- breadcrumb -->

	            </div>
	            <!-- wrapper -->
        		
        	</div>
        	<!-- page-header-inner -->

        	
        	<div class="album-icon">
                <div class="icon-inner-1">
                    <span class="icon-inner-2"></span>
                </div>
                <span class="fa fa-music"></span>
            </div>            
            
        </header>
        <!-- page-header -->

        <div class="wrapper clearfix">

        	<div class="product type-product status-publish shipping-taxable purchasable product-type-simple product-cat-clothing product-cat-hoodies instock" itemtype="http://schema.org/Product" itemscope="">

                <div class="images">
                    
                    <a title="" class="woocommerce-main-image zoom" itemprop="image" href="#"><img title="" alt="" class="attachment-shop_single wp-post-image" src="[base_url]theme/theme/images/img/28.png"></a>

                </div>
                <!-- images -->

                <div class="summary entry-summary clearfix">

                    <h4 class="product_title entry-title" itemprop="name">Happy Ninja</h4>

                    <div itemtype="http://schema.org/AggregateRating" itemscope="" itemprop="aggregateRating" class="woocommerce-product-rating">
                        <div title="" class="star-rating">
                            <span><strong class="rating" itemprop="ratingValue">3.00</strong>out of 5</span>
                        </div>
                        <a rel="nofollow" class="woocommerce-review-link" href="#reviews">(<span class="count" itemprop="ratingCount">2</span> customer reviews)</a>
                    </div>
                    <!-- woocommerce-product-rating -->

                    <div itemtype="http://schema.org/Offer" itemscope="" itemprop="offers">

                    <p class="price"><del><span class="amount">£35.00</span></del><ins><span class="amount">£35.00</span></ins></p>

                        <meta content="35" itemprop="price">
                        <meta content="GBP" itemprop="priceCurrency">
                        <link href="http://schema.org/InStock" itemprop="availability">

                    </div>

                    <div itemprop="description" class="description">
                        <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.</p>
                        <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae.</p>
                    </div>
                    <!-- description -->

                    <form enctype="multipart/form-data" method="post" class="cart">

                        <div class="quantity buttons_added">
                            <input type="button" class="minus" value="-">
                            <input type="number" class="input-text qty text" title="Qty" value="1" name="quantity" min="1" step="1">
                            <input type="button" class="plus" value="+">
                        </div>
                        <input type="hidden" value="53" name="add-to-cart">

                        <button class="single_add_to_cart_button button alt" type="submit"><i class="fa fa-shopping-cart"></i>Add to cart</button>

                    </form>
                    <!-- cart -->

                    <div class="product_meta">

                        <span class="posted_in">Categories: <a rel="tag" href="#">Clothing</a>, <a rel="tag" href="#">Hoodies</a>.</span>

                    </div>
                    <!-- product_meta -->

                </div>
                <!-- entry-summary -->

                <div class="clear"></div>

                <div class="kopa-tab-container-1">

                    <ul class="nav nav-tabs kopa-tabs-1">
                        <li class="active"><a href="#tab1-1" data-toggle="tab">Description</a></li>
                        <li class=""><a href="#tab1-2" data-toggle="tab">Review (2)</a></li>  
                    </ul>
                    <!-- nav-tabs -->

                    <div class="tab-content kopa-tab-content-1">
                        <div class="tab-pane active" id="tab1-1">
                            <h4>Product Description</h4>
                            <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</p>
                        </div>
                        <!-- tab-panel -->
                        <div class="tab-pane" id="tab1-2">
                            
                            <div id="comments">
                                <h4>2 reviews</h4>
                                <ol class="comments-list clearfix">
                                    <li class="comment clearfix">
                                        <article class="comment-wrap clearfix"> 
                                            <div class="comment-avatar">
                                                <img alt="" src="[base_url]theme/theme/images/img/23.png">
                                            </div>
                                            <div class="comment-body clearfix">
                                                <header class="clearfix">                                
                                                    <div class="pull-left">
                                                        <h6>Emily Doe</h6>
                                                    </div>                                    
                                                    <a class="comment-reply-link pull-right" href="#"><i class="fa fa-mail-reply"></i><span>Reply</span></a>
                                                    <div class="clear"></div>
                                                </header>
                                                <div class="comment-content">
                                                    <p>Proin eleifend volutpat massa, vitae venenatis quam cursus sit amet. Aenean sed lacus enim. Fusce adipiscing tristique lorem, non pellentesque nisi porta elementum.</p>
                                                </div>                                
                                                <footer class="text-right text-uppercase">
                                                    <span class="entry-date">June 12, 2015 At 7:52AM</span>
                                                    <span>&nbsp;/&nbsp;</span>
                                                    <a class="comment-edit-link" href="#">Edit</a>
                                                </footer>
                                            </div><!--comment-body -->
                                        </article>                                                                     
                                    </li>                        
                                    <li>
                                        <article class="comment-wrap clearfix"> 
                                            <div class="comment-avatar">
                                                <img alt="" src="[base_url]theme/theme/images/img/23.png">
                                            </div>
                                            <div class="comment-body clearfix">
                                                <header class="clearfix">                                
                                                    <div class="pull-left">
                                                        <h6>Emily Doe</h6>
                                                    </div>                                    
                                                    <a class="comment-reply-link pull-right" href="#"><i class="fa fa-mail-reply"></i><span>Reply</span></a>
                                                    <div class="clear"></div>
                                                </header>
                                                <div class="comment-content">
                                                    <p>Proin eleifend volutpat massa, vitae venenatis quam cursus sit amet. Aenean sed lacus enim. Fusce adipiscing tristique lorem, non pellentesque nisi porta elementum.</p>
                                                </div>                                
                                                <footer class="text-right text-uppercase">
                                                    <span class="entry-date">June 12, 2015 At 7:52AM</span>
                                                    <span>&nbsp;/&nbsp;</span>
                                                    <a class="comment-edit-link" href="#">Edit</a>
                                                </footer>
                                            </div><!--comment-body -->
                                        </article>
                                        <ul class="children">
                                            <li class="comment clearfix">
                                                <article class="comment-wrap clearfix"> 
                                                    <div class="comment-avatar">
                                                        <img alt="" src="[base_url]theme/theme/images/img/23.png">
                                                    </div>
                                                    <div class="comment-body clearfix">
                                                        <header class="clearfix">                                
                                                            <div class="pull-left">
                                                                <h6>Emily Doe</h6>
                                                            </div>                                    
                                                            <a class="comment-reply-link pull-right" href="#"><i class="fa fa-mail-reply"></i><span>Reply</span></a>
                                                            <div class="clear"></div>
                                                        </header>
                                                        <div class="comment-content">
                                                            <p>Proin eleifend volutpat massa, vitae venenatis quam cursus sit amet. Aenean sed lacus enim. Fusce adipiscing tristique lorem, non pellentesque nisi porta elementum.</p>
                                                        </div>                                
                                                        <footer class="text-right text-uppercase">
                                                            <span class="entry-date">June 12, 2015 At 7:52AM</span>
                                                            <span>&nbsp;/&nbsp;</span>
                                                            <a class="comment-edit-link" href="#">Edit</a>
                                                        </footer>
                                                    </div><!--comment-body -->
                                                </article>                                                                     
                                            </li>
                                        </ul>
                                        <ul class="children">
                                            <li class="comment clearfix">
                                                <article class="comment-wrap clearfix"> 
                                                    <div class="comment-avatar">
                                                        <img alt="" src="[base_url]theme/theme/images/img/23.png">
                                                    </div>
                                                    <div class="comment-body clearfix">
                                                        <header class="clearfix">                                
                                                            <div class="pull-left">
                                                                <h6>Emily Doe</h6>
                                                            </div>                                    
                                                            <a class="comment-reply-link pull-right" href="#"><i class="fa fa-mail-reply"></i><span>Reply</span></a>
                                                            <div class="clear"></div>
                                                        </header>
                                                        <div class="comment-content">
                                                            <p>Proin eleifend volutpat massa, vitae venenatis quam cursus sit amet. Aenean sed lacus enim. Fusce adipiscing tristique lorem, non pellentesque nisi porta elementum.</p>
                                                        </div>                                
                                                        <footer class="text-right text-uppercase">
                                                            <span class="entry-date">June 12, 2015 At 7:52AM</span>
                                                            <span>&nbsp;/&nbsp;</span>
                                                            <a class="comment-edit-link" href="#">Edit</a>
                                                        </footer>
                                                    </div><!--comment-body -->
                                                </article>                                                                     
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="comment clearfix">
                                        <article class="comment-wrap clearfix"> 
                                            <div class="comment-avatar">
                                                <img alt="" src="[base_url]theme/theme/images/img/23.png">
                                            </div>
                                            <div class="comment-body clearfix">
                                                <header class="clearfix">                                
                                                    <div class="pull-left">
                                                        <h6>Emily Doe</h6>
                                                    </div>                                    
                                                    <a class="comment-reply-link pull-right" href="#"><i class="fa fa-mail-reply"></i><span>Reply</span></a>
                                                    <div class="clear"></div>
                                                </header>
                                                <div class="comment-content">
                                                    <p>Proin eleifend volutpat massa, vitae venenatis quam cursus sit amet. Aenean sed lacus enim. Fusce adipiscing tristique lorem, non pellentesque nisi porta elementum.</p>
                                                </div>                                
                                                <footer class="text-right text-uppercase">
                                                    <span class="entry-date">June 12, 2015 At 7:52AM</span>
                                                    <span>&nbsp;/&nbsp;</span>
                                                    <a class="comment-edit-link" href="#">Edit</a>
                                                </footer>
                                            </div><!--comment-body -->
                                        </article>                                                                     
                                    </li>
                                </ol><!--comments-list-->

                                <div class="pagination kopa-comment-pagination">                                    
                                    <a class="prev page-numbers" href="#comments">Previous</a>
                                    <a class="page-numbers" href="#comments">1</a>
                                    <span class="page-numbers current">2</span>
                                    <a class="page-numbers" href="#comments">3</a>         
                                    <a class="next page-numbers" href="#comments">Next</a>                                    
                                </div>
                                <!-- pagination -->

                                <div class="clear"></div>

                            </div>
                            <!-- comments -->

                            <div id="respond">
                                <h3 class="text-center">Leave a comment</h3>

                                <form class="comments-form clearfix" action="processForm.php" method="post" novalidate="novalidate">
                                    <span class="c-note text-center">Your email address will not be published. Required fields are marked *</span>

                                    <div class="row">

                                        <div class="col-md-4 col-sm-4">
                                            <p class="input-block">
                                                <label class="required" for="comment_name">Name <span>*</span></label>
                                                <input type="text" class="valid" name="name" id="comment_name" onblur="if(this.value=='')this.value='Name *';" onfocus="if(this.value=='Name *')this.value='';" value="Name *">
                                            </p>
                                        </div>
                                        <!-- col-md-4 -->

                                        <div class="col-md-4 col-sm-4">
                                            <p class="input-block">
                                                <label class="required" for="comment_email">Email <span>*</span></label>
                                                <input type="text" class="valid" name="email" id="comment_email" onblur="if(this.value=='')this.value='Email *';" onfocus="if(this.value=='Email *')this.value='';" value="Email *">
                                            </p>
                                        </div>
                                        <!-- col-md-4 -->

                                        <div class="col-md-4 col-sm-4">
                                            <p class="input-block">                                                
                                                <label class="required" for="comment_url">Website</label>
                                                <input type="text" id="comment_url" onblur="if(this.value=='')this.value='Website';" onfocus="if(this.value=='Website')this.value='';" value="Website" class="valid" name="url">                                                
                                            </p>
                                        </div>
                                        <!-- col-md-4 -->
                                        
                                    </div>
                                    <!-- row -->
                                    
                                    <div class="row">

                                        <div class="col-md-12">
                                            <p class="textarea-block">                        
                                                <label class="required" for="comment_message">Comment <span>*</span></label>
                                                <textarea rows="6" cols="88" id="comment_message" name="message" onblur="if(this.value=='')this.value='Comments *';" onfocus="if(this.value=='Comments *')this.value='';">Comments *</textarea>
                                            </p>
                                        </div>
                                        <!-- col-md-12 -->
                                        
                                    </div>
                                    <!-- row -->
                                    
                                    <div class="row">
                                        <div class="col-md-12">
                                            <p class="comment-button clearfix">                    
                                                <input type="submit" class="input-submit" id="submit-comment" value="Submit Comment">
                                            </p>
                                        </div>
                                        <!-- col-md-12 -->
                                    </div>
                                    <!-- row -->                   
                                                                                   
                                </form>
                                <div id="response"></div>
                            </div>
                            <!-- respond -->

                        </div>
                        <!-- tab-panel -->
                    </div>
                    <!-- tab-content -->

                </div>
                <!-- kopa-tab-container-1 -->

            </div>
            <!-- product -->

            <div class="product_column mb-40">
                    
                <div class="related products">

                    <h2>Related Products</h2>

                    <ul class="products">
                        
                        <li class="product">
                            
                            <div>
                                
                                <div class="product-thumb">
                                    <a href="#">
                                        <img src="[base_url]theme/theme/images/img/29.png" alt="">
                                    </a>
                                    <div class="button-box text-center">
                                        <a href="#"><i class="fa fa-heart"></i></a>
                                        <a href="#"><i class="fa fa-search"></i></a>
                                    </div>
                                </div>
                                <!-- product-thumb -->
                                <a href="#">
                                    <h3>Sample Fashion Product</h3>
                                    <footer>
                                        <span class="price"><span class="amount">£50.00</span></span>
                                        <div title="Rated 4.00 out of 5" class="star-rating"><span style="width:80%"><strong class="rating">4.00</strong> out of 5</span></div>                                     
                                    </footer>
                                </a>

                                <a class="button add_to_cart_button product_type_simple" href="#">Add to cart</a>

                            </div>                                    

                        </li>
                        <!-- product -->

                        <li class="product">
                            
                            <div>
                                
                                <div class="product-thumb">
                                    <a href="#">        
                                        <span class="onsale">Sale!</span>
                                        <img src="[base_url]theme/theme/images/img/29.png" alt="">                                    
                                    </a>
                                    <div class="button-box text-center">
                                        <a href="#"><i class="fa fa-heart"></i></a>
                                        <a href="#"><i class="fa fa-search"></i></a>
                                    </div>
                                </div>
                                <!-- product-thumb -->
                                <a href="#">
                                    <h3>Sample Fashion Product</h3>
                                    <footer>                                        
                                        <span class="price"><del><span class="amount">$75.00</span></del> <ins><span class="amount">£50.00</span></ins></span>
                                        <div title="Rated 4.00 out of 5" class="star-rating"><span style="width:80%"><strong class="rating">4.00</strong> out of 5</span></div>
                                    </footer>                                    
                                </a>

                                <a class="button add_to_cart_button product_type_simple clearfix" href="#">Add to cart</a>

                            </div>                                    

                        </li>
                        <!-- product -->

                        <li class="product">
                            
                            <div>
                                
                                <div class="product-thumb">
                                    <a href="#">
                                        <img src="[base_url]theme/theme/images/img/29.png" alt="">
                                    </a>
                                    <div class="button-box text-center">
                                        <a href="#"><i class="fa fa-heart"></i></a>
                                        <a href="#"><i class="fa fa-search"></i></a>
                                    </div>
                                </div>
                                <!-- product-thumb -->
                                <a href="#">
                                    <h3>Sample Fashion Product</h3>
                                    <footer>
                                        <span class="price"><span class="amount">£50.00</span></span>
                                        <div title="Rated 4.00 out of 5" class="star-rating"><span style="width:80%"><strong class="rating">4.00</strong> out of 5</span></div>                                     
                                    </footer>
                                </a>

                                <a class="button add_to_cart_button product_type_simple" href="#">Add to cart</a>

                            </div>                                    

                        </li>
                        <!-- product -->
                        
                    </ul>

                </div>
                <!-- end:related products -->

            </div>
            <!-- product_column -->

        </div>
        <!-- wrapper -->
        
    </div>
    <!-- main-content -->
	[footer]