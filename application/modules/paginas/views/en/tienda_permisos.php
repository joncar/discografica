<body class="woocommerce woocommerce-page kopa-subpage">
<div id="main-content">
	[menu]
		<header class="page-header have-disc-icon text-center">
			<div class="disc-bg"></div>
            <div class="mask"></div>

            <div class="page-header-bg-2 page-header-bg"></div>

            <div class="page-header-inner page-header-inner-1">

                <div class="wrapper">
                    
                    <h1 class="page-title"><?= $this->user->nombre ?></h1>

                    <div class="breadcrumb clearfix">                    
                        <span itemtype="http://data-vocabulary.org/Breadcrumb" itemscope="">
                            <a href="<?= base_url() ?>" itemprop="url">
                                <span itemprop="title">Home</span>
                            </a>
                        </span>
                        <span>&nbsp;/&nbsp;</span>
                        <span itemtype="http://data-vocabulary.org/Breadcrumb" itemscope="">
                            <a href="<?= base_url() ?>" itemprop="url">
                                <span itemprop="title">Profile</span>
                            </a>
                        </span>
                        <!-- <span>&nbsp;/&nbsp;</span> -->
                        <!-- <span itemtype="http://data-vocabulary.org/Breadcrumb" itemscope="" class="current-page"><span itemprop="title">Typography</span></span> -->
                    </div>
                    <!-- breadcrumb -->

                </div>
                <!-- wrapper -->

                </div>
                <!-- page-header-inner -->

                <div class="album-icon">
	                <div class="icon-inner-1">
	                    <span class="icon-inner-2"></span>
	                </div>
	                <span class="fa fa-music"></span>
	            </div>
            
        </header>
        <!-- page-header -->
		<div class="wrapper clearfix tiendaCuenta">
			<section class="elements-box mb-40">
				<div class="tabs vertical-tabs">
	                <ul class="tab-nav clearfix" style="padding:0">
	                    <li data-tab="cuenta">
	                    	<h6 class="tab-name uppercase">
	                    	<a rel="canonical" href="[base_url]cuenta">My orders</a></h6>
	                    </li>
	                    <li data-tab="perfil">
	                    	<h6 class="tab-name uppercase">
	                    	<a rel="canonical" href="[base_url]t/admin/perfil/edit/<?= $this->user->id ?>">User data</a></h6>
	                    </li>
	                    <li class="active">
	                    	<h6 class="tab-name uppercase">
	                    	<a rel="canonical" href="[base_url]t/admin/permisos">Access permits</a></h6>
	                    </li>
	                    <?php if($this->user->getAccess('funciones.*',array('funciones.nombre'=>'tiendas'))->num_rows()>0): ?>
		                    <li data-tab="pedidosPendientes">
		                    	<h6 class="tab-name uppercase">
		                    	<a rel="canonical" href="[base_url]t/admin/pedidos_por_pagar">Orders without payment</a></h6>
		                    </li>
	                	<?php endif ?>
	                </ul>
	                <div class="tab-container clearfix">
	                    <div class="tab-content tab1" style="display: block">
	                        <div class="panel panel-default">
	                        	<div class="row" style="margin-top:20px; border:1px solid; padding:20px; margin-left:0; margin-right:0">
		                        	<div class="col-xs-6 col-md-10">
		                        		General purchases
		                        	</div>
		                        	<div class="col-xs-6 col-md-2" style="text-align:right">
		                        		Allowed
		                        	</div>
		                        </div>
		                        <div class="row" style="margin-top:20px; border:1px solid; padding:20px;  margin-left:0; margin-right:0">
		                        	<div class="col-xs-12 col-md-8">
		                        		Access to promotions stores
		                        	</div>
		                        	<div class="col-xs-12 col-md-4" style="text-align:right">
		                        	
		                        		<?php if($this->user->paises_id==1): ?>
			                        		<?php if(empty($this->user->tienda)): ?>
			                        			<a class="button add_to_cart_button product_type_simple" href="#tienda" data-toggle="modal">Request</a>
			                        		<?php elseif(!empty($this->user->tienda) && $this->db->get_where('user_group',array('user'=>$this->user->id,'grupo'=>5))->num_rows()==0): ?>
			                        			<a class="button add_to_cart_button product_type_simple" href="#tienda" data-toggle="modal">Resend request</a>
			                        		<?php else: ?>
			                        			Allowed
			                        		<?php endif ?>
		                        		<?php else: ?>
		                        			Service is only to spain region											
		                        		<?php endif ?>

		                        	</div>
		                        </div>
		                        <div class="row" style="margin-top:20px; border:1px solid; padding:20px;  margin-left:0; margin-right:0">	
		                        	<div class="col-xs-12 col-md-8">
		                        		Access to promotions radios
		                        	</div>
		                        	<div class="col-xs-12 col-md-4" style="text-align:right">
		                        		<?php if(empty($this->user->emisora)): ?>
		                        			<a class="button add_to_cart_button product_type_simple" href="#radio" data-toggle="modal">Request</a>
		                        		<?php elseif(!empty($this->user->emisora) && $this->db->get_where('user_group',array('user'=>$this->user->id,'grupo'=>5))->num_rows()==0): ?>
		                        			<a class="button add_to_cart_button product_type_simple" href="#radio" data-toggle="modal">Resend request</a>
		                        		<?php else: ?>
		                        			Allowed
		                        		<?php endif ?>
		                        	</div>
	                        	</div>
	                        </div>
	                    </div> <!-- END .tab-content -->
	                </div> <!-- END .tab-container -->  
	            </div>
	            
	        </section>
	        </div>
		</div>

	<div class="modal fade" tabindex="-1" role="dialog" id="radio">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <form action="t/admin/solicitar" onsubmit="return sendForm(this,'#radioResponse')">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        <h4 class="modal-title">Request access to promotions radios</h4>
		      </div>
		      <div class="modal-body">
		        <div class="form-group">
				    <label for="exampleInputEmail1">Name of radio station</label>
				    <input type="text" name="emisora" class="form-control" id="exampleInputEmail1" placeholder="Name of radio station" value="<?= $this->user->emisora ?>">
			    </div>
			    <div id="radioResponse"></div>
		      </div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		        <button type="submit" class="button add_to_cart_button product_type_simple">Submit</button>
		      </div>
		  </form>
	    </div><!-- /.modal-content -->
	  </div><!-- /.modal-dialog -->
	</div><!-- /.modal -->

	<div class="modal fade" tabindex="-1" role="dialog" id="tienda">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <form action="t/admin/solicitar" onsubmit="return sendForm(this,'#tiendaResponse')">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        <h4 class="modal-title">Request access to promotions stores</h4>
		      </div>
		      <div class="modal-body">
		        <div class="form-group">
				    <label for="exampleInputEmail1">Store name</label>
				    <input type="text" name="tienda" class="form-control" id="exampleInputEmail1" placeholder="Name of store" value="<?= $this->user->tienda ?>">
			    </div>
			    <div id="tiendaResponse"></div>
		      </div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		        <button type="submit" class="button add_to_cart_button product_type_simple">Submit</button>
		      </div>
		  </form>
	    </div><!-- /.modal-content -->
	  </div><!-- /.modal-dialog -->
	</div><!-- /.modal -->


	[footer]
</div>