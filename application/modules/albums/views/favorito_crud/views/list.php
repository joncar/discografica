<?php
	if(count($list)==0){
		echo 'Lista de categorias vacia';
		die();
	}
	foreach($list as $n=>$row):
	$d = get_instance()->elements->albums(array('albums.id'=>$row->albums_id))->row();
?>
<div class="events-by-month">
	
	<div class="event-month" style="background: url(<?= $d->caratula ?>); ">
		<div><p class="text-uppercase">&nbsp;</p><span>&nbsp;</span></div>
	</div>
	
	<ul>
		<li>
			<article class="event-item" style="">
				<a href="<?php echo $row->delete_url?>" class="trash delete-row"><i class="fa fa-trash"></i></a>
				<div class="event-bg event-bg-1"></div>
				<div class="mask"></div>
				<div class="row">
					<div class="col-xs-12 col-sm-10">
						
						<div class="col-md-12 col-sm-12">
							<div class="event-heading text-uppercase"><?= $d->artista ?></div>
							<div class="event-content">
								<h3 class="event-title"><a href="<?= $d->link ?>"><?= $d->titulo ?></a></h3>
							</div>
						</div>
						<div class="col-md-12 col-sm-12 contentPlayer">
							<div class="entry-item">
								<div id="reprod<?= $n ?>" class="reproductor jp-jplayer-single-2"></div>
								<div id="reprodControl<?= $n ?>" class="jp-audio jp-audio-single2" role="application" aria-label="media player">
									<div class="jp-type-single">
										<div class="jp-gui jp-interface">
											<div class="jp-controls">
												<button class="jp-play" role="button" tabindex="0">play</button>
											</div>
											<div class="jp-progress">
												<div class="jp-seek-bar" style="width:100%;">
													<div class="jp-play-bar"></div>
												</div>
											</div>
											<div class="jp-volume-controls">
												<span class="fa fa-volume-down"></span>
												<div class="jp-volume-bar">
													<div class="jp-volume-bar-value"></div>
												</div>
											</div>
											<div class="jp-time-holder">
												<div class="jp-current-time" role="timer" aria-label="time">00:00</div>
												<div class="jp-duration" role="timer" aria-label="duration">00:00</div>
											</div>
										</div>
										<div class="jp-no-solution">
											<span>Update Required</span>
											To play the media you will need to either update your browser to a recent version or update your <a href="http://get.adobe.com/flashplayer/" target="_blank">Flash plugin</a>.
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- col-md-8 -->
					<div class="col-xs-12 col-sm-2 col-sm-2 precioContent">
						<div class="event-heading text-uppercase">
							<?= $d->precio ?>
						</div>
						<div class="event-content">
							<a class="button add_to_cart_button product_type_simple" href="javascript:addToCart(<?= $d->id ?>,1)">Añadir al carro</a>
						</div>
					</div>
					<!-- col-md-2 -->
				</div>
				<!-- row -->
				
			</article>
		</li>
		
		
	</ul>
</div>
<?php endforeach ?>