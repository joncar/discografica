<body class="kopa-subpage">
	[menu]
	<div id="main-content">

        <header class="page-header have-disc-icon text-center">

            <div class="disc-bg"></div>

            <div class="mask"></div>

            <div class="typo-bg"></div>

            <div class="page-header-bg-1 page-header-bg"></div>

            <div class="page-header-inner">

                <div class="wrapper">
                
                    <h1 class="page-title">Artistas</h1>

                    <div class="breadcrumb clearfix">                    
                        <span itemtype="http://data-vocabulary.org/Breadcrumb" itemscope="">
                            <a href="<?= base_url() ?>" itemprop="url">
                                <span itemprop="title">Inicio</span>
                            </a>
                        </span>
                        <span>&nbsp;/&nbsp;</span>
                        <span itemtype="http://data-vocabulary.org/Breadcrumb" itemscope="" class="current-page"><span itemprop="title">Álbums</span></span>
                    </div>
                    <!-- breadcrumb -->

                </div>
                <!-- wrapper -->
                
            </div>
            <!-- page-header-inner -->

            
            <div class="album-icon">
                <div class="icon-inner-1">
                    <span class="icon-inner-2"></span>
                </div>
                <span class="fa fa-music"></span>
            </div>            
            
        </header>
        <!-- page-header -->

        <div class="wrapper clearfix">

            <div class="widget kopa-artist-widget">

                <h2 class="widget-title widget-title-s5">Lista de artistas</h2>

                <div class="widget-content">
                    
                    <div class="row">
                        <div class="col-md-4 col-md-offset-4">
                            <div class="search-box clearfix">
                                <form action="#" class="search-form pull-left clearfix" method="get">
                                    <input type="text" onBlur="if (this.value == '')
                                        this.value = this.defaultValue;" onFocus="if (this.value == this.defaultValue)
                                        this.value = '';" value="Buscar artista..." name="s" class="search-text">
                                    <button type="submit" class="search-submit"><i class="fa fa-search"></i>
                                    </button>
                                </form>
                                <!-- search-form -->
                            </div>
                            <!--search-box-->
                        </div>
                        <!-- col-md-4 -->
                    </div>
                    <!-- row -->
                    
                    <div class="masonry-container">

                        <header class="clearfix">

                            <ul class="filters clearfix">
                               <li class="active" data-filter="kopa-all">Todo</li>
                               <li data-filter="kopa-songs">Cancioness</li>
                               <li data-filter="kopa-albums">Álbums</li>
                               <li data-filter="kopa-artists">Artistas</li>
                               <li data-filter="kopa-mixes">Mixes</li>
                            </ul>

                            <form method="get" class="kopa-sorting">
                                <select class="orderby" name="orderby">
                                    <option value="pop">Pop</option>
                                    <option value="rap">Rap</option>
                                    <option value="hip-hop">Hip Hop</option>
                                    <option value="rock">Rock</option>
                                    <option value="country">Country</option>
                                </select>
                                <input type="hidden" value="category" name="post_type">
                            </form>
                            <!-- kopa-sorting -->
                            
                        </header>
                        
                        <div class="container-masonry clearfix">
                            
                            <div class="item" data-filter-class='["kopa-all", "kopa-songs"]'>
                                <div class="artist-item">
                                    <div class="artist-thumb">
                                        <img src="[base_url]theme/theme/images/img/17.png" alt="">
                                        <div class="mask"><a href="[base_url]artistas-detail.html"><i class="fa fa-plus"></i></a></div>
                                    </div>
                                    <h6 class="artist-title"><a href="[base_url]artistas-detail.html">Chase Ricel</a></h6>
                                </div>                                
                            </div>

                            <div class="item" data-filter-class='["kopa-all", "kopa-albums"]'>
                                <div class="artist-item">
                                    <div class="artist-thumb">
                                        <img src="[base_url]theme/theme/images/img/17.png" alt="">
                                        <div class="mask"><a href="[base_url]artistas-detail.html"><i class="fa fa-plus"></i></a></div>
                                    </div>
                                    <h6 class="artist-title"><a href="[base_url]artistas-detail.html">Chase Ricel</a></h6>
                                </div>
                            </div>

                            <div class="item" data-filter-class='["kopa-all", "kopa-artists"]'>
                                <div class="artist-item">
                                    <div class="artist-thumb">
                                        <img src="[base_url]theme/theme/images/img/17.png" alt="">
                                        <div class="mask"><a href="[base_url]artistas-detail.html"><i class="fa fa-plus"></i></a></div>
                                    </div>
                                    <h6 class="artist-title"><a href="[base_url]artistas-detail.html">Chase Ricel</a></h6>
                                </div>
                            </div>

                            <div class="item" data-filter-class='["kopa-all", "kopa-mixes"]'>
                                <div class="artist-item">
                                    <div class="artist-thumb">
                                        <img src="[base_url]theme/theme/images/img/17.png" alt="">
                                        <div class="mask"><a href="[base_url]artistas-detail.html"><i class="fa fa-plus"></i></a></div>
                                    </div>
                                    <h6 class="artist-title"><a href="[base_url]artistas-detail.html">Chase Ricel</a></h6>
                                </div>
                            </div>

                            <div class="item" data-filter-class='["kopa-all", "kopa-songs"]'>
                                <div class="artist-item">
                                    <div class="artist-thumb">
                                        <img src="[base_url]theme/theme/images/img/17.png" alt="">
                                        <div class="mask"><a href="[base_url]artistas-detail.html"><i class="fa fa-plus"></i></a></div>
                                    </div>
                                    <h6 class="artist-title"><a href="[base_url]artistas-detail.html">Chase Ricel</a></h6>
                                </div>
                            </div>

                            <div class="item" data-filter-class='["kopa-all", "kopa-albums"]'>
                                <div class="artist-item">
                                    <div class="artist-thumb">
                                        <img src="[base_url]theme/theme/images/img/17.png" alt="">
                                        <div class="mask"><a href="[base_url]artistas-detail.html"><i class="fa fa-plus"></i></a></div>
                                    </div>
                                    <h6 class="artist-title"><a href="[base_url]artistas-detail.html">Chase Ricel</a></h6>
                                </div>
                            </div>

                            <div class="item" data-filter-class='["kopa-all", "kopa-artists"]'>
                                <div class="artist-item">
                                    <div class="artist-thumb">
                                        <img src="[base_url]theme/theme/images/img/17.png" alt="">
                                        <div class="mask"><a href="[base_url]artistas-detail.html"><i class="fa fa-plus"></i></a></div>
                                    </div>
                                    <h6 class="artist-title"><a href="[base_url]artistas-detail.html">Chase Ricel</a></h6>
                                </div>
                            </div>

                            <div class="item" data-filter-class='["kopa-all", "kopa-mixes"]'>
                                <div class="artist-item">
                                    <div class="artist-thumb">
                                        <img src="[base_url]theme/theme/images/img/17.png" alt="">
                                        <div class="mask"><a href="[base_url]artistas-detail.html"><i class="fa fa-plus"></i></a></div>
                                    </div>
                                    <h6 class="artist-title"><a href="[base_url]artistas-detail.html">Chase Ricel</a></h6>
                                </div>
                            </div>

                            <div class="item" data-filter-class='["kopa-all", "kopa-songs"]'>
                                <div class="artist-item">
                                    <div class="artist-thumb">
                                        <img src="[base_url]theme/theme/images/img/17.png" alt="">
                                        <div class="mask"><a href="[base_url]artistas-detail.html"><i class="fa fa-plus"></i></a></div>
                                    </div>
                                    <h6 class="artist-title"><a href="[base_url]artistas-detail.html">Chase Ricel</a></h6>
                                </div>
                            </div>

                        </div>
                        
                    </div>
                    <!-- masonry-container -->

                    <div class="pagination clearfix">
                        <ul class="page-numbers clearfix">
                            <li><a href="#" class="first page-numbers"><i class="fa fa-long-arrow-left"></i></a></li>
                            <li><a href="#" class="page-numbers">1</a></li>
                            <li><span class="page-numbers current">2</span></li>
                            <li><span class="page-numbers dots">…</span></li>
                            <li><a href="#" class="page-numbers">8</a></li>
                            <li><a href="#" class="page-numbers">9</a></li>
                            <li><a class="last page-numbers" href="#"><i class="fa fa-long-arrow-right"></i></a></li>
                        </ul><!--page-numbers-->
                    </div>
                    <!-- pagination -->

                </div>
                <!-- widget-content -->
                
            </div>
            <!-- kopa-artist-widget -->

        </div>
        <!-- wrapper -->

        <?php $this->load->view('_my_music',array('')); ?>
        
    </div>
    <!-- main-content -->
    [footer]