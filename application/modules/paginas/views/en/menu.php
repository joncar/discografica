<div class="loading">
    <div>
        <img src="<?= base_url() ?>img/vinilo.gif">
    </div>
</div>
<header id="navbar-spy" class="full-header header-3">
    <nav id="primary-menu" class="navbar navbar-fixed-top style-1">
        <div id="top-bar" class="top-bar hidden-xs hidden-sm">
            <div class="container">
                <div class="row" style="display: flex">
                    <div>
                        <ul class="list-inline top-widget">
                            <li class="top-social" style="white-space: nowrap;">
                                <a href="https://www.facebook.com/discotecarecords" target="_new"><i class="fa fa-facebook"></i></a>
                                <a href="https://www.instagram.com/discotecarecords/" target="_new"><i class="fa fa-instagram"></i></a>                            
                                <a href="https://www.youtube.com/channel/UCY1ZZKx4FWJnharejnMskBQ" target="_new"><i class="fa fa-youtube"></i></a>                            
                                <a href="mailto:info@discotecarecords.com"><i class="fa fa-envelope" style="padding-left: 8px;"></i></a>
                            </li>                            
                        </ul>
                        
                    </div>
                    
                    <!-- .col-md-6 end -->
                    
                    <div>
                        <ul class="list-inline top-contact">
                            <li>
                                <div class="kp-headline-wrapper pull-left clearfix">
                                    <div class="kp-headline clearfix">
                                        <dl class="ticker-1 clearfix">
                                            <dt></dt>
                                            <dd><a href="#">The best in music</a></dd>
                                            <dt></dt>
                                            <dd><a href="#">New themes</a></dd>
                                            <!--<dt></dt>
                                            <dd><a href="#">Puedes comprar ahora el tema single de shakira</a></dd>-->
                                        </dl>
                                        <!--ticker-1-->
                                    </div>
                                    <!--kp-headline-->
                                </div>
                            </li>
                            <li>
                                <p>
                                     <a href="<?= base_url() ?>especial-imports.html"><i class="transparent">Special import</i></a>
                                </p>
                            </li>
                            <li>
                                <p>
                                    <?php if(!$this->user->log): ?>
                                        <a href="<?= base_url('registro/index/add') ?>?redirect=radios"><i class="transparent">Promos Radio</i></a>                                                                                
                                    <?php else: ?>
                                        <a href="<?= base_url('radios') ?>"><i class="transparent">Promos Radio</i></a>                                                                                
                                    <?php endif ?>
                                    
                                </p>
                            </li>                            
                            <li>
                                <p>
                                    <?php if(!$this->user->log): ?>                                                                
                                        <a href="<?= base_url('registro/index/add') ?>?redirect=shops"><i class="transparent">Stores</i></a>
                                    <?php else: ?>                                        
                                        <a href="<?= base_url('shops') ?>"><i class="transparent">Stores</i></a>
                                    <?php endif ?>
                                    
                                </p>
                            </li>
                            <li>
                                <p>
                                    <a class="favoriosMenuLink" href="<?= base_url('favoritos') ?>">
                                        <i class="fa fa-heart""></i>
                                        Favorites                                        
                                    </a> 
                                 </p>
                            </li>
                            <li>
                                <p>
                                    <?php if(!$this->user->log): ?>
                                        <a href="<?= base_url('panel') ?>"><i>Account</i></a>
                                        <a href="<?= base_url('panel') ?>"><i>Register</i></a>
                                    <?php else: ?>
                                        <a href="<?= base_url('cuenta') ?>"><i>Account</i></a>
                                        <a href="<?= base_url('main/unlog') ?>"><i>Go out</i></a>
                                    <?php endif ?>                                                
                                </p>
                            </li>
                            <li>
                                <p class="flags">
                                    <a href="<?= base_url('main/traduccion/es') ?>" class="active spain"></a>
                                    <a href="#" class="england"></a>
                                </p>
                            </li>
                        </ul>
                    </div>
                    <!-- .col-md-6 end -->
                </div>
            </div>
        </div>
        <div class="row">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    </button>
                    <a href="<?= base_url() ?>tienda_carrito.html" class="navbar-toggle responsiveCart">
                        <i class="fa fa-shopping-cart"></i>
                        <span class="badge badge-success carCount">0</span>
                    </a>
                    <a class="logo" href="<?= base_url() ?>">
                        <img src="[base_url]theme/theme/placeholders/logook.png" alt="Yellow Hats">
                    </a>
                </div>
                
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse pull-right" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-left">
                        <li>
                            <a href="<?= base_url() ?>">Home</a>                          
                        </li>
                        <!-- li end -->
                        <li>
                             <a href="[base_url]empresa.html">Company</a>                           
                        </li>
                        <li class="has-dropdown">
                            <a href="[base_url]albums.html">Albums <i class="fa fa-chevron-down"></i></a>
                            <ul class="dropdown-menu">
                                <?php foreach($this->elements->albums(array('tipo'=>2))->result() as $a): ?>
                                     <li>
                                        <a href="<?= $a->link ?>"><?= $a->titulo ?></a>
                                    </li>
                                <?php endforeach ?> 
                            </ul>
                        </li>

                        <li class="has-dropdown">
                            <a href="[base_url]maxis.html">Maxis <i class="fa fa-chevron-down"></i></a>
                            <ul class="dropdown-menu">
                                <?php foreach($this->elements->albums(array('tipo'=>1))->result() as $a): ?>
                                     <li>
                                        <a href="<?= $a->link ?>"><?= $a->titulo ?></a>
                                    </li>
                                <?php endforeach ?>
                            </ul>
                        </li>
                        <!-- li end -->
                        <li>
                            <a href="[base_url]blog">Blog</a>
                        </li>
                        <!-- li end -->
                        <li>
                            <a href="[base_url]galeria.html">Gallery</a>      
                        </li>
                        
                        <!-- li end -->
                        <?php if(!$this->user->log): ?> 
                        <li class="visible-xs">
                            <a href="<?= base_url('registro/index/add') ?>?redirect=radios">Promos Radio</a>                           
                        </li>
                        <!-- li end -->
                        <li class="visible-xs">
                            <a href="<?= base_url('registro/index/add') ?>?redirect=shops">Stores</a>                           
                        </li>
                        <!-- li end -->
                        <?php else: ?>
                        <li class="visible-xs">
                            <a href="<?= base_url('radios') ?>">Promos Radio</a>                           
                        </li>
                        <!-- li end -->
                        <li class="visible-xs">
                            <a href="<?= base_url('shops') ?>">Stores</a>                           
                        </li>
                        <?php endif ?>
                        <!-- li end -->
                        <li>
                            <a href="[base_url]contacto.html">Contact us</a>                           
                        </li>
                        <!-- li end -->
                        <li class="flags visible-xs">
                            <div>
                                <a href="<?= base_url('main/traduccion/es') ?>" class="active spain"></a>
                                <a href="#" class="england"></a>
                            </div>
                        </li>
                        <!-- li end -->
                    </ul>
                    
                    <!-- Mod-->
                    <div class="module module-search pull-left">
                        <div class="search-icon">
                            <i class="fa fa-search"></i>
                            <span class="title">Search</span>
                        </div>
                        <div class="search-box">
                            <form class="search-form" action="<?= base_url('paginas/frontend/search') ?>">
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="Escribe lo que deseas buscar" name="q">
                                    <span class="input-group-btn">
                                    <button class="btn" type="button"><i class="fa fa-search"></i></button>
                                    </span>
                                </div>
                                <!-- /input-group -->
                            </form>
                        </div>
                    </div>
                    <!-- .module-search-->
                    <!-- .module-cart -->                    
                    <div class="module module-cart pull-left menubar-cart">
                        <?php $this->load->view('_carrito',array(),FALSE,'tienda') ?>
                    </div>
                    <!-- .module-cart end -->
                    
                </div>
                <!-- /.navbar-collapse -->
            </div>
            <!-- /.container-fluid -->
        </div>
    </nav>
</header>
