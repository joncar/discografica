<?php
	$action = empty($detail)?'insert':'update/'.$detail->id;
?>
<form action="" method="post" onsubmit="insertar('t/regalos/<?= $action ?>',this,'#response'); return false;">
	

	<div class="row">
		<div class="col-xs-12 col-md-6">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h1 class="panel-title">Datos del cliente</h1>
				</div>
				<div class="panel-body">		
						<div class="row">
							<div class="col-xs-12 col-md-12">
								<label for="">Cliente:</label>
								<?= form_dropdown_from_query('user_id','user','id','nombre apellido_paterno',empty($detail)?'':$detail->user_id,'id="field-user_id"') ?>
							</div>	
						</div>			
				</div>
			</div>
		</div>
		<div class="col-xs-12 col-md-6">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h1 class="panel-title">Estado del regalo</h1>
				</div>
				<div class="panel-body">		
					<div class="row">
						<div class="col-xs-12 col-md-8">
							<label for="">Comentarios:</label>
							<input type="text" class="form-control" name="comentarios" value="<?= empty($detail)?'':$detail->comentarios ?>">
						</div>
						<div class="col-xs-12 col-md-4">
							<label for="">Estado:</label>
							<?= form_dropdown('procesado',array('1'=>'Pendiente','2'=>'Enviado'),empty($detail)?'':$detail->procesado,'id="field-procesado" class="form-control"') ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h1 class="panel-title">Datos de envío</h1>
				</div>
				<div class="panel-body">		
						<div class="row">					
							<div class="col-xs-12 col-md-3">
								<div class="form-group">
									<label>País</label>
									<select name="paises_id" id="paises_id" class="form-control">
										<option value="">Seleccione una opción</option>
										<?php 
											$this->db->order_by('paises.nombre','ASC');
											foreach($this->db->get_where('paises')->result() as $p): 
											$selected = '';
											if(!empty($detail)){
												$selected = $p->id==$detail->paises_id?'selected="selected"':'';
											}
										?>
											<option value="<?= $p->id ?>" <?= $selected ?>><?= $p->nombre ?></option>
										<?php endforeach ?>
									</select>
								</div>
							</div>
							<div class="col-xs-12 col-md-3">
								<div class="form-group">
									<label>Provincia</label>
									<select name="provincias_id" id="provincias_id" class="form-control">
										<option value="">Seleccione una opción</option>
										<?php 
											$this->db->order_by('provincias.nombre','ASC');
											foreach($this->db->get_where('provincias')->result() as $p): 
											$selected = '';
											if(!empty($detail)){
												$selected = $p->id==$detail->provincias_id?'selected="selected"':'';
											}
										?>
											<option value="<?= $p->id ?>" <?= $selected ?>><?= $p->nombre ?></option>
										<?php endforeach ?>
									</select>
								</div>
							</div>
							<div class="col-xs-12 col-md-3">
								<div class="form-group">						
									<label>Ciudad</label>
									<input class="form-control" type="text" id="ciudad" name="ciudad" value="<?= empty($detail)?'':$detail->ciudad ?>" placeholder="Ciudad donde se realizará el envío">
								</div>
							</div>
							<div class="col-xs-12 col-md-3">
								<div class="form-group">						
									<label>CP</label>
									<input class="form-control" type="text" id="ciudad" name="cp" value="<?= empty($detail)?'':$detail->cp ?>" placeholder="Ciudad donde se realizará el envío">
								</div>
							</div>
							<div class="col-xs-12 col-md-8">
								<div class="form-group">						
									<label>Dirección</label>
									<input class="form-control" type="text" id="ciudad" name="direccion" value="<?= empty($detail)?'':$detail->direccion ?>" placeholder="Ciudad donde se realizará el envío">
								</div>
							</div>
							<div class="col-xs-12 col-md-4">
								<div class="form-group">						
									<label>Teléfono</label>
									<input class="form-control" type="text" id="ciudad" name="telefono" value="<?= empty($detail)?'':$detail->telefono ?>" placeholder="Ciudad donde se realizará el envío">
								</div>
							</div>
						</div>
					</div>
				</div>
		</div>
	</div>
	
	<div class="row">
	<div class="col-xs-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h1 class="panel-title">Productos</h1>
			</div>
			<div class="panel-body">		
				<div class="row" style="margin-top:20px">
					<div class="col-xs-12">
						<table class="table" id="productos">
							<thead>
								<tr>
									<th>Producto</th>
									<th>Cantidad</th>
									<th>&nbsp;</th>
								</tr>
							</thead>
							<tbody class="bod">
								<?php if(!empty($detail)):foreach($detail->detalles->result() as $d): ?>
									<tr>
										<td>
											<?= $d->producto ?>
											<input type="hidden" name="albums_id[]" value="<?= $d->albums_id ?>">
										</td>
										<td>
											<span><?= $d->cantidad ?></span>
											<input type="hidden" name="cantidad[]" value="<?= $d->cantidad ?>">
										</td>
										<th>										
											<button class="btn btn-danger rm" type="button"><i class="fa fa-times"></i></button>
										</th>
									</tr>
								<?php endforeach; endif; ?>
							</tbody>
							<tfooter>
								<tr class="forms">
									<td>
										<?= form_dropdown_from_query('producto','albums','id','titulo','','id="producto"') ?>
										<input type="hidden" name="albums_id[]" value="">
									</td>
									<td>
										<input type="number" class="form-control" placeholder="Cantidad" value="1" id="cantidad">
										<input type="hidden" name="cantidad[]" value="">
									</td>
									<th>
										<button class="btn btn-success add" type="button"><i class="fa fa-plus"></i></button>
										<button class="btn btn-danger hidden rm" type="button"><i class="fa fa-times"></i></button>
									</th>
								</tr>				
							</tfooter>
						</table>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12" id="response"></div>
				</div>
				<div class="row" style="margin-top:20px;">
					<div class="col-xs-12" style="text-align:right">
						<button class="btn btn-success" type="submit">Guardar</button>
						<a class="btn btn-danger" href="<?= base_url('t/regalos') ?>">Cancelar</a>
					</div>
				</div>		
		</div>
	</div>
	</div>
	</div>
		
		

</form>

<script>
	$(document).on('ready',function(){
		$(".add").on('click',function(){			
			var tr = $(this).parents('tr');
			var body = $("#productos .bod");
			var prodid = tr.find('#producto').val();
			var prod = tr.find('#producto option:selected').html();
			var cant = tr.find('#cantidad').val();
			tr = tr.clone();
			
			tr.find('.add').removeClass('add').addClass('hidden');
			tr.find('#producto').after(prod).remove();
			tr.find('.chzn-container').remove();
			tr.find('input[name="albums_id[]"]').val(prodid);
			tr.find('input[name="cantidad[]"]').val(cant);
			tr.find('#cantidad').replaceWith('<span>'+cant+'</span>');
			tr.find('.rm').removeClass('hidden');
			tr.find('.add').addClass('hidden');
			body.append(tr);

			$(this).parents('tr').find('#cantidad').val(1);
			$(this).parents('tr').find('#producto').val('').chosen().trigger('liszt:updated');


		});

		$(document).on('click',".rm",function(){
			$(this).parents('tr').remove();			
		});
	});
</script>