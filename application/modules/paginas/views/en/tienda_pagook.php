<body class="woocommerce woocommerce-page kopa-subpage">
<div id="main-content">
	[menu]
		<header class="page-header have-disc-icon text-center">
			<div class="disc-bg"></div>
            <div class="mask"></div>

            <div class="page-header-bg-2 page-header-bg"></div>

            <div class="page-header-inner page-header-inner-1">

                <div class="wrapper">
                    
                    <h1 class="page-title">Pago procesado con éxito</h1>

                    <div class="breadcrumb clearfix">                    
                        <span itemtype="http://data-vocabulary.org/Breadcrumb" itemscope="">
                            <a href="<?= base_url() ?>" itemprop="url">
                                <span itemprop="title">Inicio</span>
                            </a>
                        </span>
                        <span>&nbsp;/&nbsp;</span>
                        <span itemtype="http://data-vocabulary.org/Breadcrumb" itemscope="">
                            <a href="<?= base_url() ?>" itemprop="url">
                                <span itemprop="title">Tienda</span>
                            </a>
                        </span>
                        <!-- <span>&nbsp;/&nbsp;</span> -->
                        <!-- <span itemtype="http://data-vocabulary.org/Breadcrumb" itemscope="" class="current-page"><span itemprop="title">Typography</span></span> -->
                    </div>
                    <!-- breadcrumb -->

                </div>
                <!-- wrapper -->

                </div>
                <!-- page-header-inner -->

                <div class="album-icon">
	                <div class="icon-inner-1">
	                    <span class="icon-inner-2"></span>
	                </div>
	                <span class="fa fa-music"></span>
	            </div>
            
        </header>
        <!-- page-header -->
		<div class="wrapper clearfix  kopa-flickr-widget">
			<section class="elements-box mb-40">
				<div class="row">
					<div class="col-xs-12 col-sm-12">
			            <div class="element-title">
			                <h4>Resultado de pago</h4>
			                <p><i class="fa fa-check-circle fa-5x" style="color:green"></i></p>
			                <p>En breve nos pondremos en contacto con usted para enviarle el material solicitado.</p>
			            </div>			            
		            </div>		            
				</div>
	            
	        </section>
	        </div>


			
	[footer]
</div>