<body class="kopa-subpage kopa-album-page">
    [menu]
    <div id="main-content">
        <header class="page-header have-disc-icon text-center">
            <div class="disc-bg"></div>
            <div class="mask"></div>
            <!--<div class="typo-bg"></div>-->
            <div class="page-header-bg-1 page-header-bg"></div>
            <div class="page-header-inner">
                <div class="wrapper">
                    
                    <h1 class="page-title">Maxi's</h1>
                    <div class="breadcrumb clearfix">
                        <span itemtype="http://data-vocabulary.org/Breadcrumb" itemscope="">
                            <a href="<?= base_url() ?>" itemprop="url">
                                <span itemprop="title">Home</span>
                            </a>
                        </span>
                        <span>&nbsp;/&nbsp;</span>
                        <span itemtype="http://data-vocabulary.org/Breadcrumb" itemscope="" class="current-page"><span itemprop="title">Albums</span></span>
                    </div>
                    <!-- breadcrumb -->
                </div>
                <!-- wrapper -->
                
            </div>
            <!-- page-header-inner -->
            
            <div class="album-icon">
                <div class="icon-inner-1">
                    <span class="icon-inner-2"></span>
                </div>
                <span class="fa fa-music"></span>
            </div>
            
        </header>
        <!-- page-header -->
        <div class="wrapper clearfix">
            <div class="widget kopa-album-widget">
                <h2 class="widget-title widget-title-s5">List of albums</h2>
                <div class="widget-content">
                    
                    <div class="row">
                        <div class="col-md-4 col-md-offset-4">
                            <div class="search-box clearfix">
                                <form action="#" class="search-form pull-left clearfix" method="get">
                                    <input type="text" onBlur="if (this.value == '')
                                    this.value = this.defaultValue;" onFocus="if (this.value == this.defaultValue)
                                    this.value = '';" value="Write the name of the album..." name="s" class="search-text">
                                    <button type="submit" class="search-submit"><i class="fa fa-search"></i>
                                    </button>
                                </form>
                                <!-- search-form -->
                            </div>
                            <!--search-box-->
                        </div>
                        <!-- col-md-4 -->
                    </div>
                    <!-- row -->
                    
                    <div class="widget-content">
                        <div class="wrapper">
                            
                            <ul class="row kopa-album-list">
                                <?php 
                                    $discos = $this->elements->albums(array('tipo'=>1));
                                    foreach($discos->result() as $a): ?>
                                    <li class="col-xs-12 col-md-3 col-sm-4">
                                        <?php $this->load->view('_cd_party',array('d'=>$a)); ?>
                                    </li>
                                <? endforeach ?>
                            </ul>
                        </div>
                        <!-- wrapper-1 -->
                    </div>
                    <!-- masonry-container -->                                    
                    </div>
                </div>
                <!-- kopa-album-widget -->
            </div>            
            
        </div>
        <!-- main-content -->
        <!-- main-content -->
        [footer]