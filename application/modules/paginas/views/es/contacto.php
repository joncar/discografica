<body class="kopa-contact-page">
<script src="https://www.google.com/recaptcha/api.js?render=6LfHO7kUAAAAAFe8e7Z8nyzJBPGdFhU0eunMjwbh"></script>
	[menu]
	<div id="main-content">

        <header class="page-header text-center">        

            <div class="kp-map-wrapper">
                <div id="kp-map" class="kp-map" data-place="Discotecarecords" data-latitude="41.5306006" data-longitude="2.0960394"></div>
            </div>
            <!-- kp-map-wrapper -->

            <div class="mask"></div>

            <div class="page-header-inner">

                <div class="wrapper">
                    
                    <h1 class="page-title"><?= l('contacto') ?></h1>

                    <div class="breadcrumb clearfix">                    
                        <span itemtype="http://data-vocabulary.org/Breadcrumb" itemscope="">
                            <a href="<?= base_url() ?>" itemprop="url">
                                <span itemprop="title"><?= l('inicio') ?></span>
                            </a>
                        </span>
                        <span>&nbsp;/&nbsp;</span>
                        <span itemtype="http://data-vocabulary.org/Breadcrumb" itemscope="" class="current-page"><span itemprop="title"><?= l('contacto') ?></span></span>
                    </div>
                    <!-- breadcrumb -->

                </div>
                <!-- wrapper -->

            </div>
            <!-- page-header-inner -->
            
        </header>
        <!-- page-header -->

        <div class="wrapper clearfix">

            <div class="widget kopa-contact-info-widget">

                <div class="widget-title">
                    <h2 class="text-uppcase"><?= l('aqui-nos-encontraras') ?><span class="bottom-line"><span><span>&nbsp;</span></span></span></h2>
                </div>
                <!-- widget-title -->

                <div class="widget-content">
<!-- 
                    <p>Cras venenatis justo mi, non posuere enim aliquet malesuada. Nullam orci sem, adipiscing id rutrum et, blandit quis lorem. Phasellus lacus orci, cursus vitae mi eget, sagittis congue elit. Donec ac tincidunt tortor. Duis vel neque eleifend odio hendrerit consequat sed vel massa. Praesent tempor libero quis tincidunt fringilla. Aliquam congue, neque et aliquam eleifend, lacus diam aliquet urna, in sollicitudin neque nisl facilisis urna.</p>
 -->
                    <div class="row">
                        <div class="col-md-6 col-sm-4">
                            <div class="contact-item">
                                <div class="contact-icon"><i class="fa fa-home"></i></div>
                                <div class="contact-info contact-address">
                                    <h6><?= l('direccion') ?></h6>
                                    <p><?= l('direccion_calle') ?></p>
                                </div>
                                
                            </div>
                            <div class="contact-item">
                                <div class="contact-icon"><i class="fa fa-phone"></i></div>
                                <div class="contact-info contact-mobile">
                                    <h6><?= l('telefono') ?></h6>
                                    <p><a href="callto:123456789">(+34) 930 500 088</a></p>
                                    
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-md-6 col-sm-4">
                            <div class="contact-item">
                                <div class="contact-icon"><i class="fa fa-send"></i></div>
                                <div class="contact-info contact-email">
                                    <h6><?= l('email-general') ?></h6>
                                    <p><a href="mailto:<?= l('mail_contacto') ?>"><?= l('mail_contacto') ?></a></p>
                                    
                                </div>
                                
                            </div>
                            <div class="contact-item">
                                <div class="contact-icon"><i class="fa fa-send"></i></div>
                                <div class="contact-info contact-email">
                                    <h6><?= l('email-tienda') ?>*</h6>
                                    <p style=" margin-bottom: 5px; }"><a href="mailto: <?= l('email-contabilidad') ?>"><?= l('email-contabilidad') ?></a></p>
                                     <p style=" color: red;font-size: 13px;"><?= l('contacto-tut') ?></p>
                                </div>
                                
                            </div>
                            <div class="contact-item">
                                <div class="contact-icon"><i class="fa fa-send"></i></div>
                                <div class="contact-info contact-email">
                                    <h6><?= l('promos-radio') ?>*</h6>
                                     <p style=" margin-bottom: 5px; }"><a href="mailto: <?= l('mail-radios') ?>"><?= l('mail-radios') ?></a></p>
                                    <p style=" color: red;font-size: 13px;"><?= l('contacto-tut2') ?></p>
                                    
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
            <!-- kopa-contact-info-widget -->

            <div id="contact-box">
                <h3 class="text-uppercase text-center clearfix"><?= l('dejanos-un-mensaje') ?></h3>

                <form class="contact-form clearfix" action="paginas/frontend/contacto" method="post" onsubmit="sendForm(this,'#response1'); return false;">
                    <span class="c-note text-center"><?= l('tu-direccion') ?></span>

                    <div class="row">
                        <div class="col-md-4 col-sm-4">
                            <p class="input-block">
                                <label for="contact_fname" class="required"><?= l('nombre') ?> <span>*</span></label>
                                <input type="text" placeholder="<?= l('nombre') ?> *" id="contact_fname" name="nombre" class="valid">
                            </p>
                        </div>
                        <!-- col-md-4 -->

                        

                        <div class="col-md-4 col-sm-4">
                            <p class="input-block">
                                <label for="contact_email" class="required">Email <span>*</span></label>
                                <input type="text" placeholder="Email *" id="contact_email" name="email" class="valid">
                            </p>
                        </div>

                         <div class="col-md-4 col-sm-4">
                            <p class="input-block">
                                <label for="contact_email" class="required"><?= l('tema') ?> <span>*</span></label>
                                <input type="text" placeholder="<?= l('tema') ?> *" id="contact_email" name="titulo" class="valid">
                            </p>
                        </div>
                        <!-- col-md-4 -->

                    </div>
                    

                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <p class="textarea-block">                        
                                <label for="contact_message" class="required"><?= l('tu-mensaje') ?> <span>*</span></label>
                                <textarea placeholder="<?= l('tu-mensaje') ?> *" name="message" id="contact_message" cols="88" rows="6"></textarea>
                            </p>
                        </div>                        
                    </div>
                    <!-- row -->

                    <p class="checkbox">
                        <input type="checkbox" name="politicas" value="1" style="width:auto; height:auto;"> <?= str_replace('div','span',l('he-leido-y-acepto')) ?>
                    </p>

                    <p class="contact-button clearfix">                    
                        <span class="kopa-button" style="padding:0">
                            <input type="submit" style="padding: 10px 20px;" value="<?= l('enviar') ?>" id="submit-contact" class="input-submit">
                        </span>
                    </p>
                    <div id="response1"></div>
                </form>
                
            </div><!--contact-box-->

        </div>
        <!-- wrapper -->
        
    </div>
    <!-- main-content -->
	[footer]