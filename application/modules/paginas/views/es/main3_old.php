<body class="kopa-home-1">
[menu]
<div id="main-content" style="padding-bottom: 60px">
    <div class="kopa-rt">
        <div class="home-slider-box">
            <div id="carousel" class="kopa-home-slider-1 kopa-home-slider-carousel">
                
                <div class="carousel-item">
                    <img src="[base_url]theme/theme/images/img/1.png" data-thumb="[base_url]theme/theme/images/img/2.png" alt="">
                    <div class="mask"></div>
                    <div class="slide-caption">
                        <a class="author-name" href="#">MIKO MISSION</a>
                        <h2 class="entry-title"><a href="#">The World is You</a></h2>
                    </div>
                </div>
                <div class="carousel-item">
                    <img src="[base_url]theme/theme/images/img/1.png" data-thumb="[base_url]theme/theme/images/img/2.png" alt="">
                    <div class="mask"></div>
                    <div class="slide-caption">
                        <a class="author-name" href="#">Stirling Myles</a>
                        <h2 class="entry-title"><a href="#">Suge Knight Arrested For Murder</a></h2>
                    </div>
                </div>
                <div class="carousel-item">
                    <img src="[base_url]theme/theme/images/img/1.png" data-thumb="[base_url]theme/theme/images/img/2.png" alt="">
                    <div class="mask"></div>
                    <div class="slide-caption">
                        <a class="author-name" href="#">Stirling Myles</a>
                        <h2 class="entry-title"><a href="#">Suge Knight Arrested For Murder</a></h2>
                    </div>
                </div>
                <div class="carousel-item">
                    <img src="[base_url]theme/theme/images/img/1.png" data-thumb="[base_url]theme/theme/images/img/2.png" alt="">
                    <div class="mask"></div>
                    <div class="slide-caption">
                        <a class="author-name" href="#">Stirling Myles</a>
                        <h2 class="entry-title"><a href="#">Suge Knight Arrested For Murder</a></h2>
                    </div>
                </div>
            </div>
            <!-- kopa-home-slider-1 -->
            <div class="loading">
                <i class="fa fa-refresh fa-spin"></i>
            </div>
            <a href="#" title="" id="prev"></a>
            <a href="#" title="" id="next"></a>
            <div id="navi" class="clearfix">
                <p id="pagenumber">Now showing image <span></span> of 5.</p>
                <p id="title"></p>
                <p id="pager"><span></span></p>
            </div>
            <!-- navi -->
            
        </div>
        <!-- home-slider-box -->
        <div class="kopa-area-fit">
            <div class="widget kopa-audio-list-1-widget">
                <ul class="clearfix">
                    <li>
                        <article class="entry-item">
                            <div class="entry-thumb">
                                <a href="#"><img src="[base_url]theme/theme/images/img/3.png" alt=""></a>
                                <div class="thumb-hover"></div>
                            </div>
                            <div class="entry-content">
                                <h4 class="entry-title"><a href="#">pop anthems</a></h4>
                                <p class="entry-author">by: <a href="#">Marie Incontrera</a></p>
                                <div id="reprod1" class="reproductor jp-jplayer-single-1"></div>
                                <div id="reprodControl1" class="jp-audio jp-audio-single1" role="application" aria-label="media player">
                                    <div class="jp-type-single">
                                        <div class="jp-gui jp-interface">
                                            <div class="jp-controls">
                                                <button class="jp-play" role="button" tabindex="0">play</button>
                                            </div>
                                            <div class="jp-progress">
                                                <div class="jp-seek-bar" style="width:100%;">
                                                    <div class="jp-play-bar"></div>
                                                </div>
                                            </div>
                                            <div class="jp-volume-controls">
                                                <span class="fa fa-volume-down"></span>
                                                <div class="jp-volume-bar">
                                                    <div class="jp-volume-bar-value"></div>
                                                </div>
                                            </div>
                                            <div class="jp-time-holder">
                                                <div class="jp-current-time" role="timer" aria-label="time">&nbsp;</div>
                                                <div class="jp-duration" role="timer" aria-label="duration">&nbsp;</div>
                                            </div>
                                        </div>
                                        <div class="jp-no-solution">
                                            <span>Update Required</span>
                                            To play the media you will need to either update your browser to a recent version or update your <a href="http://get.adobe.com/flashplayer/" target="_blank">Flash plugin</a>.
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </article>
                    </li>
                    <li>
                        <article class="entry-item">
                            <div class="entry-thumb">
                                <a href="#"><img src="[base_url]theme/theme/images/img/3_2.png" alt=""></a>
                                <div class="thumb-hover"></div>
                            </div>
                            <div class="entry-content">
                                <h4 class="entry-title"><a href="#">beats + <br>sounds</a></h4>
                                <p class="entry-author">by: <a href="#">Marie Incontrera</a></p>
                                <div id="reprod2" class="reproductor jp-jplayer-single-2"></div>
                                <div id="reprodControl2" class="jp-audio jp-audio-single2" role="application" aria-label="media player">
                                    <div class="jp-type-single">
                                        <div class="jp-gui jp-interface">
                                            <div class="jp-controls">
                                                <button class="jp-play" role="button" tabindex="0">play</button>
                                            </div>
                                            <div class="jp-progress">
                                                <div class="jp-seek-bar" style="width:100%;">
                                                    <div class="jp-play-bar"></div>
                                                </div>
                                            </div>
                                            <div class="jp-volume-controls">
                                                <span class="fa fa-volume-down"></span>
                                                <div class="jp-volume-bar">
                                                    <div class="jp-volume-bar-value"></div>
                                                </div>
                                            </div>
                                            <div class="jp-time-holder">
                                                <div class="jp-current-time" role="timer" aria-label="time">&nbsp;</div>
                                                <div class="jp-duration" role="timer" aria-label="duration">&nbsp;</div>
                                            </div>
                                        </div>
                                        <div class="jp-no-solution">
                                            <span>Update Required</span>
                                            To play the media you will need to either update your browser to a recent version or update your <a href="http://get.adobe.com/flashplayer/" target="_blank">Flash plugin</a>.
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </article>
                    </li>
                    <li>
                        <article class="entry-item">
                            <div class="entry-thumb">
                                <a href="#"><img src="[base_url]theme/theme/images/img/3_3.png" alt=""></a>
                                <div class="thumb-hover"></div>
                            </div>
                            <div class="entry-content">
                                <h4 class="entry-title"><a href="#">dange<br>rous</a></h4>
                                <p class="entry-author">by: <a href="#">Marie Incontrera</a></p>
                                <div id="reprod3" class="reproductor jp-jplayer-single-3"></div>
                                <div id="reprodControl3" class="jp-audio jp-audio-single3" role="application" aria-label="media player">
                                    <div class="jp-type-single">
                                        <div class="jp-gui jp-interface">
                                            <div class="jp-controls">
                                                <button class="jp-play" role="button" tabindex="0">play</button>
                                            </div>
                                            <div class="jp-progress">
                                                <div class="jp-seek-bar" style="width:100%;">
                                                    <div class="jp-play-bar"></div>
                                                </div>
                                            </div>
                                            <div class="jp-volume-controls">
                                                <span class="fa fa-volume-down"></span>
                                                <div class="jp-volume-bar">
                                                    <div class="jp-volume-bar-value"></div>
                                                </div>
                                            </div>
                                            <div class="jp-time-holder">
                                                <div class="jp-current-time" role="timer" aria-label="time">&nbsp;</div>
                                                <div class="jp-duration" role="timer" aria-label="duration">&nbsp;</div>
                                            </div>
                                        </div>
                                        <div class="jp-no-solution">
                                            <span>Update Required</span>
                                            To play the media you will need to either update your browser to a recent version or update your <a href="http://get.adobe.com/flashplayer/" target="_blank">Flash plugin</a>.
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </article>
                    </li>
                </ul>
                
            </div>
            <!-- widget -->
        </div>
        <!-- kopa-area-fit -->
        <!-- kopa-ab -->
    </div>
    
    <section class="kopa-area kopa-area-9 kopa-area-dark kopa-parallax">
        <div class="span-bg"></div>
        <div class="wrapper">
            <div class="row">
                
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="widget kopa-countdown-2-widget">
                        <div class="cd-content">
                        <h3 class="entry-title"><a href="#">Próxima Actuación en Exclusiva</a></h3>
                            <h3 class="cd-title"><a href="#">SABRINA SALERNO</a></h3>
                            <p class="cd-date">18 Abril'19</p>
                            <p>Discoteca La Sala, Viladecans, Barcelona</p>
                            <ul class="kopa-countdown-2">
                            </ul>
                        </div>
                    </div>
                    <!-- widget -->
                    
                </div>
                <!-- col-md-12 -->
                
            </div>
            <!-- row -->
            
        </div>
        <!-- wrapper -->
        
    </section>
    <!-- kopa-area-9 -->
    <div class="widget kopa-album-2-widget">
        <header class="kopa-area-dark">
            <div class="wrapper">
                <h3 class="widget-title style1">top MAXI'S</h3>
                <p>Pellentesque elementum libero enim, eget gravida nunc laoreet et. Nullam ac enim auctor imperdiet turpis <br>Mauris ut tristique odio. </p>
            </div>
            <!-- wrapper -->
            <div class="album-icon">
                <div class="icon-inner-1">
                    <span class="icon-inner-2"></span>
                </div>
                <span class="fa fa-music"></span>
            </div>
        </header>
        <div class="widget-content">
            <div class="wrapper">
                
                <ul class="row kopa-album-list">
                    <?php 
                        $discos = $this->elements->albums(null,6);
                        foreach($discos->result() as $a): ?>
                        <li class="col-md-4 col-sm-4 col-xs-4">
                            <?php $this->load->view('_cd_party',array('d'=>$a)); ?>
                        </li>
                    <? endforeach ?>
                </ul>
            </div>
            <!-- wrapper-1 -->
        </div>
    </div>
    <!-- widget -->
    <section class="kopa-area kopa-area-2 kopa-parallax kopa-area-dark">
        <div class="span-bg"></div>
        <div class="wrapper">
            <div class="row">
                
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="widget kopa-sync-carousel-widget">
                        <div class="owl-carousel sync1">
                            <div class="item">
                                <article class="entry-item">
                                    <div class="entry-thumb">
                                        <a href="[base_url]artistas-detail.html"><img src="[base_url]theme/theme/images/img/4.png" alt=""></a>
                                    </div>
                                    <div class="entry-content">
                                        <h4 class="entry-title"><a href="[base_url]artistas-detail.html">CC CATCH</a></h4>
                                        <p>Pellentesque elementum libero enim, eget gravida nunc laoreet et. Nullam ac enim auctor, fringilla <br>risus at, imperdiet turpis. Mauris ut tristique odio. Aenean diam ipsum.</p>
                                    </div>
                                </article>
                            </div>
                            <!-- item -->
                            <div class="item">
                                <article class="entry-item">
                                    <div class="entry-thumb">
                                        <a href="[base_url]artistas-detail.html"><img src="[base_url]theme/theme/images/img/4_1.png" alt=""></a>
                                    </div>
                                    <div class="entry-content">
                                        <h4 class="entry-title"><a href="[base_url]artistas-detail.html">PETER GRABIEL</a></h4>
                                        <p>Pellentesque elementum libero enim, eget gravida nunc laoreet et. Nullam ac enim auctor, fringilla <br>risus at, imperdiet turpis. Mauris ut tristique odio. Aenean diam ipsum.</p>
                                    </div>
                                </article>
                            </div>
                            <!-- item -->
                            <div class="item">
                                <article class="entry-item">
                                    <div class="entry-thumb">
                                        <a href="[base_url]artistas-detail.html"><img src="[base_url]theme/theme/images/img/4_2.png" alt=""></a>
                                    </div>
                                    <div class="entry-content">
                                        <h4 class="entry-title"><a href="[base_url]artistas-detail.html">GEORGE MICHAEL</a></h4>
                                        <p>Pellentesque elementum libero enim, eget gravida nunc laoreet et. Nullam ac enim auctor, fringilla <br>risus at, imperdiet turpis. Mauris ut tristique odio. Aenean diam ipsum.</p>
                                    </div>
                                </article>
                            </div>
                            <!-- item -->
                            <div class="item">
                                <article class="entry-item">
                                    <div class="entry-thumb">
                                        <a href="[base_url]artistas-detail.html"><img src="[base_url]theme/theme/images/img/4_3.png" alt=""></a>
                                    </div>
                                    <div class="entry-content">
                                        <h4 class="entry-title"><a href="[base_url]artistas-detail.html">CC CATCHS</a></h4>
                                        <p>Pellentesque elementum libero enim, eget gravida nunc laoreet et. Nullam ac enim auctor, fringilla <br>risus at, imperdiet turpis. Mauris ut tristique odio. Aenean diam ipsum.</p>
                                    </div>
                                </article>
                            </div>
                            <!-- item -->
                            <div class="item">
                                <article class="entry-item">
                                    <div class="entry-thumb">
                                        <a href="[base_url]artistas-detail.html"><img src="[base_url]theme/theme/images/img/4_4.png" alt=""></a>
                                    </div>
                                    <div class="entry-content">
                                        <h4 class="entry-title"><a href="[base_url]artistas-detail.html">CC CATCH</a></h4>
                                        <p>Pellentesque elementum libero enim, eget gravida nunc laoreet et. Nullam ac enim auctor, fringilla <br>risus at, imperdiet turpis. Mauris ut tristique odio. Aenean diam ipsum.</p>
                                    </div>
                                </article>
                            </div>
                            <!-- item -->
                            <div class="item">
                                <article class="entry-item">
                                    <div class="entry-thumb">
                                        <a href="[base_url]artistas-detail.html"><img src="[base_url]theme/theme/images/img/4_5.png" alt=""></a>
                                    </div>
                                    <div class="entry-content">
                                        <h4 class="entry-title"><a href="[base_url]artistas-detail.html">CC CATCH</a></h4>
                                        <p>Pellentesque elementum libero enim, eget gravida nunc laoreet et. Nullam ac enim auctor, fringilla <br>risus at, imperdiet turpis. Mauris ut tristique odio. Aenean diam ipsum.</p>
                                    </div>
                                </article>
                            </div>
                            <!-- item -->
                            <div class="item">
                                <article class="entry-item">
                                    <div class="entry-thumb">
                                        <a href="[base_url]artistas-detail.html"><img src="[base_url]theme/theme/images/img/4_6.png" alt=""></a>
                                    </div>
                                    <div class="entry-content">
                                        <h4 class="entry-title"><a href="[base_url]artistas-detail.html">CC CATCH</a></h4>
                                        <p>Pellentesque elementum libero enim, eget gravida nunc laoreet et. Nullam ac enim auctor, fringilla <br>risus at, imperdiet turpis. Mauris ut tristique odio. Aenean diam ipsum.</p>
                                    </div>
                                </article>
                            </div>
                            <!-- item -->
                        </div>
                        <!-- sync1 -->
                        <div class="row">
                            
                            <div class="owl-carousel sync2">
                                <div class="item">
                                    <div class="entry-item">
                                        <div class="entry-thumb">
                                            <a href="#"><img src="[base_url]theme/theme/images/img/4.png" alt=""></a>
                                            <div class="thumb-hover">
                                                <a href="#" class="thumb-icon"><i class="fa fa-plus"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- item -->
                                <div class="item">
                                    <div class="entry-item">
                                        <div class="entry-thumb">
                                            <a href="#"><img src="[base_url]theme/theme/images/img/4_1.png" alt=""></a>
                                            <div class="thumb-hover">
                                                <a href="#" class="thumb-icon"><i class="fa fa-plus"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- item -->
                                <div class="item">
                                    <div class="entry-item">
                                        <div class="entry-thumb">
                                            <a href="#"><img src="[base_url]theme/theme/images/img/4_2.png" alt=""></a>
                                            <div class="thumb-hover">
                                                <a href="#" class="thumb-icon"><i class="fa fa-plus"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- item -->
                                <div class="item">
                                    <div class="entry-item">
                                        <div class="entry-thumb">
                                            <a href="#"><img src="[base_url]theme/theme/images/img/4_3.png" alt=""></a>
                                            <div class="thumb-hover">
                                                <a href="#" class="thumb-icon"><i class="fa fa-plus"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- item -->
                                <div class="item">
                                    <div class="entry-item">
                                        <div class="entry-thumb">
                                            <a href="#"><img src="[base_url]theme/theme/images/img/4_4.png" alt=""></a>
                                            <div class="thumb-hover">
                                                <a href="#" class="thumb-icon"><i class="fa fa-plus"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- item -->
                                <div class="item">
                                    <div class="entry-item">
                                        <div class="entry-thumb">
                                            <a href="#"><img src="[base_url]theme/theme/images/img/4_5.png" alt=""></a>
                                            <div class="thumb-hover">
                                                <a href="#" class="thumb-icon"><i class="fa fa-plus"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- item -->
                                <div class="item">
                                    <div class="entry-item">
                                        <div class="entry-thumb">
                                            <a href="#"><img src="[base_url]theme/theme/images/img/4_6.png" alt=""></a>
                                            <div class="thumb-hover">
                                                <a href="#" class="thumb-icon"><i class="fa fa-plus"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- item -->
                            </div>
                            <!-- sync2 -->
                            
                        </div>
                        <!-- row -->
                    </div>
                    <!-- widget -->
                    
                </div>
                <!-- col-md-12 -->
                
            </div>
            <!-- row -->
            
        </div>
        <!-- wrapper -->
        
    </section>
    <!-- kopa-area-2 -->
    <section class="kopa-area kopa-area-3 kopa-parallax">
        <div class="span-bg"></div>
        <div class="wrapper">
            <div class="row">
                
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="widget kopa-audio-list-widget">
                        <header>
                            <h3 class="widget-title style1">RECOPILATORIOS</h3>
                            <div class="kopa-social-links style1">
                                <ul class="clearfix">
                                    <li><a href="#" class="fa fa-facebook"></a></li>
                                    <li><a href="#" class="fa fa-twitter"></a></li>
                                    <li><a href="#" class="fa fa-google-plus"></a></li>
                                </ul>
                            </div>
                        </header>
                        
                    </div>
                    <!-- widget -->
                    
                </div>
                <!-- col-md-12 -->
                
            </div>
            <!-- row -->
            <div class="row">
                
                <div class="col-md-12 col-sm-12 col-xs-12">
                <h3 style="margin-bottom: 0"><a href="#" style="color:#392f55">I love disco</a></h3>
                <p><a href="#" style="color:#e22414"> Disco</a>&nbsp;-&nbsp;<a href="#" style="color:#e22414">Italo Disco</a></p>
                
                    <div class="widget kopa-album-carousel-widget">
                        <div class="owl-carousel owl-carousel-1">
                            <div class="item">
                                <article class="entry-item">
                                    <div class="entry-thumb">
                                        <span></span>
                                        <a href="#" class="recopilatoriolink"><img src="[base_url]theme/theme/images/img/8.png" alt=""></a>
                                    </div>
                                </article>
                            </div>
                            <!-- item -->
                            <div class="item">
                                <article class="entry-item">
                                    <div class="entry-thumb">
                                        <span></span>
                                        <a href="#" class="recopilatoriolink"><img src="[base_url]theme/theme/images/img/8.png" alt=""></a>
                                    </div>
                                </article>
                            </div>
                            <!-- item -->
                            <div class="item">
                                <article class="entry-item">
                                    <div class="entry-thumb">
                                        <span></span>
                                        <a href="#" class="recopilatoriolink"><img src="[base_url]theme/theme/images/img/8.png" alt=""></a>
                                    </div>
                                </article>
                            </div>
                            <!-- item -->
                            <div class="item">
                                <article class="entry-item">
                                    <div class="entry-thumb">
                                        <span></span>
                                        <a href="#" class="recopilatoriolink"><img src="[base_url]theme/theme/images/img/8.png" alt=""></a>
                                    </div>
                                </article>
                            </div>
                            <!-- item -->
                            <div class="item">
                                <article class="entry-item">
                                    <div class="entry-thumb">
                                        <span></span>
                                        <a href="#" class="recopilatoriolink"><img src="[base_url]theme/theme/images/img/8.png" alt=""></a>
                                    </div>
                                </article>
                            </div>
                            <!-- item -->
                            <div class="item">
                                <article class="entry-item">
                                    <div class="entry-thumb">
                                        <span></span>
                                        <a href="#" class="recopilatoriolink"><img src="[base_url]theme/theme/images/img/8.png" alt=""></a>
                                    </div>
                                </article>
                            </div>
                            <!-- item -->
                            <div class="item">
                                <article class="entry-item">
                                    <div class="entry-thumb">
                                        <span></span>
                                        <a href="#" class="recopilatoriolink"><img src="[base_url]theme/theme/images/img/8.png" alt=""></a>
                                    </div>
                                </article>
                            </div>
                            <!-- item -->
                            <div class="item">
                                <article class="entry-item">
                                    <div class="entry-thumb">
                                        <span></span>
                                        <a href="#" class="recopilatoriolink"><img src="[base_url]theme/theme/images/img/8.png" alt=""></a>
                                    </div>
                                </article>
                            </div>
                            <!-- item -->
                            <div class="item">
                                <article class="entry-item">
                                    <div class="entry-thumb">
                                        <span></span>
                                        <a href="#" class="recopilatoriolink"><img src="[base_url]theme/theme/images/img/8.png" alt=""></a>
                                    </div>
                                </article>
                            </div>
                            <!-- item -->
                            <div class="item">
                                <article class="entry-item">
                                    <div class="entry-thumb">
                                        <span></span>
                                        <a href="#" class="recopilatoriolink"><img src="[base_url]theme/theme/images/img/8.png" alt=""></a>
                                    </div>
                                </article>
                            </div>
                            <!-- item -->
                            <div class="item">
                                <article class="entry-item">
                                    <div class="entry-thumb">
                                        <span></span>
                                        <a href="#" class="recopilatoriolink"><img src="[base_url]theme/theme/images/img/8.png" alt=""></a>
                                    </div>
                                </article>
                            </div>
                            <!-- item -->
                            <div class="item">
                                <article class="entry-item">
                                    <div class="entry-thumb">
                                        <span></span>
                                        <a href="#" class="recopilatoriolink"><img src="[base_url]theme/theme/images/img/8.png" alt=""></a>
                                    </div>
                                </article>
                            </div>
                            <!-- item -->
                        </div>
                        <!-- owl-carousel-1 -->
                    </div>
                    <!-- widget -->






                    <div class="widget kopa-album-carousel-widget">
                        <div class="owl-carousel owl-carousel-1">
                            <div class="item">
                                <article class="entry-item">
                                    <div class="entry-thumb">
                                        <span></span>
                                        <a href="#" class="recopilatoriolink"><img src="[base_url]theme/theme/images/img/8.png" alt=""></a>
                                    </div>
                                </article>
                            </div>
                            <!-- item -->
                            <div class="item">
                                <article class="entry-item">
                                    <div class="entry-thumb">
                                        <span></span>
                                        <a href="#" class="recopilatoriolink"><img src="[base_url]theme/theme/images/img/8.png" alt=""></a>
                                    </div>
                                </article>
                            </div>
                            <!-- item -->
                            <div class="item">
                                <article class="entry-item">
                                    <div class="entry-thumb">
                                        <span></span>
                                        <a href="#" class="recopilatoriolink"><img src="[base_url]theme/theme/images/img/8.png" alt=""></a>
                                    </div>
                                </article>
                            </div>
                            <!-- item -->
                            <div class="item">
                                <article class="entry-item">
                                    <div class="entry-thumb">
                                        <span></span>
                                        <a href="#" class="recopilatoriolink"><img src="[base_url]theme/theme/images/img/8.png" alt=""></a>
                                    </div>
                                </article>
                            </div>
                            <!-- item -->
                            <div class="item">
                                <article class="entry-item">
                                    <div class="entry-thumb">
                                        <span></span>
                                        <a href="#" class="recopilatoriolink"><img src="[base_url]theme/theme/images/img/8.png" alt=""></a>
                                    </div>
                                </article>
                            </div>
                            <!-- item -->
                            <div class="item">
                                <article class="entry-item">
                                    <div class="entry-thumb">
                                        <span></span>
                                        <a href="#" class="recopilatoriolink"><img src="[base_url]theme/theme/images/img/8.png" alt=""></a>
                                    </div>
                                </article>
                            </div>
                            <!-- item -->
                            <div class="item">
                                <article class="entry-item">
                                    <div class="entry-thumb">
                                        <span></span>
                                        <a href="#" class="recopilatoriolink"><img src="[base_url]theme/theme/images/img/8.png" alt=""></a>
                                    </div>
                                </article>
                            </div>
                            <!-- item -->
                            <div class="item">
                                <article class="entry-item">
                                    <div class="entry-thumb">
                                        <span></span>
                                        <a href="#" class="recopilatoriolink"><img src="[base_url]theme/theme/images/img/8.png" alt=""></a>
                                    </div>
                                </article>
                            </div>
                            <!-- item -->
                            <div class="item">
                                <article class="entry-item">
                                    <div class="entry-thumb">
                                        <span></span>
                                        <a href="#" class="recopilatoriolink"><img src="[base_url]theme/theme/images/img/8.png" alt=""></a>
                                    </div>
                                </article>
                            </div>
                            <!-- item -->
                            <div class="item">
                                <article class="entry-item">
                                    <div class="entry-thumb">
                                        <span></span>
                                        <a href="#" class="recopilatoriolink"><img src="[base_url]theme/theme/images/img/8.png" alt=""></a>
                                    </div>
                                </article>
                            </div>
                            <!-- item -->
                            <div class="item">
                                <article class="entry-item">
                                    <div class="entry-thumb">
                                        <span></span>
                                        <a href="#" class="recopilatoriolink"><img src="[base_url]theme/theme/images/img/8.png" alt=""></a>
                                    </div>
                                </article>
                            </div>
                            <!-- item -->
                            <div class="item">
                                <article class="entry-item">
                                    <div class="entry-thumb">
                                        <span></span>
                                        <a href="#" class="recopilatoriolink"><img src="[base_url]theme/theme/images/img/8.png" alt=""></a>
                                    </div>
                                </article>
                            </div>
                            <!-- item -->
                        </div>
                        <!-- owl-carousel-1 -->
                    </div>
                    <!-- widget -->




                    
                </div>
                <!-- col-md-12 -->
                
            </div>
            <!-- row -->
            
        </div>
        <!-- wrapper -->
        
    </section>
    <!-- kopa-area-3 -->
    <section class="kopa-area kopa-area-1 kopa-area-dark">
        <div class="span-bg"></div>
        <div class="wrapper">
            <div class="row">
                
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="widget kopa-event-widget">
                        <h3 class="widget-title style1">PRÓXIMOS EVENTOS</h3>
                        <ul class="clearfix hovered">
                            <li>
                                <div class="col-1">
                                    <p>02/Abr</p>
                                </div>
                                <div class="col-2">
                                    <p><a href="#">Barcelona</a></p>
                                </div>
                                <div class="col-3">
                                    <p><a href="#">Entrevista a CC Catch....</a></p>
                                </div>
                                <div class="col-4">
                                    <p><a href="#" class="event-button"><span>RESERVAR</span></a></p>
                                </div>
                            </li>
                            <li>
                                <div class="col-1">
                                    <p>06/Abr</p>
                                </div>
                                <div class="col-2">
                                    <p><a href="#">Madrid</a></p>
                                </div>
                                <div class="col-3">
                                    <p><a href="#">Original Music / Your Year With Nike+</a></p>
                                </div>
                                <div class="col-4">
                                    <p><a href="#" class="event-button"><span>CANCELADO</span></a></p>
                                </div>
                            </li>
                            <li>
                                <div class="col-1">
                                    <p>14/Abr</p>
                                </div>
                                <div class="col-2">
                                    <p><a href="#">Memphis, TN</a></p>
                                </div>
                                <div class="col-3">
                                    <p><a href="#">Fractured Bones & Reputations</a></p>
                                </div>
                                <div class="col-4">
                                    <p><a href="#" class="event-button"><span>Proximante</span></a></p>
                                </div>
                            </li>
                            <li>
                                <div class="col-1">
                                    <p>18/Abr</p>
                                </div>
                                <div class="col-2">
                                    <p><a href="#">Oakland, CA</a></p>
                                </div>
                                <div class="col-3">
                                    <p><a href="#">The Otherside of Sadness</a></p>
                                </div>
                                <div class="col-4">
                                    <p><a href="#" class="event-button"><span>Anticipadas 05/01/2017</span></a></p>
                                </div>
                            </li>
                            <li>
                                <div class="col-1">
                                    <p>21/Abr</p>
                                </div>
                                <div class="col-2">
                                    <p><a href="#">North Little Rock, AR</a></p>
                                </div>
                                <div class="col-3">
                                    <p><a href="#">Picking up the Pieces (Acoustic Version)</a></p>
                                </div>
                                <div class="col-4">
                                    <p><a href="#" class="event-button"><span>Reservar</span></a></p>
                                </div>
                            </li>
                            <li>
                                <div class="col-1">
                                    <p>25/Abr</p>
                                </div>
                                <div class="col-2">
                                    <p><a href="#">New Orleans, LA</a></p>
                                </div>
                                <div class="col-3">
                                    <p><a href="#">Introducing: Holiday Mixtape Vol. 1</a></p>
                                </div>
                                <div class="col-4">
                                    <p><a href="#" class="event-button"><span>COMING SOON</span></a></p>
                                </div>
                            </li>
                            <li>
                                <div class="col-1">
                                    <p>27/Abr</p>
                                </div>
                                <div class="col-2">
                                    <p><a href="#">Memphis, TN</a></p>
                                </div>
                                <div class="col-3">
                                    <p><a href="#">Marmoset's Top 20 Albums of 2014</a></p>
                                </div>
                                <div class="col-4">
                                    <p><a href="#" class="event-button"><span>Rebajado</span></a></p>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <!-- widget -->
                    
                </div>
                <!-- col-md-12 -->
                
            </div>
            <!-- row -->
            
        </div>
        <!-- wrapper -->
        
    </section>
    <!-- kopa-area-1 -->
    <section class="kopa-area kopa-area-5 kopa-parallax kopa-area-dark">
        <div class="span-bg"></div>
        <div class="wrapper">
            <div class="row">
                
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="widget kopa-masonry-widget">
                        <header>
                            <h3 class="widget-title style1">galería</h3>
                            <p>Fusce hendrerit, mi non lobortis pellentesque, nibh ante accumsan nisi, in tristique nibh nisi nec tortor. <br>Mauris nibh sem, vulputate non consequat in, condimentum eget nulla</p>
                        </header>
                        <ul class="kopa-masonry-wrap">
                            <li class="ms-item1">
                                <div class="entry-item">
                                    <div class="entry-thumb">
                                        <a href="#"><img src="[base_url]theme/theme/images/img/6.png" alt=""></a>
                                        <div class="thumb-hover">
                                            <a href="#" class="thumb-icon"><i class="fa fa-plus"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li class="ms-item1">
                                <div class="entry-item">
                                    <div class="entry-thumb">
                                        <a href="#"><img src="[base_url]theme/theme/images/img/6_1.png" alt=""></a>
                                        <div class="thumb-hover">
                                            <a href="#" class="thumb-icon"><i class="fa fa-plus"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li class="ms-item1">
                                <div class="entry-item">
                                    <div class="entry-thumb">
                                        <a href="#"><img src="[base_url]theme/theme/images/img/6_2.png" alt=""></a>
                                        <div class="thumb-hover">
                                            <a href="#" class="thumb-icon"><i class="fa fa-plus"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li class="ms-item1">
                                <div class="entry-item">
                                    <div class="entry-thumb">
                                        <a href="#"><img src="[base_url]theme/theme/images/img/6_3.png" alt=""></a>
                                        <div class="thumb-hover">
                                            <a href="#" class="thumb-icon"><i class="fa fa-plus"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li class="ms-item1">
                                <div class="entry-item">
                                    <div class="entry-thumb">
                                        <a href="#"><img src="[base_url]theme/theme/images/img/6_4.png" alt=""></a>
                                        <div class="thumb-hover">
                                            <a href="#" class="thumb-icon"><i class="fa fa-plus"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li class="ms-item1">
                                <div class="entry-item">
                                    <div class="entry-thumb">
                                        <a href="#"><img src="[base_url]theme/theme/images/img/6_5.png" alt=""></a>
                                        <div class="thumb-hover">
                                            <a href="#" class="thumb-icon"><i class="fa fa-plus"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li class="ms-item1">
                                <div class="entry-item">
                                    <div class="entry-thumb">
                                        <a href="#"><img src="[base_url]theme/theme/images/img/6_6.png" alt=""></a>
                                        <div class="thumb-hover">
                                            <a href="#" class="thumb-icon"><i class="fa fa-plus"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li class="ms-item1">
                                <div class="entry-item">
                                    <div class="entry-thumb">
                                        <a href="#"><img src="[base_url]theme/theme/images/img/6_7.png" alt=""></a>
                                        <div class="thumb-hover">
                                            <a href="#" class="thumb-icon"><i class="fa fa-plus"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li class="ms-item1">
                                <div class="entry-item">
                                    <div class="entry-thumb">
                                        <a href="#"><img src="[base_url]theme/theme/images/img/6_8.png" alt=""></a>
                                        <div class="thumb-hover">
                                            <a href="#" class="thumb-icon"><i class="fa fa-plus"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li class="ms-item1">
                                <div class="entry-item">
                                    <div class="entry-thumb">
                                        <a href="#"><img src="[base_url]theme/theme/images/img/6_9.png" alt=""></a>
                                        <div class="thumb-hover">
                                            <a href="#" class="thumb-icon"><i class="fa fa-plus"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li class="ms-item1">
                                <div class="entry-item">
                                    <div class="entry-thumb">
                                        <a href="#"><img src="[base_url]theme/theme/images/img/6_10.png" alt=""></a>
                                        <div class="thumb-hover">
                                            <a href="#" class="thumb-icon"><i class="fa fa-plus"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li class="ms-item1">
                                <div class="entry-item">
                                    <div class="entry-thumb">
                                        <a href="#"><img src="[base_url]theme/theme/images/img/6_11.png" alt=""></a>
                                        <div class="thumb-hover">
                                            <a href="#" class="thumb-icon"><i class="fa fa-plus"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <!-- widget -->
                    
                </div>
                <!-- col-md-12 -->
                
            </div>
            <!-- row -->
            
        </div>
        <!-- wrapper -->
        
    </section>
    <!-- kopa-area-5 -->
    <section class="kopa-area kopa-area-10 kopa-area-dark">
        <div class="wrapper">
            <div class="row">
                
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="widget kopa-tab-widget">
                        <div class="kopa-tab style1">
                            <ul class="nav nav-tabs">
                                <li class="active"><a href="#e-tab21" data-toggle="tab">SELECCIÓN ESPECIAL</a></li>
                                <li><a href="#e-tab22" data-toggle="tab">MÁS VISITADOS</a></li>
                                <li><a href="#e-tab23" data-toggle="tab">MÁS RECIENTES</a></li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="e-tab21">
                                    <ul class="row">
                                        <?php for($i=1;$i<=6;$i++): ?>
                                            <li class="col-md-2 col-sm-2 col-xs-4">
                                                <article class="entry-item">
                                                    <div class="entry-thumb">
                                                        <a href="#"><img src="[base_url]theme/theme/images/img/9_<?= $i ?>.png" alt=""></a>
                                                        <div class="thumb-hover">
                                                            <a href="#" class="thumb-icon"><i class="fa fa-plus"></i></a>
                                                        </div>
                                                    </div>
                                                </article>
                                            </li>
                                        <?php endfor ?>
                                    </ul>
                                    <!-- row -->
                                </div>
                                <div class="tab-pane" id="e-tab22">
                                    
                                    <ul class="row">
                                        
                                        <li class="col-md-2 col-sm-2 col-xs-4">
                                            <article class="entry-item">
                                                <div class="entry-thumb">
                                                    <a href="#"><img src="[base_url]theme/theme/images/img/9.png" alt=""></a>
                                                    <div class="thumb-hover">
                                                        <a href="#" class="thumb-icon"><i class="fa fa-plus"></i></a>
                                                    </div>
                                                </div>
                                            </article>
                                        </li>
                                        <!-- col-md-2 -->
                                        <li class="col-md-2 col-sm-2 col-xs-4">
                                            <article class="entry-item">
                                                <div class="entry-thumb">
                                                    <a href="#"><img src="[base_url]theme/theme/images/img/9.png" alt=""></a>
                                                    <div class="thumb-hover">
                                                        <a href="#" class="thumb-icon"><i class="fa fa-plus"></i></a>
                                                    </div>
                                                </div>
                                            </article>
                                        </li>
                                        <!-- col-md-2 -->
                                        <li class="col-md-2 col-sm-2 col-xs-4">
                                            <article class="entry-item">
                                                <div class="entry-thumb">
                                                    <a href="#"><img src="[base_url]theme/theme/images/img/9.png" alt=""></a>
                                                    <div class="thumb-hover">
                                                        <a href="#" class="thumb-icon"><i class="fa fa-plus"></i></a>
                                                    </div>
                                                </div>
                                            </article>
                                        </li>
                                        <!-- col-md-2 -->
                                        <li class="col-md-2 col-sm-2 col-xs-4">
                                            <article class="entry-item">
                                                <div class="entry-thumb">
                                                    <a href="#"><img src="[base_url]theme/theme/images/img/9.png" alt=""></a>
                                                    <div class="thumb-hover">
                                                        <a href="#" class="thumb-icon"><i class="fa fa-plus"></i></a>
                                                    </div>
                                                </div>
                                            </article>
                                        </li>
                                        <!-- col-md-2 -->
                                        <li class="col-md-2 col-sm-2 col-xs-4">
                                            <article class="entry-item">
                                                <div class="entry-thumb">
                                                    <a href="#"><img src="[base_url]theme/theme/images/img/9.png" alt=""></a>
                                                    <div class="thumb-hover">
                                                        <a href="#" class="thumb-icon"><i class="fa fa-plus"></i></a>
                                                    </div>
                                                </div>
                                            </article>
                                        </li>
                                        <!-- col-md-2 -->
                                        <li class="col-md-2 col-sm-2 col-xs-4">
                                            <article class="entry-item">
                                                <div class="entry-thumb">
                                                    <a href="#"><img src="[base_url]theme/theme/images/img/9.png" alt=""></a>
                                                    <div class="thumb-hover">
                                                        <a href="#" class="thumb-icon"><i class="fa fa-plus"></i></a>
                                                    </div>
                                                </div>
                                            </article>
                                        </li>
                                        <!-- col-md-2 -->
                                    </ul>
                                    <!-- row -->
                                </div>
                                <div class="tab-pane" id="e-tab23">
                                    
                                    <ul class="row">
                                        
                                        <li class="col-md-2 col-sm-2 col-xs-4">
                                            <article class="entry-item">
                                                <div class="entry-thumb">
                                                    <a href="#"><img src="[base_url]theme/theme/images/img/9.png" alt=""></a>
                                                    <div class="thumb-hover">
                                                        <a href="#" class="thumb-icon"><i class="fa fa-plus"></i></a>
                                                    </div>
                                                </div>
                                            </article>
                                        </li>
                                        <!-- col-md-2 -->
                                        <li class="col-md-2 col-sm-2 col-xs-4">
                                            <article class="entry-item">
                                                <div class="entry-thumb">
                                                    <a href="#"><img src="[base_url]theme/theme/images/img/9.png" alt=""></a>
                                                    <div class="thumb-hover">
                                                        <a href="#" class="thumb-icon"><i class="fa fa-plus"></i></a>
                                                    </div>
                                                </div>
                                            </article>
                                        </li>
                                        <!-- col-md-2 -->
                                        <li class="col-md-2 col-sm-2 col-xs-4">
                                            <article class="entry-item">
                                                <div class="entry-thumb">
                                                    <a href="#"><img src="[base_url]theme/theme/images/img/9.png" alt=""></a>
                                                    <div class="thumb-hover">
                                                        <a href="#" class="thumb-icon"><i class="fa fa-plus"></i></a>
                                                    </div>
                                                </div>
                                            </article>
                                        </li>
                                        <!-- col-md-2 -->
                                        <li class="col-md-2 col-sm-2 col-xs-4">
                                            <article class="entry-item">
                                                <div class="entry-thumb">
                                                    <a href="#"><img src="[base_url]theme/theme/images/img/9.png" alt=""></a>
                                                    <div class="thumb-hover">
                                                        <a href="#" class="thumb-icon"><i class="fa fa-plus"></i></a>
                                                    </div>
                                                </div>
                                            </article>
                                        </li>
                                        <!-- col-md-2 -->
                                        <li class="col-md-2 col-sm-2 col-xs-4">
                                            <article class="entry-item">
                                                <div class="entry-thumb">
                                                    <a href="#"><img src="[base_url]theme/theme/images/img/9.png" alt=""></a>
                                                    <div class="thumb-hover">
                                                        <a href="#" class="thumb-icon"><i class="fa fa-plus"></i></a>
                                                    </div>
                                                </div>
                                            </article>
                                        </li>
                                        <!-- col-md-2 -->
                                        <li class="col-md-2 col-sm-2 col-xs-4">
                                            <article class="entry-item">
                                                <div class="entry-thumb">
                                                    <a href="#"><img src="[base_url]theme/theme/images/img/9.png" alt=""></a>
                                                    <div class="thumb-hover">
                                                        <a href="#" class="thumb-icon"><i class="fa fa-plus"></i></a>
                                                    </div>
                                                </div>
                                            </article>
                                        </li>
                                        <!-- col-md-2 -->
                                    </ul>
                                    <!-- row -->
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- widget -->
                    
                </div>
                <!-- col-md-12 -->
                
            </div>
            <!-- row -->
            <div class="row">
                
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="widget kopa-audio-download-widget">
                        <h3 class="widget-title style1">los + descargados</h3>
                        <ul class="clearfix">
                            <li>
                                <article class="entry-item">
                                    <div class="col-left">
                                        <span><i>1</i></span>
                                        <a href="#"><i class="fa fa-long-arrow-down"></i></a>
                                    </div>
                                    <div class="entry-content">
                                        <h4 class="entry-title"><a href="#">Lana Del Rey - Ride (Barretso Remix)</a></h4>
                                        <p class="entry-author">by <a href="#">Jonh doe</a></p>
                                    </div>
                                </article>
                            </li>
                            <li>
                                <article class="entry-item">
                                    <div class="col-left">
                                        <span><i>2</i></span>
                                        <a href="#"><i class="fa fa-long-arrow-down"></i></a>
                                    </div>
                                    <div class="entry-content">
                                        <h4 class="entry-title"><a href="#">Lana Del Rey - Ride (Barretso Remix)</a></h4>
                                        <p class="entry-author">by <a href="#">Jonh doe</a></p>
                                    </div>
                                </article>
                            </li>
                            <li>
                                <article class="entry-item">
                                    <div class="col-left">
                                        <span><i>3</i></span>
                                        <a href="#"><i class="fa fa-long-arrow-down"></i></a>
                                    </div>
                                    <div class="entry-content">
                                        <h4 class="entry-title"><a href="#">Lana Del Rey - Ride (Barretso Remix)</a></h4>
                                        <p class="entry-author">by <a href="#">Jonh doe</a></p>
                                    </div>
                                </article>
                            </li>
                            <li>
                                <article class="entry-item">
                                    <div class="col-left">
                                        <span><i>4</i></span>
                                        <a href="#"><i class="fa fa-long-arrow-down"></i></a>
                                    </div>
                                    <div class="entry-content">
                                        <h4 class="entry-title"><a href="#">Lana Del Rey - Ride (Barretso Remix)</a></h4>
                                        <p class="entry-author">by <a href="#">Jonh doe</a></p>
                                    </div>
                                </article>
                            </li>
                            <li>
                                <article class="entry-item">
                                    <div class="col-left">
                                        <span><i>5</i></span>
                                        <a href="#"><i class="fa fa-long-arrow-down"></i></a>
                                    </div>
                                    <div class="entry-content">
                                        <h4 class="entry-title"><a href="#">Lana Del Rey - Ride (Barretso Remix)</a></h4>
                                        <p class="entry-author">by <a href="#">Jonh doe</a></p>
                                    </div>
                                </article>
                            </li>
                        </ul>
                    </div>
                    <!-- widget -->
                    
                </div>
                <!-- col-md-6 -->
                
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="widget kopa-single-article-widget">
                        <h3 class="widget-title style1">ÚLTIMAS NOTICIAS</h3>
                        <article class="entry-item">
                            <div class="entry-thumb">
                                <a href="#"><img src="[base_url]theme/theme/images/img/10.png" alt=""></a>
                                <div class="thumb-hover">
                                    <a href="#" class="thumb-icon"><i class="fa fa-plus"></i></a>
                                </div>
                            </div>
                            <div class="entry-content">
                                <p class="entry-date">5 January, 2015</p>
                                <h4 class="entry-title"><a href="#">Sunrise Festival organized Croatia 2012</a></h4>
                                <p>Pellentesque elementum libero enim, eget gravida nunc laoreet et. Nullam ac enim auctor, fringilla risus at, imperdiet turpis. Mauris ut tristique odio. Aenean diam ipsum, ultricies sed consequat sed, faucibus et tellus. Pellentesque elementum libero enim, eget gravida nunc laoreet et. Nullam ac enim auctor, fringilla risus at, imperdiet turpis.</p>
                            </div>
                        </article>
                    </div>
                    <!-- widget -->
                    
                </div>
                <!-- col-md-6 -->
                
            </div>
            <!-- row -->
            
        </div>
        <!-- wrapper -->
        
    </section>
    <!-- kopa-area-10 -->
    [footer]
</div>
<!-- main-content -->




<div class="widget kopa-featured-audio-widget" style="position:fixed; left:0px; bottom:0px; width:100%; margin-bottom: 0px; z-index:10000">
        <div class="outer">
            <div class="col-left">
                <div class="album-thumb">
                    <a href="#"><img src="[base_url]theme/theme/images/img/2.png" alt=""></a>
                </div>
                <div class="album-content">
                    <h6><a href="#">DJ KoZaK - Promo Mix soundwave 125</a></h6>
                    <p><a href="#">Heavy artillery Vid</a></p>
                </div>
            </div>
            <div class="col-right">
                <div class="audio-wrap">
                    <div class="kopa-jp-jplayer"></div>
                    <div class="jp-audio kopa-jp-wrap" role="application" aria-label="media player">
                        <div class="jp-type-playlist">
                            <div class="jp-gui jp-interface">
                                <div class="col-left">
                                    <div class="jp-controls">
                                        <button class="jp-previous" role="button" tabindex="0"></button>
                                        <button class="jp-play" role="button" tabindex="0"></button>
                                        <button class="jp-next" role="button" tabindex="0"></button>
                                        <span class="fa fa-bar-chart"></span>
                                    </div>
                                </div>
                                <div class="col-right">
                                    <div class="jp-progress">
                                        <div class="jp-seek-bar">
                                            <div class="jp-play-bar"></div>
                                        </div>
                                    </div>
                                    <div class="jp-volume-controls">
                                        <span class="fa fa-volume-down"></span>
                                        <div class="jp-volume-bar">
                                            <div class="jp-volume-bar-value"></div>
                                        </div>
                                    </div>
                                    <div class="jp-time-holder">
                                        <div class="jp-current-time" role="timer" aria-label="time">&nbsp;</div>
                                        <div class="jp-duration" role="timer" aria-label="duration">&nbsp;</div>
                                    </div>
                                </div>
                            </div>
                            <div class="jp-playlist">
                                <ul style="display: none !important">
                                    <li>&nbsp;</li>
                                </ul>
                            </div>
                            <div class="jp-no-solution">
                                <span>Update Required</span>
                                To play the media you will need to either update your browser to a recent version or update your <a href="http://get.adobe.com/flashplayer/" target="_blank">Flash plugin</a>.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
    </div>
    
    <article id="recopilatoriosContent" class="contentOculto" style="display: none;">
        <div class="widget kopa-audio-list-widget" style="margin-bottom: 0; position:relative;">
            <a href="javascript:closeListRecop()" style="position: absolute; right: -20px; top:-20px">
                <i class="fa fa-times"></i>
            </a>
            <div class="widget-thumb">
                <div class="entry-thumb">
                    <a href="#"><img src="[base_url]theme/theme/images/img/5.png" alt=""></a>
                </div>
                <ul class="info">
                    <li>
                        <span>2014</span>
                    </li>
                    <li>
                        <span>19 songs</span>
                    </li>
                    <li>
                        <div class="kopa-rating">
                            <ul>
                                <li><span class="fa fa-star"></span></li>
                                <li><span class="fa fa-star"></span></li>
                                <li><span class="fa fa-star"></span></li>
                                <li><span class="fa fa-star"></span></li>
                                <li class="inactive"><span class="fa fa-star"></span></li>
                            </ul>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="widget-content">
                <header>
                    <h3><a href="#">Andreas Weise <span class="precio"> - 20€</span></a></h3>
                    <p><a href="#">Leona</a>&nbsp;-&nbsp;<a href="#">pop</a>&nbsp;-&nbsp;<a href="#" title="Añadir a la lista de favoritos"><i class="fa fa-heart"></i></a></p>
                </header>
                <ul class="row audio-list">
                    <li class="col-md-6 col-sm-6 col-xs-12"><a href="#">Zuper - Glide</a></li>
                    <li class="col-md-6 col-sm-6 col-xs-12"><a href="#">Zuper - Glide</a></li>
                    <li class="col-md-6 col-sm-6 col-xs-12"><a href="#">Lana Del Rey - Ride (Barretso Remix)</a></li>
                    <li class="col-md-6 col-sm-6 col-xs-12"><a href="#">Lana Del Rey - Ride (Barretso Remix)</a></li>
                    <li class="col-md-6 col-sm-6 col-xs-12"><a href="#">Four Tet - Lion (Jamie xx Remix)</a></li>
                    <li class="col-md-6 col-sm-6 col-xs-12"><a href="#">Four Tet - Lion (Jamie xx Remix)</a></li>
                    <li class="col-md-6 col-sm-6 col-xs-12"><a href="#">Jamie xx & Yasmin - Touch Me</a></li>
                    <li class="col-md-6 col-sm-6 col-xs-12"><a href="#">Jamie xx & Yasmin - Touch Me</a></li>
                    <li class="col-md-6 col-sm-6 col-xs-12"><a href="#">Disclosure - Tenderly</a></li>
                    <li class="col-md-6 col-sm-6 col-xs-12"><a href="#">Disclosure - Tenderly</a></li>
                </ul>
            </div>
        </div>
    </article>

    <script>

        var recopHtml = $("#recopilatoriosContent").clone();
        $("#recopilatoriosContent").remove();

        $(".recopilatoriolink").on('click',function(e){
            e.preventDefault();
            selRecopilatorio(this);
        });

        function closeListRecop(){
            $(".recopilatorioContent").remove();
            $(".entry-item.active").removeClass('active');
        }
        
        function selRecopilatorio(e){
            $(".recopilatorioContent").remove();
            $(".entry-item.active").removeClass('active');
            var content = recopHtml.clone();
            content.show();
            content.addClass('recopilatorioContent');
            console.log(e);
            $(e).parents('.owl-wrapper-outer').after(content);
            $(e).parents('.entry-item').addClass('active');
        }    
    </script>