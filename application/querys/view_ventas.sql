DROP VIEW IF EXISTS view_ventas;
CREATE VIEW view_ventas AS 
SELECT 
ventas.id as '#Compra',
ventas.user_id,
ventas.shop,
fecha_compra,productos, 
CONCAT(FORMAT(IF(total IS NULL,precio,total),2),'€') as 'Total pagado',
IF(pasarela=1,'TPV','Paypal') as tipo_pago,paises.nombre as pais_envio,
provincias.nombre as Provincia, 
ventas.direccion as 'direccion_envio',
(CASE WHEN ventas.procesado = 1 THEN 'Pago sin procesar' WHEN ventas.procesado = 2 THEN 'Procesado' WHEN ventas.procesado = 3 THEN 'Entregado' WHEN ventas.procesado = -1 THEN 'Pago rechazado' END) as estado_pago
FROM `ventas` 
INNER JOIN user ON user.id = ventas.user_id 
INNER JOIN paises ON paises.id = ventas.paises_id 
LEFT JOIN provincias ON provincias.id = ventas.provincias_id 
WHERE 1;

DROP VIEW IF EXISTS view_ventas_detalles;
CREATE VIEW view_ventas_detalles AS 
SELECT 
ventas.id as compra,
ventas.user_id,
CONCAT(user.nombre,' ',user.apellido_paterno) as Cliente,
CONCAT(albums.referencia,'-',albums.titulo) as Producto,
albums.referencia,
albums.titulo,
albums.tipo_producto,
ventas_detalles.cantidad,
ventas_detalles.precio,
ventas.shop,
ventas.forma_pago,
ventas.pagado
FROM ventas_detalles
INNER JOIN albums ON albums.id = ventas_detalles.productos_id
INNER JOIN ventas ON ventas.id = ventas_detalles.ventas_id
INNER JOIN user ON user.id = ventas.user_id;

DROP VIEW IF EXISTS view_ventas_tiendas;
CREATE VIEW view_ventas_tiendas AS 
SELECT 
ventas.id as '#Compra',
ventas.user_id,
ventas.shop,
fecha_compra,productos, 
CONCAT(FORMAT(IF(total IS NULL,precio,total),2),'€') as 'Total pagado',
IF(pasarela=1,'TPV','Paypal') as tipo_pago,paises.nombre as pais_envio,
provincias.nombre as Provincia, 
ventas.direccion as 'direccion_envio',
(CASE WHEN ventas.procesado = 1 THEN 'Pago sin procesar' WHEN ventas.procesado = 2 THEN 'Procesado' WHEN ventas.procesado = 3 THEN 'Entregado' WHEN ventas.procesado = -1 THEN 'Pago rechazado' END) as estado_pago,
ventas.forma_pago,
ventas.pagado
FROM `ventas` 
INNER JOIN user ON user.id = ventas.user_id 
INNER JOIN paises ON paises.id = ventas.paises_id 
LEFT JOIN provincias ON provincias.id = ventas.provincias_id 
WHERE 1;

DROP VIEW IF EXISTS view_tiendas;
CREATE VIEW view_tiendas AS SELECT 
user.id,
user.nombre,
user.apellido_paterno,
user.email
FROM user_group 
INNER JOIN user on user.id = user_group.user
where grupo = 5


DROP VIEW IF EXISTS view_morosos;
CREATE VIEW view_morosos AS SELECT 
user.email,
CONCAT(user.nombre,' ',user.apellido_paterno) as cliente,
COUNT(ventas.id) AS deudas,
SUM(ventas.total) AS total_deuda,
GROUP_CONCAT(CONCAT(albums.referencia,'-',albums.titulo) SEPARATOR '| ') as productos,
DATE_FORMAT(DATE_ADD(ventas.fecha_compra,INTERVAL 90 DAY),'%d/%m/%Y') AS vencimiento
FROM ventas_detalles
INNER JOIN ventas ON ventas.id = ventas_detalles.ventas_id
INNER JOIN user ON user.id = ventas.user_id
INNER JOIN albums ON albums.id = ventas_detalles.productos_id
WHERE ventas.pagado = 0 AND ventas.pasarela = 4
GROUP BY ventas.user_id

DROP VIEW IF EXISTS view_ventas_cliente;
CREATE VIEW view_ventas_cliente AS SELECT 
ventas.id,
IF(digitales.nombre IS NULL,albums.titulo,CONCAT(digitales.nombre,' - ',albums.titulo)) as productos,
ventas_detalles.precio,
ventas_detalles.cantidad,
ventas.fecha_compra,
ventas.procesado,
ventas.user_id
FROM ventas_detalles
INNER JOIN ventas ON ventas.id = ventas_id
INNER JOIN albums ON albums.id = ventas_detalles.productos_id
LEFT JOIN digitales ON digitales.id = ventas_detalles.digitales_id