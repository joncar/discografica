[menu]
<div id="main-content" class="favoritos">
	<header class="page-header have-disc-icon text-center">
		<div class="disc-bg"></div>
		<div class="mask"></div>
		<div class="typo-bg"></div>
		<div class="page-header-bg-1 page-header-bg"></div>
		<div class="page-header-inner">
			<div class="wrapper">
				
				<h1 class="page-title">Tus favoritos</h1>
				<div class="breadcrumb clearfix">
					<span itemtype="http://data-vocabulary.org/Breadcrumb" itemscope="">
						<a href="index.html" itemprop="url">
							<span itemprop="title">Inicio</span>
						</a>
					</span>
					<span>&nbsp;/&nbsp;</span>
					<span itemtype="http://data-vocabulary.org/Breadcrumb" itemscope="" class="current-page"><span itemprop="title">Favoritos</span></span>
				</div>
				<!-- breadcrumb -->
			</div>
			<!-- wrapper -->
			
		</div>
		<!-- page-header-inner -->
		
		<div class="album-icon">
			<div class="icon-inner-1">
				<span class="icon-inner-2"></span>
			</div>
			<span class="fa fa-music"></span>
		</div>
		
	</header>
	<!-- page-header -->
	<!-- page-header -->
	<div class="wrapper clearfix">
		<section class="elements-box mb-40">
			<?= $output ?>
		</section>
	</div>

[footer]

</div>
<!-- kopa-page-footer -->