<div class="form-group">
	<label for="">Seleccione un album</label>
	<select name="" id="" class="form-control albumlist">
		<option value="">Todos</option>
		<?php 
			$this->db->group_by('albums_id');
			foreach($this->db->get_where('view_digitales',array('user_id'=>$this->user->id))->result() as $t):
		?>
			<option value="<?= $t->albums_id ?>" <?php if(!empty($albums_id) && $albums_id==$t->albums_id){echo 'selected';} ?>><?= $t->titulo ?></option>
		<?php endforeach ?>
	</select>
</div>
<?php
	echo $output
?>

<script>
	$(document).on('change','.albumlist',function(){
		document.location.href = "<?= base_url() ?>t/admin/digitales/"+$(this).val()
	});
</script>