<body class="woocommerce woocommerce-page kopa-subpage">
[menu]
<div id="main-content">

        <header class="page-header have-disc-icon text-center">

        	<div class="disc-bg"></div>

        	<div class="mask"></div>

        	<div class="page-header-bg-2 page-header-bg"></div>

        	<div class="page-header-inner page-header-inner-1">

        		<div class="wrapper">
                
	                <h1 class="page-title"><?= l('carrito-de-compras') ?></h1>

	                <div class="breadcrumb clearfix">                    
	                    <span itemtype="http://data-vocabulary.org/Breadcrumb" itemscope="">
	                        <a href="<?= base_url() ?>" itemprop="url">
	                            <span itemprop="title"><?= l('inicio') ?></span>
	                        </a>
	                    </span>
	                    <span>&nbsp;/&nbsp;</span>
	                    <span itemtype="http://data-vocabulary.org/Breadcrumb" itemscope="">
	                        <a href="<?= base_url() ?>" itemprop="url">
	                            <span itemprop="title"><?= l('carrito-de-compras') ?></span>
	                        </a>
	                    </span>	                    
	                </div>
	                <!-- breadcrumb -->

	            </div>
	            <!-- wrapper -->
        		
        	</div>
        	<!-- page-header-inner -->

        	
        	<div class="album-icon">
                <div class="icon-inner-1">
                    <span class="icon-inner-2"></span>
                </div>
                <span class="fa fa-music"></span>
            </div>            
            
        </header>
        <!-- page-header -->

        <div class="wrapper clearfix cartdetail">
        	<?php $this->load->view($this->theme.'_carrito_content'); ?>			
        </div>
        <!-- wrapper -->
        
    </div>
    <!-- main-content -->
[footer]

<script>
    var datos = {};  
    $(document).on('change','#paises_id',function(){
        if($(this).val()!=='1'){
            var tarifa = parseFloat($(this).find('option:selected').data('tarifa'));
            console.log($(this).find('option:selected').data('tarifa'));
            console.log(parseFloat($(this).find('option:selected').data('tarifa')));
            //var tot = total+tarifa;
            var tot = total;
            var continente = parseFloat($(this).find('option:selected').data('continente'));
            
            //Si no tiene tarifa de pais cargado, se cobra por continente
            console.log(tarifa);
            if(tarifa<=0){
                //Sacamos las tarifas regionales
                var tarifa = [];
                for(var i in tarifas){
                    if(tarifas[i].continentes_id == continente){
                        tarifa.push(tarifas[i]);
                    }
                }
                //Calculamos la tarifa del pedido            
                var envio = 0;
                for(var i in tarifa){                
                    if(peso>=parseFloat(tarifa[i].peso_desde)){
                        envio = parseFloat(tarifa[i].precio);                    
                    }
                }
            }else{
                //Se cobra por pais
                console.log(tarifa+'*'+Math.ceil(peso));
                envio = tarifa*Math.ceil(peso);
            }
            tot+= envio;
            /*console.log(envio);
            console.log(peso);            
            console.log(continente);*/

            if($(this).val()=='49'){ //Australia
                recargo = tot*0.10;
                tot+= recargo;
                recargo = recargo.toFixed(2);
                $(".taxaustralia").show();
                recargo = recargo.toString().replace('.',',');
                $("#preciotaxaustralia").html(recargo+'€');
                $("#australiaMessage").show();
            }else{
                $(".taxaustralia").hide();
                recargo = 0;
                $("#preciotaxaustralia").html(recargo+'€');
                $("#australiaMessage").hide();
            }

            tot = tot.toFixed(2);
            envio = envio.toFixed(2).replace('.',',');
            tot = tot.toString().replace('.',',');
            $("#precioEnvio").html(envio+'€');        
            $("#total").html(tot+'€');
            $("#provincias_id").val('');
            $("#provForm").hide();
            $("#paymentcr").hide();
            $("#paymenttpv").prop('checked',true);
            
        }else{
            $("#provincias_id").trigger('change');
            $("#provForm").show();
            $("#paymentcr").show();
            
        }
    });  
    //Australia 49
    var descuento = <?= $this->elements->is_tienda()?'0.50':0 ?>;
    $(document).on('change','#provincias_id',function(){
        var tarifa = parseFloat($(this).find('option:selected').data('tarifa'));
        //tarifa = peso_tienda>0?tarifa*peso_tienda:tarifa;
        if(isNaN(tarifa)){
            tarifa = 0;
        }
        tarifan = descuento>0?tarifa*descuento:0;
        if(isNaN(tot)){
            tot = total;
        }
        var tot = total+(tarifa-tarifan);
        
        tot = tot.toFixed(2).replace('.',',');
        if(descuento>0 && tarifa>0){
            tarifan = tarifan.toFixed(2).replace('.',',')+'€';
            tarifa = tarifa.toFixed(2).replace('.',',')+'€';
            tarifa = '<span style="color: #ff000026;">'+tarifa+'</span> '+tarifan;
        }
        else{
            tarifa = tarifa.toFixed(2).replace('.',',')+'€';
        }
        $("#precioEnvio").html(tarifa);        
        $("#total").html(tot+'€');        
    });
    $(document).on('change',".table-cart .qty",function(){
        $.post('<?= base_url() ?>tt/addToCart/'+$(this).data('id')+'/'+$(this).val()+'/0/0',{},function(data){
            datos.paises_id = $("#paises_id").val();
            datos.provincias_id = $("#provincias_id").val();
            datos.ciudad = $("#ciudad").val();
            datos.direccion = $("#direccion").val();
            datos.codigo_postal = $("#codigo_postal").val();
            datos.telefono = $("#telefono").val();
            $.post('<?= base_url() ?>tt/getForm',{},function(data){
                $(".cartdetail").html(data);
                $("#paises_id").val(datos.paises_id);
                $("#provincias_id").val(datos.provincias_id);
                $("#ciudad").val(datos.ciudad);
                $("#direccion").val(datos.direccion);
                $("#codigo_postal").val(datos.codigo_postal);
                $("#telefono").val(datos.telefono);
                if(datos.paises_id!=='1'){
                    $("#paises_id").trigger('change');
                }else{
                    $("#provincias_id").trigger('change');
                }
            });
        });        
    });

    $(document).on('keyup','#codigo_postal',function(){
        var cp = $(this).val();
        cp = cp.toUpperCase(cp);
        $(this).val(cp);
    });
</script>