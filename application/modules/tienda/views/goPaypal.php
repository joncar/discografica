<form id="form" action="https://www.paypal.com/cgi-bin/webscr" method="post">
  <input type="hidden" name="business" value="eventos@discotecarecords.com">
  <input type="hidden" name="cmd" value="_xclick">
  <input type="hidden" name="item_name" value="<?= $detalle->productos ?>">
  <input type="hidden" name="item_number" value="<?= $detalle->ventas_id ?>">
  <input type="hidden" name="amount" value="<?= str_replace(',','.',$total) ?>">
  <input type="hidden" name="currency_code" value="EUR">
  <input type="hidden" name="notify_url" value="<?= base_url('tt/procesarPagoPaypal') ?>">
  <input type="hidden" name="shopping_url" value="<?= base_url() ?>">

</form>
<script>
	document.getElementById('form').submit();
</script>