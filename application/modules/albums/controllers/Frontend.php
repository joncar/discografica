<?php 
    require_once APPPATH.'controllers/Panel.php';    
    class Frontend extends Main{
        function __construct() {
            parent::__construct();
        }
        
        public function read($id){
			if(!empty($id)){
                $albums = $this->elements->albums(array('identificador_url'=>$id));
                if($albums->num_rows()>0){
                	$albums = $albums->row();
	                $this->loadView(
	                    array(
	                        'view'=>'detail',
	                        'd'=>$albums,
	                        'title'=>$albums->titulo,                        
	                    ));
            	}else{
	                throw new Exception('No se encuentra la entrada solicitada',404);
	            }
            }else{
                throw new Exception('No se encuentra la entrada solicitada',404);
            }
        }

        function favoritos(){
            $this->load->library('grocery_crud')
                       ->library('ajax_grocery_crud');
            $crud = new ajax_grocery_crud();
            $crud->set_table('favoritos')
                 ->set_subject('favoritos')
                 ->set_theme('favorito_crud')
                 ->unset_add()
                 ->unset_edit()
                 ->unset_read()
                 ->unset_export()
                 ->unset_print();
            $crud->set_url('albums/frontend/favoritos/');
            $crud = $crud->render('','application/modules/albums/views/');                        
            $crud->view = 'favoritos';
            $this->loadView($crud);
        }

        function radios($read = ''){            
            if($this->user->getAccess('funciones.*',array('funciones.nombre'=>'audio_descarga'))->num_rows()>0){
                if(empty($read)){
                    $this->loadView(
                        array(
                        'view'=>'radios',
                        'title'=>'Promos radio',
                    ));
                }else{
                    $albums = $this->elements->albums(array('promos_radio'=>1,'identificador_url'=>$read));
                    if($albums->num_rows()>0){
                        $albums = $albums->row();
                        $albums->radio = 1;
                        $this->loadView(
                            array(
                                'view'=>'detail',
                                'd'=>$albums,
                                'title'=>$albums->titulo,                        
                            ));
                    }else{
                        throw new Exception('No se encuentra la entrada solicitada',404);
                    }
                }
            }else{
                redirect('cuenta');
            }
        }

        function listar($id){
            if(is_numeric($id)){
                $tipo = $this->db->get_where('tipos_album',array('id'=>$id));
                if($tipo->num_rows()>0){
                    $this->loadView(array('view'=>'listar','tipo'=>$id,'title'=>$tipo->row()->nombre));
                }else{
                    throw new Exception('Página no encontrada',404);
                }
            }
        }

        function shops($read = ''){            
            if($this->user->getAccess('funciones.*',array('funciones.nombre'=>'tiendas'))->num_rows()>0){
                if(empty($read)){
                    $this->loadView(
                        array(
                        'view'=>'shops',
                        'title'=>'Apartado especial para tiendas',
                    ));
                }else{
                    $albums = $this->elements->promos_tiendas(array('identificador_url'=>$read));
                    if($albums->num_rows()>0){
                        $albums = $albums->row();
                        $albums->tienda = 1;
                        $this->loadView(
                            array(
                                'view'=>'detail',
                                'd'=>$albums,
                                'title'=>$albums->titulo,                        
                            ));
                    }else{
                        throw new Exception('No se encuentra la entrada solicitada',404);
                    }
                }
            }else{
                redirect('cuenta');
            }
        }

        function audio(){
            if(!empty($_GET['v'])){
                $key = base64_decode($_GET['v']);
                $key = explode('-',$key);
                if(count($key)==2 && is_numeric($key[0]) && is_numeric($key[1])){
                    $user = $key[0];
                    $venta = $key[1];
                    $this->db->select('ventas.*');
                    $this->db->join('ventas_detalles','ventas_detalles.ventas_id = ventas.id');
                    $this->db->join('albums','ventas_detalles.productos_id = albums.id');
                    $this->db->join('user','user.id = ventas.user_id');
                    $venta = $this->db->get_where('ventas',array('ventas.procesado >='=>2,'user.status'=>1,'user_id'=>$user,'ventas.id'=>$venta,'albums.tipo_producto'=>3));
                    if($venta->num_rows()>0){
                        $this->user->login_short($user);
                        redirect('t/admin/digitales/121');
                    }else{
                        throw new Exception('No posee permisos suficientes para realizar esta acción',403);    
                    }
                }else{
                    throw new Exception('No posee permisos suficientes para realizar esta acción',403);
                }
            }
        }
    }
?>
