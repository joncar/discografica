-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 31-05-2019 a las 11:54:05
-- Versión del servidor: 5.5.60-MariaDB
-- Versión de PHP: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `hipo_discoteca`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `_paises`
--

CREATE TABLE `_paises` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `tarifa` decimal(11,2) DEFAULT '0.00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `_paises`
--

INSERT INTO `_paises` (`id`, `nombre`, `tarifa`) VALUES
(1, 'España', '0.00'),
(2, 'Portugal', '9.50'),
(3, 'Francia', '9.50'),
(4, 'Alemania', '10.00'),
(5, 'Italia', '12.00'),
(6, 'Reino Unido', '10.50'),
(7, 'Irlanda', '17.00'),
(8, 'Holanda', '10.25'),
(9, 'Bélgica', '10.25'),
(10, 'Polonia', '12.00'),
(11, 'Suiza', '12.00'),
(13, 'Austria', '10.25'),
(14, 'Eslovenia', '12.00'),
(15, 'Croacia', '15.00'),
(16, 'Bosnia e Herzegovina', '15.00'),
(17, 'Montenegro', '17.00'),
(18, 'Albania', '15.00'),
(19, 'Grécia', '15.00'),
(20, 'Turquia', '20.00'),
(21, 'Chipre', '20.00'),
(22, 'Bulgária', '15.00'),
(23, 'Kosovo', '15.00'),
(24, 'Macedonia', '18.00'),
(25, 'Servia', '17.00'),
(26, 'Rumania', '15.00'),
(27, 'Hungria', '15.00'),
(28, 'Eslováquia', '17.00'),
(30, 'Lituania', '15.00'),
(31, 'Letónia', '15.00'),
(32, 'Estonia', '15.00'),
(33, 'Finlandia', '15.00'),
(34, 'Suecia', '15.00'),
(35, 'Noruega', '15.00'),
(36, 'Dinamarca', '15.00'),
(37, 'Islandia', '15.00'),
(38, 'Argentina', '25.00'),
(39, 'Mexico', '25.00'),
(40, 'Brasil', '26.00'),
(41, 'Andorra', '10.00'),
(42, 'Japón', '18.00'),
(43, 'Rusia', '25.00'),
(44, 'Canada', '25.00'),
(45, 'Estados Unidos', '25.00'),
(46, 'Israel', '17.00'),
(47, 'República Checa', '10.25'),
(48, 'Chile', '25.00'),
(49, 'Australia', '10.00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `_paises2`
--

CREATE TABLE `_paises2` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `continentes_id` int(11) NOT NULL,
  `tarifa` decimal(11,2) DEFAULT '0.00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `_paises2`
--

INSERT INTO `_paises2` (`id`, `nombre`, `continentes_id`, `tarifa`) VALUES
(1, 'ESPAÑA', 1, '0.00'),
(2, 'AUSTRIA', 1, '10.25'),
(3, 'BELGICA', 1, '10.25'),
(4, 'BULGARIA', 1, '15.00'),
(5, 'CHIPRE', 1, '20.00'),
(6, 'DINAMARCA', 1, '15.00'),
(7, 'FINLANDIA', 1, '15.00'),
(8, 'FRANCIA', 1, '9.50'),
(9, 'GRECIA', 1, '15.00'),
(10, 'HUNGRIA', 1, '15.00'),
(11, 'IRLANDA', 1, '17.00'),
(12, 'ITALIA', 1, '12.00'),
(13, 'LUXEMBURGO', 1, '0.00'),
(14, 'MALTA', 1, '0.00'),
(15, 'PAISES BAJOS', 1, '0.00'),
(16, 'POLONIA', 1, '12.00'),
(17, 'PORTUGAL', 1, '9.50'),
(18, 'REINO UNIDO', 1, '10.50'),
(19, 'ALEMANIA', 1, '10.00'),
(20, 'RUMANIA', 1, '15.00'),
(21, 'SUECIA', 1, '15.00'),
(22, 'LETONIA', 1, '15.00'),
(23, 'ESTONIA', 1, '15.00'),
(24, 'LITUANIA', 1, '15.00'),
(25, 'REPUBLICA CHECA', 1, '10.25'),
(26, 'REPUBLICA ESLOVACA', 1, '0.00'),
(27, 'ESLOVENIA', 1, '12.00'),
(29, 'ALBANIA', 1, '15.00'),
(30, 'ISLANDIA', 1, '15.00'),
(31, 'LIECHTENSTEIN', 1, '0.00'),
(32, 'MONACO', 1, '0.00'),
(33, 'NORUEGA', 1, '15.00'),
(34, 'ANDORRA', 1, '10.00'),
(35, 'SAN MARINO', 1, '0.00'),
(36, 'SANTA SEDE', 1, '0.00'),
(37, 'SUIZA', 1, '12.00'),
(38, 'UCRANIA', 1, '0.00'),
(39, 'MOLDAVIA', 1, '0.00'),
(40, 'BELARUS', 1, '0.00'),
(41, 'GEORGIA', 1, '0.00'),
(42, 'BOSNIA Y HERZEGOVINA', 1, '0.00'),
(43, 'CROACIA', 1, '15.00'),
(44, 'ARMENIA', 1, '0.00'),
(45, 'RUSIA', 1, '25.00'),
(46, 'MACEDONIA ', 1, '18.00'),
(47, 'SERBIA', 1, '0.00'),
(48, 'MONTENEGRO', 1, '17.00'),
(49, 'AUSTRALIA', 6, '10.00'),
(50, 'GUERNESEY', 1, '0.00'),
(51, 'SVALBARD Y JAN MAYEN', 1, '0.00'),
(52, 'ISLAS FEROE', 1, '0.00'),
(53, 'ISLA DE MAN', 1, '0.00'),
(54, 'GIBRALTAR', 1, '0.00'),
(55, 'ISLAS DEL CANAL', 1, '0.00'),
(56, 'JERSEY', 1, '0.00'),
(57, 'ISLAS ALAND', 1, '0.00'),
(58, 'TURQUIA', 1, '20.00'),
(59, 'OTROS PAISES O TERRITORIOS DEL RESTO DE EUROPA', 1, '0.00'),
(60, 'BURKINA FASO', 4, '0.00'),
(61, 'ANGOLA', 4, '0.00'),
(62, 'ARGELIA', 4, '0.00'),
(63, 'BENIN', 4, '0.00'),
(64, 'BOTSWANA', 4, '0.00'),
(65, 'BURUNDI', 4, '0.00'),
(66, 'CABO VERDE', 4, '0.00'),
(67, 'CAMERUN', 4, '0.00'),
(68, 'COMORES', 4, '0.00'),
(69, 'CONGO', 4, '0.00'),
(70, 'COSTA DE MARFIL', 4, '0.00'),
(71, 'DJIBOUTI', 4, '0.00'),
(72, 'EGIPTO', 4, '0.00'),
(73, 'ETIOPIA', 4, '0.00'),
(74, 'GABON', 4, '0.00'),
(75, 'GAMBIA', 4, '0.00'),
(76, 'GHANA', 4, '0.00'),
(77, 'GUINEA', 4, '0.00'),
(78, 'GUINEA-BISSAU', 4, '0.00'),
(79, 'GUINEA ECUATORIAL', 4, '0.00'),
(80, 'KENIA', 4, '0.00'),
(81, 'LESOTHO', 4, '0.00'),
(82, 'LIBERIA', 4, '0.00'),
(83, 'LIBIA', 4, '0.00'),
(84, 'MADAGASCAR', 4, '0.00'),
(85, 'MALAWI', 4, '0.00'),
(86, 'MALI', 4, '0.00'),
(87, 'MARRUECOS', 4, '0.00'),
(88, 'MAURICIO', 4, '0.00'),
(89, 'MAURITANIA', 4, '0.00'),
(90, 'MOZAMBIQUE', 4, '0.00'),
(91, 'NAMIBIA', 4, '0.00'),
(92, 'NIGER', 4, '0.00'),
(93, 'NIGERIA', 4, '0.00'),
(94, 'REPUBLICA CENTROAFRICANA', 4, '0.00'),
(95, 'SUDAFRICA', 4, '0.00'),
(96, 'RUANDA', 4, '0.00'),
(97, 'SANTO TOME Y PRINCIPE', 4, '0.00'),
(98, 'SENEGAL', 4, '0.00'),
(99, 'SEYCHELLES', 4, '0.00'),
(100, 'SIERRA LEONA', 4, '0.00'),
(101, 'SOMALIA', 4, '0.00'),
(102, 'SUDAN', 4, '0.00'),
(103, 'SWAZILANDIA', 4, '0.00'),
(104, 'TANZANIA', 4, '0.00'),
(105, 'CHAD', 4, '0.00'),
(106, 'TOGO', 4, '0.00'),
(107, 'TUNEZ', 4, '0.00'),
(108, 'UGANDA', 4, '0.00'),
(109, 'REP.DEMOCRATICA DEL CONGO', 4, '0.00'),
(110, 'ZAMBIA', 4, '0.00'),
(111, 'ZIMBABWE', 4, '0.00'),
(112, 'ERITREA', 4, '0.00'),
(113, 'SANTA HELENA', 4, '0.00'),
(114, 'REUNION', 4, '0.00'),
(115, 'MAYOTTE', 4, '0.00'),
(116, 'SAHARA OCCIDENTAL', 4, '0.00'),
(117, 'OTROS PAISES O TERRITORIOS DE AFRICA', 4, '0.00'),
(118, 'CANADA', 2, '25.00'),
(119, 'ESTADOS UNIDOS DE AMERICA', 2, '0.00'),
(120, 'MEXICO', 2, '25.00'),
(121, 'SAN PEDRO Y MIQUELON ', 2, '0.00'),
(122, 'GROENLANDIA', 2, '0.00'),
(123, 'OTROS PAISES  O TERRITORIOS DE AMERICA DEL NORTE', 2, '0.00'),
(124, 'ANTIGUA Y BARBUDA', 7, '0.00'),
(125, 'BAHAMAS', 7, '0.00'),
(126, 'BARBADOS', 7, '0.00'),
(127, 'BELICE', 7, '0.00'),
(128, 'COSTA RICA', 7, '0.00'),
(129, 'CUBA', 7, '0.00'),
(130, 'DOMINICA', 7, '0.00'),
(131, 'EL SALVADOR', 7, '0.00'),
(132, 'GRANADA', 7, '0.00'),
(133, 'GUATEMALA', 7, '0.00'),
(134, 'HAITI', 7, '0.00'),
(135, 'HONDURAS', 7, '0.00'),
(136, 'JAMAICA', 7, '0.00'),
(137, 'NICARAGUA', 7, '0.00'),
(138, 'PANAMA', 7, '0.00'),
(139, 'SAN VICENTE Y LAS GRANADINAS', 7, '0.00'),
(140, 'REPUBLICA DOMINICANA', 7, '0.00'),
(141, 'TRINIDAD Y TOBAGO', 7, '0.00'),
(142, 'SANTA LUCIA', 7, '0.00'),
(143, 'SAN CRISTOBAL Y NIEVES', 7, '0.00'),
(144, 'ISLAS CAIMÁN', 7, '0.00'),
(145, 'ISLAS TURCAS Y CAICOS', 7, '0.00'),
(146, 'ISLAS VÍRGENES DE LOS ESTADOS UNIDOS', 7, '0.00'),
(147, 'GUADALUPE', 7, '0.00'),
(148, 'ANTILLAS HOLANDESAS', 7, '0.00'),
(149, 'SAN MARTIN (PARTE FRANCESA)', 7, '0.00'),
(150, 'ARUBA', 7, '0.00'),
(151, 'MONTSERRAT', 7, '0.00'),
(152, 'ANGUILLA', 7, '0.00'),
(153, 'SAN BARTOLOME', 7, '0.00'),
(154, 'MARTINICA', 7, '0.00'),
(155, 'PUERTO RICO', 7, '0.00'),
(156, 'BERMUDAS', 7, '0.00'),
(157, 'ISLAS VIRGENES BRITANICAS', 7, '0.00'),
(158, 'OTROS PAISES O TERRITORIOS DEL CARIBE Y AMERICA CENTRAL', 7, '0.00'),
(159, 'ARGENTINA', 5, '25.00'),
(160, 'BOLIVIA', 5, '0.00'),
(161, 'BRASIL', 5, '26.00'),
(162, 'COLOMBIA', 5, '0.00'),
(163, 'CHILE', 5, '25.00'),
(164, 'ECUADOR', 5, '0.00'),
(165, 'GUYANA', 5, '0.00'),
(166, 'PARAGUAY', 5, '0.00'),
(167, 'PERU', 5, '0.00'),
(168, 'SURINAM', 5, '0.00'),
(169, 'URUGUAY', 5, '0.00'),
(170, 'VENEZUELA', 5, '0.00'),
(171, 'GUAYANA FRANCESA', 5, '0.00'),
(172, 'ISLAS MALVINAS', 5, '0.00'),
(173, 'OTROS PAISES O TERRITORIOS  DE SUDAMERICA', 5, '0.00'),
(174, 'AFGANISTAN', 3, '0.00'),
(175, 'ARABIA SAUDI', 3, '0.00'),
(176, 'BAHREIN', 3, '0.00'),
(177, 'BANGLADESH', 3, '0.00'),
(178, 'MYANMAR', 3, '0.00'),
(179, 'CHINA', 3, '0.00'),
(180, 'EMIRATOS ARABES UNIDOS', 3, '0.00'),
(181, 'FILIPINAS', 3, '0.00'),
(182, 'INDIA', 3, '0.00'),
(183, 'INDONESIA', 3, '0.00'),
(184, 'IRAQ', 3, '0.00'),
(185, 'IRAN', 3, '0.00'),
(186, 'ISRAEL', 3, '17.00'),
(187, 'JAPON', 3, '18.00'),
(188, 'JORDANIA', 3, '0.00'),
(189, 'CAMBOYA', 3, '0.00'),
(190, 'KUWAIT', 3, '0.00'),
(191, 'LAOS', 3, '0.00'),
(192, 'LIBANO', 3, '0.00'),
(193, 'MALASIA', 3, '0.00'),
(194, 'MALDIVAS', 3, '0.00'),
(195, 'MONGOLIA', 3, '0.00'),
(196, 'NEPAL', 3, '0.00'),
(197, 'OMAN', 3, '0.00'),
(198, 'PAKISTAN', 3, '0.00'),
(199, 'QATAR', 3, '0.00'),
(200, 'COREA', 3, '0.00'),
(201, 'COREA DEL NORTE ', 3, '0.00'),
(202, 'SINGAPUR', 3, '0.00'),
(203, 'SIRIA', 3, '0.00'),
(204, 'SRI LANKA', 3, '0.00'),
(205, 'TAILANDIA', 3, '0.00'),
(206, 'VIETNAM', 3, '0.00'),
(207, 'BRUNEI', 3, '0.00'),
(208, 'ISLAS MARSHALL', 3, '0.00'),
(209, 'YEMEN', 3, '0.00'),
(210, 'AZERBAIYAN', 3, '0.00'),
(211, 'KAZAJSTAN', 3, '0.00'),
(212, 'KIRGUISTAN', 3, '0.00'),
(213, 'TADYIKISTAN', 3, '0.00'),
(214, 'TURKMENISTAN', 3, '0.00'),
(215, 'UZBEKISTAN', 3, '0.00'),
(216, 'ISLAS MARIANAS DEL NORTE', 3, '0.00'),
(217, 'PALESTINA', 3, '0.00'),
(218, 'HONG KONG', 3, '0.00'),
(219, 'BHUTÁN', 3, '0.00'),
(220, 'GUAM', 3, '0.00'),
(221, 'MACAO', 3, '0.00'),
(222, 'OTROS PAISES O TERRITORIOS DE ASIA', 3, '0.00'),
(223, 'FIJI', 6, '0.00'),
(224, 'NUEVA ZELANDA', 6, '0.00'),
(225, 'PAPUA NUEVA GUINEA', 6, '0.00'),
(226, 'ISLAS SALOMON', 6, '0.00'),
(227, 'SAMOA', 6, '0.00'),
(228, 'TONGA', 6, '0.00'),
(229, 'VANUATU', 6, '0.00'),
(230, 'MICRONESIA', 6, '0.00'),
(231, 'TUVALU', 6, '0.00'),
(232, 'ISLAS COOK', 6, '0.00'),
(233, 'NAURU', 6, '0.00'),
(234, 'PALAOS', 6, '0.00'),
(235, 'TIMOR ORIENTAL', 6, '0.00'),
(236, 'POLINESIA FRANCESA', 6, '0.00'),
(237, 'ISLA NORFOLK', 6, '0.00'),
(238, 'KIRIBATI', 6, '0.00'),
(239, 'NIUE', 6, '0.00'),
(240, 'ISLAS PITCAIRN', 6, '0.00'),
(241, 'TOKELAU', 6, '0.00'),
(242, 'NUEVA CALEDONIA', 6, '0.00'),
(243, 'WALLIS Y FORTUNA', 6, '0.00'),
(244, 'SAMOA AMERICANA', 6, '0.00'),
(245, 'OTROS PAISES O TERRITORIOS DE OCEANIA', 6, '0.00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `_user`
--

CREATE TABLE `_user` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `apellido_paterno` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `telefono` varchar(255) DEFAULT NULL,
  `paises_id` int(11) NOT NULL,
  `provincias_id` int(11) DEFAULT NULL,
  `ciudad` varchar(255) DEFAULT NULL,
  `direccion` varchar(255) DEFAULT NULL,
  `codigo_postal` varchar(255) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `status` smallint(6) NOT NULL,
  `foto` varchar(255) DEFAULT NULL,
  `fecha_registro` timestamp NULL DEFAULT NULL,
  `admin` int(11) NOT NULL DEFAULT '0',
  `emisora` varchar(255) DEFAULT NULL,
  `tienda` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `_user`
--

INSERT INTO `_user` (`id`, `nombre`, `apellido_paterno`, `email`, `telefono`, `paises_id`, `provincias_id`, `ciudad`, `direccion`, `codigo_postal`, `password`, `status`, `foto`, `fecha_registro`, `admin`, `emisora`, `tienda`) VALUES
(1, 'Root', 'Admin', 'root@discoteca.com', '634028312', 1, 2, 'Igualada', 'Barcelona 30', '08700', '25d55ad283aa400af464c76d713c07ad', 1, '78bda-1.jpg', NULL, 1, NULL, NULL),
(2, 'Jonathan', 'Cardozo', 'joncar.c@gmail.com', '634028312', 1, NULL, 'Igualada', 'Santa ana 24', '08710', '25d55ad283aa400af464c76d713c07ad', 1, NULL, '2018-12-19 13:25:12', 0, NULL, NULL),
(6, 'Jose', 'Barrero', 'josejs03@hotmail.com', '671605013', 1, NULL, 'Madrid', 'C/ Daganzo 2, 2B', '28002', 'c55b4786b3d3991418bf78e6ea55675b', 1, NULL, '2018-12-21 20:47:33', 0, NULL, NULL),
(7, 'DIEGO', 'RECIO', 'J.DIEGO1977@HOTMAIL.COM', '660797872', 1, NULL, 'TOLEDO', 'FUENTENUEVA, 4. ESC.IZQ. PISO 2ºB', '45006', '3b847b93ce13a8cbfe9d1197055af8a8', 1, NULL, '2018-12-21 21:54:13', 0, NULL, NULL),
(8, 'carles antoni', 'Vilaplana', 'carlesv65@gmail.com', '651308032', 1, 2, 'Manresa', 'Calle Mossen Vall, 26 1-1', '08241', '529ed30d9abedc0ce1cda6344b2fdfbb', 1, NULL, '2018-12-21 22:09:16', 0, NULL, NULL),
(10, 'Enrique ', 'Aguilar', 'quique_aguilar@hotmail.es', '687914368', 1, NULL, 'Sant Boi De Llobregat', 'C/ Torres i Bages 17 , 5°-2°', '08830', '3ef36dabcb270a7d12794d216e3cf6e0', 1, NULL, '2018-12-21 22:41:38', 0, NULL, NULL),
(11, 'JUANMA', 'VELASCO', 'juanmamix@gmail.com', '667694036', 1, NULL, 'VALLADOLID', 'Pº PRADO DE LA MAGDALENA, 4-1º B', '47005', '245a1c6b5d3b12f85f7ff2e947669fb2', 1, NULL, '2018-12-22 00:12:03', 0, NULL, NULL),
(12, 'Miguel Ángel', 'Medina Villacreces', 'villamedina77@yahoo.es', '620522331', 1, NULL, 'Tarragona', 'C/ Gran Canaria, 1 1o 1a Urb. La Granja', '43006', '116ea2e800cfa3f487c1c562b2fb61f7', 1, NULL, '2018-12-22 01:08:21', 0, NULL, NULL),
(13, 'David', 'Sevilla campos', 'Todotechno2@gmail.com', '653694213', 1, NULL, 'Palafolls', 'C/ Mas estornell 6 1*2', '08389', 'e8bdae1a430f1da8fa9ca99b6d7b2c32', 1, NULL, '2018-12-22 05:12:54', 0, NULL, NULL),
(14, 'Jose Luis', 'Castillo', 'jlcastillo8169@gmail.com', '606827921', 1, NULL, 'El Vendrell', 'Pasaje del Zinc, 27  Casa', '43700', '8ff2a47a7fa5f4afee2089f257a83810', 1, NULL, '2018-12-22 05:14:01', 0, NULL, NULL),
(15, 'Carles', 'Bou', 'theprophet74@gmail.com', '656617415', 1, NULL, 'Granollers', 'Josep Maria de Sagarra, 93, ático 2', '08402', 'c19e4dfd7496913189f4df437bc8a09a', 1, NULL, '2018-12-22 06:35:48', 0, NULL, NULL),
(16, 'Pablo', 'Paez', 'pablo-paez@hotmail.xom', '665989844', 1, NULL, 'Sant Boí de Llobregat', 'Primer de maig 22 3-2', '08830', '32d081a3c1791ffb4522a02edeca902c', 1, NULL, '2018-12-22 07:55:46', 0, NULL, NULL),
(17, 'Pedro Antonio', 'Parrado', 'paparrado@hotmail.com', '646588795', 1, NULL, 'Cartagena', 'C/la paz número 1, 7° B', '30204', '093b60fd0557804c8ba0cbf1453da22f', 1, NULL, '2018-12-22 08:08:50', 0, NULL, NULL),
(18, 'Iván', 'Fernández ', 'ivanferlopez@gmail.com', '615053079', 1, NULL, 'Aviles', 'c/Santa Teresa, 6 - Bajo dcha', '33403', 'bd4840a3c9c69248baf899c5b906db57', 1, NULL, '2018-12-22 08:12:52', 0, NULL, NULL),
(19, 'Carlos', 'Jaen Rubio', 'Carloscjr10@hotmail.com', '639154816', 1, NULL, 'Zaragoza', 'Cuarta Avenida, 72, 2º B', '50007', '2a606421bce1d9f030169636707a2a7a', 1, '69194-project2.jpg', '2018-12-22 12:04:41', 0, NULL, NULL),
(20, 'Joan', 'Gallego', 'joan@aycaramba.net', '609381723', 1, NULL, 'Barcelona', 'Carrencà, 19, Baixos', '08017', '886e11fbf4dc1bc05753ea1878905ac2', 1, NULL, '2018-12-22 13:01:25', 0, NULL, NULL),
(21, 'jose', 'bisbal aguilar ', 'josevbisbal@gmail.com', '680621788', 1, 11, 'inca . mallorca ', 'pare miquel colom  n.11  2c ', '07300', '80ba5e3275ae826193b9d24932bbe9ef', 1, '2db98-pepe-foto-perfil.jpg', '2018-12-22 14:21:38', 0, NULL, NULL),
(22, 'Noucolors Sergio', 'Escudero', 'serchy74@hotmail.com', '660141017', 1, NULL, 'Castellon', 'Ctra. N-340 - km 970', '12550 Almazora', 'b9e03dd0873cac67e6e5e5f6d07e5940', 1, NULL, '2018-12-22 14:30:38', 0, NULL, NULL),
(23, 'Sebas', 'Querol', 'sebaspoblenoubcn@gmail.com', '664303630', 1, NULL, 'Sant vicen¢ dels horts', 'Madre de deu de Montserrat 18 4°1°', '08620', 'c32874cb24173a334c0b925abca69135', 1, NULL, '2018-12-22 14:50:36', 0, NULL, NULL),
(24, 'Pawel', 'Merlo', 'lopess1971@gmail.com', '+48501764400', 1, NULL, 'Olsztyn', 'ul.Zbicza 17, Poland', '11-041', 'a243b32a795b24384b74f7347a933ac0', 1, NULL, '2018-12-22 14:51:29', 0, NULL, NULL),
(25, 'Test', 'Test', 'test@discoteca.com', '4267158011', 1, NULL, 'LIBERTADOR', 'La Pastora Esquina Portillo Res. Leonardis', '1010', '25d55ad283aa400af464c76d713c07ad', 1, NULL, NULL, 0, 'Rad', 'tiendita.com'),
(26, 'tomas', 'garcia', 'rosarioenero2@gmail.com', '644383682', 1, NULL, 'los palacios(sevilla)', 'san juan de la cruz 12', '41720', '631894a6c2fe6367924f3bc9873ae03a', 1, NULL, '2018-12-22 15:16:18', 0, NULL, NULL),
(27, 'Sergio', 'Ordoñez Narvaez', 'serornar2011@hotmail.es', '661925225', 1, NULL, 'Lugo', 'Avenida Coruña, 224, 2ºC', '27003', '4b28b40243119e03eb0a19fce51f4db7', 1, NULL, '2018-12-22 15:18:19', 0, NULL, NULL),
(28, 'Juan Francisco', 'Garcia', 'mixmania_73@hotmail.com', '609541979', 1, NULL, 'Torrevieja', 'Dr Gregorio Marañon Nº 18 2º F', '03181', 'f256a25b0e8a75b08e1b9918d5cc9e62', 1, NULL, '2018-12-22 15:44:28', 0, NULL, NULL),
(29, 'angelito', 'fernandez', 'casual-music@hotmail.com', '696468057', 1, NULL, 'sabadell', 'ter 12 B', '08202', '045db33eece283866ccfc0a0fe4bd2de', 1, NULL, '2018-12-22 16:14:26', 0, NULL, NULL),
(30, 'Antonio', 'Gómez Casado', 'Antevagr@gmail.com', '651496833', 1, NULL, 'Masquefa', 'Calle Igualada, 10', '08783', '4181c88abe95e5187c5cc3b9353683b6', 1, NULL, '2018-12-22 16:38:37', 0, NULL, NULL),
(31, 'Jesús', 'Calvo Rodrigo', 'chusssrodrigo@hotmail.com', '629709290', 1, NULL, 'Hospitalet de ll.', 'Cr¡arrer Sant Eugeni 20 1 1', '08901', 'fefaa6b832de998cb5df2d5bf23f4219', 1, NULL, '2018-12-22 17:52:16', 0, NULL, NULL),
(32, 'Xavi', 'Mujal', 'xavibbb@gmail.com', '616864820', 1, NULL, 'Cardona', 'Ctra. Miracle, 25  Piso2-Puerta2', '08261', 'e32e7e2b1d8c1d11b5a49da5b27216fc', 1, NULL, '2018-12-22 19:57:17', 0, NULL, NULL),
(33, 'Jesús ', 'Aparicio ', 'hombreapa@gmail.com', '628 40 19 93 ', 1, NULL, 'Madrid ', 'C/Mérida N°18 Piso 3°C', '28903', '582e91d6f47f5da114db3e71f25886d5', 1, NULL, '2018-12-22 20:01:27', 0, NULL, NULL),
(34, 'Rafael ', 'Silveira Perez', 'rsilversipe@gmail.com', '686922067', 1, NULL, 'Vigo', 'Ctra. Vieja de Madrid, 102', '36214', '3b2031869bb958d596f36c6fb760f56a', 1, NULL, '2018-12-22 20:36:01', 0, NULL, NULL),
(35, 'Jorge', 'Izquierdo', 'izquierdofernandezjorge@gmail.com', '619889068', 1, NULL, 'Paradas', 'Pza. Andalucía 1, Bajo Echa.', '41610', '34c06d59ea2a182f1a1fc52a2b828691', 1, NULL, '2018-12-22 21:46:08', 0, NULL, NULL),
(36, 'Juan ', 'Enrich Ortega ', 'juanenor@gmail.com', '654044011', 1, 2, 'Aiguafreda', 'nuria,20', '08591', '0b277999cfc65e7d6733805b04968238', 1, NULL, '2018-12-22 21:51:12', 0, NULL, NULL),
(37, 'José Antonio', 'Jiménez Martínez', 'jajimenez967@gmail.com', '608506743', 1, NULL, 'Palafolls ', 'Av. Ribera de la Burgada, 18, 2-1', '08389', '03d46e62010ecc990a68db71a7e161cd', 1, NULL, '2018-12-22 22:00:41', 0, NULL, NULL),
(38, 'Jose', 'Aparicio', 'Helpmy@gmx.com', '629930590', 1, NULL, 'Barcelona', 'Apartado de correos N 25137', '08080', 'd380fe0a3b41754506d83766709ea63d', 1, NULL, '2018-12-22 22:07:01', 0, NULL, NULL),
(39, 'Francisco', 'Larripa Romeo', 'francislarri@yahoo.es', '625283827', 1, 60, 'Zaragoza', 'C/Doña Blanca de Navarra nº2 piso 5ºB', '50010', '463843f093bf389abc3a5c6f9c1f74b8', 1, NULL, '2018-12-22 23:45:37', 0, NULL, NULL),
(40, 'Jose', 'Reche', 'josereche83@gmail.com', '646853351', 1, NULL, 'Bilbao', 'Logroño, 6', '48014', '89b4968590f2c9b8cd9fdf3ed7807594', 1, NULL, '2018-12-23 08:49:55', 0, NULL, NULL),
(41, 'Manuel', 'Portillo Blanquero', 'multimegamixer@outlook.com', '615258887', 1, NULL, 'Sevilla', 'Calle Aviación, 14, Edificio Morera y Vallejo 2 - INDRA', '41007', 'cb92ab192f664f8cb66ce439e515d24a', 1, NULL, '2018-12-23 11:48:52', 0, NULL, NULL),
(42, 'Carmen', 'Portillo ', 'capobla@gmail.com', '655261764', 1, NULL, 'Sevilla', 'Plaza de los luceros 3-15', '41019', 'fefd6f835615f5d4fe0e37ab0087b61d', 1, NULL, '2018-12-23 12:04:06', 0, NULL, NULL),
(43, 'Miquel', 'Cellalbo', 'mcellalbo@gmail.com', '669063016', 1, NULL, 'L’Hospitalet', 'Av Carmen Amaya, 18, 7é 3a', '08902', '8826c7d7c9f428abab478084b8b82cb9', 1, NULL, '2018-12-23 14:13:58', 0, NULL, NULL),
(44, 'Jose Manuel', 'Alcaraz', 'jmalcarana@gmail.com', '661090914', 1, NULL, 'Algeciras', 'Avd. Oceanía Urbanización Mirasierra N 12A', '11204', '00bfc8c729f5d4d529a412b12c58ddd2', 1, NULL, '2018-12-23 14:16:42', 0, NULL, NULL),
(45, 'Luis', 'Iglesias', 'luigiz1974@gmail.com', '644420931', 1, NULL, 'Madrid', 'Calle de la Encomienda de Palacios, 264, 8B, 264', '28030', '33f60fee3dce2ce555b27d07c1de1d9a', 1, NULL, '2018-12-23 19:02:42', 0, NULL, NULL),
(46, 'Enrique', 'Martos', 'enrix_19@yahoo.es', '00376333707', 1, NULL, 'Sant Julià De Lòria ', 'Urb pleta del jaile EDF Xoriguer 3piso 3puerta ', 'AD600', '1d2f9e1b2e5559cc686d953fbb149bcb', 1, NULL, '2018-12-23 20:36:12', 0, NULL, NULL),
(47, 'Lorenzo ', 'Navarro ', 'lorenzonavarropujalte@gmail.com', '633461764', 1, NULL, 'Monforte del Cid', 'Avda. Alicante, 74', '03670', '13c2110f8b361941068e824bf1ba9321', 1, NULL, '2018-12-23 20:45:39', 0, NULL, NULL),
(48, 'Victor', 'Gomez', 'victorlluch2005@gmail.com', '680210931', 1, NULL, 'Segovia', 'Cl. Rancho N12 1D', '40005', '63af891b3e0fbe8689a3564c4b0077ce', 1, NULL, '2018-12-24 07:12:40', 0, NULL, NULL),
(49, 'pedro', 'morales', 'direccion@hyserca.com', '651904003', 1, NULL, 'sant quirze del valles', 'duran i reynals,9', '08192', '35ee11108fe14b7edf67935f1448a0b3', 1, NULL, '2018-12-24 08:21:08', 0, NULL, NULL),
(50, 'Departamento', 'Ventas', 'sales@discotecarecords.com', NULL, 1, NULL, NULL, NULL, NULL, 'f5bd3aca0b6130ca5ae0058f477e4229', 1, '321ab-1.jpg', NULL, 1, NULL, NULL),
(51, 'Paco', 'Butrón', 'pacobutron@hotmail.com', '637307698', 1, 3, 'Caldas de Malavella', 'C/Ribera del Ebro 6, buzón 661 (Urb. Aigues Bones)', '17455', 'eb1d1ede2961034702d50f6e558b7d31', 1, NULL, '2018-12-28 14:58:38', 0, NULL, NULL),
(52, 'Luis', 'Cervantes', 'homter@hotmail.com', '604253540', 1, 10, 'Cartagena', 'Pintor Portela 30, 3B', '30203', 'd826382a6a00e7038d8f4becd8f70198', 1, NULL, '2018-12-28 19:29:18', 0, NULL, NULL),
(53, 'Joan', 'Noguera', 'nemesixx@hotmail.com', '658193175', 1, 56, 'Toledo', 'Avda. Rio Ventalomar 28, 1-D', '45007', '420cc4dbc092e5c034f3698e3de1e6e9', 1, NULL, '2018-12-28 23:06:50', 0, NULL, NULL),
(54, 'Alberto', 'Beltrán Suárez', 'elnere@yahoo.es', '666242463', 1, 29, 'Granada', 'c/ Platón 1, 1ºB DCHA', '18014', '584f38a36782da3f361aa28b44187cea', 1, NULL, '2018-12-29 15:25:33', 0, NULL, NULL),
(55, 'Emilio', 'López', 'emaxmix@yahoo.es', '676103718', 1, 6, 'Madrid', 'Omega 24, 3A', '28032', '8537b016cf4550fcf96f2c5129827880', 1, NULL, '2018-12-29 23:05:51', 0, NULL, NULL),
(56, 'Manuel', 'López Caravaca ', 'lopezcaravacamanuel@gmail.com', '666783936', 1, 7, 'Gandia', 'C/Benicanena 67, tercero, puerta 31', '46702', '932ec9b89ff29cc039a5d239ccc0fbb8', 1, NULL, '2018-12-30 12:06:01', 0, NULL, NULL),
(57, 'Santi ', 'Almendros ', 'santialmendros2@gmail.com', '669216087', 1, 3, 'Figueres', 'Calle Oliva, Número 63, 2B', '17600', 'dd53ef0614742fd22ae2d95ea6e7afbf', 1, NULL, '2018-12-30 14:03:00', 0, NULL, NULL),
(58, 'Angela', 'Balsera ', 'Angelabd10@gmail.com', '661367421', 1, 2, 'Lliça d\'amunt', 'Carrera de la Safor 8', '08186', '837cd832fe90861e5ca0d77fdd9eda9b', 1, NULL, '2018-12-30 14:29:51', 0, NULL, NULL),
(59, 'José Manuel', 'Navarro', 'jm@n-informatica.com', '652084765', 1, 9, 'Xilxes', 'c/Gravina, 35 Pbj B', '12592', '810838bcfb6c8b5faef2e6a357611b87', 1, NULL, '2018-12-30 15:53:27', 0, NULL, NULL),
(60, 'PEDRO', 'EXTREMERA TORRES', 'PEXTREMERAT@GMAIL.COM', '607589889', 1, 36, 'JAEN', 'CALLE CASTILLA LA MANCHA, 1. 5ºA', '23009', '2fb910be0765621b99aa68f64187a7a3', 1, NULL, '2018-12-30 17:58:07', 0, NULL, NULL),
(61, 'NIFOPLXJ Jose Luis', 'Bernier', 'jbernier@ugr.es', '659320810', 1, 29, 'Granada', 'AV. DE PULIANAS 66 ,Local CORREOS', '18011', 'ad8af9a53243019748f15ddd551166db', 1, NULL, '2018-12-31 00:48:41', 0, NULL, NULL),
(62, 'Guillermo', 'Caballero', 'guille.10.bcn90@gmail.com', '628509978', 1, 2, 'L\'Hospitalet de Llobregat', 'Calle Justa Goicoechea Nº 1-3 Escalera A 4º1ª', '08902', 'c84a2b73babcc9779c2132750aa9183e', 1, NULL, '2018-12-31 19:44:43', 0, NULL, NULL),
(63, 'DANIEL', 'CASANOVA', 'dcasanova1971@gmail.com', '616639695', 1, 60, 'Zaragoza', 'Mariano Baselga 10-12, 2ª-g', '50015', '0e94f4afd629121fc07bd3d7cdb13be0', 1, NULL, '2019-01-01 20:36:37', 0, NULL, NULL),
(64, 'José María', 'González Codert', 'Josegonzco@gmail.com', '639619322', 1, 20, 'Algeciras', 'Plaza del Querer,Portal 4,Piso 8D(Las Colinas)', '11204', '5317463b991ef75cd5f759509ecfe7bb', 1, NULL, '2019-01-02 09:23:22', 0, NULL, NULL),
(65, 'Francisco Jose', 'Pareja Baez', 'franyo2@hotmail.com', '685462053', 1, 52, 'Sevilla', 'Avda Kansas City 28, piso 127', '41007', '62ef32b11a32d4172b8458aacb917970', 1, NULL, '2019-01-02 10:44:10', 0, NULL, NULL),
(66, 'Antonio', 'Gonzalez', 'agonzalezcoleto@gmail.com', '685890532', 1, 2, 'Barcelona', 'Premià de Mar', '08330', '23dc46bbaafbeaf688cbe8a69cead5ac', 1, NULL, '2019-01-03 09:39:34', 0, NULL, NULL),
(67, 'j.manuel', 'ruiz', 'smoothless1200@hotmail.com', '606770685', 1, 2, 'terrassa', 'avda. pintor mir  nº 69   1º 2ª', '08227', '3fe79a71b4996c5f5016e3cc5252279c', 1, NULL, '2019-01-03 11:48:23', 0, NULL, NULL),
(69, 'Juan', 'Carballo ', 'Juanon05@hotmail.es', '686303757', 1, 2, 'Masquefa ', 'Urbanización el maset calle maset 6 y 8', '08783', 'd519b86f78562c53d7127e34eba3963d', 1, NULL, '2019-01-05 17:21:04', 0, NULL, NULL),
(70, 'Jose Antonio ', 'Silva', 'djhowarin@Gmail.com', '625687588', 1, 2, 'Sabadell', 'Avda Estrasburgo 33 2o3a', 'O8206', 'e2cecf1ea7b1ccfc78d9d8eca1b032fd', 1, NULL, '2019-01-05 23:06:24', 0, NULL, NULL),
(71, 'juan manuel', 'diaz', 'juanmanueldiaz1970@hotmail.com', '640377712', 1, 20, 'algeciras', 'c/ guadalquivir n.1 2B', '11206', 'f51238cd02c93b89d8fbee5667d077fc', 1, NULL, '2019-01-06 09:57:08', 0, NULL, NULL),
(72, 'test', 'test', 'test@gmail.com', '123', 4, NULL, 'ters', '123', '123', '25d55ad283aa400af464c76d713c07ad', 1, NULL, '2019-01-07 17:06:09', 0, NULL, NULL),
(73, 'Iván', 'Santana', 'ivansantanasosa@hotmail.com', '638431760', 1, 28, 'Las Palmas de Gran Canaria ( España )', 'C/ Alfarera María Guerra nº12, La Atalaya de Santa Brigida, Gran Canaria ( España ) Titular: Iván Santana Sosa', '35300', 'b061026449b3281196e41280f91abdb5', 1, NULL, '2019-01-07 22:21:02', 0, NULL, NULL),
(74, 'Daniel', 'Mendiola', 'dmc950@outlook.com', '684254798', 1, 2, 'Barcelona', 'c/llinars del valles 4 4 1', '08030 - Barcelona', 'a072b79aa7b68618aa6edd95a7c315b5', 1, NULL, '2019-01-08 16:10:17', 0, NULL, NULL),
(75, 'Roberto', 'Secaduras', 'djboy_31@hotmail.com', '679758549', 1, 6, 'Mejorada del campo', 'C/ Tajo Nº8, Empresa : Kuehne + Nagel (Pol. Las Acacias)', '28840', '3f01a108d7ed607934a2aced227f1c77', 1, NULL, '2019-01-09 06:33:46', 0, NULL, NULL),
(76, 'Jaro', 'Pitro', 'jaropitro@hotmail.com', '5753106570454', 1, 6, NULL, NULL, NULL, 'acccb6ac79a483dc97ebe209fa54849f', 1, NULL, '2019-01-09 14:27:59', 0, NULL, NULL),
(77, 'Waldemar', 'Kilja?ski ', 'valdi10@op.pl', NULL, 10, NULL, 'Zielona Góra', 'Ul. Zawadzkiego 17/5', '65-530', 'b023a5a6ff52dcf6a0ca312861519be2', 1, NULL, '2019-01-09 19:13:51', 0, NULL, NULL),
(78, 'Jose', 'Gonzalez', 'crydamour@mixmusic.es', '680159765', 1, 2, 'Barcelona', 'Carrer Aragó, 218, Principal', '08011', 'c1bdbf949fa14f06bf457964d73a44ca', 1, NULL, '2019-01-09 19:19:06', 0, NULL, NULL),
(79, 'Dan ', 'Coman', 'twilightzapping@yahoo.com', '+40741033384', 26, NULL, 'GALATI', 'Strada Melodiei, Nr 18, Bloc B4, Apt 6', '800050', '4825cdfdc92e478655d0bdda87c03f95', 1, NULL, '2019-01-10 17:09:00', 0, NULL, NULL),
(80, 'Pruteanu', 'Dragos', 'pruteanudragos@yandex.com', '0040727101505', 26, NULL, 'Brasov', 'Marte 7, Bl.7, Sc.C, Ap.5', '500444', 'fee00391c57720bf971dccc9dcbaf770', 1, NULL, '2019-01-10 20:35:09', 0, NULL, NULL),
(81, 'john', 'charles', 'micuentaes@live.com', '612422234', 1, 2, 'badalona', 'marques monroig n 19', '08917', 'fc3592fb6c7435993af0e7aba9fe4b4f', 1, NULL, '2019-01-11 08:44:37', 0, NULL, NULL),
(82, 'MANUEL146', 'LEIVA', 'djkanon_1972@hotmail.com', '619653901', 1, 45, 'Peralta', 'C/Tafalla,n.15 Bajo', '31350', '486f6856bc66a90b7f14c2f6006078f6', 1, NULL, '2019-01-12 03:11:31', 0, NULL, NULL),
(83, 'Xavier', 'Galvis', 'nicxmusic@gmail.com', '+34661654356', 1, 2, 'Esparreguera', 'Ferran Puig 59 3er', '08292', '8f0fe4d6249632aa46760ccfd16993a6', 1, NULL, '2019-01-14 10:54:34', 0, NULL, NULL),
(84, 'Alberto', 'Costa', 'dj.funny@libelulamusic.com', '679133447', 1, 37, 'Castiñeiras - Ribeira', 'Alto do rial 17', '15966', '85a39b4aca81202c2673cc098131b4cd', 1, NULL, '2019-01-16 08:27:53', 0, NULL, NULL),
(85, '123', '123', '123@123.com', '123', 1, 2, '123', '123', '123', '25d55ad283aa400af464c76d713c07ad', 1, NULL, '2019-01-16 13:57:46', 0, NULL, NULL),
(86, 'Tt', 't', 'T@t.c', 'H', 1, 2, 'Ttt', 'y', 'H', '25d55ad283aa400af464c76d713c07ad', 1, NULL, '2019-01-16 13:59:20', 0, NULL, NULL),
(87, 'David ', 'Algora', 'davismegamix@hotmail.com', '669478657', 1, 52, 'Sevilla', 'C/Afganistán 1, Bloque 1, 4°A', '41020', '2267653c55247e994ffd334f33b18ca5', 1, NULL, '2019-01-19 16:07:52', 0, NULL, NULL),
(88, 'bopyprpr', 'bjhoupotypy', 'scaletric1234567@gmail.com', NULL, 28, 41, 'lugo', NULL, NULL, 'd0e0d62c01f636cbd779c6b4f4d2a845', 1, NULL, '2019-01-19 17:59:12', 0, NULL, NULL),
(89, 'Michael', 'Leese', 'leese201070@gmail.com', NULL, 4, NULL, 'Gelsenkirchen', 'Bülsestrasse', '45896', '488892eebb306fa4fd0c85cf9c19fe45', 1, NULL, '2019-01-24 20:52:05', 0, NULL, NULL),
(90, 'Sergio', 'Moreno', 'mpsergio@hotmail.es', '656409974', 1, 42, 'Malaga ', 'Tomas echeverria 7 3º- 3', '29002', 'c1d9e7fef2cbfcf8c4c19a7e3acc576d', 1, NULL, '2019-01-25 21:39:23', 0, NULL, NULL),
(91, 'Antonio', 'Martos Fernández', 'antoniomartosfernandez.8@gmail.com', '649326738', 1, 2, 'Piera', 'Calle Newton 9', '08784', '417d6780a83db319f693136d894104b1', 1, NULL, '2019-01-26 21:42:11', 0, NULL, NULL),
(92, 'Mariusz', 'Penczynski', 'mpenczynski@gmail.com', '+48505831033', 10, NULL, 'Bydgoszcz', 'Baczynskiego 10/205', '85-822 Bydgoszcz', '77555d1ab8c941770f88aa243739cca6', 1, NULL, '2019-01-26 22:02:28', 0, NULL, NULL),
(93, 'Antonio', 'Muñoz', 'rosayantonio@gmail.com', '619828820', 1, 2, 'Sabadell', 'Ctra.Bacelona, 251 Esc.B 3 1', '08203', '8129232e54ad27a40318e16a688f6b01', 1, NULL, '2019-01-27 19:08:19', 0, NULL, NULL),
(94, 'Jorge', 'Diaz', 'jdiazdj@gmail.com', '656591069', 1, 52, 'Mairena Del Aljarafe', 'C/ Industria, 5, Planta 1, Oficina 9', '41927', '1547449e1f421729a65466bec37348f7', 1, NULL, '2019-01-28 08:54:07', 0, NULL, NULL),
(95, 'roberto', 'flores', 'r_reyero@icloud.com', '687562133', 1, 8, 'finestrat', 'av. costa brava 19, bloq.2 - pta11.', '03509', 'ba8b9c4dfb1a224bd41bf77f4726185f', 1, NULL, '2019-01-30 09:25:43', 0, NULL, NULL),
(96, 'Roman', 'Doles', 'djromzy@gmail.com', '0038651423588', 14, NULL, 'Kocevje', 'Mlaka pri Kocevju 18', 'SI-1332  Stara Cerkev', 'f812ad9f4ba1a5cd34002b013169c35c', 1, NULL, '2019-01-31 21:21:32', 0, NULL, NULL),
(97, 'rocio', 'Auza ', 'rociomiriamsocovos@gmail.com', '648577887', 1, 13, 'Socovos', 'calle Cervantes, 20', '02435', '0c8ad06c3bcf04f0a65cff62002d637b', 1, NULL, '2019-02-02 23:44:32', 0, NULL, NULL),
(98, 'Juan', 'ropero', 'Juanjaldoropero@gmail.com', '636824277', 1, 36, 'Castellar', 'Avenida consolacion Nr 47 piso 2', '23260', 'aee1709dfea9e4a61251bac0e10e8df5', 1, NULL, '2019-02-04 05:59:25', 0, NULL, NULL),
(99, 'Eduardo Luís', 'Anidos Fernández', 'anidosfer@gmail.com', '680507991', 1, 37, 'Narón', 'Av/ Santa Cecilia Nº 55-57 Piso 3º A, 55-57', '15570', '99dc3ec2915a523dd644ab830564c466', 1, NULL, '2019-02-08 06:41:45', 0, NULL, NULL),
(100, 'saxon71', '19710520', 'wasanyi71@gmail.com', NULL, 27, NULL, NULL, NULL, NULL, '805c674f6b392d85fdfc47cfbd3f57f9', 1, NULL, '2019-02-10 13:24:03', 0, NULL, NULL),
(101, 'Carles', 'Martinez', 'carlesmartinez@ono.com', '630750250', 1, 3, 'Salt', 'Travessia de Santa Eugenia, 52, 5º, 2ª', '17190', 'babdf5b0a953b5d2dca7fcc79d87c5c4', 1, NULL, '2019-02-12 11:51:39', 0, NULL, NULL),
(102, 'José M.', 'Moro', 'jmmm2@orangecorreo.es', NULL, 1, 2, 'Barcelona', 'Floridablanca, 99 Tienda (Bcnmoviles)', '08015', 'a04c17e4b94013f1453f4671106e2d0a', 1, NULL, '2019-02-12 22:27:22', 0, NULL, NULL),
(103, 'Fischer', 'Martin', 'djmcfly1@aol.com', '+491633123054', 4, NULL, 'Heusweiler', 'Heuweg 4', '66265', '561499246b6f9cd8361717207d15b91e', 1, NULL, '2019-02-12 22:33:28', 0, NULL, NULL),
(104, 'Lidia', 'Díaz', 'liraju1@hotmail.com', '651687640', 1, 2, 'Sabadell', 'C/Mark Twain, 15', '08206', '1275ee6a2963d80137f1ed86b52ca739', 1, NULL, '2019-02-13 09:01:54', 0, NULL, 'discoteca records'),
(105, 'TEST', 'TEST', 'test@radios.com', 'test', 18, 12, '1', '1', '1', '25d55ad283aa400af464c76d713c07ad', 1, NULL, '2019-02-13 15:21:55', 0, 'Rak U', NULL),
(106, 'TEST', 'TEST', 'test@radios2.com', '123', 18, 12, '123', '123', '123', '25d55ad283aa400af464c76d713c07ad', 1, NULL, '2019-02-13 15:23:47', 0, 'Rak U', NULL),
(107, 'test', 'test', 'radios1@radios.com', '123', 18, 12, '123', '12', '123', '25d55ad283aa400af464c76d713c07ad', 1, NULL, '2019-02-13 15:25:20', 0, 'Rac 1', 'Disrr'),
(108, 'Sandra', 'Gieseler', 'Sandrita_ole@hotmail.com', NULL, 1, 3, 'Blanes', 'Plaza Montserrat Roig ', '17300', 'cb54bdaa6921af847ff61b119bb34ce0', 1, NULL, '2019-02-13 15:26:48', 0, NULL, NULL),
(109, 'Usuario1', 'Usuario1', 'Usuario1@gmail.com', '123', 18, 12, '123', '123', '123', '25d55ad283aa400af464c76d713c07ad', 1, NULL, '2019-02-13 16:10:22', 0, NULL, 'Tiendita'),
(110, 'Piotr', 'Dziuba', 'futrzasty73@interia.pl', NULL, 10, NULL, 'Zernica', 'ul.1go Maja 89', '44-144', '7c368ccfc33d19501fe1418cfad6ef06', 1, NULL, '2019-02-13 20:52:16', 0, NULL, NULL),
(111, 'Pedro', 'Morales', 'direccion@martapad.com', '651904003', 1, NULL, NULL, 'Rambla', '08201', '35ee11108fe14b7edf67935f1448a0b3', 1, NULL, '2019-02-14 04:22:34', 0, 'Mangotero radio', NULL),
(112, 'jose  ', 'montesdeoca', 'linea5020@gmail.com', '699777560', 1, 28, 'las palmas de gran canaria', 'camino viejo del cardon 75 ', '35010', '8e709403a44bb88f1e15385a8e11628f', 1, NULL, '2019-02-14 06:03:30', 0, 'vigilante radio gran canaria', NULL),
(113, 'José Miguel', 'Fuente moreno', 'josefuentesmoreno4@gmail.com', '635350331', 1, 14, 'Almería', 'Calle poeta Luis de Góngora N5  1D', '04006', 'da9b8bb55f24d807a4adaeb8598fb16d', 1, NULL, '2019-02-14 17:55:35', 0, NULL, NULL),
(114, 'Txetxu', 'Argul Varela', 'info@radiotx.es', '659077831', 1, 59, 'sestao', 'Alfredo Marin , 14 - 4ºa', '48910', 'd6021bd245a0aab1f2d390f19cb668ba', 1, NULL, '2019-02-17 09:33:26', 0, 'RADIO TX ( WWW.radiotx.es )', NULL),
(115, 'Johm Anthony ', 'Hil', 'juanajhyoniogil2009@hotmail.com', '629729291', 1, 57, 'Medina del Campo', 'C/ Colonia de Santo Tomás, Nº 6 2º Izquierda  ', '47400', '1f5d02e0f8bc8a9283395ffb9c70c6c0', 1, NULL, '2019-02-21 13:23:04', 0, NULL, NULL),
(116, 'RAFAEL', 'ANDRES', 'rafaandres72@gmail.com', '695208050', 1, 2, 'Sant Quirze del Valles', 'Dolors Monserda i Vidal, 2, 3º-2ª', '08192', '786fe30f58f5f7fb9c9d459a4f0f536d', 1, NULL, '2019-02-21 14:34:40', 0, NULL, NULL),
(117, 'sergio', 'coloma gonzalez', 'sergiocoloma@yahoo.es', '678694862', 1, 2, 'sant sadurní de anoia', 'Avda. Francesc Macià 17-19 3º1ª', '08770', '2384bf36be35e5416fdcd276ed135b1c', 1, NULL, '2019-02-21 22:41:22', 0, NULL, NULL),
(118, 'Timo', 'Sterlike', 'timo.sterlike@web.de', '+49 (0) 170 610 1535 ', 4, NULL, 'Bötzingen', 'Allmendweg 18', 'D-79268', '4c8d72ba762b93bc86dc4c95adf12688', 1, NULL, '2019-02-23 09:49:40', 0, NULL, NULL),
(119, 'FCO. JAVIER', 'LOZANO ROJO', 'javi7339@gmail.com', '649829398', 1, 6, 'MADRID', 'C/ HERMANAS ALONSO BARCELO 9 LOCAL DE CORREOS', '28025', 'a7b246faa71953311e4cda3ee722224b', 1, NULL, '2019-02-23 11:20:36', 0, NULL, NULL),
(120, 'Víctor', 'Gómez LLuch', 'victor.gomez@rtve.es', '680210931', 1, 51, 'Segovia', 'Pº Ezequiel González Nº24, 3ºB y C.', '40002', 'deabff05c97274ee850a262478ad8a50', 1, NULL, '2019-03-01 06:14:40', 0, 'Radio Nacional de España (Segovia)', NULL),
(121, 'Daniel', 'Casanova Garcia', 'musicales@aynradio.com', '616639695', 1, 60, 'Zaragoza', 'Mariano Baselga 10-12, 2-G', '50015', 'c27a3379b2db3f12ac7f3e49f56ca4ee', 1, NULL, '2019-03-01 07:14:48', 0, 'A.Y.N. Radio', NULL),
(122, 'Francid', 'Dumand', 'francis.dumont121@gmail.com', '632084582', 1, 6, 'Madrid', 'Calle san merino', '25789', 'e10a6cfb34addfb3aa073f1ad8d8e925', 1, NULL, '2019-03-06 10:04:50', 0, NULL, NULL),
(123, 'Thomas', 'Hämmerle', 'thomas@thomas-haemmerle.de', NULL, 4, NULL, 'Leipheim', 'Dr.-Rau-Str. 26a', '89340', '6d9cc3171438755ea30b2e804006a265', 1, NULL, '2019-03-09 20:01:11', 0, 'Italian Dance Network', NULL),
(124, 'Lidia', 'Díaz', 'liraju1@hotmail.com', '651687640', 1, 2, 'Sabadell', 'C/Mark Twain, 15', '08206', '1275ee6a2963d80137f1ed86b52ca739', 1, NULL, NULL, 0, NULL, 'lidia'),
(125, 'Víctor', 'Gómez', 'victor.gomez@rtve.es', '680210931', 1, 51, 'Segovia', 'Pº Ezequiel González Nº24, 3ª planta, locales B y C', '40002', 'deabff05c97274ee850a262478ad8a50', 1, NULL, NULL, 0, 'Radio Nacional de España', NULL),
(126, 'Ruben', 'Garcia Collado', 'm08011973@gmail.com', '650631499', 1, 2, 'Barcelona', 'Violant d’Hongria Reina d’Aragó nº100 entlo.2ª', '08028', '7b1f6473674d0a52c1a93243fd972be1', 1, NULL, '2019-03-14 17:36:53', 0, NULL, NULL),
(127, 'israel', 'garcia cabezas', 'neoxter@hotmail.com', NULL, 1, NULL, NULL, NULL, NULL, 'a1cda9b2019d61371da0bba1c9984e0c', 1, NULL, '2019-03-15 10:03:07', 0, NULL, NULL),
(128, 'Luis ', 'Gomez Urbano', 'lgomur@yahoo.es', '625837458', 1, 2, 'Barcelona', 'Mecanica 16 1°2°', '08038', '36ecf8a10cac3de37ea3340150b2d143', 1, NULL, '2019-03-15 23:50:17', 0, NULL, NULL),
(129, 'Cristian', 'Rubio', 'radiomatixmix@gmail.com', '699534730', 1, NULL, 'Reus', 'Closa de Mestres 15 1E', '43204', '2261206b1bec76a1dd62ee318d6dc700', 1, NULL, '2019-03-16 16:59:35', 0, 'MXM RADIO', NULL),
(130, 'Raúl ', 'Góngora ', 'raulr112@hotmail.com', '696953522', 1, 2, 'BARCELONA ', 'Avda. Paral-lel  112-6seg-Iz', '08015', 'b6e25f1baf2a387a2d942945f3f5662b', 1, NULL, '2019-03-18 09:16:39', 0, NULL, NULL),
(131, 'nika303', 'melnikov', 'sasha30398899@gmail.com', NULL, 43, NULL, NULL, NULL, NULL, '81d94e0f27db47ce6042860437dca912', 1, NULL, '2019-03-19 09:30:09', 0, NULL, NULL),
(132, 'hyserca', 'hyserca', 'contabilidad@hyserca.com', '645096328', 1, 2, 'SANT QUIRZE DEL VALLES', 'Cl: Francesc Duran i Reynals, 9', '08192', '69df4c9bf8c21d5326a50ae03db34d6b', 1, NULL, '2019-03-19 11:52:24', 0, NULL, NULL),
(133, 'Carlos', 'Moreno Espada', 'carlos-metal@hotmail.es', '607440702', 1, 31, 'Beasain', 'Errekarte 35 , 2-A', '20200', '5927cf8b1852496969c57af43344552e', 1, NULL, '2019-03-22 13:34:19', 0, NULL, NULL),
(134, 'Pau', 'Dj Rever', 'paureverterfpb@gmail.com', NULL, 1, 2, 'Hospitalet de Llobregat', 'C/ Aprestadora', '08902', 'b613aa208d5611f492c69649b6bfe935', 1, NULL, '2019-03-23 12:42:39', 0, NULL, NULL),
(135, 'Juan Antonio ', 'Gi', 'juanantoniogil2009@hotmail.com', '629729291', 1, 57, 'Medina del Campo', 'c/ Colonia de Santo Tomás, Nº 6', '47400', '1f5d02e0f8bc8a9283395ffb9c70c6c0', 1, NULL, '2019-03-24 14:37:09', 0, NULL, NULL),
(136, 'Rafa', 'Carmona', 'rafacarmona@discotecarecords.com', NULL, 1, 2, NULL, NULL, NULL, '25d55ad283aa400af464c76d713c07ad', 1, NULL, NULL, 0, NULL, NULL),
(137, 'Miguel Ángel', 'Macho Tojo', 'miguelangelnet@hotmail.es', '666007119', 1, 5, 'El Morell', 'C/Pompeu Fabra, 28', '43760', '5ed799876babf6214c1b657293bfc916', 1, NULL, '2019-03-30 16:48:43', 0, NULL, NULL),
(138, 'Miguel Ángel', 'Macho Tojo', 'miguelnet71@gmail.com', '34666007119', 1, 5, 'El Morell', 'Calle de Pompeu Fabra, 28', '43760', '6cf31fa721c92ba4fb22edf670acc2c5', 1, NULL, '2019-03-30 16:58:36', 0, NULL, NULL),
(139, 'JUAN RAMON', 'CAMACHO GARCIA', 'iluminacionysonidolanit@gmail.com', '615253171', 1, 2, 'Moià', 'C/ POMPEU FABRA, 9', '08180', '035e21e27bfe71c8a859ba4bc0f1f346', 1, NULL, '2019-04-01 16:13:10', 0, 'mianouk.', NULL),
(140, 'FRANCISCO M.', 'HIDALGO ABRANTE', 'HANSOLO20371@HOTMAIL.COM', '690285857', 1, 36, 'ANDUJAR', 'LOS VILLARES DE ANDUJAR - C/ VEREDON DEL PINO - N°: 417', '23749', 'd1b2bac0422eb0b6bae519c71360794f', 1, NULL, '2019-04-07 19:09:05', 0, NULL, NULL),
(141, 'jose', 'harcia', 'josegarciagarciamix123@gmail.com', NULL, 1, 6, NULL, 'fermincabakkero21bajo', '28034', '7090c69e5b83e429b767cac828e2de69', 1, NULL, '2019-04-12 09:11:50', 0, 'https://radiostesionmadridalmendralejo.es/?fbclid=IwAR2GPGRUj5Us8gc2K12MS8Zy5yMxW5Btl-Ik8-xm9F5BHG7Gbyefe9-Q-ws', NULL),
(142, 'Ayrat', 'Islamov', 'agislamov@gmail.com', NULL, 43, NULL, 'Izhesvk', 'Pushkinskaya, 365a, 106', '426000', 'cec0cb1ff3a42b2ea34d14c95d6d2075', 1, NULL, '2019-04-13 11:47:27', 0, NULL, NULL),
(143, 'John ', 'Davis', 'johndavis7757@msn.com', NULL, 6, NULL, NULL, NULL, NULL, '4bdd0b9698a9f07f500a9d54d2de892e', 1, NULL, '2019-04-16 15:12:52', 0, NULL, NULL),
(144, 'Christian Thibodeau', 'Mr', 'istenhozott@videotron.ca', '581-986-1613', 44, NULL, 'Quebec city', '2836 Avenue Gaspard', 'G1E3P3', '6bbf18573008dd82d35008552a0c8588', 1, NULL, '2019-04-16 20:35:33', 0, NULL, NULL),
(145, 'Peter', 'Valjebeck', 'hit@radioavesta.se', '+46-736767223', 34, NULL, 'Avesta', 'Arvidsgatan 4D', '77433', '832890a3cb01b9c21576966f9bd416a1', 1, NULL, '2019-04-17 09:57:57', 0, 'Radio Avesta, 103.5 MHz', NULL),
(146, 'Julián ', 'Ramos', 'Julianramos1966@gmail.com', '678263427', 1, 2, 'Badalona', 'Avda. Lloreda, 66-68', '08915', '4f7b49a933d0e8b2a4abe2a3b5a1882f', 1, NULL, '2019-04-17 10:32:06', 0, NULL, NULL),
(147, 'Carlos', 'Cadenas Blanco', 'takeamoment@yahoo.es', NULL, 1, 6, 'Torrejón de la Calzada', NULL, '28991', '86c9e6ac8c7c83d722626635157fa4d9', 1, NULL, '2019-04-17 11:30:46', 0, NULL, NULL),
(148, 'DANIEL', 'RAFAELI', 'ARMINVB@YAHOO.COM', '917-5605417', 45, NULL, 'BROOKLYN, NEW YORK', '3845 SHORE PARKWAY #2D', '11235', '3b2e4528a1b32ee5a5cb52ea27e4b291', 1, NULL, '2019-04-17 15:27:53', 0, NULL, NULL),
(149, 'Kazuyori', 'Kito', 'eurofire@navy.plala.or.jp', '+81 090-2552-9232', 42, NULL, 'Minato-ku,Nagoya-city, Aichi', 'Tatsumi-cho 21-26', '455-0003', 'f6d84afc9b014597de32b841143abd1b', 1, NULL, '2019-04-17 17:14:51', 0, NULL, NULL),
(150, 'Marcus', 'Schmitt Iñiguez', 'mrktdbconnection@gmail.com', '688463005', 1, 5, 'Torredembarra', 'lleida, 7, 4-1, 43830 Torredembarra Tarragona', '43830', '678a7913f8fd7b34c0d83f246f65e43b', 1, NULL, '2019-04-17 17:27:11', 0, NULL, NULL),
(151, 'jose francisco', 'hernandez alonso', 'josemix1971@hotmail.com', '628663607', 1, 50, 'salamanca', 'joselamano benette', '37007', '7e2cfce6765555a90ccc35c35bf0509d', 1, NULL, '2019-04-17 19:48:49', 0, NULL, NULL),
(152, 'Jesus Maria', 'Rey Nuno', 'elo_rey@hotmail.com', '655664479', 1, 52, 'Sevilla', 'Esperanza de Triana, 9, 4ºB', '41010', 'a7555c0066d2889aef1edbd3d8de5333', 1, NULL, '2019-04-17 23:10:15', 0, 'Radio Marisma', NULL),
(153, 'Carlos', 'Rodriguez Gonzalez', 'yildori@gmail.com', '661757909', 1, 6, 'Madrid', 'C/Artajona, 5 4ºA', '28039', '5c0e34fad97c58d34fae96a419d66068', 1, NULL, '2019-04-18 07:02:04', 0, NULL, NULL),
(154, 'Francisco', 'Vicente Martin', 'athletic43@hotmail.com', '659111832', 1, 50, 'Salamanca', 'Plaza Diego Hurtado de Mendoza Portal 4', '37006', '7e3e00fc5d718d0a36bb58f29c023593', 1, NULL, '2019-04-18 13:41:13', 0, NULL, NULL),
(155, 'BOGDAN', 'KAWA', 'BOB69@POCZTA.ONET.PL', '48502654687', 10, NULL, 'OSIEK', 'UL. OGRODOWA 51', '32-608', '5d5725c506e3481218bba43ee78ec8a1', 1, NULL, '2019-04-18 13:50:32', 0, 'Radio Italo4you', NULL),
(156, 'Richard', 'Monsalves', 'elektron3@gmail.com', '992193353', 48, NULL, 'Coronel', 'Calle Wenmapu, pasaje Futa #2179 Villa la posada - Escuadron', '4190000', '663a79358f56faa5376670aa3264357f', 1, NULL, '2019-04-18 23:45:43', 0, 'Richard', 'Richard'),
(157, 'MAICA', 'MESA UREÑA', 'maica500@gmail.com', '671637983', 1, 2, 'VALLGORGUINA', 'CAMPEROL, Nº 3, bajos', '08471', '445decdd028c65995b5105799c4d9d54', 1, NULL, '2019-04-19 09:50:38', 0, NULL, NULL),
(158, 'ERNESTO', 'ARTAL MELIA', 'ernesartal@gmail.com', '+34611493394', 1, 7, 'VALENCIA', 'CALLE YECLA 2 BAJO IZQUIERDA', '46021', 'cea45207e31eb8c2efb2a77373dbe551', 1, NULL, '2019-04-19 20:52:19', 0, NULL, NULL),
(159, 'Luiz', 'Natel', 'luiz.natel@copel.com', '41988875714', 40, NULL, 'Curitiba', 'Parintins 600 ap 201', '80320-270', '31954cd30092904fa70cc42a791f71a2', 1, NULL, '2019-04-19 22:57:30', 0, NULL, NULL),
(160, 'martin', 'austin', 'douglasaustin@mail.com', '01492339969', 6, NULL, 'colwyn bay', 'Arne house, 3 grove park west, colwyn bay, ', 'll297eh', '49be29e824e2e4931806f73e89888e3e', 1, NULL, '2019-04-20 23:40:07', 0, NULL, NULL),
(161, 'Javier', 'Garcia Folch', 'sukirecords@gmail.com', '686891832', 1, 2, 'Mataró', 'c/ Sant Pol 11, bajos', '08303', 'eb028d17dd3a38ef091074db7d2b8a9f', 1, NULL, '2019-04-23 08:05:52', 0, NULL, 'Suki Records'),
(162, 'Thomas', 'Ullrich', 'thomasullrich@yahoo.com', '0049 160 3891164', 4, NULL, 'Doerentrup', 'Industriestr. 5', '32694', '20d4ad797689d8aba51651ade171a1a9', 1, NULL, '2019-04-24 20:45:14', 0, NULL, NULL),
(163, 'CARLOS MANUEL', 'CARAVATTI', 'dr.carlosmcaravatti@hotmail.com', '9546635015', 45, NULL, 'Hollywood', '8320 NW 14 STREET, MIAMI', '33020', '3a9163f2d701d68647a6063768172a3a', 1, NULL, '2019-04-24 22:41:16', 0, NULL, NULL),
(164, 'Alberto', 'Domínguez marin', 'alberto@framevideo.net', '653095712', 1, 2, 'Hospitalet de llobregat', 'Orrius 4 3º 2ª ', '08906', 'd27725091fd30d3bce95b17169a89490', 1, NULL, '2019-04-24 23:33:55', 0, NULL, NULL),
(165, 'Maria', 'Urtasun Biurrun', 'merylery@gmail.com', '600652679', 1, 45, 'Pamplona', 'Rio Alzania 23, 14 B', '31006', 'e31cd7808149954e96bed6b72fccf460', 1, NULL, '2019-04-25 10:15:13', 0, NULL, NULL),
(166, 'Abraham', 'Soto', 'abesoto1708@hotmail.com', '7733963381', 119, NULL, 'Alsip', '5204 W.122nd St., 3B', '60803', '9e36f51a3e9a123d9b28fef218ddcd45', 1, NULL, '2019-04-25 15:10:48', 0, 'Digital 80\'s Radio', NULL),
(167, 'SIDNEI ROHDEN', 'munrra', 'sidnei.rohden@gmail.com', '55 47 34375439', 125, NULL, 'Joinville - Santa Catarina - Brasil', 'Rua Iririú, 3443, Sala 01 ', '89227-021', '52480663bd19920d0f058aa58ab032e8', 1, NULL, '2019-04-25 21:06:08', 0, NULL, NULL),
(168, 'Francisco', 'Gonzalez Sanchez', 'gonzalezsanchezfrancisco@gmail.com', '693302158', 1, 24, 'Cordoba', 'General Lazaro cardenas nº4 bloque2 3ºC (Las perlas 4)', '14013', '9be20aeff13a3be2ce3f56de07f97087', 1, NULL, '2019-04-26 00:03:30', 0, NULL, NULL),
(169, 'Rubén', 'García Collado', 'ruben000722@gmail.com', '650649895', 1, 2, 'Barcelona', 'C/Violant d\' Hongria 100 entlo 2°', '08028', '798185913521490c9a2a328b42c80e6b', 1, NULL, '2019-04-27 07:44:05', 0, NULL, NULL),
(170, 'Jesus', 'Rodriguez', 'jesusjavierpr@hotmail.com', NULL, 155, NULL, 'Cidra', 'Urb Treasure Valley Calle 2 #22', '00739', '10337b74627e3faf732092627696f956', 1, NULL, '2019-04-27 09:45:12', 0, NULL, NULL),
(171, 'antonio', 'lobato', 'lagloboflexia@hotmail.com', '697439605', 1, 20, 'Algeciras', 'calle morenito de algeciras portal 1  3 derecha', '11206', '62ce8c50fd14f9354eb348f17cb60ef1', 1, NULL, '2019-04-27 21:56:35', 0, NULL, NULL),
(172, 'JOSE MARIA', 'LACUEVA NAVARRO', 'masdiscos.com@gmail.com', '615610829', 1, 29, 'CULLAR VEGA', 'C/ LA VEGA, 39', '18195', 'd0c053d2d7a0054bdd8be35bf1a9d4c8', 1, NULL, '2019-04-28 14:33:58', 0, NULL, NULL),
(173, 'Xavi', 'Tobaja', 'xavi.tobaja@topdiscoradio.com', '610227678', 1, 2, 'Santa Perpetua de Mogoda', 'Plaça Nova 14 Entlo 2', '08130', '73ab0090b2f20b9077247b36390b69db', 1, NULL, '2019-04-29 15:17:39', 0, 'Topdisco Radio', NULL),
(174, 'Mariusz', 'Stawowy', 'dancezone@interia.pl', '+48512366469', 10, NULL, 'Brzeszcze', 'Ul.Szymanowskiego 3/1/2', '32-620', '7081573f42fcc308c312f30dbafe9dfb', 1, NULL, '2019-04-29 17:10:01', 0, NULL, NULL),
(175, 'Axel', 'Casas', 'axelcasas@gmail.com', '646329548', 1, 31, 'Donosti-San Sebastian ', 'Plaza Guipuzcoa 7, 5 Derecha', '20004', 'cafbb4a7ef62dc6613cb5fdbbaca4fd3', 1, NULL, '2019-04-29 18:37:34', 0, NULL, NULL),
(176, 'Jordi', 'Raya Saavedra', 'jordirayasaavedra@gmail.com', '934718501', 1, 2, 'Esplugues de Llobregat', 'pl. Oleguer Junyent n5', '08950', '7a02ec9de53aa7a8d81efa7f50078f70', 1, NULL, '2019-04-29 18:56:27', 0, NULL, NULL),
(177, 'Marcos', 'Ortega Jiménez', 'super5gts@gmail.com', '608300987', 1, 2, 'Barcelona', 'C/ Juan Gris 2-8 Torre Sur (VidaCaixa)', '08014', '5365c729eb8ac99ac206d6d99daf1c98', 1, NULL, '2019-04-29 19:09:17', 0, NULL, NULL),
(178, 'ramon', 'garcia', 'tlle75@yahoo.com', '0051996992269', 172, NULL, NULL, NULL, NULL, '6b900a77f3f554499578d92771b11f7f', 1, NULL, '2019-04-29 20:12:40', 0, NULL, NULL),
(179, 'Fabrice', 'Vinet', 'Fabrice120772@hotmail.fr', '635326557', 3, NULL, 'Amplepui', '53 rue saint Paul', '69550', '6368105eb8160cacd70175e94a4c46a8', 1, NULL, '2019-04-29 20:39:01', 0, 'Fun', 'Dcud '),
(180, 'José Joaquín', 'Lara Riscarts', 'jochuandj@hotmail.com', '+34639508532', 1, 20, 'Sanlúcar de Barramedaº', 'Avda. V Centenario 123 2º-B', '11540', '59a7cd44940878fbd6154f4c886bd23d', 1, NULL, '2019-04-29 21:04:48', 0, 'onda joven musical', NULL),
(181, 'Alejandro', 'Pérez Arcas', 'alxprz72@gmail.com', '654056148', 1, 2, 'L\'Hospitalet De Llobregat', 'Carrer Unió,  49   Entresuelo 1°', '08902', '0733f925f27f4001e73f533d15b3cf04', 1, NULL, '2019-04-30 06:57:43', 0, NULL, NULL),
(182, 'Francisco Jose', 'Pareja Baez', 'franyo2@yahoo.es', '685462053', 1, 52, 'Camas', 'C/ Boabdil 4 - Edificio Vega 6, 2ª planta ', '41900', 'ecd3a18bf889be9461bde7c5dea34dd7', 1, NULL, '2019-04-30 09:03:39', 0, NULL, NULL),
(183, 'Juan Carlos', 'Carreras Melero', 'jccarrerasmelero@gmail.com', '642060166', 1, 54, 'Tenerife', 'C/panadero número 14', '38611', 'b96b73e7dfdc85fd02d3423cb6ee25fe', 1, NULL, '2019-04-30 11:19:53', 0, NULL, NULL),
(184, 'Alberto', 'Lario Velasco', 'lariobyte@gmail.com', '609422892', 1, 57, 'Valladolid', 'C/ Peña Vieja, 1 - 2ºB, Valladolid', '47013', '8dd9864f4111ef09213e859185cbf690', 1, NULL, '2019-04-30 11:22:36', 0, NULL, NULL),
(185, 'Roberto', 'gutierrez', 'Cartategui68@hotmail.com', '617397064', 1, 15, 'Oviedo', 'Avenida del mar 43 1 h', '33011', '52c6341f71cd82017d5cda99763174fd', 1, NULL, '2019-04-30 15:23:51', 0, NULL, NULL);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `_paises`
--
ALTER TABLE `_paises`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `_paises2`
--
ALTER TABLE `_paises2`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `_user`
--
ALTER TABLE `_user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `_paises`
--
ALTER TABLE `_paises`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;

--
-- AUTO_INCREMENT de la tabla `_paises2`
--
ALTER TABLE `_paises2`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=246;

--
-- AUTO_INCREMENT de la tabla `_user`
--
ALTER TABLE `_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=186;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
