<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Tienda extends Panel{
        function __construct() {
            parent::__construct();
        } 
        
       	function categorias(){
       		$crud = $this->crud_function('','');
       		$crud->set_field_upload('foto','img/fotos_categorias');
          $crud->set_field_upload('icono','img/fotos_categorias');
       		$crud = $crud->render();
       		$this->loadView($crud);
       	}

        function provincias(){
          $crud = $this->crud_function('','');
          $crud->add_action('Tarifas','',base_url('t/tarifas_envio_provincias').'/');          
          $crud = $crud->render();
          $this->loadView($crud);
        }



        function continentes(){
          $crud = $this->crud_function('','');          
          $crud->add_action('Tarifas','',base_url('t/tarifas_envio').'/');
          $crud = $crud->render();          
          $this->loadView($crud);
        }

        function paises(){
          $crud = $this->crud_function('',''); 
          $crud->field_type('tarifa','hidden')
               ->unset_columns('tarifa')
               ->add_action('Tarifas','',base_url('t/tarifas_envio_europa').'/');
          $crud = $crud->render();
          $this->loadView($crud);
        }

        function tarifas_envio($x = ''){
          $crud = $this->crud_function('','');          
          $crud->where('continentes_id',$x)
               ->field_type('continentes_id','hidden',$x);
          $crud = $crud->render();
          $this->loadView($crud);
        }

        function tarifas_envio_provincias($x = ''){
          $crud = $this->crud_function('','');          
          $crud->where('provincias_id',$x)
               ->field_type('provincias_id','hidden',$x);
          $crud = $crud->render();
          $this->loadView($crud);
        }

        function tarifas_envio_europa($x = ''){
          $crud = $this->crud_function('','');          
          $crud->where('paises_id',$x)
               ->field_type('paises_id','hidden',$x)
               ->unset_columns('paises_id');
          $crud = $crud->render();
          $crud->title = 'Tarifas de envio a paises de europa';
          $this->loadView($crud);
        }

        function tarifas_tiendas($x = '',$y = ''){
          $crud = $this->crud_function('','');                    
          $crud->set_relation_n_n('Regiones','tarifas_tiendas_provincias','provincias','tarifas_tiendas_id','provincias_id','nombre','orden');
          $output = $crud->render();
          if($crud->getParameters()=='edit'){
            $tarifas = $this->tarifas_tiendas_precios($y);
            $output->js_files = array_merge($output->js_files,$tarifas->js_files);
            $output->css_files = array_merge($output->css_files,$tarifas->css_files);
            $output->tarifas = $tarifas->output;
            $output->output = $this->load->view('tarifas_tiendas',array('output'=>$output),TRUE,'tienda');
          }
          $this->loadView($output);
        }

        function tarifas_tiendas_precios($where = ''){
          $this->as['tarifas_tiendas'] = 'tarifas_tiendas_precios';
          $crud = $this->crud_function('','');                              
          $crud->where('tarifas_tiendas_id',$where)
               ->field_type('tarifas_tiendas_id','hidden',$where)
               ->unset_columns('tarifas_tiendas_id');
          if($this->router->fetch_method()!='tarifas_tiendas_precios'){
            $crud->set_url('t/tarifas_tiendas_precios/'.$where.'/');
            $crud = $crud->render(1);
            return $crud;
          }else{
            $output = $crud->render();
            $this->loadView($output);
          }
        }

       	function productos(){
       		$crud = $this->crud_function('','');
       		//$crud->set_field_upload('foto','img/fotos_productos');
          $crud->field_type('foto','image',array('path'=>'img/fotos_productos','width'=>'600px','height'=>'400px'));
          $crud->add_action('<i class="fa fa-image"></i> Adm.fotos','',base_url('tienda/productos_fotos').'/');
       		$crud = $crud->render();
       		$this->loadView($crud);
       	}

        function productos_fotos(){
          $this->load->library('image_crud');
          $crud = new image_crud();
          $crud->set_table('productos_fotos')
               ->set_image_path('img/fotos_productos')
               ->set_url_field('foto')
               ->set_relation_field('productos_id')
               ->set_ordering_field('orden');
          $crud->module = 'tienda';
          $crud = $crud->render();
          $this->loadView($crud);
        }

       	function ventas($action = '',$x = '',$y = ''){
          if($action=='print' || $action=='export'){
            $this->as['ventas'] = 'view_ventas';
          }
          if($action=='send'){
            $this->db->update('ventas',array('procesado'=>3),array('id'=>$x));
            redirect('t/ventas/success');
            die();
          }
       		$crud = $this->crud_function('',''); 
          $crud->set_relation('user_id','user','{nombre} {apellido_paterno}');
          $crud->callback_column('se8701ad4',function($val,$row){
              return '<a href="'.base_url('seguridad/user/edit/'.$row->user_id).'" target="_new">'.$val.'</a>';
          });
          $crud->callback_column('productos',function($val,$row){
            return '<a href="'.base_url('t/ventas_detalles/'.$row->id).'">'.$val.'</a>';
          });
          $crud->callback_column('procesado',function($val){
              switch($val){
                  case '-1': return '<span class="label label-danger">No procesado</span>'; break;
                  case '1': return '<span class="label label-default">Por procesar</span>'; break;
                  case '2': return '<span class="label label-info">Envío pendiente</span>'; break;
                  case '3': return '<span class="label label-success">Enviado</span>'; break;
              }
          });
          $crud->callback_after_delete(function($primary){
              get_instance()->db->delete('ventas_detalles',array('ventas_id'=>$primary));
          });
          if($crud->getParameters()=='list'){
            $crud->columns('id','user_id','productos','precio','cantidad','fecha_compra','paises_id','provincias_id','procesado');
          }
          if(!empty($_POST['desde']) && !empty($_POST['hasta'])){
              $desde = date("Y-m-d",strtotime(str_replace('/','-',$_POST['desde'])));
              $hasta = date("Y-m-d",strtotime(str_replace('/','-',$_POST['hasta'])));
              $crud->where('DATE(fecha_compra) >=',$desde);
              $crud->where('DATE(fecha_compra) <=',$hasta);
          }
          if($action=='export' && !empty($x) && !empty($y)){
            $desde = date("Y-m-d",strtotime(str_replace('/','-',$x)));
            $hasta = date("Y-m-d",strtotime(str_replace('/','-',$y)));
            $crud->where('DATE(fecha_compra) >=',$desde);
            $crud->where('DATE(fecha_compra) <=',$hasta);
            $crud->unset_columns('fecha_compra');
          }
          $crud->display_as('id','#Compra');
          $crud->add_action('<i class="fa fa-print"></i> Etiqueta','',base_url('reportes/rep/verReportes/72/pdf/ventas_id').'/');
          $crud->add_action('<i class="fa fa-envelope"></i> Notificar envio','',base_url('t/ventas/send').'/');
          $crud->field_type('procesado','dropdown',array(-1=>'No Procesado',1=>'Por procesar',2=>'Procesado',3=>'Enviado'));                  
          $crud->where('shop',1);
          $crud->order_by('id','DESC');
       		$crud = $crud->render();
          $crud->output = $this->load->view('_ventas_crud',array('output'=>$crud->output),TRUE);
       		$this->loadView($crud);
       	}

        function ventas_tiendas($action = '',$x = '',$y = ''){
          if($action=='print' || $action=='export'){
            $this->as['ventas_tiendas'] = 'view_ventas_tiendas';
          }else{
            $this->as['ventas_tiendas'] = 'ventas';
          }
          if($action=='send'){
            $this->db->update('ventas',array('procesado'=>3),array('id'=>$x));
            redirect('t/ventas/success');
            die();
          }
          $crud = $this->crud_function('',''); 
          $crud->set_relation('user_id','user','{nombre} {apellido_paterno}');
          $crud->callback_column('se8701ad4',function($val,$row){
              return '<a href="'.base_url('seguridad/user/edit/'.$row->user_id).'" target="_new">'.$val.'</a>';
          });
          if(is_numeric($x)){
              $crud->where('user_id',$x);
          }
          $crud->callback_column('productos',function($val,$row){
            return '<a href="'.base_url('t/ventas_detalles/'.$row->id).'">'.$val.'</a>';
          });
          $crud->callback_column('procesado',function($val){
              switch($val){
                  case '-1': return '<span class="label label-danger">No procesado</span>'; break;
                  case '1': return '<span class="label label-default">Por procesar</span>'; break;
                  case '2': return '<span class="label label-info">Envío pendiente</span>'; break;
                  case '3': return '<span class="label label-success">Enviado</span>'; break;
              }
          });
          $crud->callback_after_delete(function($primary){
              get_instance()->db->delete('ventas_detalles',array('ventas_id'=>$primary));
          });
          if($crud->getParameters()=='list'){
            $crud->columns('id','user_id','productos','precio','cantidad','fecha_compra','paises_id','provincias_id','procesado','forma_pago','pagado');
          }
          if(!empty($_POST['desde']) && !empty($_POST['hasta'])){
              $desde = date("Y-m-d",strtotime(str_replace('/','-',$_POST['desde'])));
              $hasta = date("Y-m-d",strtotime(str_replace('/','-',$_POST['hasta'])));
              $crud->where('DATE(fecha_compra) >=',$desde);
              $crud->where('DATE(fecha_compra) <=',$hasta);
          }
          if($action=='export' && !empty($x) && !empty($y)){
            $desde = date("Y-m-d",strtotime(str_replace('/','-',$x)));
            $hasta = date("Y-m-d",strtotime(str_replace('/','-',$y)));
            $crud->where('DATE(fecha_compra) >=',$desde);
            $crud->where('DATE(fecha_compra) <=',$hasta);
            $crud->unset_columns('fecha_compra');
          }
          $crud->display_as('id','#Compra');
          $crud->add_action('<i class="fa fa-print"></i> Etiqueta','',base_url('reportes/rep/verReportes/72/pdf/ventas_id').'/');
          $crud->add_action('<i class="fa fa-envelope"></i> Notificar envio','',base_url('t/ventas/send').'/');
          $crud->field_type('procesado','dropdown',array(-1=>'No Procesado',1=>'Por procesar',2=>'Procesado',3=>'Enviado'));                  
          $crud->where('shop',2);
          $crud->order_by('id','DESC');
          $crud = $crud->render();
          $crud->output = $this->load->view('_ventas_crud',array('output'=>$crud->output),TRUE);
          $this->loadView($crud);
        }

        function ventas_importadores($action = '',$x = '',$y = ''){
          if($action=='print' || $action=='export'){
            $this->as['ventas_importadores'] = 'view_ventas';
          }else{
            $this->as['ventas_importadores'] = 'ventas';
          }
          if($action=='send'){
            $this->db->update('ventas',array('procesado'=>3),array('id'=>$x));
            redirect('t/ventas/success');
            die();
          }
          $crud = $this->crud_function('',''); 
          $crud->set_subject('ventas spetial imports');
          $crud->set_relation('user_id','user','{nombre} {apellido_paterno}');
          $crud->callback_column('se8701ad4',function($val,$row){
              return '<a href="'.base_url('seguridad/user/edit/'.$row->user_id).'" target="_new">'.$val.'</a>';
          });
          if(is_numeric($x)){
              $crud->where('user_id',$x);
          }
          $crud->callback_column('productos',function($val,$row){
            return '<a href="'.base_url('t/ventas_detalles/'.$row->id).'">'.$val.'</a>';
          });
          $crud->callback_column('procesado',function($val){
              switch($val){
                  case '-1': return '<span class="label label-danger">No procesado</span>'; break;
                  case '1': return '<span class="label label-default">Por procesar</span>'; break;
                  case '2': return '<span class="label label-info">Envío pendiente</span>'; break;
                  case '3': return '<span class="label label-success">Enviado</span>'; break;
              }
          });
          $crud->callback_after_delete(function($primary){
              get_instance()->db->delete('ventas_detalles',array('ventas_id'=>$primary));
          });
          if($crud->getParameters()=='list'){
            $crud->columns('id','user_id','productos','precio','cantidad','fecha_compra','paises_id','provincias_id','procesado');
          }
          if(!empty($_POST['desde']) && !empty($_POST['hasta'])){
              $desde = date("Y-m-d",strtotime(str_replace('/','-',$_POST['desde'])));
              $hasta = date("Y-m-d",strtotime(str_replace('/','-',$_POST['hasta'])));
              $crud->where('DATE(fecha_compra) >=',$desde);
              $crud->where('DATE(fecha_compra) <=',$hasta);
          }
          if($action=='export' && !empty($x) && !empty($y)){
            $desde = date("Y-m-d",strtotime(str_replace('/','-',$x)));
            $hasta = date("Y-m-d",strtotime(str_replace('/','-',$y)));
            $crud->where('DATE(fecha_compra) >=',$desde);
            $crud->where('DATE(fecha_compra) <=',$hasta);
            $crud->unset_columns('fecha_compra');
          }
          $crud->display_as('id','#Compra');
          $crud->add_action('<i class="fa fa-print"></i> Etiqueta','',base_url('reportes/rep/verReportes/72/pdf/ventas_id').'/');
          $crud->add_action('<i class="fa fa-envelope"></i> Notificar envio','',base_url('t/ventas/send').'/');
          $crud->field_type('procesado','dropdown',array(-1=>'No Procesado',1=>'Por procesar',2=>'Procesado',3=>'Enviado'));                  
          $crud->where('shop',3);
          $crud->order_by('id','DESC');
          $crud = $crud->render();
          $crud->output = $this->load->view('_ventas_crud',array('output'=>$crud->output),TRUE);
          $crud->title = 'ventas spetial imports';
          $this->loadView($crud);
        }

        function ventas_user($action = '',$x = '',$y = ''){
          $this->as['ventas_user'] = 'user';
          $crud = $this->crud_function('',''); 
          $crud->set_subject('Ventas por usuarios');          
          $crud->columns('nombre','apellido_paterno','email');
          $crud->unset_add()
               ->unset_edit()
               ->unset_delete()
               ->unset_read()
               ->unset_print()
               ->unset_export()
               ->callback_column('email',function($val,$row){
                  return '<a href="'.base_url('t/ventas_detalles_user/'.$row->id).'">'.$val.'</a>';
               });
          $crud = $crud->render();                    
          $this->loadView($crud);
        }
        function ventas_por_tiendas($action = '',$x = '',$y = ''){
          $this->as['ventas_por_tiendas'] = 'view_tiendas';
          $crud = $this->crud_function('',''); 
          $crud->set_subject('Ventas por tiendas');          
          
          $crud->unset_add()
               ->unset_edit()
               ->unset_delete()
               ->unset_read()
               ->unset_print()
               ->unset_export()
               ->set_primary_key('id')
               ->callback_column('email',function($val,$row){
                  return '<a href="'.base_url('t/ventas_detalles_tiendas/'.$row->id).'">'.$val.'</a>';
               });
          $crud = $crud->render();
          $crud->title = 'Ventas por tiendas';                   
          $this->loadView($crud);
        }

        function ventas_detalles_user($ventas_id,$action = ''){           
          $this->as['ventas_detalles_user'] = 'view_ventas_detalles';      
          $crud = $this->crud_function('',''); 
          $crud->where('user_id',$ventas_id)                                                 
               ->display_as('compra','Pedido N°')                 
               ->set_subject('Detalle de venta')
               ->unset_add()
               ->unset_delete()
               ->unset_columns('user_id')
               ->set_primary_key('compra')
               ->field_type('shop','dropdown',array('1'=>'WEB','2'=>'TIENDA','3'=>'IMPORTS'))
               ->display_as('shop','Tipo de venta');
          $crud->callback_column('precio',function($val,$row){
            return moneda($val);
          });
          $crud->callback_column('tipo_producto',function($val,$row){
            return $val==1?'UNIT':'PACK';
          });

            
          $crud = $crud->render(); 
          $crud->title = 'Ventas por usuarios';
          $this->loadView($crud);          
        }

        function ventas_detalles_tiendas($ventas_id,$action = ''){           
          $this->as['ventas_detalles_tiendas'] = 'view_ventas_detalles';      
          if($action=='edit'){
            $this->as['ventas_detalles_tiendas'] = 'ventas';      
          }
          $crud = $this->crud_function('',''); 
          $crud->where('user_id',$ventas_id)                                                 
               ->display_as('compra','Pedido N°')                 
               ->set_subject('Detalle de venta')
               ->unset_add()
               ->unset_delete()
               ->unset_columns('user_id');
          if($crud->getParameters()=='list'){
            $crud->set_primary_key('compra');
          }else{
            $crud->field_type('procesado','dropdown',array(-1=>'No Procesado',1=>'Por procesar',2=>'Procesado',3=>'Enviado'));                  
            $crud->fields('procesado','pagado');
          }
          $crud->where('shop',2)
               ->field_type('shop','dropdown',array('1'=>'WEB','2'=>'TIENDA','3'=>'IMPORTS'))
               ->display_as('shop','Tipo de venta');
          $crud->callback_column('precio',function($val,$row){
            return moneda($val);
          });
          $crud->callback_column('tipo_producto',function($val,$row){
            return $val==1?'UNIT':'PACK';
          });

            
          $crud = $crud->render(); 
          $crud->title = 'Ventas por usuarios';
          $this->loadView($crud);          
        }

        

        function ventas_detalles($ventas_id,$action = ''){ 
          if($action=='print' || $action=='export'){
            $this->as['ventas_detalles'] = 'view_ventas_detalles';
          }        
          $crud = $this->crud_function('',''); 
          if($action=='print' || $action == 'export'){
            $crud->where('compra',$ventas_id);
            $crud->callback_column('precio',function($val,$row){
              return moneda($val);
            });
            $crud->columns('compra','referencia','titulo','tipo_producto','cantidad','precio');
            $crud->callback_column('tipo_producto',function($val,$row){
               return $val==1?'UNIT':'PACK';
            });
            $crud->display_as('compra','Pedido N°');
            $crud = $crud->render();
          } 
          else{         
            $crud->where('ventas_id',$ventas_id)
                 ->field_type('ventas_id','hidden',$ventas_id)                 
                 ->columns('ventas_id','j4b04c546.referencia','j4b04c546.titulo','tipo_producto','cantidad','precio')
                 ->display_as('j4b04c546.referencia','Referencia')
                 ->display_as('j4b04c546.titulo','Producto')
                 ->display_as('ventas_id','Pedido N°')
                 ->set_relation('productos_id','albums','{referencia}||{titulo}||{tipo_producto}')
                 ->set_subject('Detalle de venta')
                 ->unset_add()
                 ->unset_delete();
            $crud->callback_column('j4b04c546.referencia',function($val,$row){
              return explode('||',$row->s4b04c546)[0];
            });
            $crud->callback_column('j4b04c546.titulo',function($val,$row){
              return explode('||',$row->s4b04c546)[1];
            });
            $crud->callback_column('tipo_producto',function($val,$row){
               return explode('||',$row->s4b04c546)[2]==1?'UNIT':'PACK';
            });
            $crud->callback_column('precio',function($val,$row){
              return moneda($val);
            });
            
            $crud = $crud->render();
            $crud->title = 'Detalle de venta';

            $header = new ajax_grocery_crud();
            $header->set_table('ventas')
                   ->set_subject('Pedido #'.$ventas_id)
                   ->set_theme('header_data')
                   ->where('ventas.id',$ventas_id)
                   ->unset_add()
                   ->unset_edit()
                   ->unset_delete()
                   ->unset_read()
                   ->unset_print()
                   ->field_type('procesado','dropdown',array(-1=>'No Procesado',1=>'Por procesar',2=>'Procesado',3=>'Enviado'))
                   ->unset_columns('productos','pasarela','cantidad','precio','precio_rembolso','shop','tax_australia','peso_vinilos','peso_embalaje','peso_total')
                   ->set_relation('user_id','user','{nombre} {apellido_paterno}')
                   ->display_as('user_id','Cliente')
                   ->display_as('procesado','Estado del pedido')
                   ->display_as('paises_id','País')
                   ->display_as('provincias_id','Provincia')
                   ->display_as('telefono','Teléfono')
                   ->display_as('cp','Código postal')
                   ->display_as('fecha_compra','Fecha de compra')
                   ->display_as('forma_pago','Forma de pago')
                   ->callback_column('precio_envio',function($val){return moneda($val);})
                   ->callback_column('total',function($val){return moneda($val);})
                   ->set_url('t/ventas');
            $crud->header = $header->render(1)->output;
            $crud->output = $crud->output.'<div style="text-align:right;margin:30px 0;"><a href="javascript:window.print();" class="btn btn-info"><i class="fa fa-print"></i> Imprimir</a></div>';         
          }
          $crud->title = 'Pedido N° '.$ventas_id;
          $this->loadView($crud);          
        }

        function regalos($x = '',$y = ''){
          $crud = $this->crud_function('','');    
          $crud->set_rules('user_id','user','required|callback_validate_productos');
          $crud->callback_before_insert(function($post){
            $post['fecha'] = date("Y-m-d");
            return $post;
          });
          $crud->callback_after_insert(function($post,$primary){
            get_instance()->regalos_actualizar($post,$primary);
          });
          $crud->callback_after_update(function($post,$primary){
            get_instance()->db->delete('regalos_detalles',array('regalos_id'=>$primary));
            get_instance()->regalos_actualizar($post,$primary);
          });
          $crud->callback_before_delete(function($primary){
            get_instance()->db->delete('regalos_detalles',array('regalos_id'=>$primary));            
          });
          $output = $crud->render();
          if($crud->getParameters()=='add'){
            $output->output = $this->load->view('regalos_add',array(),TRUE);
          }
          if($crud->getParameters()=='edit'){
            $detail = $this->db->get_where('regalos',array('id'=>$y));
            $detail = $detail->row();
            $this->db->select('regalos_detalles.*,albums.titulo as producto');
            $this->db->join('albums','albums.id = regalos_detalles.albums_id');
            $detail->detalles = $this->db->get_where('regalos_detalles',array('regalos_id'=>$y));
            $output->output = $this->load->view('regalos_add',array('detail'=>$detail),TRUE);
          }
          $this->loadView($output);
        }

        function regalos_actualizar($post,$primary){
          $productos = array();
          foreach($_POST['albums_id'] as $n=>$v){
            if(!empty($v)){
              $this->db->insert('regalos_detalles',array(
                'regalos_id'=>$primary,
                'albums_id'=>$v,
                'cantidad'=>$_POST['cantidad'][$n]
              ));
              $productos[] = $this->db->get_where('albums',array('id'=>$v))->row()->titulo;
            }              
          }
          $this->db->update('regalos',array('productos'=>implode(',',$productos)),array('id'=>$primary));
        }

        function validate_productos(){
          $no = false;
          if(empty($_POST['albums_id']) || empty($_POST['cantidad'])){
            $no = true;
          }
          $x = 0;
          foreach($_POST['albums_id'] as $n=>$v){
            if(!empty($v) && !empty($_POST['cantidad'][$n])){
              $x = 1;
            }
          }
          if($x==0){
            $no = true;
          }
          if($no){
            $this->form_validation->set_message('validate_productos','Debes incluir al menos un producto');
            return false;
          }
        }

        function stock(){
          $crud = $this->crud_function('','');
          $crud->set_relation('albums_id','albums','referencia',array('promos_importadores'=>0,'tipo_producto'=>1));
          $crud = $crud->render();
          $this->loadView($crud);
        }

        function pagos_pendientes(){
          $this->as['pagos_pendientes'] = 'view_morosos';
          $crud = $this->crud_function('','');          
          $crud->unset_add()
               ->unset_edit()
               ->unset_delete()
               ->unset_read()
               ->display_as('deudas','Compras')
               ->display_as('total_deuda','Total compras')
               ->set_primary_key('cliente');
          $crud = $crud->render();
          $crud->title = 'Pagos pendientes por cobrar';
          $this->loadView($crud);
        }

        
    }
?>
