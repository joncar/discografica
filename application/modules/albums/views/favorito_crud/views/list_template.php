<?php
$this->set_js_lib($this->default_javascript_path . '/' . grocery_CRUD::JQUERY);
$this->set_js_lib($this->default_javascript_path . '/jquery_plugins/jquery.noty.js');
$this->set_js_lib($this->default_javascript_path . '/jquery_plugins/config/jquery.noty.config.js');

if (!$this->is_IE7()) {
    $this->set_js_lib($this->default_javascript_path . '/common/list.js');
}
$this->set_js('assets/grocery_crud/themes/bootstrap2/js/cookies.js');
$this->set_js('assets/grocery_crud/themes/bootstrap2/js/flexigrid.js');
$this->set_js('assets/grocery_crud/themes/bootstrap2/js/jquery.form.js');
$this->set_js($this->default_javascript_path . '/jquery_plugins/jquery.numeric.min.js');
$this->set_js('assets/grocery_crud/themes/bootstrap2/js/jquery.printElement.min.js');
$this->set_js('assets/grocery_crud/themes/bootstrap2/js/pagination.js');
/** Fancybox */
$this->set_css($this->default_css_path . '/jquery_plugins/fancybox/jquery.fancybox.css');
$this->set_js($this->default_javascript_path . '/jquery_plugins/jquery.fancybox-1.3.4.js');
$this->set_js($this->default_javascript_path . '/jquery_plugins/jquery.easing-1.3.pack.js');

/** Jquery UI */
$this->load_js_jqueryui();
?>
<script type='text/javascript'>
    var base_url = '<?php echo base_url(); ?>';
    var subject = '<?php echo $subject ?>';
    var ajax_list_info_url = '<?php echo $ajax_list_info_url; ?>';
    var ajax_list = '<?php echo $ajax_list_url; ?>';
    var unique_hash = '<?php echo $unique_hash; ?>';
    var message_alert_delete = "<?php echo $this->l('alert_delete'); ?>";
    var crud_pagin = 1;
    var fragmentos = 1;
    var total_results = <?= $total_results ?>;
</script>

<?php echo form_open($ajax_list_url, 'method="post" id="filtering_form" class="filtering_form" autocomplete = "off" data-ajax-list-info-url="' . $ajax_list_info_url . '" onsubmit="return filterSearchClick(this)"'); ?>
<div class="flexigrid wrapper clearfix">
		<button style="display:none" title='Refrescar listado'  type="button" class="ajax_refresh_and_loading dt-button buttons-collection buttons-colvis btn btn-white btn-primary btn-bold" tabindex="0" aria-controls="dynamic-table" data-original-title="" title="">
            <span>
                <i class="ace-icon fa fa-refresh bigger-110 grey"></i>
            </span>
        </button>
		<div class="widget kopa-event-4-widget">
			<div class="widget-content ajax_list">
				<?php echo $list_view?>
			</div>
		</div>
		<!-- events-by-month -->
		<div class="pagination clearfix">
			<ul class="pagination page-numbers clearfix">				
			</ul><!--page-numbers-->
		</div>
			<!-- pagination -->
	</div>
<?php echo form_close() ?>