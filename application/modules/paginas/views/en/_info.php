<!-- dark-bg -->
<section class="kopa-area kopa-area-3 kopa-parallax">
	<div class="span-bg"></div>
	<div class="wrapper">
		<div class="row">
			
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="widget kopa-audio-list-2-widget">
					<header>
						<h3 class="widget-title style1">my music</h3>
					</header>
					<div class="clearfix">
						<div class="widget-thumb">
							<div class="entry-thumb">
								<a href="[base_url]albums-detail.html"><img src="[base_url]theme/theme/images/img/14.png" alt=""></a>
							</div>
						</div>
						<div class="widget-content">
							<div class="audio-list-wrapper">
								<header>
									<h3><a href="[base_url]albums-detail.html">Andreas Weise</a></h3>
									<p><a href="[base_url]albums-detail.html">Leona</a>&nbsp;-&nbsp;<a href="[base_url]albums-detail.html">pop</a></p>
								</header>
								<ul class="audio-list">
									<li><a href="[base_url]albums-detail.html">Zuper - Glide</a></li>
									<li><a href="[base_url]albums-detail.html">Lana Del Rey - Ride (Barretso Remix)</a></li>
									<li><a href="[base_url]albums-detail.html">Four Tet - Lion (Jamie xx Remix)</a></li>
									<li><a href="[base_url]albums-detail.html">Jamie xx & Yasmin - Touch Me</a></li>
									<li><a href="[base_url]albums-detail.html">Disclosure - Tenderly</a></li>
									<li><a href="[base_url]albums-detail.html">Zuper - Glide</a></li>
									<li><a href="[base_url]albums-detail.html">Lana Del Rey - Ride (Barretso Remix)</a></li>
									<li><a href="[base_url]albums-detail.html">Four Tet - Lion (Jamie xx Remix)</a></li>
									<li><a href="[base_url]albums-detail.html">Jamie xx & Yasmin - Touch Me</a></li>
									<li><a href="[base_url]albums-detail.html">Disclosure - Tenderly</a></li>
								</ul>
							</div>
							<!-- audio-list-wrapper -->
							<div class="album-intro">
								<p><span class="kp-dropcap-1">P</span>ellentesque elementum libero enim, eget gravida   nunc laoreet et. Nullam ac enim auctor, fringilla risus at, imperdiet turpis. Mauris ut tristique odio. Aenean diam ipsum ultricies.</p>
								<p>Nulla id imperdiet purus, id tristique erat. Pellentesque elementum libero enim, eget gravida nunc laoreet et. Nullam ac enim auctor, fringilla risus at, imperdiet turpis. Mauris ut tristique odio. Aenean diam ipsum, ultricies sed consequat sed, faucibus et tellus. Nulla id imperdiet purus, id tristique erat.</p>
							</div>
						</div>
					</div>
				</div>
				<!-- widget -->
				
			</div>
			<!-- col-md-12 -->
			
		</div>
		<!-- row -->
		<div class="row">
			
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="widget kopa-album-carousel-widget">
					<div class="owl-carousel owl-carousel-1">
						<div class="item">
							<article class="entry-item">
								<div class="entry-thumb">
									<span></span>
									<a href="[base_url]albums-detail.html"><img src="[base_url]theme/theme/images/img/15.png" alt=""></a>
								</div>
							</article>
						</div>
						<!-- item -->
						<div class="item">
							<article class="entry-item">
								<div class="entry-thumb">
									<span></span>
									<a href="[base_url]albums-detail.html"><img src="[base_url]theme/theme/images/img/15.png" alt=""></a>
								</div>
							</article>
						</div>
						<!-- item -->
						<div class="item">
							<article class="entry-item">
								<div class="entry-thumb">
									<span></span>
									<a href="[base_url]albums-detail.html"><img src="[base_url]theme/theme/images/img/15.png" alt=""></a>
								</div>
							</article>
						</div>
						<!-- item -->
						<div class="item">
							<article class="entry-item">
								<div class="entry-thumb">
									<span></span>
									<a href="[base_url]albums-detail.html"><img src="[base_url]theme/theme/images/img/15.png" alt=""></a>
								</div>
							</article>
						</div>
						<!-- item -->
						<div class="item">
							<article class="entry-item">
								<div class="entry-thumb">
									<span></span>
									<a href="[base_url]albums-detail.html"><img src="[base_url]theme/theme/images/img/15.png" alt=""></a>
								</div>
							</article>
						</div>
						<!-- item -->
						<div class="item">
							<article class="entry-item">
								<div class="entry-thumb">
									<span></span>
									<a href="[base_url]albums-detail.html"><img src="[base_url]theme/theme/images/img/15.png" alt=""></a>
								</div>
							</article>
						</div>
						<!-- item -->
						<div class="item">
							<article class="entry-item">
								<div class="entry-thumb">
									<span></span>
									<a href="[base_url]albums-detail.html"><img src="[base_url]theme/theme/images/img/15.png" alt=""></a>
								</div>
							</article>
						</div>
						<!-- item -->
						<div class="item">
							<article class="entry-item">
								<div class="entry-thumb">
									<span></span>
									<a href="[base_url]albums-detail.html"><img src="[base_url]theme/theme/images/img/15.png" alt=""></a>
								</div>
							</article>
						</div>
						<!-- item -->
						<div class="item">
							<article class="entry-item">
								<div class="entry-thumb">
									<span></span>
									<a href="[base_url]albums-detail.html"><img src="[base_url]theme/theme/images/img/15.png" alt=""></a>
								</div>
							</article>
						</div>
						<!-- item -->
						<div class="item">
							<article class="entry-item">
								<div class="entry-thumb">
									<span></span>
									<a href="[base_url]albums-detail.html"><img src="[base_url]theme/theme/images/img/15.png" alt=""></a>
								</div>
							</article>
						</div>
						<!-- item -->
						<div class="item">
							<article class="entry-item">
								<div class="entry-thumb">
									<span></span>
									<a href="[base_url]albums-detail.html"><img src="[base_url]theme/theme/images/img/15.png" alt=""></a>
								</div>
							</article>
						</div>
						<!-- item -->
						<div class="item">
							<article class="entry-item">
								<div class="entry-thumb">
									<span></span>
									<a href="[base_url]albums-detail.html"><img src="[base_url]theme/theme/images/img/15.png" alt=""></a>
								</div>
							</article>
						</div>
						<!-- item -->
					</div>
					<!-- owl-carousel-1 -->
				</div>
				<!-- widget -->
				
			</div>
			<!-- col-md-12 -->
			
		</div>
		<!-- row -->
		
	</div>
	<!-- wrapper -->
	
</section>
<!-- kopa-area-3 -->