<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once('Main.php');
class Registro extends Main {
        const IDROLUSER = 2;
        const IDRADIOS = 4;
        const IDTIENDA = 5;
        
        public function __construct()
        {
            parent::__construct();         
            $this->load->library('grocery_crud');
            $this->load->library('ajax_grocery_crud');
        }
        
        public function loadView($param = array('view'=>'main'))
        {
                if(!empty($param->output)){
                    $panel = 'panel';
                    $param->view = empty($param->view)?$panel:$param->view;
                    $param->crud = empty($param->crud)?'user':$param->crud;
                    $param->title = empty($param->title)?ucfirst($this->router->fetch_method()):$param->title;
                }
                if(is_string($param)){
                    $param = array('view'=>$param);
                }
                $template = 'templateadmin';
                $this->load->view($template,$param);
        }

        public function index($url = 'main',$page = 0)
        {
            if($this->router->fetch_class()=='registro'){
                $crud = new ajax_grocery_CRUD();
                $crud->set_theme('bootstrap2');
                $crud->set_table('user');
                $crud->set_subject('<span style="font-size:18px">Nuevo usuario</span>');
                //Fields

                //unsets
                $crud->unset_back_to_list()
                     ->unset_delete()
                     ->unset_read()
                     ->unset_edit()
                     ->unset_list()
                     ->unset_export()
                     ->unset_print()
                     ->field_type('fecha_registro','invisible')
                     ->field_type('fecha_actualizacion','invisible')
                     ->field_type('status','invisible')
                     ->field_type('foto','invisible')
                     ->field_type('admin','invisible')
                     ->field_type('password','password')
                     ->field_type('cedula','invisible')
                     ->field_type('apellido_materno','invisible')
                     ->field_type('tiene_android','hidden',1)
                     ->field_type('pais','string','España')
                     ->field_type('status','hidden','1')
                     ->field_type('admin','hidden','1');

                if(!empty($_SESSION['envio'])){
                    $post = $_SESSION['envio'];
                    get_instance()->post = $post;
                    $crud->field_type('ciudad','string',$post['ciudad'])
                         ->field_type('direccion','string',$post['direccion'])
                         ->field_type('codigo_postal','string',$post['codigo_postal'])
                         ->field_type('telefono','string',$post['telefono'])
                         ->callback_field('paises_id',function($val){
                            return form_dropdown_from_query('paises_id','paises','id','nombre',get_instance()->post['paises_id'],'field-paises_id',TRUE);
                         })
                         ->callback_field('provincias_id',function($val){
                            return form_dropdown_from_query('provincias_id','provincias','id','nombre',get_instance()->post['provincias_id'],'field-provincias_id',TRUE);
                         });
                }

                $crud->display_as('password','Contraseña nuevo usuario')
                     ->display_as('emisora','Nombre emisora de radio (Si eres una emisora)')
                     ->display_as('tienda','Nombre tienda (Si eres una tienda)')
                     ->display_as('apellido_paterno','Apellidos')                     
                     ->display_as('nombre','Nombre')
                     ->display_as('email','Email de contacto')
                     ->display_as('provincias_id','Provincia')
                     ->display_as('paises_id','Pais')
                     ->display_as('referencia_grupo','Código del grupo')
                     ->display_as('telefono','Teléfono')
                     ->display_as('codigo_postal','Código Postal')
                     ->display_as('direccion','Dirección');
                     

                $redirect = empty($_SESSION['carrito'])?'panel':'tt/checkout';
                $crud->set_lang_string('insert_success_message','Datos registrados con éxito <script>setTimeout(function(){document.location.href="'.base_url($redirect).'"; },2000)</script>');                
                $crud->set_lang_string('form_add','');                
                //Displays             
                $crud->set_lang_string('form_save','REGISTRAR');
                
                $crud->callback_before_insert(array($this,'binsertion'));
                $crud->callback_after_insert(array($this,'ainsertion'));
                $crud->set_rules('email','Email','required|valid_email|is_unique[user.email]');                
                $crud->required_fields_array();
                $output = $crud->render();
                $output->view = 'registro';
                $output->crud = 'user';
                $output->title = 'REGISTRAR';
                $output->output = $output->output;
                $output->scripts = get_header_crud($output->css_files,$output->js_files,TRUE);
                $this->loadView($output);   
            }
        }              
        
        function conectar()
        {
            $this->loadView('predesign/login');
        }
        /* Callbacks */
        function binsertion($post)
        {            
            $post['status'] = 1;
            $post['admin'] = 0;
            $post['fecha_registro'] = date("Y-m-d H:i:s");
            $post['password'] = md5($post['password']);            
            return $post;
        }
        
        function ainsertion($post,$primary)
        {              
            //Asignar rol
            get_instance()->db->insert('user_group',array('user'=>$primary,'grupo'=>self::IDROLUSER));            
            get_instance()->user->login_short($primary);   
            $post['provincia'] = get_instance()->db->get_where('provincias',array('id'=>$post['provincias_id']))->row()->nombre;         
            $post['pais'] = get_instance()->db->get_where('paises',array('id'=>$post['paises_id']))->row()->nombre;         
            get_instance()->enviarcorreo((object)$post,14);
            if(!empty($post['emisora'])){
                //Enviar correo de confirmación
                $post['seccion'] = 'Promos radio';
                $post['link'] = base_url('seguridad/aprobar_ingreso/'.$primary.'/'.self::IDRADIOS);
                get_instance()->enviarcorreo((object)$post,21,'info@discotecarecords.com');
            }

            if(!empty($post['tienda'])){
                //Enviar correo de confirmación
                $post['seccion'] = 'Sección tienda';
                $post['link'] = base_url('seguridad/aprobar_ingreso/'.$primary.'/'.self::IDTIENDA);
                get_instance()->enviarcorreo((object)$post,21,'info@discotecarecords.com');
            }
            return true;
        }
        
       
        function forget($key = '',$ajax = '')
        {
            if(empty($_POST) && empty($key)){
                $this->loadView(array('view'=>'forget'));
            }
            else
            {                
                $_SESSION['email'] = empty($_SESSION['email']) && !empty($ajax)?base64_decode($ajax):$_SESSION['email'];
                $_SESSION['key'] = empty($_SESSION['key']) && !empty($ajax)?base64_decode($ajax):$_SESSION['key'];
                if(empty($key)){
                    if(empty($_SESSION['key'])){
                        $this->form_validation->set_rules('email','Email','required|valid_email');
                        if($this->form_validation->run())
                        {
                            $user = $this->db->get_where('user',array('email'=>$this->input->post('email')));
                            if($user->num_rows()>0){
                                $_SESSION['key'] = md5($this->input->post('email'));
                                $_SESSION['email'] = $this->input->post('email');
                                if($_SESSION['lang']=='es'){
                                    correo($this->input->post('email'),'Restablecimiento de contraseña',$this->load->view('email/forget',array('user'=>$user->row()),TRUE));
                                }else{
                                    correo($this->input->post('email'),'Recuperación de contraseña',$this->load->view('email/forget_en',array('user'=>$user->row()),TRUE));
                                }
                                $_SESSION['msj'] = $this->success('Se han enviado los pasos de restauración a su correo');
                                header("Location:".base_url('registro/index/add'));
                            }
                            else{
                                $this->loadView(array('view'=>'registro','msj'=>$this->error('El correo utilizado no esta registrado')));
                            }
                        }
                        else{
                            $this->loadView(array('view'=>'registro','msj'=>$this->error($this->form_validation->error_string())));
                        }
                    }
                    else
                    {
                        $this->form_validation->set_rules('email','Email','required|valid_email');
                        $this->form_validation->set_rules('pass','Password','required|min_length[8]');
                        $this->form_validation->set_rules('pass2','Password2','required|min_length[8]|matches[pass]');
                        //$this->form_validation->set_rules('key','Llave','required');
                        if($this->form_validation->run())
                        {
                            /*if($this->input->post('key') == $_SESSION['key'])
                            {*/
                                $this->db->update('user',array('password'=>md5($this->input->post('pass'))),array('email'=>$_SESSION['email']));
                                session_unset();
                                $this->loadView(array('view'=>'registro','msj'=>$this->success('Se ha restablecido su contraseña <a href="'.base_url().'">Volver al inicio</a>')));
                            /*}
                            else
                                $this->loadView(array('view'=>'recover','msj'=>$this->error('Se ha vencido el plazo para el restablecimiento, solicitelo nuevamente.')));*/
                        }
                        else{
                            if(empty($_POST['key'])){
                                $this->loadView(array('view'=>'registro','msj'=>$this->error('Se ha vencido el plazo para el restablecimiento, solicitelo nuevamente.')));    
                                session_unset();
                            }
                            else{
                                $this->loadView(array('view'=>'recover','key'=>$key,'msj'=>$this->error($this->form_validation->error_string())));
                            }
                        }
                    }
                }
                else
                {
                    $this->loadView(array('view'=>'recover','key'=>$key));
                    /*if(!empty($_SESSION['key']) && $key==$_SESSION['key'])
                    {
                        $this->loadView(array('view'=>'recover','key'=>$key));
                    }
                    else{
                        $enc = $this->db->get_where('user',array('MD5(email)'=>$key));
                        
                        if($enc->num_rows()==0){
                            if(empty($ajax)){
                                $this->loadView(array('view'=>'registro','msj'=>$this->error('Se ha vencido el plazo para el restablecimiento, solicitelo nuevamente.')));
                            }else{
                                echo $this->traduccion->traducir($this->error('Se ha vencido el plazo para el restablecimiento, solicitelo nuevamente.'),$_SESSION['lang']);
                            }
                        }else{
                            $_SESISON['key'] = md5($enc->row()->email);
                            $_SESSION['email'] = $enc->row()->email;
                            $this->loadView(array('view'=>'recover','key'=>$key));
                        }
                    }*/
                }
            }
        }   
}
/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
