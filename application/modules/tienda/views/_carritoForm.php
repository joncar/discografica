<?php $carrito = $this->carrito->getCarrito(); $total = 0; $impuesto = 0;?>
<?php if(count($carrito)>0): ?>
<section class="elements-box" style="margin-bottom: 40px;">
	<div class="woocommerce">

		<div class="row">
			<div class="col-xs-12 col-sm-6 col-md-8">
				<h2>Productos</h2>
		
				<table class="shop_table shop_table_responsive cart" cellspacing="0" style="background: white;">
					<thead>
						<tr>
							<th class="product-remove">&nbsp;</th>
							<th class="product-thumbnail hidden-xs">&nbsp;</th>
							<th class="product-name">Producto</th>
							<th class="product-price">Precio</th>
							<th class="product-quantity">Cantidad</th>
							<th class="product-subtotal">Total</th>
						</tr>
					</thead>
					<tbody>
						<?php 
							foreach($carrito as $c): $c = (array)$c;
							if($c['shop']==1){
								$e = $this->elements->promos_tiendas(array('albums.id'=>$c->id))->row(); 
							}else{
								$e = $this->elements->albums(array('albums.id'=>$c->id))->row(); 
							}

						?>
							<tr class="cart_item">
								<td class="product-remove">
									<a href="javascript:remToCart('<?= $c->id ?>')" class="remove" title="Remove this item" data-product_id="226" data-product_sku="">×</a>
								</td>
								<td class="product-thumbnail hidden-xs">
									<a href="#">
										<img class="hidden-xs" src="<?= $e->caratula ?>" class="attachment-shop_thumbnail size-shop_thumbnail wp-post-image" alt="post-34">
									</a>
								</td>
								<td class="product-name" data-title="Product">
									<a href="#">
										<img class="visible-xs" style="width:100%;" src="<?= $e->caratula ?>" class="attachment-shop_thumbnail size-shop_thumbnail wp-post-image" alt="post-34">										
										<?= $e->titulo ?>
									</a>
								</td>
								<td class="product-price" data-title="Price">
									<span class="amount"><?= moneda($e->precio) ?></span>
								</td>
								<td class="product-quantity" data-title="Quantity">
									<div class="quantity buttons_added">
										<input step="1" min="0" name="cantidad" value="<?= $c->cantidad ?>" title="Qty" class="input-text qty text" size="4" type="number">
									</div>
								</td>
								<td class="product-subtotal" data-title="Total">
									<span class="amount"><?= moneda($c->cantidad*$e->_precio) ?></span>
								</td>
							</tr>
							<?php $total+= ($c->cantidad*$e->_precio); ?>
							<?php endforeach ?>
							<tr>
								<td colspan="6" class="actions">
									<input class="button" name="update_cart" value="Update Cart" type="button">
								</td>
							</tr>
						</tbody>
					</table>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-4">
				<h2>Tu pedido</h2>
				<table class="shop_table shop_table_responsive cart" cellspacing="0" style="background:white;">
					<tbody>
						<tr class="cart-subtotal">
							<th>Subtotal</th>
							<td data-title="Subtotal"><span class="amount">20€</span></td>
						</tr>
						<tr class="shipping">
							<th>Envío</th>
							<td data-title="Shipping">
								<p>Shipping costs will be calculated once you have provided your address.</p>
							</td>
						</tr>
						<tr class="order-total">
							<th>Total</th>
							<td data-title="Total">
								<strong>
								<span class="amount">20€</span>
								</strong>
							</td>
						</tr>
					</tbody>
				</table>

				<div id="payment" class="woocommerce-checkout-payment" style="margin-top:30px;">
					<ul class="wc_payment_methods payment_methods methods">
						<li class="wc_payment_method payment_method_cheque">
							<input id="payment_method_cheque" class="input-radio" name="payment_method" value="cheque" checked="checked" data-order_button_text="" type="radio">
							<label for="payment_method_cheque">
								Cheque Payment
							</label>
							<div class="payment_box payment_method_cheque">
								<p>Please send your cheque to Store Name, Store Street, Store Town, Store State / County, Store Postcode.</p>
							</div>
						</li>
						<li class="wc_payment_method payment_method_paypal">
							<input id="payment_method_paypal" class="input-radio" name="payment_method" value="paypal" data-order_button_text="Proceed to PayPal" type="radio">
							<label for="payment_method_paypal">
							PayPal <img src="https://www.paypalobjects.com/webstatic/mktg/Logo/AM_mc_vs_ms_ae_UK.png" alt="PayPal Acceptance Mark"><a href="https://www.paypal.com/gb/webapps/mpp/paypal-popup" class="about_paypal" onclick="javascript:window.open('https://www.paypal.com/gb/webapps/mpp/paypal-popup','WIPaypal','toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, width=1060, height=700'); return false;" title="What is PayPal?">What is PayPal?</a>	</label>
							<div class="payment_box payment_method_paypal" style="display:none;">
								<p>Pay via PayPal; you can pay with your credit card if you don’t have a PayPal account.</p>
							</div>
						</li>
					</ul>
					<div class="form-row place-order">
						<noscript>
						Since your browser does not support JavaScript, or it is disabled, please ensure you click the <em>Update Totals</em> button before placing your order. You may be charged more than the amount stated above if you fail to do so.			<br/><input type="submit" class="button alt" name="woocommerce_checkout_update_totals" value="Update totals" />
						</noscript>
						<input class="button alt" name="woocommerce_checkout_place_order" id="place_order" value="Pagar" type="submit">
					</div>
				</div>
			</div>
		</div>


	</div>
	
	<!-- page-links-wrapper -->
	<!-- tag-box -->
</section>
<?php else: ?>
	El carrito se encuentra vacio
<?php endif ?>