<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Cron extends Main{
        function __construct() {
            parent::__construct();
        }

        function correos_depositos(){          
          $ventas = $this->db->get_where('view_morosos');
          $correo = '<table style="width:100%;"><thead><tr><th style="border:1px solid black">Cliente</th><th style="border:1px solid black">Pagos pendientes</th><th style="border:1px solid black">Discos</th><th style="border:1px solid black">Total deuda</th><th style="border:1px solid black">Vencimiento</th></tr></thead>';
          foreach($ventas->result() as $n=>$c){
          	$back = $n%2==0?'background:#eee;':'';
          	$correo.= '<tr style="'.$back.'""><td style="border:1px solid black;">'.$c->cliente.'</td><td style="border:1px solid black;">'.$c->deudas.'</td><td style="border:1px solid black;">'.$c->productos.'</td><td style="border:1px solid black;">'.$c->total_deuda.'</td><td style="border:1px solid black;">'.$c->vencimiento.'</td></tr>';
          	$this->enviarcorreo($c,22);
          }
          $correo.= '</table>';
          correo('sales@discotecarecords.com','Lista de pagos sin cobrar',$correo);

        }

        
    }
?>
