<body class="woocommerce woocommerce-page kopa-subpage empresa">
<div id="main-content">
	[menu]
		<header class="page-header have-disc-icon text-center">
			<div class="disc-bg"></div>
            <div class="mask"></div>

            <div class="page-header-bg-2 page-header-bg"></div>

            <div class="page-header-inner page-header-inner-1">

                <div class="wrapper">
                    
                    <h1 class="page-title"><?= l('nuestra-empresa') ?></h1>

                    <div class="breadcrumb clearfix">                    
                        <span itemtype="http://data-vocabulary.org/Breadcrumb" itemscope="">
                            <a href="<?= base_url() ?>" itemprop="url">
                                <span itemprop="title"><?= l('inicio') ?></span>
                            </a>
                        </span>
                        <span>&nbsp;/&nbsp;</span>
                        <span itemtype="http://data-vocabulary.org/Breadcrumb" itemscope="">
                            <a href="<?= base_url() ?>" itemprop="url">
                                <span itemprop="title"><?= l('empresa') ?></span>
                            </a>
                        </span>
                        <!-- <span>&nbsp;/&nbsp;</span> -->
                        <!-- <span itemtype="http://data-vocabulary.org/Breadcrumb" itemscope="" class="current-page"><span itemprop="title">Typography</span></span> -->
                    </div>
                    <!-- breadcrumb -->

                </div>
                <!-- wrapper -->

                </div>
                <!-- page-header-inner -->

                <div class="album-icon">
	                <div class="icon-inner-1">
	                    <span class="icon-inner-2"></span>
	                </div>
	                <span class="fa fa-music"></span>
	            </div>
            
        </header>
        <!-- page-header -->
		<div class="wrapper clearfix  kopa-flickr-widget">
			<section class="elements-box mb-40">
				<div class="row">
					<div class="col-xs-12 col-sm-6">
			            
			            <div class="row">
			                <div class="col-md-11 col-sm-12 col-xs-12 mb-40">
			                    <?= l('empresa1') ?>
			                </div>
			            </div>
			            <!-- row -->
		            </div>
		            <div class="col-xs-12 col-sm-6">
		            	<div class="element-title">
		                    <?= l('empresa2') ?>
		                </div>

		                <div class="row">

		                    <div class="col-md-12 col-sm-12 col-xs-12 mb-40">
		                        
		                        <div class="pro-bar-container color-midnight-blue">
		                            <div class="pro-bar bar-85 color-kopa-gradient" data-pro-bar-percent="85" data-pro-bar-delay="400">
		                                <?= l('recopilatorios') ?>
		                            </div>
		                        </div>

		                        <div class="pro-bar-container color-midnight-blue">
		                            <div class="pro-bar bar-75 color-kopa-gradient" data-pro-bar-percent="75" data-pro-bar-delay="500">
		                                <?= l('empresa_maxis') ?>
		                            </div>
		                        </div>

		                        <div class="pro-bar-container color-midnight-blue">
		                            <div class="pro-bar bar-60 color-kopa-gradient" data-pro-bar-percent="60" data-pro-bar-delay="600">
		                                <?= l('empresa-eventos') ?>
		                            </div>
		                        </div>

		                    </div>
		                    <!-- col-md-12 -->
		                    
		                </div>
		                <!-- row -->
		            </div>
				</div>
	            
	        </section>
	        </div>


			<div class="kopa-area kopa-area-4 kopa-parallax kopa-area-dark">
	        <div class="wrapper clearfix">

        	<div id="container" class="mb-40">

                <div id="content" role="main">

	        	<div class="clear"></div>

                    <ul class="products">

                        <li class="product">
                            
                            <div>
                                
                                <div class="product-thumb">
                                	<a href="#">        
	                                    
	                                    <img src="[base_url]theme/theme/images/img/11_1.png" alt="">                                    
	                                </a>
	                                <div class="button-box text-center">
	                                	<a href="#"><i class="fa fa-heart"></i></a>
	                                	
	                                </div>
                                </div>
                                <!-- product-thumb -->
                                 <a href="#">
                                    <h3><?= l('pedro-morales') ?></h3>
                                    <footer>                                    	
                                    	<span class="price"><span class="amount"><?= l('empresa-pedro') ?></span>
                                    	
                                    </footer>                                    
                                </a>

                                <a class="button add_to_cart_button product_type_simple" href="mailto:info@discotecarecords.com"><?= l('contactar') ?></a>

                            </div>                                    

                        </li>
                        <!-- end:product -->

                        <li class="product">
                            
                            <div>
                                
                                <div class="product-thumb">
                                	<a href="#">        
	                                    
	                                    <img src="[base_url]theme/theme/images/img/11.png" alt="">                                    
	                                </a>
	                                <div class="button-box text-center">
	                                	<a href="#"><i class="fa fa-heart"></i></a>
	                                	
	                                </div>
                                </div>
                                <!-- product-thumb -->
                                <a href="#">
                                    <h3><?= l('rafa-carmona') ?></h3>
                                    <footer>                                    	
                                    	<span class="price"><span class="amount">A&R Director</span>
                                    	
                                    </footer>                                    
                                </a>

                                <a class="button add_to_cart_button product_type_simple clearfix" href="mailto:info@discotecarecords.com"><?= l('contactar') ?></a>

                            </div>                                    

                        </li>
                        <!-- end:product -->

                        <li class="product">
                            
                            <div>
                                
                                <div class="product-thumb">
                                	<a href="#">
	                                    <img src="[base_url]theme/theme/images/img/11_2.png" alt="">
	                                </a>
	                                <div class="button-box text-center">
	                                	<a href="#"><i class="fa fa-heart"></i></a>
	                                	
	                                </div>
                                </div>
                                <!-- product-thumb -->
                                <a href="#">
                                    <h3>Quim Quer</h3>
                                    <footer>                                    	
                                    	<span class="price"><span class="amount">Studio</span>
                                    	
                                    </footer>                                    
                                </a>
                                <a class="button add_to_cart_button product_type_simple" href="mailto:info@discotecarecords.com"><?= l('contactar') ?></a>

                            </div>                                    

                        </li>
                        <!-- end:product -->

                        
                        
                    </ul>
                    <!-- end:products -->
                </div>
            </div>
		</div>
		</div>
	[footer]
</div>