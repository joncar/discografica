<?php 
class Elements extends CI_Model{
	function __construct(){
		parent::__construct();
	}

	function tracks($album_id){
		$this->db->order_by('orden','ASC');
		$data = $this->db->get_where('digitales',array('albums_id'=>$album_id));
		foreach($data->result() as $n=>$d){
			$data->row($n)->audio = base_url('tracks/'.$d->audio);
		}
		return $data;
	}

	function albums($where = array(),$limit = '',$activo = NULL){		
		if(!empty($where)){
			$this->db->where($where);
		}
		if(!empty($limit)){
			$this->db->limit($limit);
		}
		$this->db->select('albums.*, artistas.nombre as artista');
		$this->db->join('artistas','artistas.id = albums.artistas_id','LEFT');
		if($activo==NULL){
			$this->db->where('activo',1);
		}		
		$this->db->order_by('id','DESC');
		$albums = $this->db->get('albums');
		foreach($albums->result() as $n=>$v){
			$albums->row($n)->caratula = base_url('img/albums/'.$v->caratula);
			$albums->row($n)->caratula_grande = base_url('img/albums/'.$v->caratula_grande);
			$albums->row($n)->disco =  !empty($v->disco)?base_url('img/albums/'.$v->disco):'';
			$albums->row($n)->disco2 = !empty($v->disco2)?base_url('img/albums/'.$v->disco2):'';
			$albums->row($n)->disco3 = !empty($v->disco3)?base_url('img/albums/'.$v->disco3):'';
			$albums->row($n)->disco4 = !empty($v->disco4)?base_url('img/albums/'.$v->disco4):'';
			$albums->row($n)->promo_radio_descarga = !empty($v->promo_radio_descarga)?base_url('radio/'.$v->promo_radio_descarga):'';
			$albums->row($n)->link = base_url('album/'.toUrl($v->identificador_url));
			$albums->row($n)->link_radio = base_url('radios/'.toUrl($v->identificador_url));
			$albums->row($n)->_lanzamiento = $v->lanzamiento;
			$albums->row($n)->lanzamiento = date("d-m-Y",strtotime($v->lanzamiento));
			$albums->row($n)->_precio = $v->precio;
			$albums->row($n)->precio = moneda($v->precio);
			$albums->row($n)->pvr = moneda($v->pvr);
			$albums->row($n)->playlists = $this->playlists($v->id);			
			$albums->row($n)->demo = !empty($v->demo)?base_url().'audios/'.$v->demo:'';
			$albums->row($n)->type = '';
			$albums->row($n)->tracks = $this->tracks($v->id);
		}
		return $albums;
	}

	function promos_tiendas($where = array(),$limit = ''){
		$where['promos_tienda'] = 1;		
		//$where['promos_importadores'] = 0;		
		//$where['activo'] = 1;
		$albums = $this->albums($where,'',1);

		foreach($albums->result() as $n=>$v){
			$albums->row($n)->_precio = $v->precio_tienda;
			$albums->row($n)->precio = moneda($v->precio_tienda);
			$albums->row($n)->link = base_url('shops/'.$v->identificador_url);
			$albums->row($n)->type = 'shops';
			$albums->row($n)->id = "'".$v->id.'.1\'';
		}

		return $albums;
	}

	function getMasVentas(){		
		$albums = $this->db->query("SELECT albums.*, COUNT(productos_id)+cantidad as ventas FROM ventas_detalles INNER JOIN albums ON albums.id = ventas_detalles.productos_id group by productos_id ORDER BY ventas DESC LIMIT 5");
		foreach($albums->result() as $n=>$v){
			$album = $this->albums(array('albums.id'=>$v->id));
			if($album->num_rows()>0){
				$album = $album->row();
				$albums->row($n)->caratula = base_url('img/albums/'.$album->caratula);
				$albums->row($n)->caratula_grande = base_url('img/albums/'.$album->caratula_grande);
				$albums->row($n)->disco = base_url('img/albums/'.$album->disco);
				$albums->row($n)->disco2 = !empty($album->disco2)?base_url('img/albums/'.$album->disco2):'';
				$albums->row($n)->link = base_url('album/'.toUrl($album->titulo));
				$albums->row($n)->_lanzamiento = $album->lanzamiento;
				$albums->row($n)->lanzamiento = date("d-m-Y",strtotime($album->lanzamiento));
				$albums->row($n)->_precio = $album->precio;
				$albums->row($n)->precio = moneda($album->precio);
				$albums->row($n)->playlists = $this->playlists($album->id);			
				$albums->row($n)->demo = !empty($album->demo)?base_url().'audios/'.$album->demo:'';
				$albums->row($n)->artista = $album->artista;
			}
		}
		return $albums;
	}

	function playlists($album_id){
		$playlists = $this->db->get_where('playlists',array('albums_id'=>$album_id));
		$playlists->json_list = array();
		foreach($playlists->result() as $p){
			$playlists->json_list[] = $p;
		}
		return $playlists;
	}

	function is_tienda(){
		$tienda = $this->user->getAccess('funciones.*',array('funciones.nombre'=>'tiendas'));
		return $tienda->num_rows()>0?true:false;
	}

	function tarifas_tiendas($where = array()){
		$this->db->select('tarifas_tiendas_precios.*');
		$this->db->join('tarifas_tiendas','tarifas_tiendas.id = tarifas_tiendas_provincias.tarifas_tiendas_id');
		$this->db->join('tarifas_tiendas_precios','tarifas_tiendas.id = tarifas_tiendas_precios.tarifas_tiendas_id');
		$tarifas = $this->db->get_where('tarifas_tiendas_provincias',$where);
		return $tarifas;
	}

	

	function getTarifaEuropa($pais,$peso){
		$tarifa = 0;
		$tarifas = $this->db->get_where('tarifas_envio_europa',array('paises_id'=>$pais));
		if($tarifas->num_rows()>0){
			foreach($tarifas->result() as $t){
				if($t->peso_desde<=$peso && $t->peso_hasta>=$peso){
					$tarifa = $t->precio;
				}
			}
			$tarifa = $tarifa == 0?$tarifas->row($tarifas->num_rows()-1)->precio:$tarifa;
		}
		return str_replace(',','.',$tarifa);
	}

	function get_tarifa_tienda($provincias_id,$peso){
		$tarifas = $this->tarifas_tiendas(array('provincias_id'=>$provincias_id));
		$tarifa = 0;
		foreach($tarifas->result() as $t){
			if($t->peso_desde<=$peso && $t->peso_hasta>=$peso){
				$tarifa = ceil($peso)*$t->precio;
			}
		}
		if($tarifa==0 && $peso>=5 && $tarifas->num_rows()>0){
			$tarifa = $tarifas->row($tarifas->num_rows()-1)->precio;
		}
		return str_replace(',','.',$tarifa);
	}

	function get_tarifa_provincias($provincia,$peso){
		$tarifa = 0;
		$tarifas = $this->db->get_where('tarifas_envio_provincias',array('provincias_id'=>$provincia));		
		if($tarifas->num_rows()>0 && $peso>0){
			foreach($tarifas->result() as $t){
				if($t->peso_desde<=$peso && $t->peso_hasta>=$peso){
					$tarifa = ceil($peso)*$t->precio;
				}
			}
			$tarifa = $tarifa == 0 && $tarifas->num_rows()>0?$tarifas->row($tarifas->num_rows()-1)->precio:$tarifa;
		}
		return str_replace(',','.',$tarifa);
	}

	function getPeso($round = true,$importadores = false){
		$pesoVinilos = 0;
        $pesoCajaAdicional = 0;
        $cantidadVinilos = 0;
        $pesoTotal = 0;
        $tax = 0;        
        $carrito = $this->carrito->getCarrito();
        foreach($carrito as $c){
        	$e = $this->db->get_where('albums',array('id'=>$c->id))->row();                    	
        	if(!$importadores || ($importadores && $e->promos_importadores==1)){
	        	$peso = $e->peso*$c->cantidad;
	            $pesoTotal+= $peso;
	            $pesoVinilos+= $e->vinilo==1?$peso:0;
	            $cantidadVinilos+= $e->vinilo==1?$c->cantidad:0;
        	}
        }
        
        $pesoCajaAdicional = ceil($cantidadVinilos/5)*0.110; 
        $pesoTotal+=$pesoCajaAdicional;        
        return $round?ceil($pesoTotal):$pesoTotal;
	}

	function is_import(){
		$carrito = $this->carrito->getCarrito();
		if(empty($carrito)){
			return false;
		}		
		foreach($carrito as $c){
			if($c->promos_importadores==1){				
				return true;
			}
		}
		return false;
	}

	function getTarifaEspanya($provincia,$peso){
		$tarifa = 0;
		$peso = $this->getPeso(false);						
		$pesoImports = $this->getPeso(false,true);				
		$peso-= $pesoImports;		
		$peso = ceil($peso);	
		
		//die();
		if($this->is_tienda()){			
			if($peso<5){
				$tarifa = 0;
			}else{				
				$tarifa = $this->get_tarifa_tienda($provincia,$peso);
			}
		}else{
			$tarifa = $this->get_tarifa_provincias($provincia,$peso);
		}

		return $tarifa;
	}
}