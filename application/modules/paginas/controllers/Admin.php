<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Admin extends Panel{
        function __construct() {
            parent::__construct();
        } 
        
        function loadView($view = ''){
            if(is_string($view)){
                $output = $this->load->view($view,array(),TRUE);
                $view = array('view'=>'panel','crud'=>'user','output'=>$output);
            }
            parent::loadView($view);
        }
        
        function paginas($action = '',$id = ''){
            switch($action){
                case 'add':
                    $this->loadView('cms/add');
                break;
                case 'insert':
                    $this->form_validation->set_rules('nombre','Nombre','required');                    
                    if($this->form_validation->run()){
                        $content = '';
                        if(!empty($_POST['template'])){                            
                            $template = new DOMDocument();
                            @$template->loadHTML(file_get_contents('theme/'.$_POST['template']));                            
                            $xpath = new DOMXPath($template);
                            $xpath_resultset =  $xpath->query("//div[@id='content']");
                            $content = $template->saveHTML($xpath_resultset->item(0));
                            $content = str_replace('src="images/','src="'.base_url().'img/',$content);
                            $content = str_replace('src="img/','src="'.base_url().'img/',$content);                            
                        }
                        $content = $this->load->view('cms/empty',array('content'=>$content),TRUE);
                        file_put_contents('application/modules/paginas/views/'.$_POST['nombre'].'.php',$content);
                        header("Location:".base_url('paginas/frontend/editor/'.str_replace('.php','',$_POST['nombre'])));
                        exit;
                    }else{
                        header("Location:".base_url('paginas/admin/paginas/add?msj='.urlencode('Debe llenar los datos faltantes')));
                        exit;
                    }
                break;
                case 'edit':
                    if(!empty($_POST['data']) && !empty($id)){
                        $name = $_POST['name'];
                        $data = $_POST['data'];
                        $data = str_replace('<formm','<form',$data);
                        $data = str_replace('</formm','</form',$data);
                        file_put_contents('application/modules/paginas/views/'.$name.'.php',$data);   
                    }
                break;
                case 'file_upload': 
                    $size = getimagesize($_FILES['image']['tmp_name']);
                    $extension = $_FILES['image']['type'];
                    $extension = explode('/',$extension);
                    $extension = count($extension>1)?$extension[1]:$extension[0];
                    $name = $id.'-'.date("dmHis").'.'.$extension;
                    if(move_uploaded_file($_FILES['image']['tmp_name'],'images/'.$name)){
                        echo json_encode(array('success'=>true,'name'=>$name,'size'=>array($size[0],$size[1])));
                    }else{
                        echo json_encode(array('success'=>false,'name'=>$name));
                    }
                break;
                case 'delete':
                    unlink('application/modules/paginas/views/'.$id);
                    redirect(base_url('paginas/admin/paginas'));
                break;
                case 'traductor':
                    $textos = $this->getCrudsIdiomas();
                    $this->loadView(
                        array(
                            'view'=>'panel',
                            'crud'=>'user',
                            'output'=>$this->load->view('cms/traductor',array('textos'=>$textos),TRUE),
                            'js_files'=>$textos['es']->js_files,
                            'css_files'=>$textos['es']->css_files
                        )
                    );
                    
                break;
                case 'lang':
                default:
                    if(empty($action) || !empty($id)){
                        $idiomas = explode(', ',$this->db->get_where('ajustes')->row()->idiomas);
                        $id = empty($action)?$idiomas[0]:$id;
                        if(is_dir('application/modules/paginas/views/'.$id)){
                            $pages = scandir('application/modules/paginas/views/'.$id);
                        }else{
                            $pages = array();
                        }
                        $this->loadView(array('view'=>'panel','crud'=>'user','output'=>$this->load->view('cms/list',array('files'=>$pages,'folder'=>$id),TRUE)));
                    }
                break;
                    
            }            
        }

        function getCrudsIdiomas(){
            $idiomas = $this->db->get('ajustes')->row()->idiomas;
            $idiomas = explode(',',$idiomas);
            $textos = array();
            foreach($idiomas as $i){
                $textos[trim($i)] = $this->get_crud(trim($i));
            }
            return $textos;
        }

        function get_crud($idioma,$return = 'return'){
            
            $crud = new ajax_grocery_crud();
            $crud->set_table('traducciones')
                 ->set_theme('bootstrap2')
                 ->set_subject(trim($idioma))
                 ->where('idioma',trim($idioma))
                 ->field_type('idioma','hidden',trim($idioma))
                 ->field_type('original','editor',array('type'=>'textarea'))
                 ->field_type('traduccion','editor',array('type'=>'textarea'))
                 ->unset_columns('idioma');
            $crud->required_fields_array();
            $crud->set_url('paginas/admin/get_crud/'.$idioma.'/show/');
            $crud = $crud->render();
            if($return=='return'){
                return $crud;
            }else{
                $crud->output = '<a href="'.base_url('paginas/admin/paginas/traductor').'" class="btn btn-info">Volver a idiomas</a>'.$crud->output;
                $this->loadView($crud);
            }
        }


        function editor($lang = 'ca'){
            $page = $this->load->view('cms/editor',array('lang'=>$lang),TRUE);
            //$name = date("dmy");
            $name = '180618_'.$lang;
            file_put_contents('theme/editor/'.$name.'.php',$page);
            header('Location:'.base_url().'theme/editor/'.$name.'.php');
        }



        function ftp(){
            $this->loadView('cms/elfinder');
        }

        function verImg($connector = 0){
            if($connector==0){
                $this->load->view('cms/_elfinder_img');
            }else{
                require_once APPPATH.'libraries/elfinder/connector.minimal_img.php';                
            }
        }

        function verFtp($connector = 0){
            if($connector==0){
                $this->load->view('cms/_elfinder');
            }else{
                require_once APPPATH.'libraries/elfinder/connector.minimal.php';                
            }
        }

        function slider($x = '',$y = ''){
            $crud = $this->crud_function("","");                         
            $crud->field_type('foto','image',array('path'=>'img','width'=>'1676px','height'=>'1078px'))
                 ->field_type('idioma','dropdown',array('es'=>'Español','en'=>'Ingles'));
            $crud->set_clone();
            $crud = $crud->render();
            $this->loadView($crud);
        }

        function categoria_galeria($x = '',$y = ''){
            $crud = $this->crud_function("","");                         
            $crud->add_action('Adm. Fotos','',base_url('paginas/admin/galeria').'/');
            $crud = $crud->render();
            $this->loadView($crud);
        }
        
        function galeria($x = '',$y = ''){
            if(is_numeric($x) || $x=='upload_file' || $x=='delete_file' || $x == 'image_ajax_list' || $x == 'ordering'){
                $this->load->library('image_crud');
                $crud = new image_CRUD();
                $crud->set_table('galeria')
                         ->set_url_field('foto')
                         ->set_image_path('img/galeria')
                         ->set_subject('Galeria')
                         ->set_relation_field('categoria_galeria_id')
                         ->set_ordering_field('orden');
                $crud->module = 'paginas';
                $crud = $crud->render();            
                $crud->title = 'Galeria fotográfica';
                $this->loadView($crud);
            }else{
                $this->as['galeria'] = 'categoria_galeria';
                $crud = $this->crud_function("",""); 
                $crud->add_action('<i class="fa fa-image"></i> Adm. fotos','',base_url('paginas/admin/galeria/').'/');                                       
                $crud = $crud->render();
                $this->loadView($crud);
            }
        }

        function videos($x = '',$y = ''){
            $crud = $this->crud_function("","");
            //$crud->set_order('orden');
            $crud = $crud->render();
            $this->loadView($crud);
        }



        public function slider_main(){
            $crud = $this->crud_function('','');                    
            $crud = $crud->render();
            $this->loadView($crud);
        }
    }
?>
