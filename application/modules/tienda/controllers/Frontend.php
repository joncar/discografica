<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Frontend extends Main{        
        function __construct() {
            parent::__construct();
            $this->load->model('tienda_model');
            $this->load->library('form_validation');
        } 
        
        function tienda(){
            $this->loadView(array('view'=>'tienda','title'=>'Reserva','url'=>base_url('store')));
        }

        function listado($categoria){
            $ur = $categoria;
            $id = explode('-',$categoria);
            $id = $id[0];
            if(is_numeric($id)){
                $reserva = $this->db->get_where('categorias',array('id'=>$id));
                if($reserva->num_rows()>0){
                    $cat = 'Reserva en '.strtolower(strip_tags($reserva->row()->nombre_categoria));
                    $this->loadView(array('view'=>'listado','categoria'=>$categoria,'title'=>$cat,'url'=>base_url('store/'.$ur)));
                }else{
                    redirect('store');
                }
            }else{
                redirect('store');
            }
        }

        function listado_test($categoria){
            $ur = $categoria;
            $id = explode('-',$categoria);
            $id = $id[0];
            if(is_numeric($id)){
                $reserva = $this->db->get_where('categorias',array('id'=>$id));
                if($reserva->num_rows()>0){
                    $cat = 'Reserva en '.strtolower(strip_tags($reserva->row()->nombre_categoria));
                    $this->loadView(array('view'=>'listado_test','categoria'=>$categoria,'title'=>$cat,'url'=>base_url('store/'.$ur)));
                }else{
                    redirect('store');
                }
            }else{
                redirect('store');
            }
        }

        function producto($producto){
            $ur = $producto;
            $id = explode('-',$producto);
            $id = $id[0];
            if(is_numeric($id)){
                $reserva = $this->db->get_where('productos',array('id'=>$id));
                if($reserva->num_rows()>0){
                    $categoria = $this->db->get_where('categorias',array('id'=>$reserva->row()->categorias_id));
                    $categoria = strtolower(strip_tags($categoria->row()->nombre_categoria));
                    $cat = 'Reserva en '.$categoria.' '.strip_tags($reserva->row()->nombre_producto);
                    $this->loadView(array('view'=>'producto','producto'=>$producto,'title'=>$cat,'url'=>base_url('store/produto/'.$ur)));
                }else{
                    redirect('store');
                }                
                
            }else{
                redirect('store');
            }
        }


        function ver_carro(){
            $this->loadView('carrito');
        }

        function checkout(){    
            if(!empty($_SESSION['envio'])){
                $_POST = $_SESSION['envio'];
                //unset($_SESSION['envio']);
            }
            
            $this->form_validation->set_rules('paises_id','Pais','required')                                  
                                  ->set_rules('ciudad','Ciudad','required')
                                  ->set_rules('direccion','Dirección','required')
                                  ->set_rules('codigo_postal','Código Postal','required')
                                  ->set_rules('telefono','Teléfono','required')
                                  ->set_rules('telefono2','Confirmar telefono','required|matches[telefono]')
                                  ->set_rules('pasarela','Pasarela de pago','required')
                                  ->set_rules('politicas','Politicas de privacidad','required');
            if($this->form_validation->run() || !empty($_SESSION['envio'])){
                $carrito = $this->carrito->getCarrito(); 
                if(count($carrito)==0){
                    header("Location:".base_url('tienda_carrito').'.html');
                }else{    

                    if($_POST['paises_id']==1 && empty($_POST['provincias_id'])){
                        $_SESSION['msj'] = $this->error('Por favor seleccione una provincia');
                        header("Location:".base_url('tienda_carrito').'.html');
                        die();
                    }
                    if($_POST['paises_id']!=1 && $_POST['pasarela']==3){
                        $_SESSION['msj'] = $this->error('Transacción no permitida');
                        header("Location:".base_url('tienda_carrito').'.html');
                        die();
                    }
                    if($this->user->log){
                        if($_POST['paises_id']==1 && empty($_POST['provincias_id'])){
                            $_SESSION['msj'] = $this->error('Seleccione una provincia');
                            header("Location:".base_url('tienda_carrito').'.html#payment');
                            die();
                        }
                        //Guardamos en la bd
                        $_POST['pasarela'] = empty($_GET['pasarela'])?$_POST['pasarela']:$_GET['pasarela'];
                        $this->tienda_model->cleanVentas();
                        $id = $this->tienda_model->saveVenta();
                        $this->pagar_articulo($id);
                        unset($_SESSION['envio']);
                    }else{                        
                        $_SESSION['envio'] = $_POST;
                        redirect('registro/index/add?redirect=tt/checkout&pasarela='.$_POST["pasarela"]);
                    }
                }
            }else{
                $_SESSION['msj'] = $this->error($this->form_validation->error_string());
                header("Location:".base_url('tienda_carrito').'.html#payment');
            }

        }

        function pagar_articulo($id){
            $this->db->select('ventas_detalles.*, ventas.total,ventas.productos');
            $this->db->join('albums','albums.id = ventas_detalles.productos_id');
            $this->db->join('ventas','ventas.id = ventas_detalles.ventas_id');
            $this->db->join('provincias','provincias.id = ventas.provincias_id','left');
            $this->db->join('paises','paises.id = ventas.paises_id');
            $detalle = $this->db->get_where('ventas_detalles',array('ventas_id'=>$id));
            $total = 0;    

            if($detalle->num_rows()>0){
                $total= $detalle->row()->total;                
                if($_POST['pasarela']==1){                        
                    $miObj = new RedsysAPI;
                    // Valores de entrada                    
                    $merchantCode   ="349021543"; //Usuario TEST
                    $key = 'sq7HjrUOBfKmC576ILgskD5srU870gJ7';//Clave secreta del terminal TEST
                    $key = 'R6tiPKCHIX0haTUxCJHvlv6RXbMnxel8';//Clave secreta Modo real
                    $terminal       ="1";
                    $amount         =round($total*100,0);
                    $currency       ="978";
                    $transactionType    ="0";
                    $merchantURL    =base_url('tt/procesarPago');
                    $urlOK      =base_url('tt/pagoOk/'.$id);
                    $urlKO      =base_url('tt/pagoKo/'.$id);
                    $order      = $id.'-'.date("s");
                    $miObj->setParameter("DS_MERCHANT_AMOUNT",$amount);
                    $miObj->setParameter("DS_MERCHANT_ORDER",strval($order));
                    $miObj->setParameter("DS_MERCHANT_MERCHANTCODE",$merchantCode);
                    $miObj->setParameter("DS_MERCHANT_CURRENCY",$currency);
                    $miObj->setParameter("DS_MERCHANT_TRANSACTIONTYPE",$transactionType);
                    $miObj->setParameter("DS_MERCHANT_TERMINAL",$terminal);
                    $miObj->setParameter("DS_MERCHANT_MERCHANTURL",$merchantURL);
                    $miObj->setParameter("DS_MERCHANT_URLOK",$urlOK);       
                    $miObj->setParameter("DS_MERCHANT_URLKO",$urlKO);
                    $version="HMAC_SHA256_V1";                    
                    $request = "";
                    $params = $miObj->createMerchantParameters();
                    $signature = $miObj->createMerchantSignature($key);
                    //echo '<br/>'.$total;
                    //die();
                    echo '<div style="text-align:center">Redireccionando al banco, por favor espere</div>';                    
                    $this->db->update('ventas',array('forma_pago'=>'TPV'),array('id'=>(INT)$id));
                    $this->load->view('goTpv',array('version'=>$version,'params'=>$params,'signature'=>$signature));
                    if(!empty($_SESSION['carrito'])){
                        unset($_SESSION['carrito']);
                    }
                }elseif($_POST['pasarela']==2){
                    echo '<div style="text-align:center">Redireccionando a paypal</div>';
                    $this->db->update('ventas',array('forma_pago'=>'PayPal'),array('id'=>(INT)$id));
                    $this->load->view('goPaypal',array('detalle'=>$detalle->row(),'total'=>$total));
                    if(!empty($_SESSION['carrito'])){
                        unset($_SESSION['carrito']);
                    }
                }elseif($_POST['pasarela']==3){ //Contra reembolso
                    //Contrarembolso
                    $ajustes = $this->db->get('ajustes')->row();
                    $rembolso = $total * ($ajustes->porcentaje_contrarembolso/100);
                    $rembolso = $rembolso < $ajustes->minimo_contrarembolso?$ajustes->minimo_contrarembolso:$rembolso;
                    $total+= $rembolso;
                    $this->db->update('ventas',array('forma_pago'=>'Contrareembolso','precio_rembolso'=>str_replace(',','.',$rembolso),'total'=>str_replace(',','.',$total)),array('id'=>(INT)$id));
                    $this->tienda_model->pagoOk($id);
                    if(!empty($_SESSION['carrito'])){
                        unset($_SESSION['carrito']);
                    }
                    redirect(base_url('tt/pagoOk/'.$id));
                }
                elseif($_POST['pasarela']==4){ //Deposito
                    if($total>75){
                        //Contrarembolso
                        
                        $this->tienda_model->pagoOk($id);       
                        $this->db->update('ventas',array('forma_pago'=>'Deposito','total'=>str_replace(',','.',$total),'pagado'=>0),array('id'=>(INT)$id));             
                        if(!empty($_SESSION['carrito'])){
                            unset($_SESSION['carrito']);
                        }
                        redirect(base_url('tt/pagoOk/'.$id));
                    }else{
                        $_SESSION['msj'] = $this->error('La compra no supera el mínimo establecido');
                        header("Location:".base_url('tienda_carrito').'.html');
                        die();
                    }
                }
            }else{
                redirect('carrito');
            }
        }

        public function addToFav($prod = ''){
            if($this->user->log && !empty($prod) && is_numeric($prod) && $prod>0){                
                $favs = $this->db->get_where('favoritos',array('albums_id'=>$prod,'user_id'=>$this->user->id));
                if($favs->num_rows()==0){
                    $this->db->insert('favoritos',array('albums_id'=>$prod,'user_id'=>$this->user->id));
                }
            }
        }

        public function refreshCartForm(){
            $this->load->view('includes/fragmentos/carritoForm');
        }
        
        public function carrito($venta_id = ''){
            $this->loadView('carrito');
        }

        public function addToCart($prod = '',$cantidad = '',$return = TRUE,$sumarcantidad = TRUE){
            if(strpos($prod,'.')>-1){
                $prod = explode('.',$prod)[0];
                $shops = 1;
            }else{
                $shops = 0;
            }

            if(strpos($prod,'D')>-1){
                list($digital,$prod) = explode('D',$prod);
            }

            if(!empty($prod) && is_numeric($prod) && $prod>0 && is_numeric($cantidad) && $cantidad>0){                
                $sumarcantidad = $sumarcantidad=='0'?FALSE:$sumarcantidad;
                $this->db->where('albums.id',$prod);
                $this->db->where('albums.stock >',0);
                //$this->db->where('activo',1);
                $producto = $this->carrito->getProductos();
                if($producto->num_rows()>0){
                    $producto = $producto->row();
                    $producto->cantidad = $cantidad;
                    if($shops==1){
                        $cart = $this->carrito->getCarrito();
                        if(empty($cart[0]->shop)){
                            //Limpiamo el carro
                            unset($_SESSION['carrito']);
                        }
                        $producto->precio = $producto->precio_tienda;
                        $producto->shop = 1;
                    }else{
                        $cart = $this->carrito->getCarrito();
                        if(!empty($cart[0]->shop)){
                            //Limpiamo el carro
                            unset($_SESSION['carrito']);
                        }
                    }
                    if(!empty($digital)){
                        $digital = $this->db->get_where('digitales',array('id'=>$digital));
                        if($digital->num_rows()>0){
                            $digital = $digital->row();
                            $producto->titulo = $digital->nombre.'-'.$producto->titulo;
                            $producto->id = $producto->id.'D'.$digital->id;
                            $producto->digital = $digital;
                        }
                    }
                    $this->carrito->setCarrito($producto,$sumarcantidad);
                }                
            }
            if($return){
                $this->load->view('_carrito');
            }
        }
        
        public function addToCartArray(){
            $response = 'success';
            foreach($_POST['id'] as $n=>$v){
                $this->addToCart($v,$_POST['cantidad'][$n]);
            }
            $total = 0;
            foreach($_SESSION['carrito'] as $n=>$v){                        
                $total+= ($v->precio*$v->cantidad);
            }
            echo $response;
        }
        
        public function delToCart($prod = ''){
            if(strpos($prod,'D')>-1){
                list($digital,$prods) = explode('D',$prod);
            }
            if(!empty($prod) && (is_numeric($prod) || isset($digital)) && $prod>0){                
                $this->carrito->delCarrito($prod);                
            }
            $this->load->view($this->theme.'_carrito_content',array(),FALSE,'paginas');
        }

        public function delToCartNav($prod = ''){            
            if(strpos($prod,'D')>-1){
                list($digital,$prods) = explode('D',$prod);
            }
            if(!empty($prod) && (is_numeric($prod) || isset($digital)) && $prod>0){                
                $this->carrito->delCarrito($prod);                
            }
            $this->load->view('_carrito');   
        }

        function getForm(){
            $this->load->view($this->theme.'_carrito_content',array(),FALSE,'paginas');
        }

        function procesarPagoTest(){      
            $this->tienda_model->pagoOk('684');                     
        }

        function procesarPago(){      
            //$this->tienda_model->pagoOk('449');         
            $this->load->library('redsysapi');
            $miObj = new RedsysAPI;
            if (!empty($_POST)){                
                $datos = $_POST["Ds_MerchantParameters"];
                $decodec = json_decode($miObj->decodeMerchantParameters($datos));
                if(!empty($decodec) && ($decodec->Ds_Response=='0000' || $decodec->Ds_Response=='0001' || $decodec->Ds_Response=='0002' || $decodec->Ds_Response=='0099') && empty($decodec->Ds_ErrorCode)){                    
                    $id = $decodec->Ds_Order;
                    $id = explode('-',$id)[0];                                    
                    $this->tienda_model->pagoOk($id);
                }else{                    
                    $id = $decodec->Ds_Order;
                    $id = explode('-',$id)[0];                  
                    $this->tienda_model->pagoNoOk($id,$decodec);
                }
            }
        }

        function procesarPagoPaypal(){
            //correo('joncar.c@gmail.com','TPV Response',print_r($_POST,TRUE));
            /*$_POST = array(
                'payment_status'=>'Completed',
                'item_number'=>'400'
            );*/
            if (!empty($_POST)){                
                $d = $_POST;                              
                if(!empty($d['payment_status']) && ($d['payment_status']=='Pending' || $d['payment_status']=='Completed') && !empty($d['item_number']) && is_numeric($d['item_number'])){                    
                    $id = $d['item_number'];                                    
                    $this->tienda_model->pagoOk($id);
                }else{
                    $id = $d['item_number'];                  
                    $this->tienda_model->pagoNoOk($id,$decodec);
                }
            }
        }

        function pagoOk($id = ''){
            $this->loadView(array('view'=>'pagoOk','title'=>'Resultado de compra'));
        }

        function pagoKo($id = ''){
            $this->loadView(array('view'=>'pagoKo','title'=>'Resultado de compra'));
        }
    }
