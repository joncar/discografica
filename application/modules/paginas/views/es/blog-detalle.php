<body class="kopa-single-page kopa-subpage">
	[menu]
	<div id="main-content">

        <header class="page-header text-center">

        	<div class="disc-bg"></div>

            <div class="mask"></div>

        	<div class="page-header-bg-1 page-header-bg" style="background:url([foto])"></div>

        	<div class="page-header-inner page-header-inner-1">

        		<div class="wrapper">
                
	                <h1 class="page-title">[titulo]</h1>	                
	                <!-- breadcrumb -->

	            </div>
	            <!-- wrapper -->
        		
        	</div>
        	<!-- page-header-inner -->

            <div class="album-icon">
                <div class="icon-inner-1">
                    <span class="icon-inner-2"></span>
                </div>
                <span class="fa fa-music"></span>
            </div>       
            
        </header>
        <!-- page-header -->
	<div class="wrapper">

        	<div class="entry-box standard-post">                

                <div class="entry-content-wrap" style="margin-top:150px;">

                    <div class="left-col">
                        <!--<div class="rating-score">
                            <div class="rating-score-inner">
                                <p>9.7</p>
                                <div class="pro-bar-container color-midnight-blue">
                                    <div class="pro-bar bar-75 color-green" data-pro-bar-percent="75" data-pro-bar-delay="500"></div>
                                </div>
                            </div>                            
                        </div>-->
                        <div class="about-author">
                            <div class="author-avatar">
                                <a href="#"><img alt="" src="<?= base_url() ?>img/fotos/78bda-1.jpg" /></a>
                            </div>
                            <div class="author-content">
                                <h5>Por <a href="#">[user]</a></h5>
                                <!--<footer class="text-center">
                                    <ul class="social-links clearfix">
                                        <li><a href="#" class="fa fa-facebook"></a></li>
                                        <li><a href="#" class="fa fa-twitter"></a></li>
                                        <li><a href="#" class="fa fa-google-plus"></a></li>
                                    </ul>
                                </footer>                                -->
                            </div>
                        </div>
                    </div>

                    <div class="entry-content clearfix" style="min-height: 230px;">

                        [texto]
                        
                    </div>
                    <!-- entry-content -->
                    
                </div>
                <!-- entry-content-wrap -->


                <div class="tag-box text-center">

                    <span><i class="fa fa-tags"></i>Tags:</span>

                    [tags]
                    
                </div>
                <!-- tag-box -->

            </div>
            <!-- entry-box -->


            <div id="related-post">

                <h3 class="text-center"><?= l('noticias-relacionadas') ?></h3>

                <div class="row">

                    [foreach:relacionados]
                        <div class="col-md-4 col-sm-4">

                            <article class="entry-item">
                                
                                <div class="entry-content">
                                    <span class="entry-date">[fecha]</span>
                                    <h4 class="entry-title entry-title-s1"><a href="[link]">[titulo]</a></h4>
                                </div>

                                <div class="entry-thumb">
                                    <a href="[link]"><img src="[foto]" alt=""></a>
                                    <div class="mask"><a href="#"><i class="fa fa-plus"></i></a></div>
                                </div>
                                
                            </article>
                            
                        </div>
                        <!-- col-md-4 -->
                    [/foreach]                    
                    
                </div>
                <!-- row -->
                
            </div>
            <!-- related-post -->

            

        </div>
        <!-- wrapper -->
        
    </div>
    <!-- main-content -->
	[footer]