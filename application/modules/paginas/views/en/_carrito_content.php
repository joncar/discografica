<section class="elements-box" style="margin-bottom: 40px;">
	<div class="woocommerce">
		<form action="<?= base_url('tt/checkout') ?>" method="post">
		<?php 
			$carrito = $this->carrito->getCarrito(); 
			$total = 0; 
			$impuesto = 0;
		?>
		<?php if(count($carrito)>0): ?>


		<div class="row">
			<div class="col-xs-12 col-sm-6 col-md-8">
				<h2>Productos</h2>
		
				<table class="shop_table shop_table_responsive cart table-cart" cellspacing="0" style="background: white;">
					<thead>
						<tr>
							<th class="product-remove">&nbsp;</th>
							<th class="product-thumbnail hidden-xs hidden-sm">&nbsp;</th>
							<th class="product-name">Producto</th>
							<th class="product-price">Precio</th>
							<th class="product-quantity">Cantidad</th>
							<th class="product-quantity">Peso</th>
							<th class="product-subtotal">Total</th>
						</tr>
					</thead>
					<tbody>
						<?php 
							$pesoVinilos = 0;
							$pesoCajaAdicional = 0;
							$cantidadVinilos = 0;
							$pesoTotal = 0;
							foreach($carrito as $c): 
							if(!empty($c->shop)){
								$e = $this->elements->promos_tiendas(array('albums.id'=>$c->id))->row(); 
							}else{
								$e = $this->elements->albums(array('albums.id'=>$c->id))->row(); 
							}
							
						?>
							<tr class="cart_item">
								<td class="product-remove">
									<a href="javascript:remToCart(<?= $c->id ?>)" class="remove" title="Remove this item" data-product_id="226" data-product_sku="">×</a>
								</td>
								<td class="product-thumbnail hidden-xs hidden-sm">
									<a href="#">
										<img class="hidden-xs" src="<?= $e->caratula ?>" class="attachment-shop_thumbnail size-shop_thumbnail wp-post-image" alt="post-34">
									</a>
								</td>
								<td class="product-name" data-title="Product">
									<a href="#">
										<img class="visible-xs" style="width:100%;" src="<?= $e->caratula ?>" class="attachment-shop_thumbnail size-shop_thumbnail wp-post-image" alt="post-34">										
										<?= $e->titulo ?>
									</a>
								</td>
								<td class="product-price" data-title="Price">
									<span class="amount"><?= moneda($e->precio) ?></span>
								</td>
								<td class="product-quantity" data-title="Quantity">
									<div class="quantity buttons_added">
										<input step="1" min="0" name="cantidad" value="<?= $c->cantidad ?>" title="Qty" class="input-text qty text" data-id="<?= !empty($c->shop)?$c->id.'.1':$c->id ?>" size="4" type="number">
									</div>
								</td>
								<td class="product-quantity" data-title="Quantity">
									<div class="quantity buttons_added">
										<?php
											$peso = $e->peso*$c->cantidad;
											$pesoTotal+= $peso;
											$pesoVinilos+= $e->vinilo==1?$peso:0;
											$cantidadVinilos+= $e->vinilo==1?$c->cantidad:0;
											echo $peso;
										?>kg
									</div>
								</td>
								<td class="product-subtotal" data-title="Total">
									<span class="amount"><?= moneda($c->cantidad*$e->_precio) ?></span>
								</td>
							</tr>
							<?php $total+= ($c->cantidad*$e->_precio); ?>
							<?php endforeach ?>
							
						</tbody>
					</table>



					<div  id="australiaMessage" class="woocommerce-checkout-payment" style="margin:14px 0;background: #9b5e8f;color: #fff;padding: 14px; display: none;">
                        <div class="form-row place-order">
                            <p><b>Note: </b>Always i ship registered mail , for the returned Ítems, the buyer, always pay the shipping costs and also certified.</p>
                            <p>
                                In orders to Australia the buyer should know that a 10% increase must be added to the total purchase + shipping for the tax (GST) that the Australian government imposes.
                            </p>
                        </div>
                    </div>
	

					<div id="direccionEnvio">
						<h2>Dirección de envío</h2>					
						<div id="payment" style="padding: 20px;">
							<div class="form-group">
								<label>País</label>
								<select name="paises_id" id="paises_id" class="form-control">
									<option value="">Seleccione una opción</option>
									<?php 
										$this->db->order_by('paises.nombre','ASC');
										foreach($this->db->get_where('paises')->result() as $p): 
									?>
										<option value="<?= $p->id ?>" data-continente="<?= $p->continentes_id ?>" data-tarifa="<?= $p->tarifa ?>"><?= $p->nombre ?></option>
									<?php endforeach ?>
								</select>
							</div>
							<div class="form-group" id="provForm">
								<label>Provincia</label>
								<select name="provincias_id" id="provincias_id" class="form-control">
									<option value="">Seleccione una opción</option>
									<?php 
										$this->db->order_by('provincias.nombre','ASC');
										foreach($this->db->get_where('provincias')->result() as $p):
										if($this->elements->is_tienda()){
											if($pesoTotal<5){
												$tarifa = 0;
											}else{
												$tarifa = $this->elements->get_tarifa_tienda($p->id,$pesoTotal);
											}
										}else{
											$tarifa = $p->tarifa;
										}
									?>
										<option value="<?= $p->id ?>" data-tarifa="<?= $tarifa ?>"><?= $p->nombre ?></option>
									<?php endforeach ?>
								</select>
							</div>
							<div class="form-group">
								<label>Ciudad</label>
								<input class="form-control" type="text" id="ciudad" name="ciudad" value="<?= @$this->user->ciudad ?>" placeholder="Ciudad donde se realizará el envío">
							</div>
							<div class="form-group">
								<label>Dirección</label>
								<input class="form-control" type="text" id="direccion" name="direccion" value="<?= @$this->user->direccion ?>" placeholder="Dirección donde se realizará el envío">
							</div>
							<div class="form-group">
								<label>CP</label>
								<input class="form-control" type="text" id="codigo_postal" name="codigo_postal" value="<?= @$this->user->codigo_postal ?>" placeholder="Código postal donde se realizará el envío">
							</div>
							<div class="form-group">
								<label>Teléfono</label>
								<input class="form-control" type="text" id="telefono" name="telefono" value="<?= @$this->user->telefono ?>" placeholder="Teléfono de contacto">
							</div>
							<div class="form-group">
								<label>Confirmar Teléfono</label>
								<input class="form-control" type="text" id="telefono2" name="telefono2" value="<?= @$this->user->telefono ?>" placeholder="Confirmar Teléfono de contacto">
							</div>
							<div class="form-group">
								<label>Comments</label>
								<textarea class="form-control" type="text" id="telefono" name="observaciones" placeholder="Write your comments here"></textarea>
							</div>
						</div>		
					</div>	

					<div class="woocommerce-checkout-payment"  style="margin-top:14px;background: #9b5e8f;color: #fff;padding: 14px;">
						<div class="form-row place-order">
							<p>
								<b>Solo para tiendas en territorio nacional*:</b>
							</p>
							<p>							
								Los gastos de envio del pedido son a cargo de Discoteca Records, excepto en pedidos con peso superior
								a 5 kg. pues en este caso se factura añadido el 50%  de los gastos totales de transporte a dicho envio.
							</p>
							<p>										 
								En caso de devolución los gastos serán a cargo del cliente, avisandonos previamente de este envio a al email:
								<a href="mailto:contabilidad@discotecarecords.com" style="color:#fff;text-decoration: underline !important;">contabilidad@discotecarecords.com</a>, el envio adebe realizarse mediante correo certificado o agencia
								de transporte indicandonos el número de seguimiento en ambos a nuestra direccíon en:
							</p>
							<p>
								Discoteca Records (entrada Hyserca) C/ Francesc Duran I Reynals, Nº 9 , 08192 - Sant Quirze del Vallés – Barcelona
							</p>
							<p>
								*Pedidos internacionales tramitados sin estas condiciones, contactar directamente en <a href="mailto:sales@discotecarecords.com" style="color:#fff;text-decoration: underline !important;">sales@discotecarecords.com</a>
							</p>
						</div>
					</div>			
			</div>
			<div class="col-xs-12 col-sm-6 col-md-4">
				<h2>Tu pedido</h2>
				<table class="shop_table shop_table_responsive cart" cellspacing="0" style="background:white;">
					<tbody>
						<tr class="cart-subtotal">
							<th>Subtotal</th>
							<td data-title="Subtotal" style="width:80%; text-align:right"><span class="amount"><?= moneda($total) ?></span></td>
						</tr>						
						<?php if($cantidadVinilos>0): /* Quitar comentario para motivos de calculo y testing */ ?>
							<!--<tr class="shipping">
								<th>Peso por embalaje</th>
								<td data-title="Shipping" style="width:80%; text-align:right">
									<span class="amount" id="precioPeso">
										<?php 
											$pesoCajaAdicional = ceil($cantidadVinilos/5)*0.110; 
											echo number_format($pesoCajaAdicional,3,',','.');
										?>kg
									</span>
								</td>
							</tr>-->
						<?php endif ?>
						<tr class="shipping">
							<th>Peso</th>
							<td data-title="Shipping" style="width:80%; text-align:right">
								<span class="amount" id="precioPeso"><?= number_format($pesoTotal+$pesoCajaAdicional,3,',','.') ?>kg</span>
							</td>
						</tr>						
						<tr class="shipping">
							<th>Envío</th>
							<td data-title="Shipping" style="width:80%; text-align:right">
								<span class="amount" id="precioEnvio">0€</span>
							</td>
						</tr>
						<tr class="taxaustralia" style="display: none">
							<th style="width:80%">10% tax for goverment Australian</th>
							<td data-title="Shipping" style="width:80%; text-align:right">
								<span class="amount" id="preciotaxaustralia">0€</span>
							</td>
						</tr>
						<tr class="taxaustralia">
							<th style="width:80%">VAT</th>
							<td data-title="Shipping" style="width:80%; text-align:right">
								<span class="amount" id="preciotaxaustralia">INCLUIDED</span>
							</td>
						</tr>
						<tr class="order-total">
							<th>Total</th>
							<td data-title="Total" style="width:80%; text-align:right">
								<strong>
								<span class="amount" id="total"><?= moneda($total) ?></span>
								</strong>
							</td>
						</tr>
					</tbody>
				</table>

				<h2>Forma de pago</h2>
				<h5>Seleccione una forma de pago</h5>
					<div id="payment" class="woocommerce-checkout-payment" style="margin-top:30px;">						
						<ul class="wc_payment_methods payment_methods methods">
							<li class="wc_payment_method payment_method_cheque">
								<input id="paymenttpv" class="input-radio" name="pasarela" value="1" checked="checked" data-order_button_text="" type="radio">
								<label for="payment_method_cheque">
									TPV <img src="https://canales.redsys.es/iconos/graficos/logotipos/2100logo.jpg" style="width:84%;">
								</label>
								<div class="payment_box payment_method_cheque" style="display:none;">
									<p>Directamente a la web del banco Caixa</p>
								</div>
							</li>
							<li class="wc_payment_method payment_method_paypal">
								<input id="payment_method_paypal" class="input-radio" name="pasarela" value="2" data-order_button_text="Proceed to PayPal" type="radio">
								<label for="payment_method_paypal">
								PayPal <img src="https://www.paypalobjects.com/webstatic/mktg/Logo/AM_mc_vs_ms_ae_UK.png" alt="PayPal Acceptance Mark"><a href="https://www.paypal.com/gb/webapps/mpp/paypal-popup" class="about_paypal" onclick="javascript:window.open('https://www.paypal.com/gb/webapps/mpp/paypal-popup','WIPaypal','toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, width=1060, height=700'); return false;" title="What is PayPal?">What is PayPal?</a>	</label>
								<div class="payment_box payment_method_paypal" style="display:none;">
									<p>Pago vía paypal</p>
								</div>
							</li>
							<li id="paymentcr" style="display: none" class="wc_payment_method payment_method_paypal">
								<input id="rembolso" class="input-radio" name="pasarela" value="3" data-order_button_text="" type="radio">
								<label for="rembolso">
								CR <img src="<?= base_url() ?>img/contra.png" style="width:60%;">
								<div class="payment_box rembolso">
									<?php $ajustes = $this->db->get('ajustes')->row(); ?>
									<p>Cargo del <?= $ajustes->porcentaje_contrarembolso ?>% del total de la compra con un minímo de <?= $ajustes->minimo_contrarembolso ?>€</p>
								</div>
							</li>
							<?php if($this->user->getAccess('funciones.*',array('funciones.nombre'=>'tiendas'))->num_rows()>0): ?>
								<li id="paymentcr" class="wc_payment_method payment_method_paypal">
									<input id="deposito" class="input-radio" name="pasarela" value="4" data-order_button_text="" type="radio">
									<label for="deposito">
										Depósito 
									</label>
									<div class="payment_box deposito">										
										<p>Como tienda si deseas usar el sistema de artículos en depósito, tienes que tener en cuenta las siguientes condiciones:</p>
										<ol style="padding:0 10px">
											<li>Los importes tienen que ser superiores a 75€.</li>
											<li>La liquidación de las compras serán cada dia 25 del mes siguiente a la fecha de compra.</li>
											<li>El depósito de los artículos no debe superar los 90 dias sin liquidar, debiendo estos abonarse o devolverse.</li>
										</ol>
									</div>
								</li>
							<?php endif ?>
						</ul>
						<div class="checkbox" style="margin-left: 13px;">
							<label for="">
								<input type="checkbox" name="politicas" value="1"> He leído y acepto las <a href="<?= base_url('aviso-legal') ?>.html">Politícas de privacidad</a>
							</label>
						</div>
						<div class="form-row place-order">
							<?php echo @$_SESSION['msj']; unset($_SESSION['msj']); ?>
							<noscript>
							Since your browser does not support JavaScript, or it is disabled, please ensure you click the <em>Update Totals</em> button before placing your order. You may be charged more than the amount stated above if you fail to do so.			<br/><input type="submit" class="button alt" name="woocommerce_checkout_update_totals" value="Update totals" />
							</noscript>
							<input class="button alt" name="woocommerce_checkout_place_order" id="place_order" value="Pagar" type="submit">
						</div>
					</div>

					<div class="woocommerce-checkout-payment" style="margin-top:14px;background: #9b5e8f;color: #fff;padding: 14px;">
						<div class="form-row place-order">
							<p>Contact our accounting department to provide your company's information and have your corresponding invoice.
  							<a href="#" style="color: #fff;text-decoration: underline !important;">contabilidad@discotecarecords.com</a></p>													
						</div>
					</div>
				
			</div>
		</div>
	

</form>
<script>
	var total = <?= str_replace(',','.',$total) ?>;
	var peso = <?= str_replace(',','.',$pesoTotal+$pesoCajaAdicional) ?>;
	var peso_tienda = <?= $this->elements->is_tienda()?str_replace(',','.',$pesoTotal+$pesoCajaAdicional):0 ?>;
	var tarifas = <?php 
		$tarifas = array();
		foreach($this->db->get_where('tarifas_envio')->result() as $t){
			$tarifas[] = $t;
		}
		echo json_encode($tarifas);
	?>
</script>
<?php else: ?>
	El carrito se encuentra vacio
<?php endif ?>
	</div>
	
	<!-- page-links-wrapper -->
	<!-- tag-box -->
</section>