<!-- wrapper -->
<div class="dark-bg">
	
	<div class="wrapper clearfix">
		<div class="widget kopa-tab-widget">
			<div class="kopa-tab style1">
				<ul class="nav nav-tabs">
                    <li class="active"><a href="#e-tab21" data-toggle="tab">SELECCION ESPECIAL</a></li>
                    <li><a href="#e-tab22" data-toggle="tab">MÁS VISITADOS</a></li>
                    <li><a href="#e-tab23" data-toggle="tab">MÁS RECIENTES</a></li>
                </ul>
				<div class="tab-content">
					<div class="tab-pane active" id="e-tab21">
						<ul class="row">
							
							<?php for($i=1;$i<=6;$i++): ?>
                                <li class="col-md-2 col-sm-2 col-xs-4">
                                    <article class="entry-item">
                                        <div class="entry-thumb">
                                            <a href="#"><img src="[base_url]theme/theme/images/img/9_<?= $i ?>.png" alt=""></a>
                                            <div class="thumb-hover">
                                                <a href="#" class="thumb-icon"><i class="fa fa-plus"></i></a>
                                            </div>
                                        </div>
                                    </article>
                                </li>
                            <?php endfor ?>
						</ul>
						<!-- row -->
					</div>
					<div class="tab-pane" id="e-tab22">
						
						<ul class="row">
							
							<li class="col-md-2 col-sm-2 col-xs-3">
								<article class="entry-item">
									<div class="entry-thumb">
										<a href="[base_url]albums-detail.html"><img src="[base_url]theme/theme/images/img/9.png" alt=""></a>
										<div class="thumb-hover">
											<a href="#" class="thumb-icon"><i class="fa fa-plus"></i></a>
										</div>
									</div>
								</article>
							</li>
							<!-- col-md-2 -->
							<li class="col-md-2 col-sm-2 col-xs-3">
								<article class="entry-item">
									<div class="entry-thumb">
										<a href="[base_url]albums-detail.html"><img src="[base_url]theme/theme/images/img/9.png" alt=""></a>
										<div class="thumb-hover">
											<a href="#" class="thumb-icon"><i class="fa fa-plus"></i></a>
										</div>
									</div>
								</article>
							</li>
							<!-- col-md-2 -->
							<li class="col-md-2 col-sm-2 col-xs-3">
								<article class="entry-item">
									<div class="entry-thumb">
										<a href="[base_url]albums-detail.html"><img src="[base_url]theme/theme/images/img/9.png" alt=""></a>
										<div class="thumb-hover">
											<a href="#" class="thumb-icon"><i class="fa fa-plus"></i></a>
										</div>
									</div>
								</article>
							</li>
							<!-- col-md-2 -->
							<li class="col-md-2 col-sm-2 col-xs-3">
								<article class="entry-item">
									<div class="entry-thumb">
										<a href="[base_url]albums-detail.html"><img src="[base_url]theme/theme/images/img/9.png" alt=""></a>
										<div class="thumb-hover">
											<a href="#" class="thumb-icon"><i class="fa fa-plus"></i></a>
										</div>
									</div>
								</article>
							</li>
							<!-- col-md-2 -->
							<li class="col-md-2 col-sm-2 col-xs-3">
								<article class="entry-item">
									<div class="entry-thumb">
										<a href="[base_url]albums-detail.html"><img src="[base_url]theme/theme/images/img/9.png" alt=""></a>
										<div class="thumb-hover">
											<a href="#" class="thumb-icon"><i class="fa fa-plus"></i></a>
										</div>
									</div>
								</article>
							</li>
							<!-- col-md-2 -->
							<li class="col-md-2 col-sm-2 col-xs-3">
								<article class="entry-item">
									<div class="entry-thumb">
										<a href="[base_url]albums-detail.html"><img src="[base_url]theme/theme/images/img/9.png" alt=""></a>
										<div class="thumb-hover">
											<a href="#" class="thumb-icon"><i class="fa fa-plus"></i></a>
										</div>
									</div>
								</article>
							</li>
							<!-- col-md-2 -->
						</ul>
						<!-- row -->
					</div>
					<div class="tab-pane" id="e-tab23">
						
						<ul class="row">
							
							<li class="col-md-2 col-sm-2 col-xs-3">
								<article class="entry-item">
									<div class="entry-thumb">
										<a href="[base_url]albums-detail.html"><img src="[base_url]theme/theme/images/img/9.png" alt=""></a>
										<div class="thumb-hover">
											<a href="#" class="thumb-icon"><i class="fa fa-plus"></i></a>
										</div>
									</div>
								</article>
							</li>
							<!-- col-md-2 -->
							<li class="col-md-2 col-sm-2 col-xs-3">
								<article class="entry-item">
									<div class="entry-thumb">
										<a href="[base_url]albums-detail.html"><img src="[base_url]theme/theme/images/img/9.png" alt=""></a>
										<div class="thumb-hover">
											<a href="#" class="thumb-icon"><i class="fa fa-plus"></i></a>
										</div>
									</div>
								</article>
							</li>
							<!-- col-md-2 -->
							<li class="col-md-2 col-sm-2 col-xs-3">
								<article class="entry-item">
									<div class="entry-thumb">
										<a href="[base_url]albums-detail.html"><img src="[base_url]theme/theme/images/img/9.png" alt=""></a>
										<div class="thumb-hover">
											<a href="#" class="thumb-icon"><i class="fa fa-plus"></i></a>
										</div>
									</div>
								</article>
							</li>
							<!-- col-md-2 -->
							<li class="col-md-2 col-sm-2 col-xs-3">
								<article class="entry-item">
									<div class="entry-thumb">
										<a href="[base_url]albums-detail.html"><img src="[base_url]theme/theme/images/img/9.png" alt=""></a>
										<div class="thumb-hover">
											<a href="#" class="thumb-icon"><i class="fa fa-plus"></i></a>
										</div>
									</div>
								</article>
							</li>
							<!-- col-md-2 -->
							<li class="col-md-2 col-sm-2 col-xs-3">
								<article class="entry-item">
									<div class="entry-thumb">
										<a href="[base_url]albums-detail.html"><img src="[base_url]theme/theme/images/img/9.png" alt=""></a>
										<div class="thumb-hover">
											<a href="#" class="thumb-icon"><i class="fa fa-plus"></i></a>
										</div>
									</div>
								</article>
							</li>
							<!-- col-md-2 -->
							<li class="col-md-2 col-sm-2 col-xs-3">
								<article class="entry-item">
									<div class="entry-thumb">
										<a href="[base_url]albums-detail.html"><img src="[base_url]theme/theme/images/img/9.png" alt=""></a>
										<div class="thumb-hover">
											<a href="#" class="thumb-icon"><i class="fa fa-plus"></i></a>
										</div>
									</div>
								</article>
							</li>
							<!-- col-md-2 -->
						</ul>
						<!-- row -->
					</div>
				</div>
			</div>
		</div>
		<!-- kopa-tab-widget -->
		
	</div>
	<!-- wrapper -->
</div>
