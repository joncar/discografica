<div class="loading">
    <div>
        <img src="<?= base_url() ?>img/vinilo.gif">
    </div>
</div>
<header id="navbar-spy" class="full-header header-3">
    <nav id="primary-menu" class="navbar navbar-fixed-top style-1">
        <div id="top-bar" class="top-bar hidden-xs hidden-sm">
            <div class="container">
                <div class="row" style="display: flex">
                    <div>
                        <ul class="list-inline top-widget">
                            <li class="top-social" style="white-space: nowrap;">
                                <a href="https://www.facebook.com/discotecarecords" target="_new"><i class="fa fa-facebook"></i></a>
                                <a href="https://www.instagram.com/discotecarecords/" target="_new"><i class="fa fa-instagram"></i></a>                            
                                <a href="https://www.youtube.com/channel/UCY1ZZKx4FWJnharejnMskBQ" target="_new"><i class="fa fa-youtube"></i></a> 
                                <a href="mailto:info@discotecarecords.com"><i class="fa fa-envelope" style="padding-left: 8px;"></i></a>                            
                                
                            </li>                            
                        </ul>
                        
                    </div>
                    
                    <!-- .col-md-6 end -->
                    
                    <div>
                        <ul class="list-inline top-contact">
                            <li>
                                <p>
                                    <a href="<?= base_url() ?>festival-80.html"><i class="botonnegro"><?= l('promos_80_festival') ?></i></a>
                                </p>
                            </li>
                            <li>
                                <p>
                                    <a href="<?= base_url() ?>especial-imports.html"><i class="transparent"><?= l('spetial-imports') ?></i></a>
                                </p>
                            </li>
                            <li>
                                <p>
                                    <?php if(!$this->user->log): ?>
                                        <a href="<?= base_url('registro/index/add') ?>?redirect=radios"><i class="transparent"><?= l('promos-radio') ?></i></a>                                                                                
                                    <?php else: ?>
                                        <a href="<?= base_url('radios') ?>"><i class="transparent"><?= l('promos-radio') ?></i></a>                                                                                
                                    <?php endif ?>
                                    
                                </p>
                            </li>
                            <li>
                                <p>
                                    <?php if(!$this->user->log): ?>                                                                
                                        <a href="<?= base_url('registro/index/add') ?>?redirect=shops"><i class="transparent"><?= l('tiendas') ?></i></a>
                                    <?php else: ?>                                        
                                        <a href="<?= base_url('shops') ?>"><i class="transparent"><?= l('tiendas') ?></i></a>
                                    <?php endif ?>
                                    
                                </p>
                            </li>
                            <li>
                                <p>
                                    <a class="favoriosMenuLink" href="<?= base_url('favoritos') ?>">
                                        <i class="fa fa-heart"></i>
                                        <?= l('favoritos') ?>                                        
                                    </a> 
                                 </p>
                            </li>
                            <li class="has-dropdown" style="position: relative;">
                                <p>
                                    <a href="#" class="favoriosMenuLink"><?= l('nosotros') ?> <i class="fa fa-chevron-down"></i></a>                           
                                </p>
                                <ul class="dropdown-menu">
                                     <li>
                                        <a href="[base_url]empresa.html"><?= l('empresa') ?></a>
                                     </li>
                                     <li>
                                        <a href="[base_url]blog"><?= l('blog') ?></a>
                                    </li>
                                    <li>
                                        <a href="[base_url]contacto.html"><?= l('contactenos') ?></a>                           
                                    </li>
                                    <!-- li end -->
                                    <li>
                                        <a href="[base_url]galeria.html"><?= l('galeria') ?></a>      
                                    </li>
                                 </ul>
                            </li>
                            <li>
                                <p>
                                    <?php if(!$this->user->log): ?>
                                        <a href="<?= base_url('panel') ?>"><i><?= l('cuenta') ?></i></a>
                                        <a href="<?= base_url('panel') ?>"><i><?= l('registro') ?></i></a>
                                    <?php else: ?>
                                        <a href="<?= base_url('cuenta') ?>"><i><?= l('cuenta') ?></i></a>
                                        <a href="<?= base_url('main/unlog') ?>"><i><?= l('salir') ?></i></a>
                                    <?php endif ?>                                                
                                </p>
                            </li>
                            <li>
                                <p class="flags">
                                    <?php if($_SESSION['lang']=='es'): ?>
                                    <a href="#" class="spain"></a>
                                    <a href="<?= base_url('main/traduccion/en') ?>" class="active england"></a>
                                    <?php else: ?>
                                    <a href="<?= base_url('main/traduccion/es') ?>" class="active spain"></a>
                                    <a href="#" class="england"></a>
                                    <?php endif ?>
                                </p>
                            </li>
                        </ul>
                    </div>
                    <!-- .col-md-6 end -->
                </div>
            </div>
        </div>
        <div class="row">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">                    
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a href="<?= base_url() ?>tienda_carrito.html" class="navbar-toggle responsiveCart">
                        <i class="fa fa-shopping-cart"></i>
                        <span class="badge badge-success carCount">0</span>
                    </a>
                    <a class="logo" href="<?= base_url() ?>">
                        <img src="[base_url]theme/theme/placeholders/logook.png" alt="Yellow Hats">
                    </a>
                </div>
                
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse pull-right" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-left">                        
                        <li class="visible-xs">
                            <a href="<?= base_url() ?>festival-80.html"><?= l('promos_80_festival') ?></a>                           
                        </li>
                        <?php foreach($this->db->get_where('tipos_album',array('mostrar_menu'=>1))->result() as $t): ?>
                        <?php $discos = $this->elements->albums(array('tipo'=>$t->id)); ?>
                            <li class="has-dropdown <?= $discos->num_rows()>15?'mega':'' ?>">
                                <a href="[base_url]albums-<?= toUrl($t->id) ?>"><?= $t->nombre ?> <i class="fa fa-chevron-down"></i></a>                                
                                <ul class="dropdown-menu">
                                    <?php foreach($discos->result() as $a): ?>
                                         <li>
                                            <a href="<?= $a->link ?>"><?= strip_tags($a->titulo) ?></a>
                                        </li>
                                    <?php endforeach ?> 
                                </ul>
                            </li>
                        <?php endforeach ?>
                        <!-- li end -->
                        <?php if(!$this->user->log): ?> 
                        <li class="visible-xs">
                            <a href="<?= base_url('registro/index/add') ?>?redirect=radios"><?= l('promos-radio') ?></a>                           
                        </li>
                        <!-- li end -->
                        <li class="visible-xs">
                            <a href="<?= base_url('registro/index/add') ?>?redirect=shops"><?= l('tiendas') ?></a>                           
                        </li>
                        <!-- li end -->
                        <?php else: ?>
                        <li class="visible-xs">
                            <a href="<?= base_url('radios') ?>"><?= l('promos-radio') ?></a>                           
                        </li>                        
                        <!-- li end -->
                        <li class="visible-xs">
                            <a href="<?= base_url('shops') ?>"><?= l('tiendas') ?></a>                           
                        </li>
                        <?php endif ?>
                        <li class="visible-xs">
                            <a href="<?= base_url() ?>especial-imports.html"><?= l('spetial-imports') ?></a>                           
                        </li>
                        
                        
                        <li class="flags visible-xs">
                            <div>
                                <a href="#" class="spain"></a>
                                <a href="<?= base_url('main/traduccion/en') ?>" class="active england"></a>
                            </div>
                        </li>
                        <!-- li end -->
                    </ul>
                    
                    <!-- Mod-->
                    <div class="module module-search pull-left">
                        <div class="search-icon">
                            <i class="fa fa-search"></i>
                            <span class="title"><?= l('buscar') ?></span>
                        </div>
                        <div class="search-box">
                            <form class="search-form" action="<?= base_url('paginas/frontend/search') ?>">
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="<?= l('escribe-buscar') ?>" name="q">
                                    <span class="input-group-btn">
                                    <button class="btn" type="button"><i class="fa fa-search"></i></button>
                                    </span>
                                </div>
                                <!-- /input-group -->
                            </form>
                        </div>
                    </div>
                    <!-- .module-search-->
                    <!-- .module-cart -->                    
                    <div class="module module-cart pull-left menubar-cart">
                        <?php $this->load->view('_carrito',array(),FALSE,'tienda') ?>
                    </div>
                    <!-- .module-cart end -->
                    <div class="module module-search pull-left visible-xs">
                        <a href="<?= base_url('panel') ?>">
                            <div class="search-icon">
                                <i class="fa fa-user"></i>
                                <span class="title"><?= l('cuenta') ?></span>
                            </div>     
                        </a>                   
                    </div>
                </div>
                <!-- /.navbar-collapse -->
            </div>
            <!-- /.container-fluid -->
        </div>
    </nav>
</header>
