<body class="kopa-subpage kopa-subpage kopa-album-page">
    [menu]
    <div id="main-content">
        <header class="page-header have-disc-icon text-center">
            <div class="disc-bg"></div>
            <div class="mask"></div>
            <!--<div class="typo-bg"></div>-->
            <div class="page-header-bg-1 page-header-bg"></div>
            <div class="page-header-inner">
                <div class="wrapper">
                    
                    <h1 class="page-title">Promos radio</h1>
                    <div class="breadcrumb clearfix">
                        <span itemtype="http://data-vocabulary.org/Breadcrumb" itemscope="">
                            <a href="<?= base_url() ?>" itemprop="url">
                                <span itemprop="title">Inicio</span>
                            </a>
                        </span>
                        <span>&nbsp;/&nbsp;</span>
                        <span itemtype="http://data-vocabulary.org/Breadcrumb" itemscope="" class="current-page"><span itemprop="title">List of audios</span></span>
                    </div>
                    <!-- breadcrumb -->
                </div>
                <!-- wrapper -->
                
            </div>
            <!-- page-header-inner -->
            
            <div class="album-icon">
                <div class="icon-inner-1">
                    <span class="icon-inner-2"></span>
                </div>
                <span class="fa fa-music"></span>
            </div>
            
        </header>
        <!-- page-header -->
        <div class="wrapper clearfix">
            <div class="widget kopa-album-widget">
                <h2 class="widget-title widget-title-s5">List of audios</h2>
	                <div class="widget-content">
	                    <div class="widget-content">
	                        <div class="wrapper">
	                            
	                            <ul class="row kopa-album-list">
	                                <?php 
	                                    $discos = $this->elements->albums(array('promo_radio'=>1));
	                                    foreach($discos->result() as $a): ?>
	                                    <li class="col-md-3 col-sm-4 col-xs-12">
	                                        <?php $this->load->view($this->theme.'_cd_party_radio',array('d'=>$a),FALSE,'paginas'); ?>
	                                    </li>
	                                <? endforeach ?>
	                            </ul>
	                        </div>
	                        <!-- wrapper-1 -->
	                    </div>
	                    <!-- masonry-container -->                                    
	                </div>
            </div>
                <!-- kopa-album-widget -->
        </div>            
            
    </div>
    <!-- main-content -->
    [footer]