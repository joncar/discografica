<?php
    $carrito = $this->carrito->getCarrito();     
    $total = 0;
    $impuestos = 0;
    $cobrarenvio = true;
    foreach($carrito as $c){
        if($c->tipo_producto==3){
            $cobrarenvio = false;
        }
    }
?>

<div class="cart-icon">
    <a href="<?= base_url() ?>tienda_carrito.html" class="show-cart">
        <i class="fa fa-shopping-cart"></i>
        <span class="title"> <?= l('carrito') ?></span>
        <span class="cart-label">
            <?php if(count($carrito)>0): ?>
                <?= count($carrito) ?>
            <?php else: ?>
                0        
            <?php endif ?>        
        </span>
    </a>
</div>
<div class="cart-box">                            
    <div class="cart-overview">
        <ul class="list-unstyled">
            
            <?php foreach($carrito as $c): ?>
            <li>
                <img class="img-responsive" src="<?= base_url().'img/albums/'.$c->caratula ?>" alt="product"/>
                <div class="product-meta">
                    <h5 class="product-title"><?= $c->titulo ?></h5>
                    <p class="product-price"><?= l('precio') ?>: <?= moneda($c->precio) ?></p>
                    <p class="product-quantity"><?= l('cantidad') ?>: <?= $c->cantidad ?></p>
                </div>
                <a class="cancel" href="javascript:remCart('<?= $c->id ?>')"><?= l('cancelar') ?></a>
            </li>
            <?php /*$total+= ($c->cantidad*$c->precio); $impuestos+= $c->cantidad*3.50;*/ ?>
            <?php $total+= ($c->cantidad*$c->precio); ?>
            <?php endforeach ?>          
        </ul>
    </div>
    <?php if(count($carrito)==0): ?> 
        <span style="color:black" class="hidden-xs"><?= l('carrito-vacio') ?></span>
    <?php endif ?>
    <?php if($total>0): ?>
    <div class="cart-total">
        <div class="total-desc">
            <h5><?= l('subtotal') ?>:</h5>
        </div>
        <div class="total-price">
            <h5><?= moneda($total) ?></h5>
        </div>
    </div>
    <?php if($cobrarenvio): ?>
    <div class="cart-total">
        <div class="total-desc">
            <h5><?= l('gastos-de-envio') ?>:</h5>
        </div>
        <div class="total-price">
            <h5><?= moneda($impuestos) ?></h5>
        </div>
    </div>
    <?php endif ?>
    <div class="cart-total">
        <div class="total-desc">
            <h5><?= l('total') ?>:</h5>
        </div>
        <div class="total-price">
            <h5><?= moneda($total+$impuestos) ?></h5>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="cart-control">
        <a class="btn btn-primary" href="<?= base_url() ?>tienda_carrito.html"><?= l('ver-carrito') ?></a>
        <a class="btn btn-secondary pull-right" href="<?= base_url() ?>tienda_carrito.html"><?= l('procesar-pago') ?></a>
    </div>
    <div class="cart-control text-center" style="margin-top: 20px;">
        <a href="javascript:closeCart()" class="" style="color: #e22414;"><?= l('cerrar') ?></a>        
    </div>
    <?php endif ?>
</div>

<script>
    var cartCount = <?= count($carrito)>0?count($carrito):0; ?>;
    $(".carCount").html(cartCount);
</script>