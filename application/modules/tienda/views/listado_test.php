<?php

$page = $this->load->view($this->theme.'tienda_listado_test',array(),TRUE,'paginas');
$categorias = $this->db->get_where('categorias',array('id'=>$categoria));

$this->db->order_by('priority','ASC');
$productos = $this->db->get_where('productos',array('categorias_id'=>$categoria));
foreach($productos->result() as $n=>$v){
	$productos->row($n)->link = base_url().'store/producto/'.toUrl($v->id.'-'.$v->nombre_producto);
	$productos->row($n)->precio = moneda($v->precio);
}
$page = $this->querys->fillFields($page,array('productos'=>$productos));

$page = str_replace('[categoria_foto]',base_url('img/fotos_categorias/'.$categorias->row()->foto),$page);
foreach($categorias->row() as $n=>$v){
	$page = str_replace('['.$n.']',$v,$page);
}


$page = $this->load->view('read',array('page'=>$page),TRUE,'paginas');
echo $page;
?>
