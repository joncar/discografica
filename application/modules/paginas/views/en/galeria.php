<body class="kopa-gallery-page kopa-subpage">
[menu]
<div id="main-content">

        <header class="page-header have-disc-icon text-center">

        	<div class="disc-bg"></div>

        	<div class="mask"></div>

        	<div class="page-header-bg-2 page-header-bg"></div>

        	<div class="page-header-inner page-header-inner-1">

        		<div class="wrapper">
                
	                <h1 class="page-title">Gallery</h1>

	                <div class="breadcrumb clearfix">                    
	                    <span itemtype="http://data-vocabulary.org/Breadcrumb" itemscope="">
	                        <a href="<?= base_url() ?>" itemprop="url">
	                            <span itemprop="title">Home</span>
	                        </a>
	                    </span>	                    
	                    <span>&nbsp;/&nbsp;</span>
	                    <span itemtype="http://data-vocabulary.org/Breadcrumb" itemscope="" class="current-page">
                            <span itemprop="title">Gallery</span>
                        </span>
	                </div>
	                <!-- breadcrumb -->

	            </div>
	            <!-- wrapper -->
        		
        	</div>
        	<!-- page-header-inner -->

        	
        	<div class="album-icon">
                <div class="icon-inner-1">
                    <span class="icon-inner-2"></span>
                </div>
                <span class="fa fa-music"></span>
            </div>             
            
        </header>
        <!-- page-header -->

        <div class="wrapper clearfix">

        	<div class="widget kopa-gallery-widget">

                <div class="masonry-container">

                    <header class="text-center">

                        <ul class="filters clearfix">
                           <li class="active" data-filter="kopa-all">All </li>
                           <?php foreach($this->db->get_where('categoria_galeria')->result() as $g): ?>
                            <li data-filter="cat<?= $g->id ?>"><?= $g->nombre ?></li>
                           <?php endforeach ?>
                           <li data-filter="videos">Videos</li>
                        </ul>
                        
                    </header>
                    
                    <div class="container-masonry clearfix lightgallery">
                        <?php foreach($this->db->get_where('galeria')->result() as $g): ?>
                            <div class="item" data-filter-class='["kopa-all","cat<?= $g->categoria_galeria_id ?>"]'  data-src="<?= base_url('img/galeria/'.$g->foto) ?>">
                                <img src="<?= base_url('img/galeria/'.$g->foto) ?>" alt="">
                                <div class="mask"><a href="#"><i class="fa fa-plus"></i></a></div>
                            </div>
                        <?php endforeach ?>   

                        <?php foreach($this->db->get_where('videos')->result() as $g): ?>
                            <div class="item" data-filter-class='["kopa-all","videos"]'  data-src="<?= $g->url ?>" data-poster="<?= $g->thumb ?>">
                                <img src="<?= $g->thumb ?>" alt="">
                                <div class="mask"><a href="#"><i class="fa fa-plus"></i></a></div>
                            </div>
                        <?php endforeach ?>                      
                    </div>                    
                    
                </div>
                <!-- masonry-container -->
                
            </div>
            <!-- kopa-gallery-widget -->

        </div>
        <!-- wrapper -->
        
    </div>
    <!-- main-content -->
[footer]