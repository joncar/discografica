<?php	
    $this->set_css($this->default_theme_path.'/bootstrap2/css/flexigrid.css');
	$this->set_js_lib($this->default_theme_path.'/bootstrap2/js/jquery.form.js');
	$this->set_js_config($this->default_theme_path.'/bootstrap3/js/flexigrid-edit.js');
	$this->set_js_lib($this->default_javascript_path.'/jquery_plugins/jquery.noty.js');
	$this->set_js_lib($this->default_javascript_path.'/jquery_plugins/config/jquery.noty.config.js');
?>
<div class="panel panel-default flexigrid crud-form" style='width: 100%;' data-unique-hash="<?php echo $unique_hash; ?>">
    <?php echo form_open( $update_url, 'method="post" id="crudForm" autocomplete="off" enctype="multipart/form-data"'); ?>
        <?php foreach($fields as $field): ?>
            <div class="form-row" id="<?php echo $field->field_name; ?>_field_box">
                <label for="name"><?php echo $input_fields[$field->field_name]->display_as; ?> <?php echo ($input_fields[$field->field_name]->required)? '<abbr title="required" class="required">*</abbr></label>' : ""; ?>
                <?php echo $input_fields[$field->field_name]->input ?>
            </div>
        <?php endforeach ?>
        <?php
            foreach($hidden_fields as $hidden_field){
                    echo $hidden_field->input;
            }
        ?>
        <?php if ($is_ajax) { ?><input type="hidden" name="is_ajax" value="true" /><?php }?>
        <div id='report-error' style="display:none" class='alert alert-danger'></div>
        <div id='report-success' style="display:none" class='alert alert-success'></div>
        <div class="form-row">
            <input class="submit" value="<?php echo $this->l('form_save'); ?>" type="submit">
        </div>         
<?php echo form_close(); ?>
</div>
<script>
	var validation_url = '<?php echo $validation_url?>';
	var list_url = '<?php echo $list_url?>';
	var message_alert_edit_form = "<?php echo $this->l('alert_edit_form')?>";
	var message_update_error = "<?php echo $this->l('update_error')?>";
        $("input[type='text'],input[type='password']").addClass('form-control');
</script>