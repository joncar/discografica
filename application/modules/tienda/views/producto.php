<?php

$page = $this->load->view($this->theme.'tienda_detail',array(),TRUE,'paginas');

$productos = $this->db->get_where('productos',array('id'=>$producto));
$categorias = $this->db->get_where('categorias',array('id'=>$productos->row()->categorias_id));
$this->db->order_by('priority','ASC');
$relacionados = $this->db->get_where('productos',array('id !='=>$producto,'categorias_id'=>$categorias->row()->id));
$this->db->select('productos_fotos.foto as fote');
$fotos = $this->db->get_where('productos_fotos',array('productos_id'=>$producto));
foreach($relacionados->result() as $n=>$v){
	$relacionados->row($n)->link = base_url().'store/producto/'.toUrl($v->id.'-'.$v->nombre_producto);
	$relacionados->row($n)->precio = moneda($v->precio);
}

$page = $this->querys->fillFields($page,array('relacionados'=>$relacionados,'fotos'=>$fotos));



$productos->row()->precio = moneda($productos->row()->precio);
foreach($productos->row() as $n=>$v){
	$page = str_replace('['.$n.']',$v,$page);
}


$page = str_replace('[categoria_foto]',base_url('img/fotos_categorias/'.$categorias->row()->foto),$page);
foreach($categorias->row() as $n=>$v){
	$page = str_replace('['.$n.']',$v,$page);
}



$page = $this->load->view('read',array('page'=>$page),TRUE,'paginas');
echo $page;

?>

