<body class="kopa-home-1">
[menu]


<!-- Hero Section
============================================= -->
<section id="hero" class="hero hero-3">
    
    <!-- START REVOLUTION SLIDER 5.0 -->
    <div class="rev_slider_wrapper">
        <div id="slider1" class="rev_slider"  data-version="5.0">
            <ul>
                <?php 
                    $this->db->order_by('orden','ASC');
                    foreach($this->db->get_where('slider',array('idioma'=>$_SESSION['lang']))->result() as $s): 
                ?>
                    <!-- slide 1 -->
                    <li data-transition="zoomout" data-slotamount="default"  data-easein="Power4.easeInOut" data-easeout="Power4.easeInOut" data-masterspeed="2000" style="background-color: rgba(34, 34, 34, 0.3);">
                        <!-- MAIN IMAGE -->
                        <img src="<?= base_url('img/'.$s->foto) ?>"  alt=""  width="1920" height="1280">
                        <!-- LAYER NR. 1 -->
                        <div class="tp-caption" 
                            data-x="center" data-hoffset="0" 
                            data-y="center" data-voffset="-60" 
                            data-whitespace="nowrap"
                            data-width="none"
                            data-height="none"
                            data-transform_idle="o:1;"
                            data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;" 
                            data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" 
                            data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;" 
                            data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" 
                            data-start="500" 
                            data-splitin="none" 
                            data-splitout="none" 
                            data-responsive_offset="on"
                            data-fontsize="['17','17','50','15']"
                            data-lineheight="['45','45','60','25']"
                            data-fontweight="['300','300','300','300']"
                            data-color="#ffffff" style=" margin-bottom:30px;">
                             <?= $s->subtitulo ?>
                        </div>
                        
                        <!-- LAYER NR. 2 -->
                        <div class="tp-caption text-uppercase" 
                            data-x="center" data-hoffset="0" 
                            data-y="center" data-voffset="0" 
                            data-whitespace="nowrap"
                            data-width="none"
                            data-height="none"
                            data-transform_idle="o:1;"
                            data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;" 
                            data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" 
                            data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;" 
                            data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" 
                            data-start="750" 
                            data-splitin="none" 
                            data-splitout="none" 
                            data-responsive_offset="on"
                            data-fontsize="['72','17','50','15']"
                            data-lineheight="['78','45','60','25']"
                            data-fontweight="['400','400','400','400']"
                            data-color="#ffffff" style="font-family: 'Source Sans Pro', sans-serif; margin-bottom:30px; "
                            >                        
                            <?= $s->titulo ?>
                        </div>
                    </li>
                <?php endforeach ?>
            </ul>
        </div>
        <!-- END REVOLUTION SLIDER -->
    </div>    
</section>
<!-- #hero end -->


<div id="main-content" style="padding-bottom: 60px">
    <div class="kopa-rt">        
        <!-- home-slider-box -->
        <div class="kopa-area-fit">
            <div class="widget kopa-audio-list-1-widget">
                <ul class="clearfix">
                    <?php foreach($this->elements->albums(array('mostrar_slider'=>1),4)->result() as $a): ?>
                    <li>
                        <article class="entry-item">
                            <div class="entry-thumb">
                                <a href="#"><img src="<?= $a->caratula_grande ?>" alt=""></a>
                                <div class="thumb-hover"></div>
                            </div>
                            <div class="entry-content" >
                                <!--<h4 class="entry-title"><a href="#"><?= $a->titulo ?></a></h4>
                                <p class="entry-author">por: <a href="#"><?= $a->artista ?></a></p>-->
                                <div id="<?= 'main3cuadros'.$a->id ?>reprod1" class="reproductor jp-jplayer-single-1"></div>
                                <div id="<?= 'main3cuadros'.$a->id ?>reprodControl1" class="jp-audio jp-audio-single1" role="application" aria-label="media player">
                                    <div class="jp-type-single">
                                        <div class="jp-gui jp-interface">
                                            <div class="jp-controls">
                                                <button class="jp-play" role="button" tabindex="0" data-audio="<?= $a->demo ?>" data-title="<?= $a->titulo ?>">play</button>
                                            </div>
                                            <div class="jp-progress">
                                                <div class="jp-seek-bar" style="width:100%;">
                                                    <div class="jp-play-bar"></div>
                                                </div>
                                            </div>
                                            <div class="jp-volume-controls">
                                                <span class="fa fa-volume-down"></span>
                                                <div class="jp-volume-bar">
                                                    <div class="jp-volume-bar-value"></div>
                                                </div>
                                            </div>
                                            <div class="jp-time-holder">
                                                <div class="jp-current-time" role="timer" aria-label="time">00:00</div>
                                                <div class="jp-duration" role="timer" aria-label="duration">00:00</div>
                                            </div>
                                        </div>
                                        <div class="jp-no-solution">
                                            <span>Update Required</span>
                                            To play the media you will need to either update your browser to a recent version or update your <a href="http://get.adobe.com/flashplayer/" target="_blank">Flash plugin</a>.
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </article>
                    </li>
                    <?php endforeach ?>
                </ul>
                
            </div>
            <!-- widget -->
        </div>
        <!-- kopa-area-fit -->
        <!-- kopa-ab -->
    </div>
    
    
    <!-- kopa-area-9 -->
    <div class="widget kopa-album-2-widget">
        <header class="kopa-area-dark">
            
            <div class="kopasliderhome">
                <?php 
                    $this->db->order_by('orden','ASC');
                    foreach($this->db->get('slider_main')->result() as $s): 
                ?>
                    <div class="item">
                        <div class="wrapper">
                            <h3 class="widget-title style1"><?= $s->nombre_en ?></h3>
                            <h5 style="color:white"><?= $s->descripcion_en ?></h5>
                        </div>
                    </div>
                <?php endforeach ?>
                
            </div>

        </header>
        <div class="widget-content">
            <div class="wrapper">
                
                <ul class="row kopa-album-list">
                    <?php 
                        $discos = $this->elements->albums(array('tipo'=>1),8);
                        foreach($discos->result() as $a): ?>
                        <li class="col-xs-12 col-md-3 col-sm-6">
                            <?php $this->load->view('_cd_party',array('d'=>$a)); ?>
                        </li>
                    <? endforeach ?>
                </ul>

                <div style="text-align:center;">
                    <a class="kopa-button-border" style="border:1px solid #000" href="<?= base_url() ?>maxis.html">All</a>
                </div>
            </div>
            <!-- wrapper-1 -->
        </div>
    </div>

    <!-- kopa-area-9 -->
    <div class="widget kopa-album-2-widget" style="background-image: ">
        <header class="kopa-area-dark" style="background:url(<?= base_url() ?>theme/theme/placeholders/parallax/11.gif); background-size: cover; background-position: bottom;">
            <div class="wrapper">
                <h3 class="widget-title style1">TOP COMPILATIONS</h3>
                <!-- <p>Pellentesque elementum libero enim, eget gravida nunc laoreet et. Nullam ac enim auctor imperdiet turpis <br>Mauris ut tristique odio. </p> -->
            </div>
            <!-- wrapper -->
            <div class="album-icon">
                <div class="icon-inner-1">
                    <span class="icon-inner-2"></span>
                </div>
                <span class="fa fa-music"></span>
            </div>
        </header>
        <div class="kopa-area kopa-area-3 kopa-parallax">
            <div class="span-bg"></div>
            <div class="wrapper">
                
                <ul class="row kopa-album-list">
                    <?php 
                        $discos = $this->elements->albums(array('tipo'=>2),8);
                        foreach($discos->result() as $a): ?>
                        <li class="col-xs-12 col-md-3 col-sm-6">
                            <?php $this->load->view('_cd_party',array('d'=>$a)); ?>
                        </li>
                    <? endforeach ?>
                </ul>
                <div style="text-align:center;">
                    <a class="kopa-button-border" style="border:1px solid #000" href="<?= base_url() ?>albums.html">Ver todos</a>
                </div>
            </div>
            <!-- wrapper-1 -->
        </div>
    </div>

    <section class="kopa-area kopa-area-2 kopa-parallax kopa-area-dark">

            <div class="span-bg"></div>

            <div class="wrapper">

                <div class="row">
                
                    <?php $this->db->limit(2); $this->db->order_by('orden','ASC'); ?>
                    <?php foreach($this->db->get_where('videos')->result() as $v): ?>
                    <div class="col-md-6 col-sm-6 col-xs-12">

                        <div class="widget kopa-article-list-widget article-list-2" style="border-bottom:0; padding-bottom:0; margin-bottom:0">
                            <h3 class="widget-title style1"><?= $v->nombre ?></h3>
                            <ul class="row" style="text-align:center">
                                <li class="col-md-12 col-sm-12 col-xs-12 videMainPromo" style="">
                                    <article class="entry-item video-post">
                                        <div class="entry-thumb">
                                            <iframe style="border: 1px solid #fff;" src="<?= $v->embed ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                        </div>                                        
                                    </article>
                                </li>
                            </ul>
                        </div>
                
                    </div>
                    <?php endforeach ?>
                
                </div>
                <!-- row --> 

                <div class="row" style="margin-top:30px">
                    <div class="col-xs-12" style="text-align:center;">
                        <a class="kopa-button-border" style="border:1px solid #000" href="<?= base_url() ?>galeria.html">More</a>
                    </div>
                </div>
            
            </div>
            <!-- wrapper -->
            
        </section>
    
    
    
    <section class="kopa-area kopa-area-5 kopa-parallax kopa-area-dark">
        <div class="span-bg"></div>
        <div class="wrapper">
            <div class="row">
                
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="widget kopa-masonry-widget">
                        <header>
                            <h3 class="widget-title style1">gallery</h3>
                            <!--<p>Fusce hendrerit, mi non lobortis pellentesque, nibh ante accumsan nisi, in tristique nibh nisi nec tortor. <br>Mauris nibh sem, vulputate non consequat in, condimentum eget nulla</p>-->
                        </header>
                        <ul class="kopa-masonry-wrap lightgallery">
                            
                            <?php 
                                $this->db->limit(10);
                                foreach($this->db->get_where('galeria')->result() as $g): 
                            ?>
                                <li class="ms-item1" data-src="<?= base_url('img/galeria/'.$g->foto) ?>">
                                    <div class="entry-item">
                                        <div class="entry-thumb">
                                            <a href="#"><img src="<?= base_url('img/galeria/'.$g->foto) ?>" alt="" style="border:3px solid #fff;"></a>
                                            <div class="thumb-hover">
                                                <a href="#" class="thumb-icon"><i class="fa fa-plus"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            <?php endforeach ?>               
                        </ul>                        
                    </div>
                    <div style="text-align:center;">
                        <a class="kopa-button-border" style="border:1px solid #fff" href="<?= base_url('galeria').'.html' ?>">More</a>
                    </div>
                    <!-- widget -->
                    
                </div>
                <!-- col-md-12 -->
                
            </div>
            <!-- row -->
            
        </div>
        <!-- wrapper -->
        
    </section>
    <!-- kopa-area-5 -->
    <section class="kopa-area kopa-area-10 kopa-area-dark">
        <div class="wrapper">
            <div class="row">
                
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="widget kopa-tab-widget">
                        <div class="kopa-tab style1">
                            <ul class="nav nav-tabs">
                                <li class="active"><a href="#e-tab21" data-toggle="tab">SPECIAL SELECTION</a></li>
                                <li><a href="#e-tab22" data-toggle="tab">MOST VISITED</a></li>
                                <li><a href="#e-tab23" data-toggle="tab">THE LATEST</a></li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="e-tab21">
                                    <ul class="row">
                                        <?php 
                                            $discos = $this->elements->albums(array('seleccion_especial'=>1),6);
                                            foreach($discos->result() as $n=>$a): ?>
                                                <li class="col-md-2 col-sm-2 col-xs-4">
                                                    <article class="entry-item">
                                                        <div class="entry-thumb">
                                                            <a href="<?= $a->link ?>"><img src="<?= $a->caratula ?>" alt=""></a>
                                                            <div class="thumb-hover">
                                                                <a href="<?= $a->link ?>" class="thumb-icon"><i class="fa fa-plus"></i></a>
                                                            </div>
                                                        </div>
                                                    </article>
                                                </li>
                                        <?php endforeach ?>
                                    </ul>
                                    <!-- row -->
                                </div>
                                <div class="tab-pane" id="e-tab22">
                                    
                                    <ul class="row">
                                        
                                        <?php 
                                            $this->db->order_by('visitas','DESC');
                                            $discos = $this->elements->albums(array(),6);
                                            foreach($discos->result() as $n=>$a): ?>
                                                <li class="col-md-2 col-sm-2 col-xs-4">
                                                    <article class="entry-item">
                                                        <div class="entry-thumb">
                                                            <a href="<?= $a->link ?>"><img src="<?= $a->caratula ?>" alt=""></a>
                                                            <div class="thumb-hover">
                                                                <a href="<?= $a->link ?>" class="thumb-icon"><i class="fa fa-plus"></i></a>
                                                            </div>
                                                        </div>
                                                    </article>
                                                </li>
                                        <?php endforeach ?>
                                    </ul>
                                    <!-- row -->
                                </div>
                                <div class="tab-pane" id="e-tab23">
                                    
                                    <ul class="row">
                                        
                                        <?php 
                                            $this->db->order_by('orden','ASC');
                                            $discos = $this->elements->albums(array());
                                            foreach($discos->result() as $n=>$a): ?>
                                                <li class="col-md-2 col-sm-2 col-xs-4">
                                                    <article class="entry-item">
                                                        <div class="entry-thumb">
                                                            <a href="<?= $a->link ?>"><img src="<?= $a->caratula ?>" alt=""></a>
                                                            <div class="thumb-hover">
                                                                <a href="<?= $a->link ?>" class="thumb-icon"><i class="fa fa-plus"></i></a>
                                                            </div>
                                                        </div>
                                                    </article>
                                                </li>
                                        <?php endforeach ?>
                                    </ul>
                                    <!-- row -->
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- widget -->
                    
                </div>
                <!-- col-md-12 -->
                
            </div>
            <!-- row -->
            <div class="row">
                
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="widget kopa-audio-download-widget">
                        <h3 class="widget-title style1">best sellers</h3>
                        <ul class="clearfix">
                            
                            <?php 
                                    $discos = $this->elements->getMasVentas();
                                    foreach($discos->result() as $n=>$a): 
                                    if(!empty($a->link)):
                            ?>
                            <li>
                                <article class="entry-item">
                                    <div class="col-left">
                                        <span><i><?= $n+1 ?></i></span>
                                        <a href="<?= $a->link ?>"><i class="fa fa-shopping-cart"></i></a>
                                    </div>
                                    <div class="entry-content">
                                        <h4 class="entry-title"><a href="<?= $a->link ?>"><?= $a->titulo ?></a></h4>
                                        <p class="entry-author">por <a href="<?= $a->link ?>"><?= @$a->artista ?></a></p>
                                    </div>
                                </article>
                            </li>
                            <?php endif ?>
                            <? endforeach ?>
                        </ul>
                    </div>
                    <!-- widget -->
                    
                </div>
                <!-- col-md-6 -->
                
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="widget kopa-single-article-widget">
                        <h3 class="widget-title style1">LAST NEWS</h3>
                        <?php 
                            $this->db->order_by('id','DESC');
                            $discos = $this->db->get_where('blog');
                            if($discos->num_rows()>0): 
                                $discos = $discos->row();
                        ?>
                        <article class="entry-item">
                            <div class="entry-thumb">
                                <a href="<?= base_url('blog/'.toUrl($discos->id.' '.$discos->titulo)) ?>">
                                    <img src="<?= base_url('img/blog/'.$discos->foto) ?>" alt="">
                                </a>
                                <div class="thumb-hover">
                                    <a href="<?= base_url('blog/'.toUrl($discos->id.' '.$discos->titulo)) ?>" class="thumb-icon"><i class="fa fa-plus"></i></a>
                                </div>
                            </div>
                            <div class="entry-content">
                                <p class="entry-date"><?= strftime('%d %B %Y',strtotime($discos->fecha)) ?></p>
                                <h4 class="entry-title"><a href="<?= base_url('blog/'.toUrl($discos->id.' '.$discos->titulo)) ?>"><?= $discos->titulo ?></a></h4>
                                <p><?= cortar_palabras(strip_tags($discos->texto),10) ?></p>
                                <p><a href="<?= base_url('blog/'.toUrl($discos->id.' '.$discos->titulo)) ?>">Leer más</a></p>
                            </div>
                        </article>
                        <?php endif ?>
                    </div>
                    <!-- widget -->
                    
                </div>
                <!-- col-md-6 -->
                
            </div>
            <!-- row -->
            
        </div>
        <!-- wrapper -->
        
    </section>
    <!-- kopa-area-10 -->
    [footer]
</div>
<!-- main-content -->




<div class="widget kopa-featured-audio-widget audioFlot" style="position:fixed; left:0px; bottom:0px; width:100%; margin-bottom: 0px; z-index:10000">
        <div class="outer">
            <a href="javascript:void(0)" onclick="$('.audioFlot').remove();" class="closeRep" style=""><i class="fa fa-times"></i></a>
            <div class="col-left">
                <div class="album-thumb">
                    <a href="#"><img src="[base_url]theme/theme/images/img/2.png" alt=""></a>
                </div>
                <div class="album-content">
                    <h6><a href="#">Presentación Sello Discográfico</a></h6>
                    <p><a href="#">Discoteca Records</a></p>
                </div>
            </div>
            <div class="col-right">
                <div class="audio-wrap">
                    <div class="kopa-jp-jplayer"></div>
                    <div class="jp-audio kopa-jp-wrap" role="application" aria-label="media player">
                        <div class="jp-type-playlist">
                            <div class="jp-gui jp-interface">
                                <div class="col-left">
                                    <div class="jp-controls">
                                        <button class="jp-previous" role="button" tabindex="0"></button>
                                        <button class="jp-play" role="button" tabindex="0"></button>
                                        <button class="jp-next" role="button" tabindex="0"></button>
                                        <span class="fa fa-bar-chart"></span>
                                    </div>
                                </div>
                                <div class="col-right">
                                    <div class="jp-progress">
                                        <div class="jp-seek-bar">
                                            <div class="jp-play-bar"></div>
                                        </div>
                                    </div>
                                    <div class="jp-volume-controls">
                                        <span class="fa fa-volume-down"></span>
                                        <div class="jp-volume-bar">
                                            <div class="jp-volume-bar-value"></div>
                                        </div>
                                    </div>
                                    <div class="jp-time-holder">
                                        <div class="jp-current-time" role="timer" aria-label="time">&nbsp;</div>
                                        <div class="jp-duration" role="timer" aria-label="duration">&nbsp;</div>
                                    </div>
                                </div>
                            </div>
                            <div class="jp-playlist">
                                <ul style="display: none !important">
                                    <li>&nbsp;</li>
                                </ul>
                            </div>
                            <div class="jp-no-solution">
                                <span>Update Required</span>
                                To play the media you will need to either update your browser to a recent version or update your <a href="http://get.adobe.com/flashplayer/" target="_blank">Flash plugin</a>.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
    </div>
    
    <article id="recopilatoriosContent" class="contentOculto" style="display: none;">
        <div class="widget kopa-audio-list-widget" style="margin-bottom: 0; position:relative;">
            <a href="javascript:closeListRecop()" style="position: absolute; right: -20px; top:-20px">
                <i class="fa fa-times"></i>
            </a>
            <div class="widget-thumb">
                <div class="entry-thumb">
                    <a href="#"><img src="[base_url]theme/theme/images/img/5.png" alt=""></a>
                </div>
                <ul class="info">
                    <li>
                        <span>2014</span>
                    </li>
                    <li>
                        <span>19 songs</span>
                    </li>
                    <li>
                        <div class="kopa-rating">
                            <ul>
                                <li><span class="fa fa-star"></span></li>
                                <li><span class="fa fa-star"></span></li>
                                <li><span class="fa fa-star"></span></li>
                                <li><span class="fa fa-star"></span></li>
                                <li class="inactive"><span class="fa fa-star"></span></li>
                            </ul>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="widget-content">
                <header>
                    <h3><a href="#">Andreas Weise <span class="precio"> - 20€</span></a></h3>
                    <p><a href="#">Leona</a>&nbsp;-&nbsp;<a href="#">pop</a>&nbsp;-&nbsp;<a href="#" title="Añadir a la lista de favoritos"><i class="fa fa-heart"></i></a></p>
                </header>
                <ul class="row audio-list">
                    <li class="col-md-6 col-sm-6 col-xs-12"><a href="#">Zuper - Glide</a></li>
                    <li class="col-md-6 col-sm-6 col-xs-12"><a href="#">Zuper - Glide</a></li>
                    <li class="col-md-6 col-sm-6 col-xs-12"><a href="#">Lana Del Rey - Ride (Barretso Remix)</a></li>
                    <li class="col-md-6 col-sm-6 col-xs-12"><a href="#">Lana Del Rey - Ride (Barretso Remix)</a></li>
                    <li class="col-md-6 col-sm-6 col-xs-12"><a href="#">Four Tet - Lion (Jamie xx Remix)</a></li>
                    <li class="col-md-6 col-sm-6 col-xs-12"><a href="#">Four Tet - Lion (Jamie xx Remix)</a></li>
                    <li class="col-md-6 col-sm-6 col-xs-12"><a href="#">Jamie xx & Yasmin - Touch Me</a></li>
                    <li class="col-md-6 col-sm-6 col-xs-12"><a href="#">Jamie xx & Yasmin - Touch Me</a></li>
                    <li class="col-md-6 col-sm-6 col-xs-12"><a href="#">Disclosure - Tenderly</a></li>
                    <li class="col-md-6 col-sm-6 col-xs-12"><a href="#">Disclosure - Tenderly</a></li>
                </ul>
            </div>
        </div>
    </article>

    <script>

        var recopHtml = $("#recopilatoriosContent").clone();
        $("#recopilatoriosContent").remove();

        $(".recopilatoriolink").on('click',function(e){
            e.preventDefault();
            selRecopilatorio(this);
        });

        function closeListRecop(){
            $(".recopilatorioContent").remove();
            $(".entry-item.active").removeClass('active');
        }
        
        function selRecopilatorio(e){
            $(".recopilatorioContent").remove();
            $(".entry-item.active").removeClass('active');
            var content = recopHtml.clone();
            content.show();
            content.addClass('recopilatorioContent');
            console.log(e);
            $(e).parents('.owl-wrapper-outer').after(content);
            $(e).parents('.entry-item').addClass('active');
        }    
    </script>