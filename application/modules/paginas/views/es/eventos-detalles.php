<body class="kopa-single-event kopa-subpage">
	[menu]
	<div id="main-content">

        <header class="page-header text-center">        

            <div class="kp-map-wrapper">
                <div id="kp-map" class="kp-map" data-place="Ha Noi" data-latitude="21.029532" data-longitude="105.852345"></div>
            </div>
            <!-- kp-map-wrapper -->

            <div class="mask"></div>

            <div class="page-header-inner">

                <div class="wrapper">
                    
                    <h1 class="page-title">Event</h1>

                    <div class="breadcrumb clearfix">                    
                        <span itemtype="http://data-vocabulary.org/Breadcrumb" itemscope="">
                            <a href="<?= base_url() ?>" itemprop="url">
                                <span itemprop="title">Home</span>
                            </a>
                        </span>
                        <span>&nbsp;/&nbsp;</span>
                        <span itemtype="http://data-vocabulary.org/Breadcrumb" itemscope="" class="current-page"><span itemprop="title">Single Event</span></span>
                    </div>
                    <!-- breadcrumb -->

                </div>
                <!-- wrapper -->

            </div>
            <!-- page-header-inner -->
            
        </header>
        <!-- page-header -->

        <section class="kopa-area kopa-area-9 kopa-area-dark kopa-parallax">

            <div class="span-bg"></div>

            <div class="wrapper">

                <div class="row">
                
                    <div class="col-md-12 col-sm-12 col-xs-12">

                        <div class="widget kopa-countdown-2-widget">
                            <div class="cd-content">
                                <h3 class="cd-title"><a href="#">OaKLAND, CA</a></h3>
                                <p class="cd-date">18 Apr</p>
                                <p>Amsterdam Dance Amsterdam Netherlands</p>
                                <ul class="kopa-countdown-2">
                                </ul> 
                            </div>
                        </div>
                        <!-- widget --> 
                
                    </div>
                    <!-- col-md-12 -->
                
                </div>
                <!-- row --> 
            
            </div>
            <!-- wrapper -->
            
        </section>
        <!-- kopa-area-9 -->

        <section class="kopa-area kopa-area-pattern">

            <div class="wrapper clearfix">

                <div class="row single-event-box">

                    <div class="col-md-4 col-sm-4">

                        <div class="event-info">
                            <p><span>Date: </span>12/Jul/2015</p>
                            <p><span>Time: </span>20:00 - 24:00</p>
                            <p><span>Address: </span>Oracle Arena W/ Aloe 230 Main street River Side, London 453692</p>
                            <p><span>Address: </span><a href="callto:442222188" "call me">(04)-442-222-188</a></p>
                        </div>
                        <!-- event-info -->

                        <h3>Share This Album</h3>

                        <div class="kopa-social-links style2">
                            <ul class="clearfix">
                                <li><a class="fa fa-facebook" href="#"></a></li>
                                <li><a class="fa fa-twitter" href="#"></a></li>
                                <li><a class="fa fa-instagram" href="#"></a></li>
                                <li><a class="fa fa-rss" href="#"></a></li>
                                <li><a class="fa fa-google-plus" href="#"></a></li>
                            </ul>
                        </div>
                        <!-- kopa-social-links -->

                        <a href="#" class="kopa-button">Buy Now</a>
                        
                    </div>
                    <!-- col-md-4 -->

                    <div class="col-md-8 col-sm-8">

                        <div class="event-content">

                            <h3 class="event-title">Credit Union Centre</h3>

                            <p>Nulla vitae elit libero, a pharetra augue. Vestibulum id ligula porta felis euismod semper. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Nullam quis risus eget urna mollis ornare vel eu leo.</p>

                            <p>Donec id elit non mi porta gravida at eget metus. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Donec sed odio dui. Curabitur blandit tempus porttitor. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit.</p>

                            <br>

                            <img src="placeholders/post-image/post-41.jpg" alt="">

                            <br><br><br>

                            <h3 class="event-title">Video</h3>

                            <p>Nulla vitae elit libero, a pharetra augue. Vestibulum id ligula porta felis euismod semper. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Nullam quis risus eget urna mollis ornare vel eu leo.</p>

                            <br>

                            <div class="video-wrapper mb-30">
                                <iframe height="400" allowfullscreen="" src="http://player.vimeo.com/video/104421565"></iframe> 
                            </div>
                            <!-- video-wrapper -->

                            <div class="comment-box-1">
                                <form class="comments-form-1 clearfix" action="processForm.php" method="post" novalidate="novalidate">
                                    <div class="row">
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <p class="input-block">
                                                <input type="text" value="First Name" onfocus="if(this.value=='First Name')this.value='';" onblur="if(this.value=='')this.value='First Name';" id="comment_name-1" name="name1" class="valid">
                                            </p>
                                        </div>
                                        <!-- col-md-6 -->
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <p class="input-block">
                                                <input type="text" value="Last Name" onfocus="if(this.value=='Last Name')this.value='';" onblur="if(this.value=='')this.value='Last Name';" id="comment_name-2" name="name2" class="valid">
                                            </p>
                                        </div>
                                        <!-- col-md-6 -->
                                    </div>
                                    <!-- row --> 
                                    <div class="row">
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <p class="input-block">
                                                <input type="text" value="Email" onfocus="if(this.value=='Email')this.value='';" onblur="if(this.value=='')this.value='Email';" id="comment_email" name="email" class="valid">
                                            </p>
                                        </div>
                                        <!-- col-md-6 -->
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <p class="input-block">
                                                <input type="text" value="Website" onfocus="if(this.value=='Website')this.value='';" onblur="if(this.value=='')this.value='Website';" id="comment_ws-link" name="website" class="valid">
                                            </p>
                                        </div>
                                        <!-- col-md-6 -->
                                    </div>
                                    <p class="textarea-block">  
                                        <textarea name="message" id="comment_message" cols="88" rows="7"></textarea>
                                    </p>
                                    <p class="comment-button clearfix">           
                                        <span><input type="submit" value="Post Comment" id="submit-comment-1"></span>
                                    </p>
                                </form>
                                <div id="response2"></div>
                            </div>
                            <!-- comment-box -->
                            
                        </div>
                        <!-- event-content -->
                        
                    </div>
                    <!-- col-md-8 -->
                    
                </div>
                <!-- row -->

            </div>
            <!-- wrapper -->
            
        </section>
        <!-- kopa-area-pattern -->
        
    </div>
    <!-- main-content -->
	[footer]