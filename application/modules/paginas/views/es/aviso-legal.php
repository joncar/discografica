<body class="woocommerce woocommerce-page kopa-subpage">
<div id="main-content">
	[menu]
		<header class="page-header have-disc-icon text-center">
			<div class="disc-bg"></div>
            <div class="mask"></div>

            <div class="page-header-bg-2 page-header-bg"></div>

            <div class="page-header-inner page-header-inner-1">

                <div class="wrapper">
                    
                    <h1 class="page-title">Aviso legal</h1>

                    <div class="breadcrumb clearfix">                    
                        <span itemtype="http://data-vocabulary.org/Breadcrumb" itemscope="">
                            <a href="<?= base_url() ?>" itemprop="url">
                                <span itemprop="title">Inicio</span>
                            </a>
                        </span>
                        <span>&nbsp;/&nbsp;</span>
                        <span itemtype="http://data-vocabulary.org/Breadcrumb" itemscope="">
                            <a href="<?= base_url() ?>" itemprop="url">
                                <span itemprop="title">Empresa</span>
                            </a>
                        </span>
                        <!-- <span>&nbsp;/&nbsp;</span> -->
                        <!-- <span itemtype="http://data-vocabulary.org/Breadcrumb" itemscope="" class="current-page"><span itemprop="title">Typography</span></span> -->
                    </div>
                    <!-- breadcrumb -->

                </div>
                <!-- wrapper -->

                </div>
                <!-- page-header-inner -->

                <div class="album-icon">
	                <div class="icon-inner-1">
	                    <span class="icon-inner-2"></span>
	                </div>
	                <span class="fa fa-music"></span>
	            </div>
            
        </header>
        <!-- page-header -->
		<div class="wrapper clearfix  kopa-flickr-widget">
			<section class="elements-box mb-40">
				<div class="row">
					<div class="col-xs-12 col-sm-12">
			            
			            <div class="row">
			                <div class="col-md-11 col-sm-12 col-xs-12 mb-40">
			                    <p>
		                    		<div class="widget widget_text">
				                        <p> </p>
				                        <h2 style="text-align: left;">Protecci&oacute;n de datos de car&aacute;cter personal seg&uacute;n la LOPD</h2>
<p style="text-align: left;">Discoteca Records, en aplicaci&oacute;n de la normativa vigente en materia de protecci&oacute;n de datos de car&aacute;cter&nbsp;personal, informa que los datos personales&nbsp; que &nbsp; se &nbsp; recogen a trav&eacute;s de lo formularios&nbsp; del sitio web, se incluyen en los ficheros automatizados espec&iacute;ficos de usuarios para servicios en nuestra web.</p>
<p style="text-align: left;"><a href="http://www.discotecarecords.com">www.discotecarecords.com</a><br />Empresa: Discoteca Records&nbsp;una divisi&oacute;n de *Desarrollo Guirao &amp; Camino SL&nbsp;<br />Domicilio: C/Antoni Forrellad i Sola , n&ordm; 17</p>
<p style="text-align: left;">NIF: B 65603201<br />E-mail:<a href="mailto:info@discotecarecords.com"> info@discotecarecords.com</a><br />Telf: +34 930 50 00 88</p>
<p style="text-align: left;"><br />La recogida y tratamiento automatizado de los datos de car&aacute;cter personal tiene como finalidad el mantenimiento de la relaci&oacute;n comercial y el desempe&ntilde;o de tareas de informaci&oacute;n, formaci&oacute;n, asesoramiento y otras actividades propias de&nbsp;nuestra empresa.</p>
<p style="text-align: left;">Estos datos &uacute;nicamente ser&aacute;n cedidos a aquellas entidades que sean necesarias con el &uacute;nico objetivo de dar cumplimiento a la finalidad anteriormente expuesta.</p>
<p style="text-align: left;">Discoteca Records adopta las medidas necesarias para garantizar la seguridad, integridad y confidencialidad de los datos conforme a lo dispuesto en el Reglamento (UE) 2016/679 del Parlamento Europeo y del Consejo, de 27 de abril de 2016, relativo a la protecci&oacute;n de las personas f&iacute;sicas en lo que respecta al tratamiento de datos personales y a la libre circulaci&oacute;n de los mismos.</p>
<p style="text-align: left;">El usuario podr&aacute; en cualquier momento ejercitar los derechos de acceso, oposici&oacute;n, rectificaci&oacute;n y cancelaci&oacute;n reconocidos en el citado Reglamento (UE). El ejercicio de estos derechos puede realizarlo el propio usuario a trav&eacute;s de email a: <a href="mailto:info@discotecarecords.com">info@discotecarecords.com</a> , o via postal a nuestra direcci&oacute;n:&nbsp;</p>
<p style="text-align: left;"></p>
<h4 style="text-align: left;">Discoteca Records*&nbsp;</h4>
<p style="text-align: left;">C/Antoni Forrellad i Sola , n&ordm; 17<br />C.P. 08192 - Sant Quirze Del Vall&eacute;s - Barcelona.</p>
<p style="text-align: left;">El usuario manifiesta que todos los datos facilitados por &eacute;l son ciertos y correctos, y se compromete a mantenerlos&nbsp;actualizados, comunicando los cambios a <a href="mailto:info@discotecarecords.com">info@discotecarecords.com</a></p>
<p style="text-align: left;"></p>
<h4 style="text-align: left;"></h4>
<h4 style="text-align: left;"><br />Finalidad del tratamiento de los datos personales:<br />&iquest;Con qu&eacute; finalidad trataremos tus datos personales?</h4>
<p style="text-align: left;">En Discoteca Records,&nbsp; trataremos&nbsp; tus&nbsp; datos&nbsp; personales&nbsp; recabados&nbsp; a&nbsp; trav&eacute;s&nbsp; del&nbsp; Sitio&nbsp; <a href="http://www.discotecarecords.com">www.discotecarecords.com</a>, con las siguientes finalidades:</p>
<p style="text-align: left;">1.&nbsp; &nbsp; En caso de contrataci&oacute;n de los bienes y servicios ofertados a trav&eacute;s de <a href="http://www.discotecarecords.com">www.discotecarecords.com</a>, para mantener la relaci&oacute;n contractual, as&iacute; como la gesti&oacute;n, administraci&oacute;n, informaci&oacute;n, prestaci&oacute;n y mejora del servicio.<br />2.&nbsp; &nbsp; Env&iacute;o de informaci&oacute;n solicitada a trav&eacute;s de los formularios dispuestos en <a href="http://www.discotecarecords.com">www.discotecarecords.com</a><br />3.&nbsp; &nbsp; Remitir newsletters, as&iacute; como comunicaciones comerciales de promociones y/o publicidad del sector.</p>
<p style="text-align: left;">Te recordamos que puedes oponerte al env&iacute;o de comunicaciones comerciales por cualquier v&iacute;a y en cualquier momento, remitiendo un correo electr&oacute;nico a la direcci&oacute;n indicada anteriormente.</p>
<p style="text-align: left;">Los campos de dichos registros son de cumplimentaci&oacute;n obligatoria, siendo imposible realizar las finalidades expresadas si no se aportan esos datos.</p>
<h4 style="text-align: left;"></h4>
<h4 style="text-align: left;"></h4>
<h4 style="text-align: left;"><br />&iquest;Por cu&aacute;nto tiempo se conservan los datos personales recabados?</h4>
<p style="text-align: left;">Los datos personales proporcionados se conservar&aacute;n mientras se mantenga la relaci&oacute;n comercial o no solicites su supresi&oacute;n y durante el plazo por el cu&aacute;l pudieran derivarse responsabilidades legales por los servicios prestados.</p>
<p style="text-align: left;"></p>
<h4 style="text-align: left;"></h4>
<h4 style="text-align: left;"><br />Legitimaci&oacute;n:</h4>
<p style="text-align: left;">El tratamiento de tus datos se realiza con las siguientes bases jur&iacute;dicas que legitiman el mismo:</p>
<p style="text-align: left;">1. La solicitud de informaci&oacute;n y/o la contrataci&oacute;n de los servicios de Discoteca Records, cuyos t&eacute;rminos y condiciones se pondr&aacute;n a tu disposici&oacute;n en todo caso, de forma previa a una eventual contrataci&oacute;n.</p>
<p style="text-align: left;">2. El consentimiento libre, espec&iacute;fico, informado e inequ&iacute;voco, en tanto que te informamos poniendo a tu disposici&oacute;n la presente pol&iacute;tica de privacidad, que, tras la lectura de la misma, en caso de estar conforme, puedes aceptar mediante una declaraci&oacute;n o una clara acci&oacute;n afirmativa.</p>
<p style="text-align: left;"></p>
<h4 style="text-align: left;"></h4>
<h4 style="text-align: left;"><br />Datos recopilados por usuarios de los servicios</h4>
<p style="text-align: left;">En los casos en que el usuario incluya ficheros con datos de car&aacute;cter personal en los servidores de alojamiento compartido, Discoteca Records no se hace responsable del incumplimiento por parte del usuario del RGPD.</p>
<p style="text-align: left;"></p>
<h4 style="text-align: left;"></h4>
<h4 style="text-align: left;"><br />Retenci&oacute;n de datos en conformidad a la LSSI</h4>
<p style="text-align: left;">Discoteca Records* , informa de que, como prestador de servicio de alojamiento de datos y en virtud de lo establecido en la Ley 34/2002 de 11 de julio de Servicios de la Sociedad de la Informaci&oacute;n y de Comercio Electr&oacute;nico (LSSI), retiene por un periodo m&aacute;ximo de 12 meses la informaci&oacute;n imprescindible para identificar el origen de los datos alojados y el momento en que se inici&oacute; la prestaci&oacute;n del servicio. La retenci&oacute;n de estos datos no afecta al secreto de las comunicaciones y s&oacute;lo podr&aacute;n ser utilizados en el marco de una investigaci&oacute;n criminal o para la salvaguardia de la seguridad p&uacute;blica, poni&eacute;ndose a disposici&oacute;n de los jueces y/o tribunales o del Ministerio que as&iacute; los requiera.</p>
<p style="text-align: left;">La comunicaci&oacute;n de datos a las Fuerzas y Cuerpos del Estado se har&aacute; en virtud a lo dispuesto en la normativa sobre protecci&oacute;n de datos personales.</p>
<p style="text-align: left;"></p>
<h4 style="text-align: left;"></h4>
<h4 style="text-align: left;"><br />Derechos propiedad intelectual&nbsp;</h4>
<p style="text-align: left;">Discoteca Records es titular de todos los derechos de autor, propiedad intelectual, industrial, y cuantos otros derechos guardan relaci&oacute;n con los contenidos del sitio web <a href="http://www.discotecarecords.com">www.discotecarecords.com</a> y los servicios ofertados en el mismo, as&iacute; como de los programas necesarios para su implementaci&oacute;n y la informaci&oacute;n relacionada.</p>
<p style="text-align: left;">No se permite la reproducci&oacute;n, publicaci&oacute;n y/o uso no estrictamente privado de los contenidos, totales o parciales, del sitio web <a href="http://www.discotecarecords.com">www.discotecarecords.com</a> sin el consentimiento previo y por escrito.</p>
<h4 style="text-align: left;"></h4>
<h4 style="text-align: left;"></h4>
<h4 style="text-align: left;"><br />Propiedad intelectual del software</h4>
<p style="text-align: left;">El usuario debe respetar los programas de terceros puestos a su disposici&oacute;n por Discoteca Records aun siendo gratuitos y/o de disposici&oacute;n p&uacute;blica.</p>
<p style="text-align: left;">Discoteca Records dispone de los derechos de explotaci&oacute;n y propiedad intelectual necesarios del software.</p>
<p style="text-align: left;">El usuario no adquiere derecho alguno o licencia por el servicio contratado, sobre el software necesario para la prestaci&oacute;n del servicio, ni tampoco sobre la informaci&oacute;n t&eacute;cnica de seguimiento del servicio, excepci&oacute;n hecha de los derechos y licencias necesarios para el cumplimiento de los servicios contratados y &uacute;nicamente durante la duraci&oacute;n de los mismos.</p>
<p style="text-align: left;">Para toda actuaci&oacute;n que exceda del cumplimiento del contrato, el usuario necesitar&aacute; autorizaci&oacute;n por escrito por parte de Discoteca Records* , quedando prohibido al usuario acceder, modificar, visualizar la configuraci&oacute;n, estructura y ficheros de los servidores propiedad de la empresa , asumiendo la responsabilidad civil y penal derivada de cualquier incidencia que se pudiera producir en los servidores y sistemas de seguridad como consecuencia directa de una actuaci&oacute;n negligente o maliciosa por su parte.</p>
<p style="text-align: left;"></p>
<h4 style="text-align: left;"></h4>
<h4 style="text-align: left;"><br />Propiedad intelectual de los contenidos alojados</h4>
<p style="text-align: left;">Se proh&iacute;be el uso contrario a la legislaci&oacute;n sobre propiedad intelectual de los servicios prestados por&nbsp;Discoteca Records y, en particular de:</p>
<p style="text-align: left;">&bull; La utilizaci&oacute;n que resulte contraria a las leyes espa&ntilde;olas o que infrinja los derechos de terceros.<br />&bull; La publicaci&oacute;n o la transmisi&oacute;n de cualquier contenido que, a juicio de Discoteca Records resulte violento, obsceno, abusivo, ilegal, racial, xen&oacute;fobo o difamatorio.<br />&bull; Los cracks, n&uacute;meros de serie de programas o cualquier otro contenido que vulnere derechos de la propiedad intelectual de terceros.<br />&bull; La recogida y/o utilizaci&oacute;n de datos personales de otros usuarios sin su consentimiento expreso o contraviniendo lo dispuesto en Reglamento (UE) 2016/679 del Parlamento Europeo y del Consejo, de 27 de abril de 2016, relativo a la protecci&oacute;n de las personas f&iacute;sicas en lo que respecta al tratamiento de datos personales y a la libre circulaci&oacute;n de los mismos.&bull; La utilizaci&oacute;n del servidor de correo del dominio y de las direcciones de correo electr&oacute;nico para el env&iacute;o de correo masivo no deseado.<br />&bull; Legislaci&oacute;n, todas las reclamaciones surgidas de la interpretaci&oacute;n o ejecuci&oacute;n del presente aviso legal se regir&aacute;n por la legislaci&oacute;n espa&ntilde;ola, y se someter&aacute;n a la jurisdicci&oacute;n de los juzgados y tribunales del domicilio del titular del web.</p>
<p style="text-align: left;">El usuario tiene toda la responsabilidad sobre el contenido de su web, la informaci&oacute;n transmitida y almacenada, los enlaces de hipertexto, las reivindicaciones de terceros y las acciones legales en referencia a propiedad intelectual, derechos de terceros y protecci&oacute;n de menores.</p>
<p style="text-align: left;">El usuario es responsable respecto a las leyes y reglamentos en vigor y las reglas que tienen que ver con el funcionamiento del servicio online, comercio electr&oacute;nico, derechos de autor, mantenimiento del orden p&uacute;blico, as&iacute; como principios universales de uso de Internet.</p>
<p style="text-align: left;">El usuario indemnizar&aacute; a Discoteca Records* por los gastos que generar&aacute; la imputaci&oacute;n de en alguna causa cuya responsabilidad fuera atribuible al usuario,&nbsp; incluidos honorarios y gastos de defensa jur&iacute;dica, incluso en el caso de una decisi&oacute;n judicial no definitiva.</p>
<p style="text-align: left;"></p>
<h4 style="text-align: left;"></h4>
<h4 style="text-align: left;"><br />Protecci&oacute;n de la informaci&oacute;n alojada</h4>
<p style="text-align: left;">Discoteca Records realiza copias&nbsp; de&nbsp; seguridad&nbsp; de&nbsp; los&nbsp; contenidos alojados&nbsp; en&nbsp; sus&nbsp; servidores,&nbsp; sin embargo no se responsabiliza de la p&eacute;rdida o el borrado accidental de los datos por parte de los usuarios. De igual manera, no garantiza la reposici&oacute;n total de los datos borrados por los usuarios, ya que los citados datos podr&iacute;an haber sido suprimidos y/o modificados durante el periodo del tiempo transcurrido desde la &uacute;ltima copia de seguridad.</p>
<p style="text-align: left;">Los servicios ofertados, excepto los servicios espec&iacute;ficos de backup, no incluyen la reposici&oacute;n de los contenidos conservados en las copias de seguridad realizadas por Discoteca Records cuando esta p&eacute;rdida sea imputable al usuario; en este caso, se determinar&aacute; una tarifa acorde a la complejidad y volumen de la recuperaci&oacute;n, siempre previa aceptaci&oacute;n del usuario.</p>
<p style="text-align: left;"></p>
<h4 style="text-align: left;"></h4>
<h4 style="text-align: left;"><br />Comunicaciones comerciales</h4>
<p style="text-align: left;">En aplicaci&oacute;n de la LSSI. Discoteca Records no enviar&aacute; comunicaciones publicitarias o promocionales por correo electr&oacute;nico u otro medio de comunicaci&oacute;n electr&oacute;nica equivalente que previamente no hubieran sido solicitadas o expresamente autorizadas por los destinatarios de las mismas.</p>
<p style="text-align: left;">En el caso de usuarios con los que exista una relaci&oacute;n contractual previa, Discoteca Records* s&iacute; est&aacute; autorizado al env&iacute;o de comunicaciones comerciales referentes a productos o servicios que sean similares a los que inicialmente fueron objeto de contrataci&oacute;n con el cliente.</p>
<p style="text-align: left;">En todo caso, el usuario, tras acreditar su identidad, podr&aacute; solicitar que no se le haga llegar m&aacute;s informaci&oacute;n comercial a trav&eacute;s de los canales de Atenci&oacute;n al Cliente.</p>
<p style="text-align: left;">Contacto <a href="info@discotecarecords.com%20">info@discotecarecords.com&nbsp;</a></p>
			                    	</div>
			                    </p>
			                </div>
			                <!-- col-md-12 -->
			            </div>
			            <!-- row -->
		            </div>
		            
				</div>
	            
	        </section>
        </div>
	[footer]
</div>