<body class="kopa-contact-page">
	[menu]
	<div id="main-content">

        <header class="page-header text-center">        

            <div class="kp-map-wrapper">
                <div id="kp-map" class="kp-map" data-place="Discotecarecords" data-latitude="41.5306006" data-longitude="2.0960394"></div>
            </div>
            <!-- kp-map-wrapper -->

            <div class="mask"></div>

            <div class="page-header-inner">

                <div class="wrapper">
                    
                    <h1 class="page-title">Contact</h1>

                    <div class="breadcrumb clearfix">                    
                        <span itemtype="http://data-vocabulary.org/Breadcrumb" itemscope="">
                            <a href="<?= base_url() ?>" itemprop="url">
                                <span itemprop="title">Home</span>
                            </a>
                        </span>
                        <span>&nbsp;/&nbsp;</span>
                        <span itemtype="http://data-vocabulary.org/Breadcrumb" itemscope="" class="current-page"><span itemprop="title">Contact</span></span>
                    </div>
                    <!-- breadcrumb -->

                </div>
                <!-- wrapper -->

            </div>
            <!-- page-header-inner -->
            
        </header>
        <!-- page-header -->

        <div class="wrapper clearfix">

            <div class="widget kopa-contact-info-widget">

                <div class="widget-title">
                    <h2 class="text-uppcase">HERE YOU WILL FIND US<span class="bottom-line"><span><span>&nbsp;</span></span></span></h2>
                </div>
                <!-- widget-title -->

                <div class="widget-content">
<!-- 
                    <p>Cras venenatis justo mi, non posuere enim aliquet malesuada. Nullam orci sem, adipiscing id rutrum et, blandit quis lorem. Phasellus lacus orci, cursus vitae mi eget, sagittis congue elit. Donec ac tincidunt tortor. Duis vel neque eleifend odio hendrerit consequat sed vel massa. Praesent tempor libero quis tincidunt fringilla. Aliquam congue, neque et aliquam eleifend, lacus diam aliquet urna, in sollicitudin neque nisl facilisis urna.</p>
 -->
                    <div class="row">
                        <div class="col-md-4 col-sm-4">
                            <div class="contact-item">
                                <div class="contact-icon"><i class="fa fa-home"></i></div>
                                <div class="contact-info contact-address">
                                    <h6>Address</h6>
                                    <p>C/ Antoni Forrellad i Solà, 17 08192 Sant Quirze Del Vallès. BARCELONA. Spain</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4">
                            <div class="contact-item">
                                <div class="contact-icon"><i class="fa fa-phone"></i></div>
                                <div class="contact-info contact-mobile">
                                    <h6>Phone</h6>
                                    <p><a href="callto:123456789">(+34) 930 500 088</a></p>
                                    
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4">
                            <div class="contact-item">
                                <div class="contact-icon"><i class="fa fa-send"></i></div>
                                <div class="contact-info contact-email">
                                    <h6>Email</h6>
                                    <p><a href="mailto:info@discotecarecords.com">info@discotecarecords.com</a></p>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
            <!-- kopa-contact-info-widget -->

            <div id="contact-box">
                <h3 class="text-uppercase text-center clearfix">Leave us a message and we will respond</h3>

                <form class="contact-form clearfix" action="paginas/frontend/contacto" method="post" onsubmit="sendForm(this); return false;">
                    <span class="c-note text-center">Your email address will not be published. The required fields are marked *</span>

                    <div class="row">
                        <div class="col-md-4 col-sm-4">
                            <p class="input-block">
                                <label for="contact_fname" class="required">Name <span>*</span></label>
                                <input type="text" placeholder="Name *" id="contact_fname" name="nombre" class="valid">
                            </p>
                        </div>
                        <!-- col-md-4 -->

                        

                        <div class="col-md-4 col-sm-4">
                            <p class="input-block">
                                <label for="contact_email" class="required">Email <span>*</span></label>
                                <input type="text" placeholder="Email *" id="contact_email" name="email" class="valid">
                            </p>
                        </div>

                         <div class="col-md-4 col-sm-4">
                            <p class="input-block">
                                <label for="contact_email" class="required">Subject <span>*</span></label>
                                <input type="text" placeholder="Subject *" id="contact_email" name="titulo" class="valid">
                            </p>
                        </div>
                        <!-- col-md-4 -->

                    </div>
                    <!-- row -->

                    <!-- 
<div class="row">
                        <div class="col-md-4 col-sm-4">
                            <p class="input-block">                                                
                                <label for="contact_date" class="required">Fecha</label>
                                <input type="text" name="date" class="valid" value="Date" onfocus="if(this.value=='Date')this.value='';" onblur="if(this.value=='')this.value='Date';" id="contact_date">                                                
                            </p>
                        </div>
 -->
                        <!-- col-md-4 -->

                      <!-- 
  <div class="col-md-4 col-sm-4">
                            <p class="input-block">
                                <label for="contact_phone" class="required">Teléfono</label>
                                <input type="text" value="Phone" onfocus="if(this.value=='Phone')this.value='';" onblur="if(this.value=='')this.value='Phone';" id="contact_phone" name="phone" class="valid">
                            </p>
                        </div>
 -->
                        <!-- col-md-4 -->

                    <!-- 
    <div class="col-md-4 col-sm-4">
                            <p class="input-block">
                                <label for="contact_hour" class="required">Hora</label>
                                <input type="text" value="Hour" onfocus="if(this.value=='Hour')this.value='';" onblur="if(this.value=='')this.value='Hour';" id="contact_hour" name="hour" class="valid">
                            </p>
                        </div>
 -->
                        <!-- col-md-4 -->

                 <!--    </div> -->
                    <!-- row -->

                   <!-- 
 <div class="row">
                        
                        <div class="col-md-6 col-sm-6">
                            <div class="time-frame">
                                <p>Time Frame</p>
                                <ul class="clearfix">
                                    <li>
                                        <span>1 Hora</span>
                                        <input type="radio" value="1 Hour" name="radio-1">
                                    </li>
                                    <li>
                                        <span>2-3 Horas</span>
                                        <input type="radio" value="2-3 Hour" name="radio-1">
                                    </li>
                                    <li>
                                        <span>5 Horas</span>
                                        <input type="radio" value="5 Hour" name="radio-1">
                                    </li>
                                    <li>
                                        <span>Tota la noche</span>
                                        <input type="radio" value="All Night" name="radio-1">
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <div class="col-md-6 col-sm-6">
                            <div class="party">
                                <p>Fiesta</p>
                                <ul class="clearfix">
                                    <li>
                                        <span>Festival</span>
                                        <input type="radio" value="Festival" name="radio-2">
                                    </li>
                                    <li>
                                        <span>Live Act</span>
                                        <input type="radio" value="Live Act" name="radio-2">
                                    </li>
                                    <li>
                                        <span>DJ Set</span>
                                        <input type="radio" value="DJ Set" name="radio-2">
                                    </li>
                                </ul>
                            </div>
                        </div>

                    </div>
 -->
                    <!-- row -->

                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <p class="textarea-block">                        
                                <label for="contact_message" class="required">Your message <span>*</span></label>
                                <textarea onfocus="if(this.value=='Your message *')this.value='';" onblur="if(this.value=='')this.value='Your message *';" name="message" id="contact_message" cols="88" rows="6">Your message *</textarea>
                            </p>
                        </div>                        
                    </div>
                    <!-- row -->

                    <p class="checkbox">
                        <input type="checkbox" name="politicas" value="1" style="width:auto; height:auto;"> I read and accept <a href="<?= base_url('aviso-legal') ?>.html">Privacy policies</a>
                    </p>

                    <p class="contact-button clearfix">                    
                        <span class="kopa-button" style="padding:0">
                            <input type="submit" style="padding: 10px 20px;" value="Send" id="submit-contact" class="input-submit">
                        </span>
                    </p>

                </form>
                <div id="response"></div>
            </div><!--contact-box-->

        </div>
        <!-- wrapper -->
        
    </div>
    <!-- main-content -->
	[footer]