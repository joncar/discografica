<?php

class Tienda_model extends CI_Model{

	const NOPROCESADO = '-1';
    const PORPROCESAR = '1';
    const PROCESADO = '2';    

	function __construct(){
		parent::__construct();
		$this->load->model('carrito');
		$this->load->model('user');
	}	

	function cleanVentas(){
		foreach($this->db->get_where('ventas',array('user_id'=>$this->user->id,'procesado'=>self::PORPROCESAR))->result() as $v){
			$this->db->delete('ventas',array('id'=>$v->id));
			$this->db->delete('ventas_detalles',array('ventas_id'=>$v->id));
		}
	}

	function saveVenta(){
		if(!empty($_SESSION['carrito'])){
            $precio = 0;
            $cantidad = 0;
            $productos = '';
            $pasarela = empty($_POST['pasarela'])?1:$_POST['pasarela'];
            $carrito = $this->carrito->getCarrito();

            $pesoVinilos = 0;
            $pesoCajaAdicional = 0;
            $cantidadVinilos = 0;
            $pesoTotal = 0;
            $tax = 0;
            $shop = 1;
            
            foreach($carrito as $c){
                $e = $this->db->get_where('albums',array('id'=>$c->id))->row();
                $precio+= ($c->precio*$c->cantidad);
                $cantidad+= $c->cantidad;
                $productos.= $c->referencia.'-'.$c->titulo.', ';
                $peso = $e->peso*$c->cantidad;
                $pesoTotal+= $peso;
                $pesoVinilos+= $e->vinilo==1?$peso:0;
                $cantidadVinilos+= $e->vinilo==1?$c->cantidad:0;
                if(empty($e->promos_importadores)){
                    $shop = $shop==1 && !empty($c->shop)?2:$shop;
                }else{
                    $shop = 3;
                }

            }
            
            $pesoCajaAdicional = ceil($cantidadVinilos/5)*0.110; 
            $pesoTotal = $this->elements->getPeso(); 
            $peso = ceil($pesoTotal-$this->elements->getPeso(false,true));
            $envio = 0;
            //Precio envio
            if($_POST['paises_id']==1){ //España
                $envio = $this->db->get_where('provincias',array('id'=>$_POST['provincias_id']))->row()->tarifa;
                $envio = 0;                
                if($this->elements->is_tienda()){
                    if($pesoTotal<5){
                        $envio = 0;
                    }else{
                        $envio = $this->elements->get_tarifa_tienda($_POST['provincias_id'],$peso);                        

                        //echo $envio.'*'.$pesoTotal.'<br/>';
                        //$envio = $envio>0?$envio*$pesoTotal:$envio;                        
                        //echo $envio;
                        $envio = $envio>0?$envio*0.50:$envio;                        
                    }                    
                }else{
                    $envio=$this->elements->getTarifaEspanya($_POST['provincias_id'],$peso);                    
                }
                
            }else{ //Resto del mundo

                $tarifaEuropa = $this->elements->getTarifaEuropa($_POST['paises_id'],$pesoTotal);
                if($tarifaEuropa==0){ //Se cobra por continente
                    $continente = $this->db->get_where('paises',array('id'=>$_POST['paises_id']))->row()->continentes_id;
                    $this->db->order_by('peso_desde','ASC');
                    foreach($this->db->get_where('tarifas_envio',array('continentes_id'=>$continente))->result() as $t){
                        if($pesoTotal>=$t->peso_desde){
                            $envio = $t->precio;
                        }
                    }
                }else{//Se cobra por pais
                    $envio = ceil($pesoTotal)*$tarifaEuropa;                    
                }   

                if($_POST['paises_id']==49){//Australia                    
                    $env = ($precio+$envio)*0.10;
                    $tax = $env;                    
                }
            }

            $total = $precio+$envio+$tax;
            //echo $total;
            //die();
            $this->db->insert('ventas',
        		array(	    			
	    			'productos'=>$productos,
	                'user_id'=>$this->user->id,
	                'fecha_compra'=>date("Y-m-d H:i:s"),
	                'precio'=>str_replace(',','.',$precio),
                    'precio_envio'=>str_replace(',','.',$envio),
	                'cantidad'=>$cantidad,
	                'procesado'=>self::PORPROCESAR,
                    'pasarela'=>$pasarela,
                    'paises_id'=>$_POST['paises_id'],
                    'provincias_id'=>$_POST['provincias_id'],
                    'ciudad'=>$_POST['ciudad'],
                    'direccion'=>$_POST['direccion'],
                    'telefono'=>$_POST['telefono'],
                    'cp'=>$_POST['codigo_postal'],
                    'peso_vinilos'=>str_replace(',','.',$pesoVinilos),
                    'peso_total'=>str_replace(',','.',$pesoTotal),
                    'peso_embalaje'=>str_replace(',','.',$pesoCajaAdicional),
                    'tax_australia'=>str_replace(',','.',$tax),
                    'total'=>str_replace(',','.',$total),
                    'shop'=>$shop,
                    'comentarios'=>$_POST['observaciones']
            	)
        	);
            $id = $this->db->insert_id();
            foreach($carrito as $c){
                $digital = 0;
                if(isset($c->digital)){
                    $digital = $c->digital->id;
                }
                $this->db->insert('ventas_detalles',array(
                    'ventas_id'=>$id,
                    'productos_id'=>$c->id,
                    'digitales_id'=>$digital,
                    'cantidad'=>$c->cantidad,
                    'precio'=>$c->precio
                ));
            }
            return '000'.$id;

        }
	}

	function pagoOk($id){
        if(!empty($id)){                
            $this->db->select('user.id as userid, ventas.id as nrocompra,provincias.nombre as provincia, paises.nombre as pais,ventas.precio as totalP, user.*, ventas.*, albums.*, ventas_detalles.cantidad as cantidadProd, ventas_detalles.id as idDetalle');
            $this->db->join('ventas_detalles','ventas_detalles.ventas_id = ventas.id');
            $this->db->join('albums','ventas_detalles.productos_id = albums.id');
            $this->db->join('provincias','provincias.id = ventas.provincias_id','LEFT');
            $this->db->join('paises','paises.id = ventas.paises_id','LEFT');
            $this->db->join('user','user.id = ventas.user_id','inner');
            $producto = $this->db->get_where('ventas',array('ventas.id'=>$id));
            if($producto->num_rows()>0){      

                $producto = $producto->row();
                $producto->totalP = @moneda($producto->totalP);
                $producto->precio_envio = @moneda($producto->precio_envio);
                $producto->total = @moneda($producto->total);
                $producto->precio_rembolso = $producto->precio_rembolso==0?'No usado':@moneda($producto->precio_rembolso);
                $producto->fecha_compra = @strftime('%d %b %Y',strtotime($producto->fecha_compra));                
                get_instance()->enviarcorreo($producto,11);
                get_instance()->enviarcorreo($producto,12,'info@discotecarecords.com');
                if($producto->pasarela==4){
                    get_instance()->enviarcorreo($producto,12,'contabilidad@discotecarecords.com');                    
                }else{
                    get_instance()->enviarcorreo($producto,12,'sales@discotecarecords.com');                    
                }
                //Verificar si la compra contiene digitales
                if($producto->tipo_producto==3){
                    $link = base64_encode($producto->userid.'-'.$producto->nrocompra);
                    $producto->link = base_url('albums/frontend/audio?v='.$link);
                    get_instance()->enviarcorreo($producto,23);
                }
            }
            $this->db->update('ventas',array('procesado'=>self::PROCESADO,'pagado'=>1),array('id'=>$id));
            unset($id);
        }
    }
    
    function pagoNoOk($id,$tpv = ''){
        if(!empty($id)){                            
            $this->db->update('ventas',array('procesado'=>self::NOPROCESADO),array('id'=>$id));
            $this->db->select('ventas.id as nrocompra, ventas.precio as totalP, user.*, ventas.*, albums.*, ventas_detalles.cantidad as cantidadProd, ventas_detalles.id as idDetalle');
            $this->db->join('ventas_detalles','ventas_detalles.ventas_id = ventas.id');
            $this->db->join('albums','ventas_detalles.productos_id = albums.id');
            $this->db->join('user','user.id = ventas.user_id','inner');
            $producto = $this->db->get_where('ventas',array('ventas.id'=>$id));
            if($producto->num_rows()>0){    
                $producto = $producto->row();
                $this->load->helper('redsys_responses');
                $producto->tpv = (INT)$tpv->Ds_Response;
                $producto->response = getMessageRedsys((INT)$tpv->Ds_Response);
                get_instance()->enviarcorreo($producto,13,'info@discotecarecords.com');
            }            
            unset($id);
        }
    }
}