<body class="kopa-home-1">

        <!-- kopa-area-fit -->
        <!-- kopa-ab -->
    </div>
    
    <section class="kopa-area kopa-area-9 kopa-area-dark kopa-parallax" style="padding-bottom: 100px;">
        <div class="span-bg"></div>
        <div class="wrapper">
            <div class="row">
                
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="widget kopa-countdown-2-widget">
                        <div class="cd-content">                            
                            <h3 class="entry-title"><a href="#">PRESENTACIÓN DEL NUEVO SELLO DISCOGRÁFICO</a></h3>
                            <img src="[base_url]theme/theme/placeholders/logo33.png" alt="">
                            <p class="cd-date">15 Diciembre 2018</p>
                            
                            <img src="[base_url]theme/theme/placeholders/logodisco.png" alt="">
                            <p style=" margin-top: 20px;">C/ Miquel Torelló i Pagés, 5. Molins de Rei (BARCELONA)</p>
                            <h3 class="entry-title" style=" color: white;">AFORO LIMITADO • ENTRADA GRATUITA</h3>
                            <ul class="kopa-countdown-2">
                            </ul>
                        </div>
                    </div>
                    <!-- widget -->
                    <div class="col-xs-12 mostrarSlider" style="text-align: center; margin-top: 40px;">
                        <div class="col-xs-12 col-md-6" style="display:inline-block; float:none;">
                            <article class="entry-item video-post">
                                <!-- Hidden video div -->
                                <div style="display:none;" id="video1">
                                    <video class="lg-video-object lg-html5" controls preload="none">
                                        <source src="<?= base_url() ?>video/video.mp4" type="video/mp4">
                                        <source src="<?= base_url() ?>video/video.ogg" type="video/ogg">
                                        <source src="<?= base_url() ?>video/video.ogv" type="video/ogv">
                                         Your browser does not support HTML5 video.
                                    </video>
                                </div>
                                 
                                <!-- data-src should not be provided when you use html5 videos -->
                                <article class="kopa-span-rt-1 entry-item video-post" style="position:inherit">
                                    <div class="entry-thumb" style="z-index: 1000;">
                                        <a href="#"><img src="<?= base_url() ?>img/previewVideo.png" alt=""></a>
                                        <div id="video-gallery" class="thumb-hover" style="opacity: 1">
                                            <a href="https://www.youtube.com/watch?v=jtpU72Z54lI" class="thumb-icon style1">
                                                <i class="fa fa-play"></i>
                                            </a>
                                        </div>
                                    </div>
                                </article>
                            </article>
                        </div>
                    </div>
                </div>
                <!-- col-md-12 -->
                
            </div>
            <!-- row -->
            
        </div>
        <!-- wrapper -->
        
    </section>

        
    
    <!-- kopa-area-9 -->
    <div class="widget kopa-album-2-widget">
        <header class="kopa-area-dark">
            
            <div class="kopasliderhome">
                <div class="item">
                    <div class="wrapper">
                        <h3 class="widget-title style1">top MAXI'S</h3>
                        <h5 style="color:white">THE BEST QUALITY MASTERS</h5>
                    </div>
                </div>
                <div class="item">
                    <div class="wrapper">
                        <h3 class="widget-title style1">top MAXI'S</h3>
                        <h5 style="color:white">ORIGINAL EXTENDED VERSIONS</h5>
                    </div>
                </div>
                <div class="item">
                    <div class="wrapper">
                        <h3 class="widget-title style1">top MAXI'S</h3>
                        <h5 style="color:white">IN SPECIAL EDITION</h5>
                    </div>
                </div>
                <div class="item">
                    <div class="wrapper">
                        <h3 class="widget-title style1">top MAXI'S</h3>
                        <h5 style="color:white">12” MAXI SINGLE</h5>
                    </div>
                </div>
            </div>

        </header>
        
    </div>
    <!-- widget -->


   
    
  
    [footer]
</div>
<!-- main-content -->




<div class="widget kopa-featured-audio-widget" style="position:fixed; left:0px; bottom:0px; width:100%; margin-bottom: 0px; z-index:10000">
        <div class="outer">
            <div class="col-left">
                <div class="album-thumb">
                    <a href="#"><img src="[base_url]theme/theme/images/img/2.png" alt=""></a>
                </div>
                <div class="album-content">
                    <h6><a href="#">Presentación Sello Discográfico</a></h6>
                    <p><a href="#">DISCOTECA RECORDS</a></p>
                </div>
            </div>
            <div class="col-right">
                <div class="audio-wrap">
                    <div class="kopa-jp-jplayer"></div>
                    <div class="jp-audio kopa-jp-wrap" role="application" aria-label="media player">
                        <div class="jp-type-playlist">
                            <div class="jp-gui jp-interface">
                                <div class="col-left">
                                    <div class="jp-controls">
                                        <button class="jp-previous" role="button" tabindex="0"></button>
                                        <button class="jp-play" role="button" tabindex="0"></button>
                                        <button class="jp-next" role="button" tabindex="0"></button>
                                        <span class="fa fa-bar-chart"></span>
                                    </div>
                                </div>
                                <div class="col-right">
                                    <div class="jp-progress">
                                        <div class="jp-seek-bar">
                                            <div class="jp-play-bar"></div>
                                        </div>
                                    </div>
                                    <div class="jp-volume-controls">
                                        <span class="fa fa-volume-down"></span>
                                        <div class="jp-volume-bar">
                                            <div class="jp-volume-bar-value"></div>
                                        </div>
                                    </div>
                                    <div class="jp-time-holder">
                                        <div class="jp-current-time" role="timer" aria-label="time">&nbsp;</div>
                                        <div class="jp-duration" role="timer" aria-label="duration">&nbsp;</div>
                                    </div>
                                </div>
                            </div>
                            <div class="jp-playlist">
                                <ul style="display: none !important">
                                    <li>&nbsp;</li>
                                </ul>
                            </div>
                            <div class="jp-no-solution">
                                <span>Update Required</span>
                                To play the media you will need to either update your browser to a recent version or update your <a href="http://get.adobe.com/flashplayer/" target="_blank">Flash plugin</a>.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
    </div>
    
    <article id="recopilatoriosContent" class="contentOculto" style="display: none;">
        <div class="widget kopa-audio-list-widget" style="margin-bottom: 0; position:relative;">
            <a href="javascript:closeListRecop()" style="position: absolute; right: -20px; top:-20px">
                <i class="fa fa-times"></i>
            </a>
            <div class="widget-thumb">
                <div class="entry-thumb">
                    <a href="#"><img src="[base_url]theme/theme/images/img/5.png" alt=""></a>
                </div>
                <ul class="info">
                    <li>
                        <span>2014</span>
                    </li>
                    <li>
                        <span>19 songs</span>
                    </li>
                    <li>
                        <div class="kopa-rating">
                            <ul>
                                <li><span class="fa fa-star"></span></li>
                                <li><span class="fa fa-star"></span></li>
                                <li><span class="fa fa-star"></span></li>
                                <li><span class="fa fa-star"></span></li>
                                <li class="inactive"><span class="fa fa-star"></span></li>
                            </ul>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="widget-content">
                <header>
                    <h3><a href="#">Andreas Weise <span class="precio"> - 20€</span></a></h3>
                    <p><a href="#">Leona</a>&nbsp;-&nbsp;<a href="#">pop</a>&nbsp;-&nbsp;<a href="#" title="Añadir a la lista de favoritos"><i class="fa fa-heart"></i></a></p>
                </header>
                <ul class="row audio-list">
                    <li class="col-md-6 col-sm-6 col-xs-12"><a href="#">Zuper - Glide</a></li>
                    <li class="col-md-6 col-sm-6 col-xs-12"><a href="#">Zuper - Glide</a></li>
                    <li class="col-md-6 col-sm-6 col-xs-12"><a href="#">Lana Del Rey - Ride (Barretso Remix)</a></li>
                    <li class="col-md-6 col-sm-6 col-xs-12"><a href="#">Lana Del Rey - Ride (Barretso Remix)</a></li>
                    <li class="col-md-6 col-sm-6 col-xs-12"><a href="#">Four Tet - Lion (Jamie xx Remix)</a></li>
                    <li class="col-md-6 col-sm-6 col-xs-12"><a href="#">Four Tet - Lion (Jamie xx Remix)</a></li>
                    <li class="col-md-6 col-sm-6 col-xs-12"><a href="#">Jamie xx & Yasmin - Touch Me</a></li>
                    <li class="col-md-6 col-sm-6 col-xs-12"><a href="#">Jamie xx & Yasmin - Touch Me</a></li>
                    <li class="col-md-6 col-sm-6 col-xs-12"><a href="#">Disclosure - Tenderly</a></li>
                    <li class="col-md-6 col-sm-6 col-xs-12"><a href="#">Disclosure - Tenderly</a></li>
                </ul>
            </div>
        </div>
    </article>

    <script>

        var recopHtml = $("#recopilatoriosContent").clone();
        $("#recopilatoriosContent").remove();

        $(".recopilatoriolink").on('click',function(e){
            e.preventDefault();
            selRecopilatorio(this);
        });

        function closeListRecop(){
            $(".recopilatorioContent").remove();
            $(".entry-item.active").removeClass('active');
        }
        
        function selRecopilatorio(e){
            $(".recopilatorioContent").remove();
            $(".entry-item.active").removeClass('active');
            var content = recopHtml.clone();
            content.show();
            content.addClass('recopilatorioContent');
            console.log(e);
            $(e).parents('.owl-wrapper-outer').after(content);
            $(e).parents('.entry-item').addClass('active');
        }    
    </script>