
	<article class="entry-item cdparty">
		<div class="entry-wrap">
			<div class="entry-top">
				<div class="entry-thumb">
					<?php if(!empty($d->precio_tachado)): ?>
						<span class="onsale"><?= $d->precio_tachado ?></span>
					<?php endif ?>
					<span class="sd"></span>
					<?php if(!empty($d->disco)): ?>
                     <img src="<?= $d->disco ?>" alt="" style="">
                    <?php endif ?>
					<?php if(!empty($d->disco2)): ?>
                     <img src="<?= $d->disco2 ?>" alt="" style="">
                    <?php endif ?>
                    <?php if(!empty($d->disco3)): ?>
                     <img src="<?= $d->disco3 ?>" alt="" style="">
                    <?php endif ?>
                    <?php if(!empty($d->disco4)): ?>
                     <img src="<?= $d->disco4 ?>" alt="" style="">
                    <?php endif ?>
					<a href="<?= $d->link ?>">
						<img src="<?= $d->caratula ?>" alt="">
					</a>					
				</div>
			</div>
			<?php if(empty($visible)): ?>
				<div class="entry-bottom trackAudio">
					<div class="audio-wrap">
						<div id="<?= 'reproductor'.$d->id ?>" class="kopa-jp-jplayer2"></div>
						<div id="<?= 'reproductorControls'.$d->id ?>" class="jp-audio kopa-jp-wrap2" role="application" aria-label="media player">
							<div class="jp-type-playlist">
								<div class="jp-gui jp-interface">
									<div class="jp-top">
										<div class="jp-controls">
											<button class="jp-previous" role="button" tabindex="0"></button>
											<button class="jp-play" role="button" tabindex="0" data-audio="<?= $d->demo ?>" data-title="<?= $d->titulo ?>"></button>
											<button class="jp-next" role="button" tabindex="0"></button>
										</div>
									</div>
									<div class="jp-bottom">
										<div class="jp-progress">
											<div class="jp-seek-bar" style="width: 100%;">
												<div class="jp-play-bar"></div>
											</div>
										</div>
										<div class="jp-volume-controls">
											<span class="fa fa-volume-down"></span>
											<div class="jp-volume-bar">
												<div class="jp-volume-bar-value"></div>
											</div>
										</div>
										<div class="jp-time-holder">
											<div class="jp-current-time" role="timer" aria-label="time">00:00</div>
											<div class="jp-duration" role="timer" aria-label="duration">00:00</div>
										</div>
									</div>
								</div>
								<div class="jp-playlist">
									<ul style="display: none !important">
										<li>&nbsp;</li>
									</ul>
								</div>
								<div class="jp-no-solution">
									<span>Update Required</span>
									To play the media you will need to either update your browser to a recent version or update your <a href="http://get.adobe.com/flashplayer/" target="_blank">Flash plugin</a>.
								</div>
							</div>
						</div>
					</div>
				</div>
			<?php endif ?>
		</div>
		<div class="entry-content" style="position:relative;">			
			<p class="entry-cat" style="position:absolute; right:5px;font-size: 22px;">
				<?= $d->precio ?>
				<span style="display: block;font-size: 10px;text-align: right;">VAT INC.</span>	
			</p>
			<h4 class="entry-title">
				<a href="<?= $d->link ?>">
					<span><?= $d->referencia ?></span>
					<span><?= $d->titulo ?></span>
				</a>
			</h4>
			<p class="entry-cat"><a href="<?= $d->link ?>"><?= $d->artista ?></a></p>
			<footer>
				<ul class="clearfix">
					<li>
						<span><?= date("Y",strtotime($d->_lanzamiento)); ?></span>
					</li>
					<li>
						<span><?= $d->canciones ?> tracks</span>
					</li>
					<li class="fav">
						<a href="javascript:addToFav(<?= $d->id ?>)" class="linkcuadro" data-toggle="tooltip" data-placement="top" title="Haz añadido este disco a tu lista de favoritos, tu lista la encontrarás en el menú superior de la página en el símbolo del corazón"><i class="fa fa-heart icon-like fav<?= $d->id ?>"></i></a>
					</li>
					<li class="addToCartBtn">
						<a class="<?= $d->_precio==0?'opa':'' ?> button add_to_cart_button product_type_simple" href="javascript:addToCart(<?= $d->id ?>,1)">Add to card</a>
					</li>
				</ul>
			</footer>
		</div>
	</article>