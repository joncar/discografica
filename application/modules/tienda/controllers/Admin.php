<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Admin extends Panel{
        const IDRADIOS = 4;
        const IDTIENDA = 5;
        function __construct() {
            parent::__construct();            
        } 

        public function loadView($param = array('view'=>'main'))
        {
            if($this->router->fetch_class()!=='registro' && empty($_SESSION['user']))
            {               
                header("Location:".base_url('registro/index/add'));
            }
            else{
                if($this->router->fetch_class()!=='registro' && !$this->user->hasAccess()){
                    throw  new Exception('<b>ERROR: 403<b/> No posseeix permisos per a realitzar aquesta operació','403');
                }
                else{
                    if(!empty($param->output)){
                        $panel = 'panelUsuario';
                        $param->view = empty($param->view)?$panel:$param->view;
                        $param->crud = empty($param->crud)?'user':$param->crud;
                        $param->title = empty($param->title)?ucfirst($this->router->fetch_method()):$param->title;
                    }
                    if(is_string($param)){
                        $param = array('view'=>$param);
                    }
                    $template = 'template';
                    $this->load->view($template,$param);
                }                
            }
        }

        function perfil($x = '',$y = ''){
            if(is_numeric($y) && $y!=$this->user->id){
                redirect('cuenta');
            }else{
                if(empty($x) || $x=='list' || $x=='success'){
                    header("Location:".base_url('panel'));
                }
                $this->as['perfil'] = 'user';
                $this->norequireds = array('foto','email','status','admin');
                $crud = $this->crud_function($x,$y);
                $crud->unset_list();
                $crud->where('id',$this->user->id)
                     ->set_theme('bootstrap3');            
                //$crud->fields('nombre','apellido_paterno','email','password','foto','ciudad','provincias_id','codigo_postal','');
                $crud->field_type('status','invisible');
                $crud->field_type('email','invisible');
                $crud->field_type('password','password')
                     ->field_type('admin','invisible')
                     ->field_type('fecha_registro','invisible')
                     ->field_type('foto','hidden'); 


                $crud->display_as('password',l('password'))
                     ->display_as('emisora',l('emisora'))
                     ->display_as('tienda',l('tiendaname'))
                     ->display_as('apellido_paterno',l('apellidos'))
                     ->display_as('ciudad',l('ciudad'))                     
                     ->display_as('nombre',l('nombre'))
                     ->display_as('email','Email')
                     ->display_as('provincias_id',l('provincia'))
                     ->display_as('paises_id',l('pais'))
                     ->display_as('telefono',l('telefono'))
                     ->display_as('codigo_postal',l('cp'))
                     ->display_as('direccion',l('direccion'));

                $crud->callback_before_update(function($post,$primary){
                    if(!empty($primary) && get_instance()->db->get_where('user',array('id'=>$primary))->row()->password!=$post['password'] || empty($primary)){
                        $post['password'] = md5($post['password']);                
                    }                
                    return $post;
                });
                $crud->callback_after_update(function($post,$id){
                    get_instance()->user->login_short($id);
                });
                $crud->unset_jquery();
                $crud = $crud->render();
                $crud->view = 'cuenta';
                $crud->activeTab = 'data-tab="perfil"';
                $this->loadView($crud);
            }
        }

        function pedidos_por_pagar(){
            $this->as['pedidos_por_pagar'] = 'ventas';
            $crud = $this->crud_function('','');
            $crud->where('user_id',$this->user->id);
            $crud->set_theme('bootstrap3')
                 ->unset_add()
                ->unset_edit()
                ->unset_read()
                ->unset_delete()
                ->unset_export();
            $crud->unset_columns('user_id');            
            $crud->display_as('productos_id','Producto');
            $crud->columns('id','productos','precio','cantidad','fecha_compra','procesado');
            $crud->display_as('id','Nº Compra')
                 ->display_as('user_id','Usuario');
            $crud->where('pagado = 0',NULL,'ESCAPE');
            $crud->where('pasarela',4);
            $crud->callback_column('procesado',function($val,$row){
                switch($val){
                    case '-1': return '<span class="label label-danger">'.l('no-procesado').'</span>'; break;
                    case '1': return '<a href="'.site_url('store/pagar_articulo/'.$row->id).'" class="sr-button button-1 button-tiny">'.l('por-procesar').'</a>'; break;
                    case '2': return '<span class="label label-info">'.l('envio-pendiente').'</span>'; break;
                    case '3': return '<span class="label label-success">'.l('enviado').'</span>'; break;
                }
            });
            $crud->unset_jquery();
            $crud->set_url('t/admin/cuenta/');
            $crud = $crud->render();
            $crud->view = 'cuenta';
            $crud->activeTab = 'data-tab="pedidosPendientes"';
            $this->loadView($crud);
        }
        
        function cuenta(){
            $this->as['cuenta'] = 'view_ventas_cliente';
        	$crud = $this->crud_function('','');
            $crud->where('user_id',$this->user->id);
            $crud->set_theme('bootstrap3')
                 ->unset_add()
                ->unset_edit()
                ->unset_read()
                ->unset_delete()
                ->unset_export();
            $crud->unset_columns('user_id');            
            $crud->display_as('productos_id','Producto');
            $crud->columns('id','productos','precio','cantidad','fecha_compra','procesado');
            $crud->display_as('id','Nº Compra')
                 ->display_as('user_id','Usuario')
                 ->set_primary_key('id');
            $crud->callback_column('procesado',function($val,$row){
                switch($val){
                    case '-1': return '<span class="label label-danger">'.l('no-procesado').'</span>'; break;
                    case '1': return '<a href="'.site_url('store/pagar_articulo/'.$row->id).'" class="sr-button button-1 button-tiny">'.l('por-procesar').'</a>'; break;
                    case '2': return '<span class="label label-info">'.l('envio-pendiente').'</span>'; break;
                    case '3': return '<span class="label label-success">'.l('enviado').'</span>'; break;
                }
            });
            $crud->unset_jquery();
            $crud->callback_before_delete(function($primary){
                $producto = get_instance()->db->get_where('ventas',array('id'=>$primary));
                if($producto->num_rows>0){
                    if($producto->row()->procesado!=1){
                        return false;
                    }
                }
            });
            $crud->set_url('t/admin/cuenta/');
            $crud = $crud->render();
            $crud->view = 'cuenta';
            $crud->activeTab = 'data-tab="cuenta"';
            $this->loadView($crud);
        }

        function digitales($x = ''){            
            $this->as['digitales'] = 'view_digitales';
            $crud = $this->crud_function('','');    
            if(!empty($x) && is_numeric($x)){
                $crud->where('albums_id',$x);
            }        
            $crud->set_theme('bootstrap4')
                 ->unset_add()
                ->unset_edit()
                ->unset_read()
                ->unset_delete()
                ->unset_export()
                ->unset_delete()
                ->order_by('orden','ASC')
                ->unset_columns('id','orden',$crud->encodeFieldName('user_id'),$crud->encodeFieldName('albums_id'))
                ->set_primary_key('id')
                ->callback_column('fichero',function($val,$row){
                    return '<a href="'.base_url().'tracks/'.$val.'" target="_new" style="color:#07d755"><i class="fa fa-cloud-download fa-2x"></i></a>';
                })
                ->where('user_id',$this->user->id);            
            $crud->unset_jquery();                        
            $crud = $crud->render();
            $crud->view = 'cuenta';
            $crud->activeTab = 'data-tab="digitales"';
            $crud->output = $this->load->view('digitales',array('output'=>$crud->output,'albums_id'=>$x),TRUE);
            $this->loadView($crud);
        }

        function permisos(){                        
            $this->loadView(array('view'=>'permisos','title'=>'Permisos de usuario'));
        }

        function solicitar(){
            $post = $_POST;
            if(!empty($post['emisora'])){
                //Enviar correo de confirmación
                $this->db->update('user',array('emisora'=>$post['emisora']),array('id'=>$this->user->id));
                $_SESSION['emisora'] = $post['emisora'];
                $this->db->select('user.*, provincias.nombre as provincia, paises.nombre as pais');
                $this->db->join('provincias','user.provincias_id = provincias.id','LEFT')
                         ->join('paises','user.paises_id = paises.id','LEFT');
                $post = $this->db->get_where('user',array('user.id'=>$this->user->id))->row();
                $post = (array)$post;
                $post['seccion'] = 'Promos radio';
                $post['link'] = base_url('seguridad/aprobar_ingreso/'.$this->user->id.'/'.self::IDRADIOS);
                $this->enviarcorreo((object)$post,21,'radios@discotecarecords.com');
                
                
                

            }

            if(!empty($post['tienda'])){
                //Enviar correo de confirmación
                $this->db->select('user.*, provincias.nombre as provincia, paises.nombre as pais');
                $this->db->join('provincias','user.provincias_id = provincias.id','LEFT')
                         ->join('paises','user.paises_id = paises.id','LEFT');
                $post = $this->db->get_where('user',array('user.id'=>$this->user->id))->row();
                $post = (array)$post;
                $post['seccion'] = 'Sección tienda';
                $post['link'] = base_url('seguridad/aprobar_ingreso/'.$this->user->id.'/'.self::IDTIENDA);
                
                $this->enviarcorreo((object)$post,21,'info@discotecarecords.com');
                $this->db->update('user',array('tienda'=>$post['tienda']),array('id'=>$this->user->id));
                $_SESSION['tienda'] = $post['tienda'];
            }

            echo $this->success('Solicitud realizada con éxito, se le notificará por email cuando sea efectiva');
        }
    }
