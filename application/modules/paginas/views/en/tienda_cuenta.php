<body class="woocommerce woocommerce-page kopa-subpage">
<div id="main-content">
	[menu]
		<header class="page-header have-disc-icon text-center">
			<div class="disc-bg"></div>
            <div class="mask"></div>

            <div class="page-header-bg-2 page-header-bg"></div>

            <div class="page-header-inner page-header-inner-1">

                <div class="wrapper">
                    
                    <h1 class="page-title"><?= $this->user->nombre ?></h1>

                    <div class="breadcrumb clearfix">                    
                        <span itemtype="http://data-vocabulary.org/Breadcrumb" itemscope="">
                            <a href="<?= base_url() ?>" itemprop="url">
                                <span itemprop="title">Home</span>
                            </a>
                        </span>
                        <span>&nbsp;/&nbsp;</span>
                        <span itemtype="http://data-vocabulary.org/Breadcrumb" itemscope="">
                            <a href="<?= base_url() ?>" itemprop="url">
                                <span itemprop="title">Profile</span>
                            </a>
                        </span>
                        <!-- <span>&nbsp;/&nbsp;</span> -->
                        <!-- <span itemtype="http://data-vocabulary.org/Breadcrumb" itemscope="" class="current-page"><span itemprop="title">Typography</span></span> -->
                    </div>
                    <!-- breadcrumb -->

                </div>
                <!-- wrapper -->

                </div>
                <!-- page-header-inner -->

                <div class="album-icon">
	                <div class="icon-inner-1">
	                    <span class="icon-inner-2"></span>
	                </div>
	                <span class="fa fa-music"></span>
	            </div>
            
        </header>
        <!-- page-header -->
		<div class="wrapper clearfix tiendaCuenta">
			<section class="elements-box mb-40">
				<div class="tabs vertical-tabs">
	                <ul class="tab-nav clearfix" style="padding:0">
	                    <li data-tab="cuenta">
	                    	<h6 class="tab-name uppercase">
	                    	<a rel="canonical" href="[base_url]cuenta">My orders</a></h6>
	                    </li>
	                    <li data-tab="perfil">
	                    	<h6 class="tab-name uppercase">
	                    	<a rel="canonical" href="[base_url]t/admin/perfil/edit/<?= $this->user->id ?>">User data</a></h6>
	                    </li>
	                    <li>
	                    	<h6 class="tab-name uppercase">
	                    	<a rel="canonical" href="[base_url]t/admin/permisos">Access permits</a></h6>
	                    </li>
	                    <?php if($this->user->getAccess('funciones.*',array('funciones.nombre'=>'tiendas'))->num_rows()>0): ?>
		                    <li data-tab="pedidosPendientes">
		                    	<h6 class="tab-name uppercase">
		                    	<a rel="canonical" href="[base_url]t/admin/pedidos_por_pagar">Orders without payment</a></h6>
		                    </li>
	                	<?php endif ?>
	                </ul>
	                <div class="tab-container clearfix">
	                    <div class="tab-content tab1" style="display: block">
	                        [output]
	                    </div> <!-- END .tab-content -->
	                </div> <!-- END .tab-container -->  
	            </div>
	            
	        </section>
	        </div>
		</div>
	<script src="https://cdn.jsdelivr.net/jquery.migrate/1.4.1/jquery-migrate.min.js"></script>
	[footer]
</div>