<body class="woocommerce woocommerce-page kopa-subpage">
<div id="main-content">
	[menu]
		<header class="page-header have-disc-icon text-center">
			<div class="disc-bg"></div>
            <div class="mask"></div>

            <div class="page-header-bg-2 page-header-bg"></div>

            <div class="page-header-inner page-header-inner-1">

                <div class="wrapper">
                    
                    <h1 class="page-title"><?= $this->user->nombre ?></h1>

                    <div class="breadcrumb clearfix">                    
                        <span itemtype="http://data-vocabulary.org/Breadcrumb" itemscope="">
                            <a href="<?= base_url() ?>" itemprop="url">
                                <span itemprop="title"><?= l('inicio') ?></span>
                            </a>
                        </span>
                        <span>&nbsp;/&nbsp;</span>
                        <span itemtype="http://data-vocabulary.org/Breadcrumb" itemscope="">
                            <a href="<?= base_url() ?>" itemprop="url">
                                <span itemprop="title"><?= l('perfil-de-usuario') ?></span>
                            </a>
                        </span>
                        <!-- <span>&nbsp;/&nbsp;</span> -->
                        <!-- <span itemtype="http://data-vocabulary.org/Breadcrumb" itemscope="" class="current-page"><span itemprop="title">Typography</span></span> -->
                    </div>
                    <!-- breadcrumb -->

                </div>
                <!-- wrapper -->

                </div>
                <!-- page-header-inner -->

                <div class="album-icon">
	                <div class="icon-inner-1">
	                    <span class="icon-inner-2"></span>
	                </div>
	                <span class="fa fa-music"></span>
	            </div>
            
        </header>
        <!-- page-header -->
		<div class="wrapper clearfix tiendaCuenta">
			<section class="elements-box mb-40">
				<div class="tabs vertical-tabs">
	                <ul class="tab-nav clearfix" style="padding:0">
	                    <li data-tab="cuenta">
	                    	<h6 class="tab-name uppercase">
	                    	<a rel="canonical" href="[base_url]cuenta"><?= l('mis-pedidos') ?></a></h6>
	                    </li>
	                    <li data-tab="perfil">
	                    	<h6 class="tab-name uppercase">
	                    	<a rel="canonical" href="[base_url]t/admin/perfil/edit/<?= $this->user->id ?>"><?= l('datos-de-usuario') ?></a></h6>
	                    </li>
	                    <li class="active">
	                    	<h6 class="tab-name uppercase">
	                    	<a rel="canonical" href="[base_url]t/admin/permisos"><?= l('permisos-de-acceso') ?></a></h6>
	                    </li>
	                    <?php if($this->user->getAccess('funciones.*',array('funciones.nombre'=>'tiendas'))->num_rows()>0): ?>
		                    <li data-tab="pedidosPendientes">
		                    	<h6 class="tab-name uppercase">
		                    	<a rel="canonical" href="[base_url]t/admin/pedidos_por_pagar"><?= l('pedidos-por-pagar') ?></a></h6>
		                    </li>
	                	<?php endif ?>
	                </ul>
	                <div class="tab-container clearfix">
	                    <div class="tab-content tab1" style="display: block">
	                        <div class="panel panel-default">
	                        	<div class="row" style="margin-top:20px; border:1px solid; padding:20px; margin-left:0; margin-right:0">
		                        	<div class="col-xs-6 col-md-10">
		                        		<?= l('compras-generales') ?>
		                        	</div>
		                        	<div class="col-xs-6 col-md-2" style="text-align:right">
		                        		<?= l('concedido') ?>
		                        	</div>
		                        </div>
		                        <div class="row" style="margin-top:20px; border:1px solid; padding:20px; margin-left:0; margin-right:0">
		                        	<div class="col-xs-12 col-md-8">
		                        		<?= l('acceso-a-promos-tienda') ?>
		                        	</div>
		                        	<div class="col-xs-12 col-md-4" style="text-align:right">
		                        		<?php if($this->user->paises_id==1): ?>
			                        		<?php if(empty($this->user->tienda)): ?>
			                        			<a class="button add_to_cart_button product_type_simple" href="#tienda" data-toggle="modal"><?= l('solicitar') ?></a>
			                        		<?php elseif(!empty($this->user->tienda) && $this->db->get_where('user_group',array('user'=>$this->user->id,'grupo'=>5))->num_rows()==0): ?>
			                        			<a class="button add_to_cart_button product_type_simple" href="#tienda" data-toggle="modal"><?= l('reenviar-solicitud') ?></a>
			                        		<?php else: ?>
			                        			<?= l('concedido') ?>
			                        		<?php endif ?>
		                        		<?php else: ?>
											<?= l('servicio-solo-para-espana') ?>
		                        		<?php endif ?>

		                        	</div>
		                        </div>
		                        <div class="row" style="margin-top:20px; border:1px solid; padding:20px; margin-left:0; margin-right:0">	
		                        	<div class="col-xs-12 col-md-8">
		                        		<?= l('acceso-a-promos-radio') ?>
		                        	</div>
		                        	<div class="col-xs-12 col-md-4" style="text-align:right">
		                        		<?php if(empty($this->user->emisora)): ?>
		                        			<a class="button add_to_cart_button product_type_simple" href="#radio" data-toggle="modal"><?= l('solicitar') ?></a>
		                        		<?php elseif(!empty($this->user->emisora) && $this->db->get_where('user_group',array('user'=>$this->user->id,'grupo'=>5))->num_rows()==0): ?>
		                        			<a class="button add_to_cart_button product_type_simple" href="#radio" data-toggle="modal"><?= l('reenviar-solicitud') ?></a>
		                        		<?php else: ?>
		                        			<?= l('concedido') ?>
		                        		<?php endif ?>
		                        	</div>
	                        	</div>
	                        </div>
	                    </div> <!-- END .tab-content -->
	                </div> <!-- END .tab-container -->  
	            </div>
	            
	        </section>
	        </div>
		</div>

	<div class="modal fade" tabindex="-1" role="dialog" id="radio">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <form action="t/admin/solicitar" onsubmit="return sendForm(this,'#radioResponse')">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        <h4 class="modal-title"><?= l('solicitar-acceso-a-promos-radio') ?></h4>
		      </div>
		      <div class="modal-body">
		        <div class="form-group">
				    <label for="exampleInputEmail1"><?= l('nombre-emisora') ?></label>
				    <input type="text" name="emisora" class="form-control" id="exampleInputEmail1" placeholder="<?= l('nombre-emisora') ?>" value="<?= $this->user->emisora ?>">
			    </div>
			    <div id="radioResponse"></div>
		      </div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-default" data-dismiss="modal"><?= l('cerrar') ?></button>
		        <button type="submit" class="button add_to_cart_button product_type_simple"><?= l('enviar') ?></button>
		      </div>
		  </form>
	    </div><!-- /.modal-content -->
	  </div><!-- /.modal-dialog -->
	</div><!-- /.modal -->

	<div class="modal fade" tabindex="-1" role="dialog" id="tienda">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <form action="t/admin/solicitar" onsubmit="return sendForm(this,'#tiendaResponse')">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        <h4 class="modal-title"><?= l('solicitar-acceso-a-promos-tienda') ?></h4>
		      </div>
		      <div class="modal-body">
		        <div class="form-group">
				    <label for="exampleInputEmail1"><?= l('nombre-tienda') ?></label>
				    <input type="text" name="tienda" class="form-control" id="exampleInputEmail1" placeholder="" value="<?= l('nombre-tienda') ?>">
			    </div>
			    <div id="tiendaResponse"></div>
		      </div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-default" data-dismiss="modal"><?= l('cerrar') ?></button>
		        <button type="submit" class="button add_to_cart_button product_type_simple"><?= l('enviar') ?></button>
		      </div>
		  </form>
	    </div><!-- /.modal-content -->
	  </div><!-- /.modal-dialog -->
	</div><!-- /.modal -->


	[footer]
</div>