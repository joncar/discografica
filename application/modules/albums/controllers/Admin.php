<?php 
    require_once APPPATH.'controllers/Panel.php';    
    class Admin extends Panel{
        function __construct() {
            parent::__construct();
        }
        
        public function artistas(){
            $crud = $this->crud_function('','');            
            $crud = $crud->render();
            $this->loadView($crud);
        }
        public function tipos_album(){
            $crud = $this->crud_function('','');            
            $crud = $crud->render();
            $this->loadView($crud);
        }

        public function albums($x = '',$y = ''){
            $crud = $this->crud_function('',''); 
            $crud->columns('tipo','titulo','caratula','precio','activo','peso','orden','stock');
            $crud->field_type('caratula','image',array('path'=>'img/albums','width'=>'300px','height'=>'300px'))
                 ->field_type('caratula_grande','image',array('path'=>'img/albums','width'=>'600px','height'=>'600px'))
            	 ->field_type('disco','image',array('path'=>'img/albums','width'=>'360px','height'=>'360px'))
                 ->field_type('disco2','image',array('path'=>'img/albums','width'=>'360px','height'=>'360px'))
                 ->field_type('disco3','image',array('path'=>'img/albums','width'=>'360px','height'=>'360px'))
                 ->field_type('disco4','image',array('path'=>'img/albums','width'=>'360px','height'=>'360px'))
                 ->display_as('activo','Publicar')
                 ->field_type('activo','true_false',array('0'=>'NO','1'=>'SI'))
                 ->display_as('vinilo','Es vinilo?')
                 ->field_type('vinilo','true_false',array('0'=>'NO','1'=>'SI'))
                 ->set_relation('tipo','tipos_album','nombre')
                 ->field_type('tipo_producto','dropdown',array('1'=>'UNIT','2'=>'PACK'))                 
                 ->field_type('promos_importadores','hidden')
                 ->field_type('idiomas','hidden')
                 ->set_field_upload('demo','audios')
                 ->set_field_upload('promo_radio_descarga','radio')
                 ->set_rules('identificador_url','URL','required|alpha_dash')
                 ->where('(promos_importadores IS NULL OR promos_importadores = 0) AND tipo_producto!=3','ESCAPE',FALSE)
                 ->set_clone()
                 ->set_order('orden');
            if($crud->getParameters()=='edit'){                      
                $packs = $this->db->get_where('albums',array('tipo_producto'=>1,'promos_importadores'=>0));
                $units = array();
                foreach($packs->result() as $p){
                    $units[$p->id.'-'.$p->referencia] = $p->id.'-'.$p->referencia;
                }
                $crud->field_type('descontar_referencias','set',$units);
            }else{
                $crud->set_rules('identificador_url','URL','required|alpha_dash|is_unique[albums.identificador_url]');
                $crud->field_type('descontar_referencias','hidden','');
            }
            $crud->add_action('Traducir','',base_url('albums/admin/albums/traducir').'/');            
            $output = $crud->render();
            if($crud->getParameters()=='edit'){
                $output->output = $this->load->view('albums',array('output'=>$output->output),TRUE);
            }
            $this->loadView($output);
        }

        public function digitales($albums_id = '',$y = ''){
            if(empty($albums_id) || !is_numeric($albums_id)){
                $this->as['digitales'] = 'albums';                
                $crud = $this->crud_function('','');     
                $crud->columns('tipo','caratula','titulo','referencia','precio')
                     ->field_type('caratula','image',array('path'=>'img/albums','width'=>'300px','height'=>'300px'))
                     ->field_type('caratula_grande','image',array('path'=>'img/albums','width'=>'600px','height'=>'600px'))
                     ->field_type('disco','image',array('path'=>'img/albums','width'=>'360px','height'=>'360px'))
                     ->field_type('disco2','image',array('path'=>'img/albums','width'=>'360px','height'=>'360px'))
                     ->field_type('disco3','image',array('path'=>'img/albums','width'=>'360px','height'=>'360px'))
                     ->field_type('disco4','image',array('path'=>'img/albums','width'=>'360px','height'=>'360px'))
                     ->display_as('activo','Publicar')
                     ->field_type('activo','true_false',array('0'=>'NO','1'=>'SI'))
                     ->display_as('vinilo','Es vinilo?')
                     ->field_type('vinilo','true_false',array('0'=>'NO','1'=>'SI'))
                     ->set_relation('tipo','tipos_album','nombre')
                     ->field_type('tipo_producto','hidden',3)
                     ->field_type('promos_importadores','hidden')
                     ->field_type('idiomas','hidden')
                     ->field_type('descontar_referencias','hidden')
                     ->field_type('promos_80_festival','hidden')
                     ->field_type('peso','hidden',1)
                     ->field_type('vinilo','hidden')
                     ->field_type('stock','hidden',1)
                     ->set_field_upload('demo','audios')
                     ->set_field_upload('promo_radio_descarga','radio')
                     ->set_rules('identificador_url','URL','required|alpha_dash')
                     ->where('tipo_producto',3)
                     ->set_clone()
                     ->set_order('orden')
                     ->add_action('Tracks descargables','',base_url('albums/admin/digitales').'/')
                     ->set_lang_string('insert_success_message','Datos almacenados con éxito <script>document.location.href="'.base_url().'albums/admin/digitales/{id}/add";</script>');
                    if(is_numeric($y)){
                        $packs = $this->db->get_where('albums',array('tipo_producto'=>3,'id !='=>$y));
                        $units = array();
                        foreach($packs->result() as $p){
                            $units[$p->id.'-'.$p->referencia] = $p->id.'-'.$p->referencia;
                        }
                        $crud->field_type('descontar_referencias','set',$units)
                             ->display_as('descontar_referencias','Relacionar con');
                    }
            }else{
                $crud = $this->crud_function('','');     
                $crud->field_type('albums_id','hidden',$albums_id)
                     ->where('albums_id',$albums_id)
                     ->unset_columns('albums_id')
                     ->set_order('orden')
                     ->set_field_upload('audio','tracks')
                     ->display_as('audio','Fichero');
                }
            $crud = $crud->render();            
            $this->loadView($crud);
        }

        public function discos_spetial_imports(){
            $this->as['discos_spetial_imports'] = 'albums';
            $crud = $this->crud_function('',''); 
            $crud->set_subject('Spetial Imports');
            $crud->columns('tipo','titulo','caratula','precio','activo','peso','orden','stock');
            $crud->field_type('caratula','image',array('path'=>'img/albums','width'=>'300px','height'=>'300px'))
                 ->field_type('caratula_grande','image',array('path'=>'img/albums','width'=>'600px','height'=>'600px'))
                 ->field_type('disco','image',array('path'=>'img/albums','width'=>'360px','height'=>'360px'))
                 ->field_type('disco2','image',array('path'=>'img/albums','width'=>'360px','height'=>'360px'))
                 ->field_type('disco3','image',array('path'=>'img/albums','width'=>'360px','height'=>'360px'))
                 ->field_type('disco4','image',array('path'=>'img/albums','width'=>'360px','height'=>'360px'))
                 ->set_relation('tipo','tipos_album','nombre')
                 ->field_type('tipo_producto','dropdown',array('1'=>'UNIT','2'=>'PACK'))                 
                 ->field_type('promos_importadores','hidden',1)                 
                 ->field_type('precio_tienda','hidden')
                 ->field_type('mas_vendido','hidden')
                 ->field_type('seleccion_especial','hidden')
                 ->field_type('promos_tienda','hidden')
                 ->field_type('promos_radio','hidden')
                 ->field_type('idiomas','hidden')
                 ->field_type('pvr','hidden')
                 ->display_as('vinilo','Es vinilo?')
                 ->field_type('vinilo','true_false',array('0'=>'NO','1'=>'SI'))
                 ->display_as('activo','Publicar')
                 ->field_type('activo','true_false',array('0'=>'NO','1'=>'SI'))
                 ->set_rules('identificador_url','URL','required|alpha_dash')
                 ->where('promos_importadores',1)
                 //->where('tipo_producto',3)
                 ->set_field_upload('demo','audios')
                 ->set_field_upload('promo_radio_descarga','radio')
                 ->set_order('orden')
                 ->set_clone();
            if($crud->getParameters()=='edit'){                      
                $packs = $this->db->get_where('albums',array('tipo_producto'=>1,'promos_importadores'=>0));
                $units = array();
                foreach($packs->result() as $p){
                    $units[$p->id.'-'.$p->referencia] = $p->id.'-'.$p->referencia;
                }
                $crud->field_type('descontar_referencias','set',$units);
            }else{
                $crud->set_rules('identificador_url','URL','required|alpha_dash|is_unique[albums.identificador_url]');
                $crud->field_type('descontar_referencias','hidden','');
            }
            $crud->add_action('Traducir','',base_url('albums/admin/discos_spetial_imports/traducir').'/');            
            if($crud->getParameters()=='add'){
                $crud->set_rules('identificador_url','URL','required|alpha_dash|is_unique[albums.identificador_url]');
            }
            $crud = $crud->render();
            $crud->title = 'Spetial imports';
            $this->loadView($crud);
        }

        public function playlists(){
            $crud = $this->crud_function('','');            
            $crud = $crud->render();
            $this->loadView($crud);
        }
        
        
    }
?>
