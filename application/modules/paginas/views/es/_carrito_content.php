<?php 
	$carrito = $this->carrito->getCarrito(); 
	$mostrarAvisos = true;
	$is_digital = false;
	foreach($carrito as $c){
		if($c->promos_80_festival==1 || $c->tipo_producto==3){
			$mostrarAvisos = false;
		}
		if($c->tipo_producto==3){
			$is_digital = true;
		}
	}
?>
<section class="elements-box" style="margin-bottom: 40px;">
	<div class="woocommerce">
		<form action="<?= base_url('tt/checkout') ?>" method="post">
		<?php 			
			$total = 0; 
			$impuesto = 0;
		?>
		<?php if(count($carrito)>0): ?>


		<div class="row">
			<div class="col-xs-12 col-sm-6 col-md-8">
				<h2><?= l('productos') ?></h2>
		
				<table class="shop_table shop_table_responsive cart table-cart" cellspacing="0" style="background: white;">
					<thead>
						<tr>
							<th class="product-remove">&nbsp;</th>
							<th class="product-thumbnail hidden-xs hidden-sm">&nbsp;</th>
							<th class="product-name"><?= l('producto') ?></th>
							<th class="product-price"><?= l('precio') ?></th>
							<th class="product-quantity"><?= l('cantidad') ?></th>
							<?php if($mostrarAvisos): ?><th class="product-quantity"><?= l('peso') ?></th><?php endif ?>
							<th class="product-subtotal"><?= l('total') ?></th>
						</tr>
					</thead>
					<tbody>
						<?php 
							$pesoVinilos = 0;
							$pesoCajaAdicional = 0;
							$cantidadVinilos = 0;
							$pesoTotal = 0;
							foreach($carrito as $c): 
							if(!empty($c->shop)){
								$e = $this->elements->promos_tiendas(array('albums.id'=>$c->id))->row(); 
							}else{
								$e = $this->elements->albums(array('albums.id'=>$c->id))->row(); 
							}
							
						?>
							<tr class="cart_item">
								<td class="product-remove">
									<a href="javascript:remToCart('<?= $c->id ?>')" class="remove" title="Remove this item" data-product_id="226" data-product_sku="">×</a>
								</td>
								<td class="product-thumbnail hidden-xs hidden-sm">
									<a href="#">
										<img class="hidden-xs" src="<?= $e->caratula ?>" class="attachment-shop_thumbnail size-shop_thumbnail wp-post-image" alt="post-34">
									</a>
								</td>
								<td class="product-name" data-title="Product">
									<a href="#">
										<img class="visible-xs" style="width:100%;" src="<?= $e->caratula ?>" class="attachment-shop_thumbnail size-shop_thumbnail wp-post-image" alt="post-34">										
										<?= isset($c->digital)?$c->titulo:$e->titulo ?>
									</a>
								</td>
								<td class="product-price" data-title="Price">
									<span class="amount"><?= moneda($e->precio) ?></span>
								</td>
								<td class="product-quantity" data-title="Quantity">
									<div class="quantity buttons_added">
										<input step="1" min="0" name="cantidad" value="<?= $c->cantidad ?>" title="Qty" class="input-text qty text" data-id="<?= !empty($c->shop)?$c->id.'.1':$c->id ?>" size="4" type="number">
									</div>
								</td>
								<?php if($mostrarAvisos): ?>
								<td class="product-quantity" data-title="Quantity">
									<div class="quantity buttons_added">
										<?php
											$peso = $e->peso*$c->cantidad;
											$pesoTotal+= $peso;
											$pesoVinilos+= $e->vinilo==1?$peso:0;
											$cantidadVinilos+= $e->vinilo==1?$c->cantidad:0;
											echo number_format($peso,3,',','.');
										?>kg
									</div>
								</td>
								<?php endif ?>
								<td class="product-subtotal" data-title="Total">
									<span class="amount"><?= moneda($c->cantidad*$e->_precio) ?></span>
								</td>
							</tr>
							<?php $total+= ($c->cantidad*$e->_precio); ?>
							<?php endforeach ?>
							<?php $pesoTotal = $this->elements->getPeso(); ?>
						</tbody>
					</table>





					<div id="australiaMessage" class="woocommerce-checkout-payment" style="margin:14px 0;background: #9b5e8f;color: #fff;padding: 14px; display: none;">
                        <div class="form-row place-order">
                            <?= l('album-detail-nota') ?>
                        </div>
                    </div>
	

					<div id="direccionEnvio">
						<h2><?= l('direccion-de-envio') ?></h2>					
						<div id="payment" style="padding: 20px;">
							<div class="form-group">
								<label><?= l('pais') ?></label>
								<select name="paises_id" id="paises_id" class="form-control">
									<option value=""><?= l('seleccione-una-opcion') ?></option>
									<?php 
										$this->db->order_by('paises.nombre','ASC');
										foreach($this->db->get_where('paises')->result() as $p):
										$p->tarifa = $this->elements->getTarifaEuropa($p->id,$pesoTotal);
									?>
										<option value="<?= $p->id ?>" data-continente="<?= $p->continentes_id ?>" data-tarifa="<?= $p->tarifa ?>"><?= $p->nombre ?></option>
									<?php endforeach ?>
								</select>
							</div>
							<div class="form-group" id="provForm">
								<label><?= l('provincia') ?></label>
								<select name="provincias_id" id="provincias_id" class="form-control">
									<option value=""><?= l('seleccione-una-opcion') ?></option>
									<?php 
										$this->db->order_by('provincias.nombre','ASC');
										foreach($this->db->get_where('provincias')->result() as $p):
											$tarifa = $this->elements->getTarifaEspanya($p->id,$pesoTotal);
									?>
										<option value="<?= $p->id ?>" data-tarifa="<?= $tarifa ?>"><?= $p->nombre ?></option>
									<?php endforeach ?>
								</select>								
							</div>
							<div class="form-group">
								<label><?= l('ciudad') ?></label>
								<input class="form-control" type="text" id="ciudad" name="ciudad" value="<?= @$this->user->ciudad ?>" placeholder="<?= l('ciudad-de-envio') ?>">
							</div>
							<div class="form-group">
								<label><?= l('direccion') ?></label>
								<input class="form-control" type="text" id="direccion" name="direccion" value="<?= @$this->user->direccion ?>" placeholder="<?= l('direccion-de-envio') ?>">
							</div>
							<div class="form-group">
								<label><?= l('cp') ?></label>
								<input class="form-control" type="text" id="codigo_postal" name="codigo_postal" value="<?= @$this->user->codigo_postal ?>" placeholder="<?= l('cp-de-envio') ?>">
							</div>
							<div class="form-group">
								<label><?= l('telefono') ?></label>
								<input class="form-control" type="text" id="telefono" name="telefono" value="<?= @$this->user->telefono ?>" placeholder="<?= l('telefono-de-contacto') ?>">
							</div>
							<div class="form-group">
								<label><?= l('confirmar-telefono') ?></label>
								<input class="form-control" type="text" id="telefono2" name="telefono2" value="<?= @$this->user->telefono ?>" placeholder="<?= l('confirmar-telefono') ?>">
							</div>
							<div class="form-group">
								<label><?= l('observaciones') ?></label>
								<textarea class="form-control" type="text" id="telefono" name="observaciones" placeholder="<?= l('deja-aqui-tu-comentario') ?>"></textarea>
							</div>
						</div>		
					</div>


					<?php if($mostrarAvisos): ?>
						<div class="woocommerce-checkout-payment"  style="margin-top:14px;background: #9b5e8f;color: #fff;padding: 14px;">
							<div class="form-row place-order">
								<?= l('clausula-envio') ?>
							</div>
						</div>		
					<?php endif ?>


			</div>
			<div class="col-xs-12 col-sm-6 col-md-4">
				<h2><?= l('tu-pedido') ?></h2>
				<table class="shop_table shop_table_responsive cart" cellspacing="0" style="background:white;">
					<tbody>
						<tr class="cart-subtotal">
							<th><?= l('subtotal') ?></th>
							<td data-title="Subtotal" style="width:80%; text-align:right"><span class="amount"><?= moneda($total) ?></span></td>
						</tr>	
						<?php if($mostrarAvisos): ?>					
							<?php if($cantidadVinilos>0): /* Quitar comentario para motivos de calculo y testing */ ?>
								<tr class="shipping">
									<th>Peso por embalaje</th>
									<td data-title="Shipping" style="width:80%; text-align:right">
										<span class="amount" id="precioPeso">
											<?php 
												$pesoCajaAdicional = ceil($cantidadVinilos/5)*0.110; 
												echo number_format($pesoCajaAdicional,3,',','.');
											?>kg
										</span>
									</td>
								</tr>
							<?php endif ?>
							<tr class="shipping">
								<th><?= l('peso') ?></th>
								<td data-title="Shipping" style="width:80%; text-align:right">
									<span class="amount" id="precioPeso"><?= number_format($this->elements->getPeso(false),3,',','.') ?>kg</span>
								</td>
							</tr>						
							<tr class="shipping">
								<th><?= l('envio') ?></th>
								<td data-title="Shipping" style="width:80%; text-align:right">
									<span class="amount" id="precioEnvio">0€</span>
								</td>
							</tr>
						<?php endif ?>
						<tr class="taxaustralia" style="display: none">
							<th style="width:80%"><?= l('tasa-australia') ?></th>
							<td data-title="Shipping" style="width:80%; text-align:right">
								<span class="amount" id="preciotaxaustralia">0€</span>
							</td>
						</tr>
						<tr class="taxaustralia">
							<th style="width:80%"><?= l('iva') ?></th>
							<td data-title="Shipping" style="width:80%; text-align:right">
								<span class="amount" id="preciotaxaustralia"><?= l('incluido') ?></span>
							</td>
						</tr>
						<tr class="order-total">
							<th><?= l('total') ?></th>
							<td data-title="Total" style="width:80%; text-align:right">
								<strong>
								<span class="amount" id="total"><?= moneda($total) ?></span>
								</strong>
							</td>
						</tr>
					</tbody>
				</table>
				<?php if($is_digital): ?>
					<?= l('digital_nota'); ?>					
				<?php endif ?>
				<h2><?= l('forma-de-pago') ?></h2>
				<h5><?= l('seleccione-una-forma-de-pago') ?></h5>
					<div id="payment" class="woocommerce-checkout-payment" style="margin-top:30px;">						
						<ul class="wc_payment_methods payment_methods methods">
							<li class="wc_payment_method payment_method_paypal">
								<input id="paymenttpv" class="input-radio" name="pasarela" value="1" checked="checked" data-order_button_text="" type="radio">
								<label for="paymenttpv">
									TPV<img src="https://canales.redsys.es/iconos/graficos/logotipos/2100logo.jpg" alt="PayPal Acceptance Mark" style="max-width: 200px;width: 100%;">
								</label>
								<div class="payment_box payment_method_cheque" style="display:none;">
									<p><?= l('directamente-al-banco') ?></p>
								</div>
							</li>
							<li class="wc_payment_method payment_method_paypal">
								<input id="payment_method_paypal" class="input-radio" name="pasarela" value="2" data-order_button_text="Proceed to PayPal" type="radio">
								<label for="payment_method_paypal">
								PayPal <img src="https://www.paypalobjects.com/webstatic/mktg/Logo/AM_mc_vs_ms_ae_UK.png" alt="PayPal Acceptance Mark"><a href="https://www.paypal.com/gb/webapps/mpp/paypal-popup" class="about_paypal" onclick="javascript:window.open('https://www.paypal.com/gb/webapps/mpp/paypal-popup','WIPaypal','toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, width=1060, height=700'); return false;" title="What is PayPal?">What is PayPal?</a>	</label>
								<div class="payment_box payment_method_paypal" style="display:none;">
									<p><?= l('paypal') ?></p>
								</div>
							</li>
							<li id="paymentcr" style="display: none" class="wc_payment_method payment_method_paypal">
								<input id="rembolso" class="input-radio" name="pasarela" value="3" data-order_button_text="" type="radio">
								<label for="rembolso">
								CR <img src="<?= base_url() ?>img/contra.png" style="width:60%;">
								<div class="payment_box rembolso">
									<?php $ajustes = $this->db->get('ajustes')->row(); ?>
									<p><?= l('cr_cargo') ?></p>
								</div>
							</li>
							<?php if($this->user->getAccess('funciones.*',array('funciones.nombre'=>'tiendas'))->num_rows()>0 && $mostrarAvisos): ?>
								<li id="paymentcr" class="wc_payment_method payment_method_paypal">
									<input id="deposito" class="input-radio" name="pasarela" value="4" data-order_button_text="" type="radio">
									<label for="deposito">
										<?= l('deposito') ?> 
									</label>
									<div class="payment_box deposito">										
										<?= l('deposito-tut') ?>									
									</div>
								</li>
							<?php endif ?>
						</ul>
						<div class="checkbox" style="margin-left: 13px;">
							<label for="">
								<input type="checkbox" name="politicas" value="1"> <?= str_replace('div','span',l('he-leido-y-acepto')) ?>
							</label>
						</div>
						<div class="form-row place-order">
							<?php echo @$_SESSION['msj']; unset($_SESSION['msj']); ?>
							<noscript>
							Since your browser does not support JavaScript, or it is disabled, please ensure you click the <em>Update Totals</em> button before placing your order. You may be charged more than the amount stated above if you fail to do so.			<br/><input type="submit" class="button alt" name="woocommerce_checkout_update_totals" value="Update totals" />
							</noscript>
							<input class="button alt" name="woocommerce_checkout_place_order" id="place_order" value="<?= l('pagar') ?>" type="submit">
						</div>
					</div>
					<?php if($mostrarAvisos): ?>
					<div class="woocommerce-checkout-payment" style="margin-top:14px;background: #9b5e8f;color: #fff;padding: 14px;">
						<div class="form-row place-order">
							<?= l('shopping-contact') ?>													
						</div>
					</div>					
					<?php endif ?>
				
			</div>
		</div>
	

</form>
<script>
	var total = <?= str_replace(',','.',$total) ?>;
	var peso = <?= $this->elements->getPeso() ?>;
	var peso_tienda = <?= $this->elements->getPeso() ?>;
	var tarifas = <?php 
		$tarifas = array();
		foreach($this->db->get_where('tarifas_envio')->result() as $t){
			$tarifas[] = $t;
		}
		echo json_encode($tarifas);
	?>
</script>
<?php else: ?>
	<?= l('carrito-vacio-content') ?>
<?php endif ?>
	</div>
	
	<!-- page-links-wrapper -->
	<!-- tag-box -->
</section>