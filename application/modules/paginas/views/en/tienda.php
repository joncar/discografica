<body class="woocommerce woocommerce-page kopa-subpage">
[menu]
<div id="main-content">

        <header class="page-header have-disc-icon text-center">

        	<div class="disc-bg"></div>

        	<div class="mask"></div>

        	<div class="page-header-bg-2 page-header-bg"></div>

        	<div class="page-header-inner page-header-inner-1">

        		<div class="wrapper">
                
	                <h1 class="page-title">Shop Categories</h1>

	                <div class="breadcrumb clearfix">                    
	                    <span itemtype="http://data-vocabulary.org/Breadcrumb" itemscope="">
	                        <a href="<?= base_url() ?>" itemprop="url">
	                            <span itemprop="title">Home</span>
	                        </a>
	                    </span>
	                    <span>&nbsp;/&nbsp;</span>
	                    <span itemtype="http://data-vocabulary.org/Breadcrumb" itemscope="">
	                        <a href="<?= base_url() ?>" itemprop="url">
	                            <span itemprop="title">Shop</span>
	                        </a>
	                    </span>
	                    <span>&nbsp;/&nbsp;</span>
	                    <span itemtype="http://data-vocabulary.org/Breadcrumb" itemscope="" class="current-page"><span itemprop="title">Shop Categories</span></span>
	                </div>
	                <!-- breadcrumb -->

	            </div>
	            <!-- wrapper -->
        		
        	</div>
        	<!-- page-header-inner -->

        	
        	<div class="album-icon">
                <div class="icon-inner-1">
                    <span class="icon-inner-2"></span>
                </div>
                <span class="fa fa-music"></span>
            </div>            
            
        </header>
        <!-- page-header -->

        <div class="wrapper clearfix">

        	<div id="container" class="mb-40">

                <div id="content" role="main">
                    
                    <h1 class="page-title">Shop</h1>

                    <p class="woocommerce-result-count">Showing 1&ndash;10 of 11 results</p>

                    <form method="get" class="woocommerce-ordering">
                        <select class="orderby" name="orderby">
                            <option value="popularity">Sort by popularity</option>
                            <option value="rating">Sort by average rating</option>
                            <option value="date">Sort by newness</option>
                            <option value="price">Sort by price: low to high</option>
                            <option value="price-desc">Sort by price: high to low</option>
                        </select>
                        <input type="hidden" value="product" name="post_type">
                    </form>
                    <!-- end:woocommerce-ordering -->

                    <div class="clear"></div>

                    <ul class="products">

                        <li class="product">
                            
                            <div>
                                
                                <div class="product-thumb">
                                	<a href="[base_url]tienda-detail.html">
	                                    <img src="[base_url]theme/theme/images/img/27.png" alt="">
	                                </a>
	                                <div class="button-box text-center">
	                                	<a href="[base_url]tienda-detail.html"><i class="fa fa-heart"></i></a>
	                                	<a href="[base_url]tienda-detail.html"><i class="fa fa-search"></i></a>
	                                </div>
                                </div>
                                <!-- product-thumb -->
                                <a href="[base_url]tienda-detail.html">
                                    <h3>Sample Fashion Product</h3>
                                    <footer>
                                    	<span class="price"><span class="amount">£50.00</span></span>
                                    	<div title="Rated 4.00 out of 5" class="star-rating"><span style="width:80%"><strong class="rating">4.00</strong> out of 5</span></div>                                    	
                                    </footer>
                                </a>

                                <a class="button add_to_cart_button product_type_simple" href="#">Add to cart</a>

                            </div>                                    

                        </li>
                        <!-- end:product -->

                        <li class="product">
                            
                            <div>
                                
                                <div class="product-thumb">
                                	<a href="[base_url]tienda-detail.html">        
	                                    <span class="onsale">Sale!</span>
	                                    <img src="[base_url]theme/theme/images/img/27.png" alt="">                                    
	                                </a>
	                                <div class="button-box text-center">
	                                	<a href="[base_url]tienda-detail.html"><i class="fa fa-heart"></i></a>
	                                	<a href="[base_url]tienda-detail.html"><i class="fa fa-search"></i></a>
	                                </div>
                                </div>
                                <!-- product-thumb -->
                                <a href="[base_url]tienda-detail.html">
                                    <h3>Sample Fashion Product</h3>
                                    <footer>                                    	
                                    	<span class="price"><del><span class="amount">$75.00</span></del> <ins><span class="amount">£50.00</span></ins></span>
                                    	<div title="Rated 4.00 out of 5" class="star-rating"><span style="width:80%"><strong class="rating">4.00</strong> out of 5</span></div>
                                    </footer>                                    
                                </a>

                                <a class="button add_to_cart_button product_type_simple clearfix" href="#">Add to cart</a>

                            </div>                                    

                        </li>
                        <!-- end:product -->

                        <li class="product">
                            
                            <div>
                                
                                <div class="product-thumb">
                                	<a href="[base_url]tienda-detail.html">
	                                    <img src="[base_url]theme/theme/images/img/27.png" alt="">
	                                </a>
	                                <div class="button-box text-center">
	                                	<a href="[base_url]tienda-detail.html"><i class="fa fa-heart"></i></a>
	                                	<a href="[base_url]tienda-detail.html"><i class="fa fa-search"></i></a>
	                                </div>
                                </div>
                                <!-- product-thumb -->
                                <a href="[base_url]tienda-detail.html">
                                    <h3>Sample Fashion Product</h3>
                                    <footer>
                                    	<span class="price"><span class="amount">£50.00</span></span>
                                    	<div title="Rated 4.00 out of 5" class="star-rating"><span style="width:80%"><strong class="rating">4.00</strong> out of 5</span></div>                                    	
                                    </footer>
                                </a>

                                <a class="button add_to_cart_button product_type_simple" href="#">Add to cart</a>

                            </div>                                    

                        </li>
                        <!-- end:product -->

                        <li class="clear"></li>

                        <li class="product">
                            
                            <div>
                                
                                <div class="product-thumb">
                                	<a href="[base_url]tienda-detail.html">
	                                    <img src="[base_url]theme/theme/images/img/27.png" alt="">
	                                </a>
	                                <div class="button-box text-center">
	                                	<a href="[base_url]tienda-detail.html"><i class="fa fa-heart"></i></a>
	                                	<a href="[base_url]tienda-detail.html"><i class="fa fa-search"></i></a>
	                                </div>
                                </div>
                                <!-- product-thumb -->
                                <a href="[base_url]tienda-detail.html">
                                    <h3>Sample Fashion Product</h3>
                                    <footer>
                                    	<span class="price"><span class="amount">£50.00</span></span>
                                    	<div title="Rated 4.00 out of 5" class="star-rating"><span style="width:80%"><strong class="rating">4.00</strong> out of 5</span></div>                                    	
                                    </footer>
                                </a>

                                <a class="button add_to_cart_button product_type_simple" href="#">Add to cart</a>

                            </div>                                    

                        </li>
                        <!-- end:product -->

                        <li class="product">
                            
                            <div>
                                
                                <div class="product-thumb">
                                	<a href="[base_url]tienda-detail.html">        
	                                    <span class="onsale">Sale!</span>
	                                    <img src="[base_url]theme/theme/images/img/27.png" alt="">                                    
	                                </a>
	                                <div class="button-box text-center">
	                                	<a href="[base_url]tienda-detail.html"><i class="fa fa-heart"></i></a>
	                                	<a href="[base_url]tienda-detail.html"><i class="fa fa-search"></i></a>
	                                </div>
                                </div>
                                <!-- product-thumb -->
                                <a href="[base_url]tienda-detail.html">
                                    <h3>Sample Fashion Product</h3>
                                    <footer>                                    	
                                    	<span class="price"><del><span class="amount">$75.00</span></del> <ins><span class="amount">£50.00</span></ins></span>
                                    	<div title="Rated 4.00 out of 5" class="star-rating"><span style="width:80%"><strong class="rating">4.00</strong> out of 5</span></div>
                                    </footer>                                    
                                </a>

                                <a class="button add_to_cart_button product_type_simple clearfix" href="#">Add to cart</a>

                            </div>                                    

                        </li>
                        <!-- end:product -->

                        <li class="product">
                            
                            <div>
                                
                                <div class="product-thumb">
                                	<a href="[base_url]tienda-detail.html">
	                                    <img src="[base_url]theme/theme/images/img/27.png" alt="">
	                                </a>
	                                <div class="button-box text-center">
	                                	<a href="[base_url]tienda-detail.html"><i class="fa fa-heart"></i></a>
	                                	<a href="[base_url]tienda-detail.html"><i class="fa fa-search"></i></a>
	                                </div>
                                </div>
                                <!-- product-thumb -->
                                <a href="[base_url]tienda-detail.html">
                                    <h3>Sample Fashion Product</h3>
                                    <footer>
                                    	<span class="price"><span class="amount">£50.00</span></span>
                                    	<div title="Rated 4.00 out of 5" class="star-rating"><span style="width:80%"><strong class="rating">4.00</strong> out of 5</span></div>                                    	
                                    </footer>
                                </a>

                                <a class="button add_to_cart_button product_type_simple" href="#">Add to cart</a>

                            </div>                                    

                        </li>
                        <!-- end:product -->

                        <li class="clear"></li>

                        <li class="product">
                            
                            <div>
                                
                                <div class="product-thumb">
                                	<a href="[base_url]tienda-detail.html">
	                                    <img src="[base_url]theme/theme/images/img/27.png" alt="">
	                                </a>
	                                <div class="button-box text-center">
	                                	<a href="[base_url]tienda-detail.html"><i class="fa fa-heart"></i></a>
	                                	<a href="[base_url]tienda-detail.html"><i class="fa fa-search"></i></a>
	                                </div>
                                </div>
                                <!-- product-thumb -->
                                <a href="[base_url]tienda-detail.html">
                                    <h3>Sample Fashion Product</h3>
                                    <footer>
                                    	<span class="price"><span class="amount">£50.00</span></span>
                                    	<div title="Rated 4.00 out of 5" class="star-rating"><span style="width:80%"><strong class="rating">4.00</strong> out of 5</span></div>                                    	
                                    </footer>
                                </a>

                                <a class="button add_to_cart_button product_type_simple" href="#">Add to cart</a>

                            </div>                                    

                        </li>
                        <!-- end:product -->

                        <li class="product">
                            
                            <div>
                                
                                <div class="product-thumb">
                                	<a href="[base_url]tienda-detail.html">        
	                                    <span class="onsale">Sale!</span>
	                                    <img src="[base_url]theme/theme/images/img/27.png" alt="">                                    
	                                </a>
	                                <div class="button-box text-center">
	                                	<a href="[base_url]tienda-detail.html"><i class="fa fa-heart"></i></a>
	                                	<a href="[base_url]tienda-detail.html"><i class="fa fa-search"></i></a>
	                                </div>
                                </div>
                                <!-- product-thumb -->
                                <a href="[base_url]tienda-detail.html">
                                    <h3>Sample Fashion Product</h3>
                                    <footer>                                    	
                                    	<span class="price"><del><span class="amount">$75.00</span></del> <ins><span class="amount">£50.00</span></ins></span>
                                    	<div title="Rated 4.00 out of 5" class="star-rating"><span style="width:80%"><strong class="rating">4.00</strong> out of 5</span></div>
                                    </footer>                                    
                                </a>

                                <a class="button add_to_cart_button product_type_simple clearfix" href="#">Add to cart</a>

                            </div>                                    

                        </li>
                        <!-- end:product -->

                        <li class="product">
                            
                            <div>
                                
                                <div class="product-thumb">
                                	<a href="[base_url]tienda-detail.html">
	                                    <img src="[base_url]theme/theme/images/img/27.png" alt="">
	                                </a>
	                                <div class="button-box text-center">
	                                	<a href="[base_url]tienda-detail.html"><i class="fa fa-heart"></i></a>
	                                	<a href="[base_url]tienda-detail.html"><i class="fa fa-search"></i></a>
	                                </div>
                                </div>
                                <!-- product-thumb -->
                                <a href="[base_url]tienda-detail.html">
                                    <h3>Sample Fashion Product</h3>
                                    <footer>
                                    	<span class="price"><span class="amount">£50.00</span></span>
                                    	<div title="Rated 4.00 out of 5" class="star-rating"><span style="width:80%"><strong class="rating">4.00</strong> out of 5</span></div>                                    	
                                    </footer>
                                </a>

                                <a class="button add_to_cart_button product_type_simple" href="#">Add to cart</a>

                            </div>                                    

                        </li>
                        <!-- end:product -->

                        <li class="clear"></li>
                        
                    </ul>
                    <!-- end:products -->

                    <nav class="woocommerce-pagination">
                        <ul class="page-numbers">
                            <li><a class="first page-numbers" href="#"><i class="fa fa-long-arrow-left"></i></a></li>
                            <li><span class="page-numbers current">1</span></li>
                            <li><a href="#" class="page-numbers">2</a></li>
                            <li><a href="#" class="page-numbers">3</a></li>
                            <li><a href="#" class="last page-numbers"><i class="fa fa-long-arrow-right"></i></a></li>
                        </ul>
                        <!-- end:page-numbers -->
                    </nav>
                    <!-- end:woocommerce-pagination -->

                </div>
                <!-- end:content -->
                
            </div>
            <!-- end:container -->

        </div>
        <!-- wrapper -->
        
    </div>
    <!-- main-content -->
[footer]