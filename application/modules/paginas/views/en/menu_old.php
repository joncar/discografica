<div id="kopa-page-header">
    <div id="kopa-header-top" >
        
        <div class="outer clearfix">
            <div id="logo-image" class="pull-left">
                
                <a href="[base_url]">
                    <img src="[base_url]theme/theme/placeholders/logo.png" alt="">
                </a>
            </div>
            <div id="kopa-header-top-inner" class="pull-left">
                <div class="clearfix">
                    <div class="left-col pull-left line-divider clearfix">
                        
                        <div class="kp-headline-wrapper pull-left clearfix">
                            <div class="kp-headline clearfix">
                                <dl class="ticker-1 clearfix">
                                    <dt></dt>
                                    <dd><a href="#">Lo mejor en musica</a></dd>
                                    <dt></dt>
                                    <dd><a href="#">Se han incorporado nuevos temas</a></dd>
                                    <dt></dt>
                                    <dd><a href="#">Puedes comprar ahora el tema single de shakira</a></dd>
                                </dl>
                                <!--ticker-1-->
                            </div>
                            <!--kp-headline-->
                        </div>
                        <!-- kp-headline-wrapper -->
                        <ul class="social-links pull-right clearfix">
                            <li><a href="#" class="fa fa-facebook"></a></li>
                            <li><a href="#" class="fa fa-twitter"></a></li>
                            <li><a href="#" class="fa fa-instagram"></a></li>
                            <li><a href="#" class="fa fa-youtube"></a></li>
                        </ul>
                        <!-- social-links -->
                    </div>
                    <!-- left-col -->
                    <div class="right-col line-divider pull-left">
                        <?php if(!$this->user->log): ?>
                        <a class="signin-button" href="<?= base_url('panel') ?>">Cuenta</a>
                        <a class="reg-button" href="<?= base_url('panel') ?>">Registro</a>
                        <?php else: ?>
                        <a class="signin-button" href="<?= base_url('cuenta') ?>">Cuenta</a>
                        <a class="reg-button" href="<?= base_url('main/unlog') ?>">Salir</a>
                        <?php endif ?>
                    </div>
                    <!-- right-col -->
                    
                </div>
                <div class="clearfix visible-xs">
                        
                    <div class="left-col pull-left clearfix" style="width:100%;">
                
                        <div class="search-box clearfix">
                            <form action="#" class="search-form pull-left clearfix" method="get">
                                <input type="text" onBlur="if (this.value == '')
                                    this.value = this.defaultValue;" onFocus="if (this.value == this.defaultValue)
                                    this.value = '';" value="Search" name="s" class="search-text">
                                <button type="submit" class="search-submit"><i class="fa fa-search"></i>
                                </button>
                            </form>
                            <!-- search-form -->
                        </div>
                        <!--search-box-->

                    </div>
                    <!-- left-col -->
                </div>
                <!-- clearfix -->
                <div class="clearfix hidden-xs">
                    <div class="left-col pull-left clearfix">
                        
                        <div class="cajabuscar kp-headline-wrapper pull-left clearfix">
                            <div class="search-box clearfix">
                                <form action="#" class="search-form pull-left clearfix" method="get">
                                    <input type="text" placeholder="Buscar" name="s" class="search-text">
                                    <button type="submit" class="search-submit"><i class="fa fa-search"></i></button>
                                </form>
                                <!-- search-form -->
                                
                            </div>
                        </div>
                        <!-- kp-headline-wrapper -->
                        <!-- kp-headline-wrapper -->
                        <div class="social-links pull-right clearfix wishlistButtonMenu" style="">
                            <a href="[base_url]favoritos">
                                <i class="fa fa-heart iconoredondo"></i>
                                FAVORITOS
                            </a>
                        </div>                        
                        <!-- social-links -->
                    </div>
                    <!-- left-col -->
                    <div class="right-col pull-left">
                        <a class="shopping-cart-button clearfix" href="[base_url]tienda_carrito.html">
                            <i class="fa fa-shopping-cart pull-left"></i><span class="pull-left">Carrito de compra</span>
                        </a>
                        
                    </div>
                    <!-- right-col -->
                    
                </div>
                <!-- clearfix -->
                
                
            </div>
            <!-- kopa-header-top-inner -->
        </div>
        <!-- outer -->
    </div>
    <!-- kopa-header-top -->
    <div id="kopa-header-bottom">
        <div class="outer clearfix">
            <nav id="main-nav">
                
                <ul id="main-menu" class="clearfix">
                    <li data-active-main>
                        <a href="[base_url]">Inicio</a>                        
                    </li>
                    <li data-active-empresa>
                        <a href="[base_url]empresa.html">Empresa</a>
                    </li>
                    <li data-active-recopilatorios>
                        <a href="[base_url]albums.html">Albums <i class="fa fa-chevron-down"></i></a>
                        <ul>
                          <?php foreach($this->elements->albums(array('tipo'=>2),4)->result() as $a): ?>
                              <li><a href="<?= $a->link ?>"><?= $a->titulo ?></a></li>
                          <?php endforeach ?>                          
                        </ul> 
                    </li>
                    <li data-active-maxis>
                        <a href="[base_url]maxis.html">MAXI'S <i class="fa fa-chevron-down"></i></a>
                        <ul>
                            <?php foreach($this->elements->albums(array('tipo'=>1),4)->result() as $a): ?>
                                  <li><a href="<?= $a->link ?>"><?= $a->titulo ?></a></li>
                            <?php endforeach ?>
                        </ul>
                    </li>
                    <li data-active-blog>
                        <a href="[base_url]blog">Blog</a>
                    </li>
                    <li data-active-galeria>
                        <a href="[base_url]galeria.html">Galeria</a>                        
                    </li> 
                    <li data-active-contactenos>
                        <a href="[base_url]contacto.html">Contáctenos</a>                        
                    </li> 
                </ul>
                <!-- main-menu -->
                <i class='fa fa-align-justify'></i>
                
                <div class="mobile-menu-wrapper">
                    <ul id="mobile-menu">
                        <li data-active-main>
                            <a href="[base_url]">Inicio</a>                        
                        </li>
                        <li data-active-empresa>
                            <a href="[base_url]empresa.html">Empresa</a>
                        </li>
                        <li data-active-recopilatorios>
                            <a href="[base_url]albums.html">Albums</a>
                        </li>
                        <li data-active-maxis>
                            <a href="[base_url]maxis.html">MAXI'S</a>
                            <ul>
                                <li><a href="[base_url]maxis.html">MAXI'S</a></li>
                                <li><a href="[base_url]maxis.html">MAXI'S</a></li>
                                <li><a href="[base_url]maxis.html">MAXI'S</a></li>
                                <li><a href="[base_url]maxis.html">MAXI'S</a></li>
                            </ul>
                        </li>
                        <li data-active-artistas>
                            <a href="[base_url]artistas.html">Artistas</a>  
                            <ul>
                              <li><a href="[base_url]artistas.html">Artista 1</a></li>
                              <li><a href="[base_url]artistas.html">Artista 1</a></li>
                              <li><a href="[base_url]artistas.html">Artista 1</a></li>
                              <li><a href="[base_url]artistas.html">Artista 1</a></li>
                          </ul>                      
                        </li>                     
                        <li data-active-blog>
                            <a href="[base_url]blog">Blog</a>
                        </li>
                        <li data-active-eventos>
                            <a href="[base_url]eventos.html">Eventos</a>                        
                        </li>
                        <li data-active-galeria>
                            <a href="[base_url]galeria.html">Galeria</a>                        
                        </li> 
                        <li data-active-contactenos>
                            <a href="[base_url]contacto.html">Contáctenos</a>                        
                        </li>
                        <li data-active-contactenos>
                            <a href="[base_url]tienda_carrito.html"><i class="fa fa-shopping-cart"></i> Carrito</a>                        
                        </li> 
                        <li data-active-contactenos>
                            <a href="[base_url]favoritos"><i class="fa fa-heart"></i> Favoritos</a>                        
                        </li>  
                    </ul>
                    <!-- mobile-menu -->
                </div>
                <!-- mobile-menu-wrapper -->
            </nav>
            <!-- main-nav -->
            
        </div>
        <!-- outer -->
        
    </div>
    <!-- kopa-header-bottom -->
    
</div>
<!-- kopa-page-header -->