<!doctype html>
<html lang="en">

	<!-- Google Web Fonts
	================================================== -->

	<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:300,300i,400,400i,700,700i%7CFrank+Ruhl+Libre:300,400,500,700,900" rel="stylesheet">

	<!-- Basic Page Needs
	================================================== -->

	<title><?= empty($title) ? 'Monalco' : $title ?></title>
  	<meta name="keywords" content="<?= empty($keywords) ?'': $keywords ?>" />
	<meta name="description" content="<?= empty($keywords) ?'': strip_tags($description) ?>" /> 
	<meta name="viewport" content="width=device-width, initial-scale=1.0">	
	<link rel="icon" href="<?= empty($favicon) ?'': base_url().'img/'.$favicon ?>" type="image/x-icon"/>	
	<link rel="shortcut icon" href="<?= empty($favicon) ?'': base_url().'img/'.$favicon ?>" type="image/x-icon"/>
	<link href="<?= base_url() ?>js/stocookie/stoCookie.css" rel="stylesheet">

	<script>var URL = '<?= base_url() ?>';</script>


	<?php 
    if(!empty($css_files)):
    foreach($css_files as $file): ?>
    <link type="text/css" rel="stylesheet" href="<?= $file ?>" />
    <?php endforeach; ?>
    <?php endif; ?>

    
	<link rel="stylesheet" href="<?= base_url() ?>theme/theme/css/bootstrap.min.css" media="all" />
    <link rel="stylesheet" href="<?= base_url() ?>theme/theme/css/font-awesome.min.css" media="all" />
    <link rel="stylesheet" href="<?= base_url() ?>theme/theme/css/superfish.css" media="all" />
    <link rel="stylesheet" href="<?= base_url() ?>theme/theme/css/owl.carousel.css" media="all" />
    <link rel="stylesheet" href="<?= base_url() ?>theme/theme/css/owl.theme.css" media="all" />
    <link rel="stylesheet" href="<?= base_url() ?>theme/theme/css/jquery.navgoco.css"/>
	<link rel="stylesheet" href="<?= base_url() ?>theme/theme/css/pro-bars.min.css"/>
    <link rel="stylesheet" href="<?= base_url() ?>theme/theme/css/jplayer.css"/>
    <link rel="stylesheet" href="<?= base_url() ?>theme/theme/css/jquery.mCustomScrollbar.css"/>
    <link rel="stylesheet" href="<?= base_url() ?>theme/theme/css/woocommerce.css"/>
    <link rel="stylesheet" id="ionic-icons-style-css" href="<?= base_url() ?>theme/theme/css/ionicons.css" type="text/css" media="all" />
    <link rel="stylesheet" href="<?= base_url() ?>theme/theme/style_custom.css">
    <link rel="stylesheet" href="<?= base_url() ?>theme/theme/css/responsive.css"/>
    <link rel="stylesheet" href="<?= base_url() ?>js/lightgallery/css/lightgallery.css"/>
    
    <!-- RS5.0 Main Stylesheet -->
	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>theme/theme/css/revolution/css/settings.css">
	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>theme/theme/css/revolution/css/layers.css">
	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>theme/theme/css/revolution/css/navigation.css"> 
	<!-- HTML5 shim, for IE6-8 support of HTML5 elements. All other JS at the end of file. -->


    <link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,200,300,600,700,400italic,300italic' rel='stylesheet'>
    <link href='https://fonts.googleapis.com/css?family=Lato:400,700,300' rel='stylesheet'>
    <script src="<?= base_url() ?>theme/theme/js/modernizr.custom.js"></script>
    <script src="<?= base_url() ?>theme/theme/js/jquery-1.11.1.min.js"></script>
    <!-- Le fav and touch icons --> 
    <style>  
		.icon-like.active{
			color:#e31f22 !important;
		}
		<?php 
			$user = $this->user->log?$this->user->id:$_SESSION['user_id_temp'];
			$favoritos = $this->db->get_where('favoritos',array('user_id'=>$user));
		foreach($favoritos->result() as $f=>$v){
			echo '.icon-like.fav'.$v->albums_id.'{color:#e31f22 !important;}';
		}
		?>
	</style>
</head>


	<?php 
		if(empty($editor)){
			$this->load->view($view); 
		}else{
			echo $view;
		}
	?>		
	<?php $this->load->view('includes/template/scripts'); ?>
	<!--End Scroll to top-->
</body>
</html>