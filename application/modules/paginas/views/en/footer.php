<div class="footer-social-box">
    <div class="wrapper">
        <ul class="socials-link clearfix">            
            <li><a class="clearfix" href="https://www.facebook.com/discotecarecords"><i class="fa fa-facebook pull-left"></i><span class="pull-left">Facebook</span></a></li>
            <li><a class="clearfix" href="https://www.instagram.com/discotecarecords/"><i class="fa fa-instagram pull-left"></i><span class="pull-left">Instagram</span></a></li>
            <li><a class="clearfix" href="https://www.youtube.com/channel/UCY1ZZKx4FWJnharejnMskBQ"><i class="fa fa-youtube pull-left"></i><span class="pull-left">Youtube</span></a></li>
            
        </ul>
        <!-- socials-link -->
        
    </div>
    <!-- wrapper -->
    
</div>
<!-- footer-social-box -->
<div id="kopa-page-footer" style="margin-bottom: 20px;">
    <div class="mask"></div>
    <div id="bottom-sidebar">
        <div class="wrapper">
            <div class="row" style="margin-top: 10px;">
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <div id="footer-logo-image">
                        <a href="[base_url]"><img alt="" src="[base_url]theme/theme/placeholders/logo2.png"></a>
                    </div>
                    <div class="widget widget_text">
                        <p>Discoteca records is a new record company which intends to bring back the past dancefloor hits as well as rare old dance tunes in their full length, extended and remixed versions. </p>
                        <p>Our motto is "Quality, Love & Passion" because we firmly believe that by offering the best repertoire and sound quality, as a result of our love for music and passion for our job, our customers can enjoy it with the same vitality and dynamics as the producers and artists originally intended. </p>
                        <p>“What matters most in music is not only to listen to it, but to feel it.”</p>
                    </div>
                    <!-- widget_text -->
                </div>
                
                <div class="col-md-8 col-sm-8 col-xs-12">
                    <div class="widget kopa-contact-widget">
                        <div class="kopa-span-rt-1"></div>
                        <div class="widget-wrap">
                            <h3 class="widget-title style1">CONTACT</h3>
                            <div class="contact-box-1">
                                <form class="contact-form-1 clearfix" action="paginas/frontend/contacto" method="post" onsubmit="sendForm(this); return false;">
                                <div id="response"></div>
                                <p class="input-block">
                                    <input value="Name" onfocus="if(this.value=='Nombre')this.value='';" onblur="if(this.value=='')this.value='Nombre';" id="contact_name-1" name="nombre" class="valid" type="text">
                                </p>
                                <p class="input-block">
                                    <input value="Email" onfocus="if(this.value=='Email')this.value='';" onblur="if(this.value=='')this.value='Email';" id="contact_email-1" name="email" class="valid" type="text">
                                </p>
                                <p class="input-block">
                                    <input value="Subject" onfocus="if(this.value=='Tema')this.value='';" onblur="if(this.value=='')this.value='Tema';" id="contact_subject-1" name="titulo" class="valid" type="text">
                                </p>
                                <p class="textarea-block">
                                    <textarea name="message" id="contact_message" onfocus="if(this.value=='Tu comentario *')this.value='';" onblur="if(this.value=='')this.value='Tu comentario *';" cols="88" rows="2"></textarea>
                                </p>

                                <p class="checkbox">
                                    <input type="checkbox" name="politicas" value="1" style="width:auto; height:auto;"> I read and accept <a href="<?= base_url('aviso-legal') ?>.html">Privacy policies</a>
                                </p>

                                <p class="contact-button clearfix">           
                                    <span><input value="Send" id="submit-contact-1" type="submit"></span>
                                </p>
                                </form>
                                

                            </div>
                            <!-- contact-box-1 -->
                        </div>

                    </div>
                    <!-- widget --> 
            
                </div>
                
            </div>
            <!-- row -->

                
            </div>
            <!-- kopa-featured-audio-widget -->
            
            
        </div>
        <!-- wrapper -->

        
    <!-- bottom-sidebar -->
    <footer id="kopa-footer">
        <div class="wrapper clearfix">
            <div class="row">
                <div class="col-xs-12 col-md-7">
                    <p id="copyright" class="pull-left">© 2018 Discoteca Records. All rights reserved | <a href="<?= base_url('aviso-legal.html') ?>">Aviso legal</a> | <a href="http://jordimagana.com">By Jordi Magana</a></p>
                    <nav id="footer-nav" class="pull-right">
                        <!--<ul id="footer-menu" class="clearfix">
                            <li><a href="#">Rss</a></li>
                            <li><a href="#">Nota Legal</a></li>
                            <li><a href="#">Álbums</a></li>
                            <li><a href="#">Artistas</a></li>
                            <li><a href="#">Noticias</a></li>
                            <li><a href="#">contacto</a></li>
                        </ul>-->
                        <!-- footer-menu -->
                        
                    </nav>
                    <!-- footer-nav -->
                </div>
                <div class="col-xs-12 col-md-5 col-sm-6 col-sm-offset-3 col-md-offset-0">
                    <img src="<?= base_url('img/pago-seguro.png') ?>" style="width:100%;">
                </div>
            </div>
            <p id="back-top">
                <a href="#top"><i class="fa fa-chevron-up"></i></a>
            </p>
            
        </div>
        <!-- wrapper -->
        
    </footer>
    <!-- kopa-footer -->