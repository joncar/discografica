<body class="kopa-home-1">
[menu]


<!-- Hero Section
============================================= -->
<section id="hero" class="hero hero-3">
    
     <!-- START REVOLUTION SLIDER 5.0.7 -->
        <div id="rev_slider_home_wrapper" class="rev_slider_wrapper fullwidthbanner-container" data-alias="news-gallery34" style="margin:0px auto;background-color:#ffffff;padding:0px;margin-top:0px;margin-bottom:0px;">
          <!-- START REVOLUTION SLIDER 5.0.7 fullwidth mode -->
          <div id="rev_slider_home" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.0.7">
            <ul>

              <?php 
                    $this->db->order_by('orden','ASC');
                    foreach($this->db->get_where('slider',array('idioma'=>$_SESSION['lang']))->result() as $n=>$s): 
                ?>
                    <!-- slide 1 -->
                    <li data-height="['auto', 'auto', 'auto', 'auto']" data-index="rs-<?= $n ?>" data-transition="slidingoverlayhorizontal" data-slotamount="default" data-easein="default" data-easeout="default" data-masterspeed="default" data-thumb="<?= base_url('img/'.$s->foto) ?>" data-rotate="0"  data-fstransition="fade" data-fsmasterspeed="1500" data-fsslotamount="7" data-saveperformance="off" data-title="Make an Impact">
                        <!-- MAIN IMAGE -->
                        <img src="<?= base_url('img/'.$s->foto) ?>"   alt=""  width="1920" height="1280">
                        <!-- LAYER NR. 1 -->
                        <div class="tp-caption" 
                            data-x="center" data-hoffset="0" 
                            data-y="center" data-voffset="-60" 
                            data-whitespace="nowrap"
                            data-width="none"
                            data-height="none"
                            data-transform_idle="o:1;"
                            data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;" 
                            data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" 
                            data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;" 
                            data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" 
                            data-start="500" 
                            data-splitin="none" 
                            data-splitout="none" 
                            data-responsive_offset="on"
                            data-fontsize="['17','17','50','50']"
                            data-lineheight="['45','45','60','60']"
                            data-fontweight="['300','300','300','300']"
                            data-color="#ffffff" style=" margin-bottom:30px;">
                             <?= $s->subtitulo ?>
                        </div>
                        
                        <!-- LAYER NR. 2 -->
                        <div class="tp-caption text-uppercase" 
                            data-x="center" data-hoffset="0" 
                            data-y="center" data-voffset="0" 
                            data-whitespace="nowrap"
                            data-width="none"
                            data-height="none"
                            data-transform_idle="o:1;"
                            data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;" 
                            data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" 
                            data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;" 
                            data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" 
                            data-start="750" 
                            data-splitin="none" 
                            data-splitout="none" 
                            data-responsive_offset="on"
                            data-fontsize="['72','17','50','80']"
                            data-lineheight="['78','45','60','100']"
                            data-fontweight="['400','400','400','400']"
                            data-color="#ffffff" style="font-family: 'Source Sans Pro', sans-serif; margin-bottom:30px; margin-top:30px; "
                            >                        
                            <?= $s->titulo ?>
                        </div>
                    </li>
                <?php endforeach ?>

            </ul>
            <div class="tp-bannertimer tp-bottom" style="height: 5px; background-color: #93d50a;"></div>
          </div>
        </div>

        <!-- END REVOLUTION SLIDER -->
        <script type="text/javascript">
          var tpj=jQuery;         
          var revapi34;
          tpj(document).ready(function() {
            if(tpj("#rev_slider_home").revolution == undefined){
              revslider_showDoubleJqueryError("#rev_slider_home");
            }else{
              revapi34 = tpj("#rev_slider_home").show().revolution({
                sliderType:"standard",
                jsFileLocation:"js/revolution-slider/js/",
                delay:6000,
                disableProgressBar:"off",
                navigation: {
                  keyboardNavigation:"off",
                keyboard_direction: "horizontal",
                mouseScrollNavigation:"off",
                onHoverStop:"off",
                  
                  arrows: {
                        style:"arrow",
                        enable:true,
                        hide_onmobile:true,
                        hide_onleave:false,
                        tmp:'',
                        left: {
                            h_align:"left",
                            v_align:"bottom",
                            h_offset:110,
                            v_offset:57
                        },
                        right: {
                            h_align:"left",
                            v_align:"bottom",
                            h_offset:150,
                            v_offset:57
                        }
                    }
                },
                viewPort: {
                  enable:true,
                  outof:"pause",
                  visible_area:"80%"
                },
                responsiveLevels: [1240, 1024, 778, 240],
                gridwidth:[1230,1230,1230,1230],
                gridheight:[900, 900, 900, 1500],                
                spinner:"off",
              });
            }
          }); /*ready*/
        </script>
      <!-- END REVOLUTION SLIDER -->
    
</section>
<!-- #hero end -->


<div id="main-content" style="padding-bottom: 60px">
    <div class="kopa-rt hidden-xs">        
        <!-- home-slider-box -->
        <div class="kopa-area-fit">
            <div class="widget kopa-audio-list-1-widget">
                <ul class="clearfix">
                    <?php 
                        $this->db->order_by('id','DESC');
                        foreach($this->elements->albums(array('activo'=>1,'mostrar_slider'=>1),4)->result() as $a): 
                    ?>
                    <li>
                        <article class="entry-item">
                            <div class="entry-thumb">
                                <a href="#"><img src="<?= $a->caratula_grande ?>" alt=""></a>
                                <div class="thumb-hover"></div>
                            </div>
                            <div class="entry-content" >                                
                                <div id="<?= 'main3cuadros'.$a->id ?>reprod1" class="reproductor jp-jplayer-single-1"></div>
                                <div id="<?= 'main3cuadros'.$a->id ?>reprodControl1" class="jp-audio jp-audio-single1" role="application" aria-label="media player">
                                    <div class="jp-type-single">
                                        <div class="jp-gui jp-interface">
                                            <div class="jp-controls">
                                                <button class="jp-play" role="button" tabindex="0" data-audio="<?= $a->demo ?>" data-title="<?= $a->titulo ?>">play</button>
                                            </div>
                                            <div class="jp-progress">
                                                <div class="jp-seek-bar" style="width:100%;">
                                                    <div class="jp-play-bar"></div>
                                                </div>
                                            </div>
                                            <div class="jp-volume-controls">
                                                <span class="fa fa-volume-down"></span>
                                                <div class="jp-volume-bar">
                                                    <div class="jp-volume-bar-value"></div>
                                                </div>
                                            </div>
                                            <div class="jp-time-holder">
                                                <div class="jp-current-time" role="timer" aria-label="time">00:00</div>
                                                <div class="jp-duration" role="timer" aria-label="duration">00:00</div>
                                            </div>
                                        </div>
                                        <div class="jp-no-solution">
                                            <span>Update Required</span>
                                            To play the media you will need to either update your browser to a recent version or update your <a href="http://get.adobe.com/flashplayer/" target="_blank">Flash plugin</a>.
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </article>
                    </li>
                    <?php endforeach ?>
                </ul>
                
            </div>
            <!-- widget -->
        </div>
        <!-- kopa-area-fit -->
        <!-- kopa-ab -->
    </div>

    <?php 
        $this->db->order_by('orden_main','ASC');        
        foreach($this->db->get_where('tipos_album',array())->result() as $t): 
            $this->db->order_by('id','DESC');
            $discos = $this->elements->albums(array('tipo'=>$t->id),12);
            if($discos->num_rows()>0):
    ?>
        <!-- kopa-area-9 -->
        <div class="widget kopa-album-2-widget <?= $t->id==5 || $t->id==6?'visible-xs':'hidden-xs hidden-sm hidden-md hidden-lg' ?>" style="background-image: ">
            <header class="kopa-area-dark" style="background:url(<?= base_url() ?>theme/theme/placeholders/parallax/11.gif); background-size: cover; background-position: bottom;">
                <div class="wrapper">  
                    <h3 class="widget-title style1">Top <?= $t->nombre ?></h3>              
                </div>
                <!-- wrapper -->
                <div class="album-icon">
                    <div class="icon-inner-1">
                        <span class="icon-inner-2"></span>
                    </div>
                    <span class="fa fa-music"></span>
                </div>
            </header>
            <div class="kopa-area kopa-area-3 kopa-parallax">
                <div class="span-bg"></div>
                <div class="wrapper">                    
                    <ul class="row kopa-album-list owl-carousel mainSli">
                        <?php                             
                            foreach($discos->result() as $a): ?>
                            <li class="col-xs-12 <?= $discos->num_rows()==1?'col-md-3':'col-md-12' ?>">
                                <?php $this->load->view('_cd_party',array('d'=>$a)); ?>
                            </li>
                        <? endforeach ?>
                    </ul>
                    <div style="text-align:center;">
                        <a class="kopa-button-border" style="border:1px solid #000" href="<?= base_url() ?>albums-<?= $t->id ?>"><?= l('ver-todos') ?></a>
                    </div>
                </div>
                <!-- wrapper-1 -->
            </div>
        </div>
    <?php endif ?>
    <?php endforeach ?>
    
    
    <!-- kopa-area-9 -->
    <div class="widget kopa-album-2-widget">
        <header class="kopa-area-dark">
            
            <div class="kopasliderhome">
                 <?php 
                    $this->db->order_by('orden','ASC');
                    foreach($this->db->get('slider_main')->result() as $s): 
                ?>
                    <div class="item">
                        <div class="wrapper">
                            <h3 class="widget-title style1"><?= $s->nombre_es ?></h3>
                            <h5 style="color:white"><?= $s->descripcion_es ?></h5>
                        </div>
                    </div>
                <?php endforeach ?>
            </div>

            <div class="album-icon">
                <div class="icon-inner-1">
                    <span class="icon-inner-2"></span>
                </div>
                <span class="fa fa-music"></span>
            </div>

        </header>




        <div class="widget-content">
            <div class="wrapper">
                <h2 class="widget-title widget-title-s5">Novedades</h2>
                <ul class="row kopa-album-list">
                    <?php 
                        $this->db->order_by('lanzamiento','DESC');
                        $discos = $this->elements->albums(array(),8);
                        foreach($discos->result() as $a): ?>
                        <li class="col-xs-12 col-md-3 col-sm-6">
                            <?php $this->load->view('_cd_party',array('d'=>$a)); ?>
                        </li>
                    <? endforeach ?>
                </ul>
            </div>
            

            <!-- wrapper-1 -->
        </div>
    </div>


    <?php 
        $this->db->order_by('orden_main','ASC');
        foreach($this->db->get_where('tipos_album',array())->result() as $t): 
        $this->db->order_by('id','DESC');
        $discos = $this->elements->albums(array('tipo'=>$t->id),12);
        if($discos->num_rows()>0): 
    ?>
        <!-- kopa-area-9 -->
        <div class="widget kopa-album-2-widget <?= $t->id==5 || $t->id==6?'hidden-xs':'' ?>" style="background-image: ">
            <header class="kopa-area-dark" style="background:url(<?= base_url() ?>theme/theme/placeholders/parallax/11.gif); background-size: cover; background-position: bottom;">
                <div class="wrapper">  
                    <h3 class="widget-title style1">Top <?= $t->nombre ?></h3>              
                </div>
                <!-- wrapper -->
                <div class="album-icon">
                    <div class="icon-inner-1">
                        <span class="icon-inner-2"></span>
                    </div>
                    <span class="fa fa-music"></span>
                </div>
            </header>
            <div class="kopa-area kopa-area-3 kopa-parallax">
                <div class="span-bg"></div>
                <div class="wrapper">                    
                    <ul class="row kopa-album-list owl-carousel mainSli">
                        <?php                             
                            foreach($discos->result() as $a): ?>
                            <li class="col-xs-12 <?= $discos->num_rows()==1?'col-md-3':'col-md-12' ?>">
                                <?php $this->load->view('_cd_party',array('d'=>$a)); ?>
                            </li>
                        <? endforeach ?>
                    </ul>
                    <div style="text-align:center;">
                        <a class="kopa-button-border" style="border:1px solid #000" href="<?= base_url() ?>albums-<?= $t->id ?>"><?= l('ver-todos') ?></a>
                    </div>
                </div>
                <!-- wrapper-1 -->
            </div>
        </div>
    <?php endif ?>
    <?php endforeach ?>


    <section class="kopa-area kopa-area-2 kopa-parallax kopa-area-dark">

            <div class="span-bg"></div>

            <div class="wrapper videosMain kopa-gallery-widget">
                <div class="widget kopa-masonry-widget">
                    <header>
                        <h3 class="widget-title style1" style="text-align: center"><?= l('videos') ?></h3>
                        <!--<p>Fusce hendrerit, mi non lobortis pellentesque, nibh ante accumsan nisi, in tristique nibh nisi nec tortor. <br>Mauris nibh sem, vulputate non consequat in, condimentum eget nulla</p>-->
                    </header>
                </div>
                <div class="masonry-container">
                    <div class="container-masonry clearfix lightgallery">
                        <?php foreach($this->db->get_where('videos')->result() as $g): ?>
                            <div class="item" data-filter-class='["kopa-all","videos"]'  data-src="<?= $g->url ?>" data-poster="<?= $g->thumb ?>">
                                <img src="<?= $g->thumb ?>" alt="">
                                <div class="mask"><a href="#"><i class="fa fa-plus"></i></a></div>
                            </div>
                        <?php endforeach ?>                      
                    </div> 
                </div> 
            </div>
            <!-- wrapper -->
            
        </section>
    
    
    
    <section class="kopa-area kopa-area-5 kopa-parallax kopa-area-dark">
        <div class="span-bg"></div>
        <div class="wrapper">
            <div class="row">
                
                <div class="col-md-12 col-sm-12 col-xs-12" style="padding:0">
                    <div class="widget kopa-masonry-widget">
                        <header>
                            <h3 class="widget-title style1"><?= l('galeria') ?></h3>
                            <!--<p>Fusce hendrerit, mi non lobortis pellentesque, nibh ante accumsan nisi, in tristique nibh nisi nec tortor. <br>Mauris nibh sem, vulputate non consequat in, condimentum eget nulla</p>-->
                        </header>
                        <ul class="kopa-masonry-wrap lightgallery">
                            
                            <?php 
                                $this->db->limit(10);
                                $this->db->order_by("orden","Asc");
                                foreach($this->db->get_where('galeria')->result() as $g): 
                            ?>
                                <li class="ms-item1" data-src="<?= base_url('img/galeria/'.$g->foto) ?>">
                                    <div class="entry-item">
                                        <div class="entry-thumb">
                                            <a href="#">
                                                <img src="<?= base_url('img/galeria/'.$g->foto) ?>" alt="" style="border:3px solid #fff;">
                                            </a>
                                            <div class="thumb-hover">
                                                <a href="#" class="thumb-icon">
                                                    <i class="fa fa-plus"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            <?php endforeach ?>               
                        </ul>                        
                    </div>
                    <div style="text-align:center;">
                        <a class="kopa-button-border" style="border:1px solid #fff" href="<?= base_url('galeria').'.html' ?>"><?= l('ver-mas') ?></a>
                    </div>
                    <!-- widget -->
                    
                </div>
                <!-- col-md-12 -->
                
            </div>
            <!-- row -->
            
        </div>
        <!-- wrapper -->
        
    </section>
    <!-- kopa-area-5 -->
    <section class="kopa-area kopa-area-10 kopa-area-dark" id="recientes">
        <div class="wrapper">
            <div class="row">
                
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="widget kopa-tab-widget">
                        <div class="kopa-tab style1">
                            <ul class="nav nav-tabs">

                                <li class="active"><a href="#e-tab21" data-toggle="tab"><?= l('seleccion-especial') ?></a></li>
                                <li><a href="#e-tab22" data-toggle="tab"><?= l('mas-visitados') ?></a></li>
                                <li><a href="#e-tab23" data-toggle="tab"><?= l('mas-recientes') ?></a></li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="e-tab21">
                                    <ul class="row">
                                        <?php 
                                            $discos = $this->elements->albums(array('seleccion_especial'=>1),6);
                                            foreach($discos->result() as $n=>$a): ?>
                                                <li class="col-md-2 col-sm-2 col-xs-4">
                                                    <article class="entry-item">
                                                        <div class="entry-thumb">
                                                            <a href="<?= $a->link ?>"><img src="<?= $a->caratula ?>" alt=""></a>
                                                            <div class="thumb-hover">
                                                                <a href="<?= $a->link ?>" class="thumb-icon"><i class="fa fa-plus"></i></a>
                                                            </div>
                                                        </div>
                                                    </article>
                                                </li>
                                        <?php endforeach ?>
                                    </ul>
                                    <!-- row -->
                                </div>
                                <div class="tab-pane" id="e-tab22">
                                    
                                    <ul class="row">
                                        
                                        <?php 
                                            $this->db->order_by('visitas','DESC');
                                            $discos = $this->elements->albums(array(),6);
                                            foreach($discos->result() as $n=>$a): ?>
                                                <li class="col-md-2 col-sm-2 col-xs-4">
                                                    <article class="entry-item">
                                                        <div class="entry-thumb">
                                                            <a href="<?= $a->link ?>"><img src="<?= $a->caratula ?>" alt=""></a>
                                                            <div class="thumb-hover">
                                                                <a href="<?= $a->link ?>" class="thumb-icon"><i class="fa fa-plus"></i></a>
                                                            </div>
                                                        </div>
                                                    </article>
                                                </li>
                                        <?php endforeach ?>
                                    </ul>
                                    <!-- row -->
                                </div>
                                <div class="tab-pane" id="e-tab23">
                                    
                                    <ul class="row">
                                        
                                        <?php 
                                            $this->db->order_by('id','DESC');
                                            $discos = $this->elements->albums(array());
                                            foreach($discos->result() as $n=>$a): ?>
                                                <li class="col-md-2 col-sm-2 col-xs-4">
                                                    <article class="entry-item">
                                                        <div class="entry-thumb">
                                                            <a href="<?= $a->link ?>"><img src="<?= $a->caratula ?>" alt=""></a>
                                                            <div class="thumb-hover">
                                                                <a href="<?= $a->link ?>" class="thumb-icon"><i class="fa fa-plus"></i></a>
                                                            </div>
                                                        </div>
                                                    </article>
                                                </li>
                                        <?php endforeach ?>
                                    </ul>
                                    <!-- row -->
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- widget -->
                    
                </div>
                <!-- col-md-12 -->
                
            </div>
            <!-- row -->
            <div class="row">
                
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="widget kopa-audio-download-widget">
                        <h3 class="widget-title style1"><?= l('los-mas-vendidos') ?></h3>
                        <ul class="clearfix">
                            
                            <?php                                     
                                    $discos = $this->elements->getMasVentas();
                                    foreach($discos->result() as $n=>$a): 
                                    if(!empty($a->link)):
                            ?>
                                <li>
                                    <article class="entry-item">
                                        <div class="col-left">
                                            <span><i><?= $n+1 ?></i></span>
                                            <a href="<?= $a->link ?>"><i class="fa fa-shopping-cart"></i></a>
                                        </div>
                                        <div class="entry-content">
                                            <h4 class="entry-title"><a href="<?= $a->link ?>"><?= $a->titulo ?></a></h4>
                                            <p class="entry-author"><?= l('por') ?> <a href="<?= $a->link ?>"><?= $a->artista ?></a></p>
                                        </div>
                                    </article>
                                </li>
                            <?php endif ?>
                            <? endforeach ?>
                        </ul>
                    </div>
                    <!-- widget -->
                    
                </div>
                <!-- col-md-6 -->
                
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="widget kopa-single-article-widget">
                        <h3 class="widget-title style1"><?= l('ultimas-noticias') ?></h3>
                        <?php 
                            $this->db->order_by('id','DESC');
                            $discos = $this->db->get_where('blog');
                            if($discos->num_rows()>0): 
                                $discos = $discos->row();
                        ?>
                        <article class="entry-item">
                            <div class="entry-thumb">
                                <a href="<?= base_url('blog/'.toUrl($discos->id.' '.$discos->titulo)) ?>">
                                    <img src="<?= base_url('img/blog/'.$discos->foto) ?>" alt="">
                                </a>
                                <div class="thumb-hover">
                                    <a href="<?= base_url('blog/'.toUrl($discos->id.' '.$discos->titulo)) ?>" class="thumb-icon"><i class="fa fa-plus"></i></a>
                                </div>
                            </div>
                            <div class="entry-content">
                                <p class="entry-date"><?= strftime('%d %B %Y',strtotime($discos->fecha)) ?></p>
                                <h4 class="entry-title"><a href="<?= base_url('blog/'.toUrl($discos->id.' '.$discos->titulo)) ?>"><?= $discos->titulo ?></a></h4>
                                <p><?= cortar_palabras(strip_tags($discos->texto),10) ?></p>
                                <p><a href="<?= base_url('blog/'.toUrl($discos->id.' '.$discos->titulo)) ?>"><?= l('leer-mas') ?></a></p>
                            </div>
                        </article>
                        <?php endif ?>
                    </div>
                    <!-- widget -->
                    
                </div>
                <!-- col-md-6 -->
                
            </div>
            <!-- row -->
            
        </div>
        <!-- wrapper -->
        
    </section>
    <!-- kopa-area-10 -->
    [footer]
</div>
<!-- main-content -->




<div class="widget kopa-featured-audio-widget audioFlot hidden-xs" style="position:fixed; left:0px; bottom:0px; width:100%; margin-bottom: 0px; z-index:10000">
        <div class="outer">
            <a href="javascript:void(0)" onclick="$('.audioFlot').remove();" class="closeRep" style=""><i class="fa fa-times"></i></a>
            <div class="col-left">
                <div class="album-thumb">
                    <a href="#"><img src="[base_url]theme/theme/images/img/2.png" alt=""></a>
                </div>
                <div class="album-content">
                    <h6><a href="#"><?= l('presentacion') ?></a></h6>
                    <p><a href="#"><?= l('discotecarecords') ?></a></p>
                </div>
            </div>
            <div class="col-right">
                <div class="audio-wrap">
                    <div class="kopa-jp-jplayer"
                    data-audio-mp3="<?= base_url().'audio/'.$this->db->get('ajustes')->row()->audio_presentacion_mp3 ?>"
                    data-audio-ogg="<?= base_url().'audio/'.$this->db->get('ajustes')->row()->audio_presentacion_ogg ?>"
                    >
                    </div>
                    <div class="jp-audio kopa-jp-wrap" role="application" aria-label="media player">
                        <div class="jp-type-playlist">
                            <div class="jp-gui jp-interface">
                                <div class="col-left">
                                    <div class="jp-controls">
                                        <button class="jp-previous" role="button" tabindex="0"></button>
                                        <button class="jp-play" role="button" tabindex="0"></button>
                                        <button class="jp-next" role="button" tabindex="0"></button>
                                        <span class="fa fa-bar-chart"></span>
                                    </div>
                                </div>
                                <div class="col-right">
                                    <div class="jp-progress">
                                        <div class="jp-seek-bar">
                                            <div class="jp-play-bar"></div>
                                        </div>
                                    </div>
                                    <div class="jp-volume-controls">
                                        <span class="fa fa-volume-down"></span>
                                        <div class="jp-volume-bar">
                                            <div class="jp-volume-bar-value"></div>
                                        </div>
                                    </div>
                                    <div class="jp-time-holder">
                                        <div class="jp-current-time" role="timer" aria-label="time">&nbsp;</div>
                                        <div class="jp-duration" role="timer" aria-label="duration">&nbsp;</div>
                                    </div>
                                </div>
                            </div>
                            <div class="jp-playlist">
                                <ul style="display: none !important">
                                    <li>&nbsp;</li>
                                </ul>
                            </div>
                            <div class="jp-no-solution">
                                <span>Update Required</span>
                                To play the media you will need to either update your browser to a recent version or update your <a href="http://get.adobe.com/flashplayer/" target="_blank">Flash plugin</a>.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
    </div>
    
    <article id="recopilatoriosContent" class="contentOculto" style="display: none;">
        <div class="widget kopa-audio-list-widget" style="margin-bottom: 0; position:relative;">
            <a href="javascript:closeListRecop()" style="position: absolute; right: -20px; top:-20px">
                <i class="fa fa-times"></i>
            </a>
            <div class="widget-thumb">
                <div class="entry-thumb">
                    <a href="#"><img src="[base_url]theme/theme/images/img/5.png" alt=""></a>
                </div>
                <ul class="info">
                    <li>
                        <span>2014</span>
                    </li>
                    <li>
                        <span>19 songs</span>
                    </li>
                    <li>
                        <div class="kopa-rating">
                            <ul>
                                <li><span class="fa fa-star"></span></li>
                                <li><span class="fa fa-star"></span></li>
                                <li><span class="fa fa-star"></span></li>
                                <li><span class="fa fa-star"></span></li>
                                <li class="inactive"><span class="fa fa-star"></span></li>
                            </ul>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="widget-content">
                <header>
                    <h3><a href="#">Andreas Weise <span class="precio"> - 20€</span></a></h3>
                    <p><a href="#">Leona</a>&nbsp;-&nbsp;<a href="#">pop</a>&nbsp;-&nbsp;<a href="#" title="Añadir a la lista de favoritos"><i class="fa fa-heart"></i></a></p>
                </header>
                <ul class="row audio-list">
                    <li class="col-md-6 col-sm-6 col-xs-12"><a href="#">Zuper - Glide</a></li>
                    <li class="col-md-6 col-sm-6 col-xs-12"><a href="#">Zuper - Glide</a></li>
                    <li class="col-md-6 col-sm-6 col-xs-12"><a href="#">Lana Del Rey - Ride (Barretso Remix)</a></li>
                    <li class="col-md-6 col-sm-6 col-xs-12"><a href="#">Lana Del Rey - Ride (Barretso Remix)</a></li>
                    <li class="col-md-6 col-sm-6 col-xs-12"><a href="#">Four Tet - Lion (Jamie xx Remix)</a></li>
                    <li class="col-md-6 col-sm-6 col-xs-12"><a href="#">Four Tet - Lion (Jamie xx Remix)</a></li>
                    <li class="col-md-6 col-sm-6 col-xs-12"><a href="#">Jamie xx & Yasmin - Touch Me</a></li>
                    <li class="col-md-6 col-sm-6 col-xs-12"><a href="#">Jamie xx & Yasmin - Touch Me</a></li>
                    <li class="col-md-6 col-sm-6 col-xs-12"><a href="#">Disclosure - Tenderly</a></li>
                    <li class="col-md-6 col-sm-6 col-xs-12"><a href="#">Disclosure - Tenderly</a></li>
                </ul>
            </div>
        </div>
    </article>

    <script>

        var recopHtml = $("#recopilatoriosContent").clone();
        $("#recopilatoriosContent").remove();

        $(".recopilatoriolink").on('click',function(e){
            e.preventDefault();
            selRecopilatorio(this);
        });

        function closeListRecop(){
            $(".recopilatorioContent").remove();
            $(".entry-item.active").removeClass('active');
        }
        
        function selRecopilatorio(e){
            $(".recopilatorioContent").remove();
            $(".entry-item.active").removeClass('active');
            var content = recopHtml.clone();
            content.show();
            content.addClass('recopilatorioContent');
            console.log(e);
            $(e).parents('.owl-wrapper-outer').after(content);
            $(e).parents('.entry-item').addClass('active');
        }    

        $(document).on('ready',function(){
            var x = document.location.href; x = x.split('#'); x = x.length>1?x[1]:x[0]; if(x=='recientes'){$("a[href='#e-tab23']").tab('show'); setTimeout(function(){$("html,body,document").animate({scrollTop:$("#recientes").offset().top},600);},0);}
        });
    </script>