<body class="kopa-event-1-page">
[menu]
<div id="main-content">

        <header class="page-header text-center">

            <div class="mask"></div>

            <div class="page-header-bg-1 page-header-bg"></div>

            <div class="page-header-inner page-header-inner-1">

                <div class="wrapper">
                    
                    <h1 class="page-title">Lista de eventos de este mes</h1>

                    <div class="breadcrumb clearfix">                    
                        <span itemtype="http://data-vocabulary.org/Breadcrumb" itemscope="">
                            <a href="<?= base_url() ?>" itemprop="url">
                                <span itemprop="title">Inicio</span>
                            </a>
                        </span>
                        <span>&nbsp;/&nbsp;</span>
                        <span itemtype="http://data-vocabulary.org/Breadcrumb" itemscope="">
                            <a href="<?= base_url() ?>" itemprop="url">
                                <span itemprop="title"></span>
                            </a>
                        </span>
                        <span>&nbsp;/&nbsp;</span>
                        <span itemtype="http://data-vocabulary.org/Breadcrumb" itemscope="" class="current-page"><span itemprop="title">Eventos</span></span>
                    </div>
                    <!-- breadcrumb -->

                </div>
                <!-- wrapper -->

            </div>
            <!-- page-header-inner -->
            
        </header>
        <!-- page-header -->

        <section class="kopa-area kopa-area-6">

            <div class="wrapper">

                <div class="row">
                
                    <div class="col-md-12 col-sm-12 col-xs-12">

                        <div class="widget kopa-countdown-widget">
                            <span class="kopa-span-rt"></span>
                            <div class="widget-wrap">
                                <h3 class="widget-title style1">SABRINA SALERNO</h3>
                                <div class="countdown-wrap">
                                    <ul class="kopa-countdown">
                                    </ul> 
                                </div>
                                <div class="cd-info">   
                                    <p class="pos-date">18 Abril de 2019</p> 
                                    <p class="pos-location">Discoteca La Sala, Viladecans, Barcelona</p>
                                </div>
                                <ul class="btn-ca clearfix">
                                    <li><a href="#"><i class="fa fa-bolt"></i>Comprar entradas!</a></li>
                                    <li><a href="#"><i class="fa fa-map-marker"></i>Ver localización</a></li>
                                </ul> 
                            </div>
                        </div>
                        <!-- widget --> 
                
                    </div>
                    <!-- col-md-12 -->
                
                </div>
                <!-- row --> 
            
            </div>
            <!-- wrapper -->
            
        </section>
        <!-- kopa-area-6 -->

        <section class="kopa-area kopa-area-12 kopa-area-dark">

            <div class="wrapper">

                <div class="row">
                
                    <div class="col-md-12 col-sm-12 col-xs-12">

                        <div class="widget kopa-event-3-widget">
                            <h3 class="widget-title style1">eventos</h3>

                            <div class="owl-carousel kopa-event-carousel">

                                <div class="item">
                                    <ul class="clearfix">
                                        <li>
                                            <div class="col-1">
                                                <p>Fecha</p>
                                            </div>
                                            <div class="col-2">
                                                <p><a href="#">evento</a></p>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="col-1">
                                                <p>02/Abr</p>
                                            </div>
                                            <div class="col-2">
                                                <p><a href="#">Columbia, SC</a></p>
                                            </div>
                                            <div class="col-3">
                                                <p><a href="#">Field Notes: An interview with Romain L...</a></p>
                                            </div>
                                            <div class="col-4">
                                                <p><a href="#" class="event-button"><span>Comprar</span></a></p>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="col-1">
                                                <p>06/Abr</p>
                                            </div>
                                            <div class="col-2">
                                                <p><a href="#">New Orleans, LA</a></p>
                                            </div>
                                            <div class="col-3">
                                                <p><a href="#">Original Music / Your Year With Nike+</a></p>
                                            </div>
                                            <div class="col-4">
                                                <p><a href="#" class="event-button"><span>Cancelado</span></a></p>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="col-1">
                                                <p>14/Abr</p>
                                            </div>
                                            <div class="col-2">
                                                <p><a href="#">Memphis, TN</a></p>
                                            </div>
                                            <div class="col-3">
                                                <p><a href="#">Fractured Bones & Reputations</a></p>
                                            </div>
                                            <div class="col-4">
                                                <p><a href="#">Próximamente</a></p>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="col-1">
                                                <p>18/Abr</p>
                                            </div>
                                            <div class="col-2">
                                                <p><a href="#">Oakland, CA</a></p>
                                            </div>
                                            <div class="col-3">
                                                <p><a href="#">The Otherside of Sadness</a></p>
                                            </div>
                                            <div class="col-4">
                                                <p><a href="#">A la venta el 05/01/19</a></p>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="col-1">
                                                <p>21/Abr</p>
                                            </div>
                                            <div class="col-2">
                                                <p><a href="#">North Little Rock, AR</a></p>
                                            </div>
                                            <div class="col-3">
                                                <p><a href="#">Picking up the Pieces (Acoustic Version)</a></p>
                                            </div>
                                            <div class="col-4">
                                                <p><a href="#" class="event-button"><span>Comprar</span></a></p>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="col-1">
                                                <p>25/Abr</p>
                                            </div>
                                            <div class="col-2">
                                                <p><a href="#">New Orleans, LA</a></p>
                                            </div>
                                            <div class="col-3">
                                                <p><a href="#">Introducing: Holiday Mixtape Vol. 1</a></p>
                                            </div>
                                            <div class="col-4">
                                                <p><a href="#">Próximamente</a></p>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="col-1">
                                                <p>27/Abr</p>
                                            </div>
                                            <div class="col-2">
                                                <p><a href="#">Memphis, TN</a></p>
                                            </div>
                                            <div class="col-3">
                                                <p><a href="#">Marmoset's Top 20 Albums of 2014</a></p>
                                            </div>
                                            <div class="col-4">
                                                <p><a href="#" class="event-button"><span>Agotado</span></a></p>
                                            </div>
                                        </li>
                                    </ul>
                                </div>

                                <div class="item">
                                    <ul class="clearfix">
                                        <li>
                                            <div class="col-1">
                                                <p>date</p>
                                            </div>
                                            <div class="col-2">
                                                <p><a href="#">evento</a></p>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="col-1">
                                                <p>02/Abr</p>
                                            </div>
                                            <div class="col-2">
                                                <p><a href="#">Columbia, SC</a></p>
                                            </div>
                                            <div class="col-3">
                                                <p><a href="#">Field Notes: An interview with Romain L...</a></p>
                                            </div>
                                            <div class="col-4">
                                                <p><a href="#" class="event-button"><span>Comprar</span></a></p>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="col-1">
                                                <p>06/Abr</p>
                                            </div>
                                            <div class="col-2">
                                                <p><a href="#">New Orleans, LA</a></p>
                                            </div>
                                            <div class="col-3">
                                                <p><a href="#">Original Music / Your Year With Nike+</a></p>
                                            </div>
                                            <div class="col-4">
                                                <p><a href="#" class="event-button"><span>Cancelado</span></a></p>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="col-1">
                                                <p>14/Abr</p>
                                            </div>
                                            <div class="col-2">
                                                <p><a href="#">Memphis, TN</a></p>
                                            </div>
                                            <div class="col-3">
                                                <p><a href="#">Fractured Bones & Reputations</a></p>
                                            </div>
                                            <div class="col-4">
                                                <p><a href="#">Próximamente</a></p>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="col-1">
                                                <p>18/Abr</p>
                                            </div>
                                            <div class="col-2">
                                                <p><a href="#">Oakland, CA</a></p>
                                            </div>
                                            <div class="col-3">
                                                <p><a href="#">The Otherside of Sadness</a></p>
                                            </div>
                                            <div class="col-4">
                                                <p><a href="#">A la venta el 05/01/2017</a></p>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="col-1">
                                                <p>21/Abr</p>
                                            </div>
                                            <div class="col-2">
                                                <p><a href="#">North Little Rock, AR</a></p>
                                            </div>
                                            <div class="col-3">
                                                <p><a href="#">Picking up the Pieces (Acoustic Version)</a></p>
                                            </div>
                                            <div class="col-4">
                                                <p><a href="#" class="event-button"><span>Comprar</span></a></p>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="col-1">
                                                <p>25/Abr</p>
                                            </div>
                                            <div class="col-2">
                                                <p><a href="#">New Orleans, LA</a></p>
                                            </div>
                                            <div class="col-3">
                                                <p><a href="#">Introducing: Holiday Mixtape Vol. 1</a></p>
                                            </div>
                                            <div class="col-4">
                                                <p><a href="#">Próximamente</a></p>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="col-1">
                                                <p>27/Abr</p>
                                            </div>
                                            <div class="col-2">
                                                <p><a href="#">Memphis, TN</a></p>
                                            </div>
                                            <div class="col-3">
                                                <p><a href="#">Marmoset's Top 20 Albums of 2014</a></p>
                                            </div>
                                            <div class="col-4">
                                                <p><a href="#" class="event-button"><span>Agotado</span></a></p>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                
                            </div>
                            <!-- kopa-event-carousel -->
                            
                        </div>
                        <!-- widget --> 
                
                    </div>
                    <!-- col-md-12 -->
                
                </div>
                <!-- row --> 
            
            </div>
            <!-- wrapper -->
            
        </section>
        <!-- kopa-area-12 -->
        
    </div>
    <!-- main-content -->
[footer]