DROP VIEW IF EXISTS view_digitales;
CREATE VIEW view_digitales AS SELECT 
digitales.id,
digitales.albums_id,
ventas.user_id,
albums.referencia,
albums.titulo,
digitales.nombre as Track,
digitales.audio as fichero,
digitales.orden
FROM ventas_detalles
INNER JOIN ventas ON ventas.id = ventas_detalles.ventas_id
INNER JOIN albums ON albums.id = ventas_detalles.productos_id
INNER JOIN digitales ON digitales.id = ventas_detalles.digitales_id
WHERE ventas.procesado >= 2