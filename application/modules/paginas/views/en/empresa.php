<body class="woocommerce woocommerce-page kopa-subpage empresa">
<div id="main-content">
	[menu]
		<header class="page-header have-disc-icon text-center">
			<div class="disc-bg"></div>
            <div class="mask"></div>

            <div class="page-header-bg-2 page-header-bg"></div>

            <div class="page-header-inner page-header-inner-1">

                <div class="wrapper">
                    
                    <h1 class="page-title">Our company</h1>

                    <div class="breadcrumb clearfix">                    
                        <span itemtype="http://data-vocabulary.org/Breadcrumb" itemscope="">
                            <a href="<?= base_url() ?>" itemprop="url">
                                <span itemprop="title">Home</span>
                            </a>
                        </span>
                        <span>&nbsp;/&nbsp;</span>
                        <span itemtype="http://data-vocabulary.org/Breadcrumb" itemscope="">
                            <a href="<?= base_url() ?>" itemprop="url">
                                <span itemprop="title">Company</span>
                            </a>
                        </span>
                        <!-- <span>&nbsp;/&nbsp;</span> -->
                        <!-- <span itemtype="http://data-vocabulary.org/Breadcrumb" itemscope="" class="current-page"><span itemprop="title">Typography</span></span> -->
                    </div>
                    <!-- breadcrumb -->

                </div>
                <!-- wrapper -->

                </div>
                <!-- page-header-inner -->

                <div class="album-icon">
	                <div class="icon-inner-1">
	                    <span class="icon-inner-2"></span>
	                </div>
	                <span class="fa fa-music"></span>
	            </div>
            
        </header>
        <!-- page-header -->
		<div class="wrapper clearfix  kopa-flickr-widget">
			<section class="elements-box mb-40">
				<div class="row">
					<div class="col-xs-12 col-sm-6">
			            
			            <div class="row">
			                <div class="col-md-11 col-sm-12 col-xs-12 mb-40">
			                    <h3 ><span class="kp-dropcap-2">D</span>iscoteca records is a new record company which intends to bring back the past dancefloor hits as well as rare old dance tunes in their full length, extended and remixed versions.</h3>
			                    <p><div class="widget widget_text">
                        <p> </p>
                        <h4>Our motto is "Quality, Love & Passion" because we firmly believe that by offering the best repertoire and sound quality, as a result of our love for music and passion for our job, our customers can enjoy it with the same vitality and dynamics as the producers and artists originally intended.</h4>
                        <h4>“What matters most in music is not only to listen to it, but to feel it.”</h4>
                    </div>
                    </p>
			                </div>
			                <!-- col-md-12 -->
			            </div>
			            <!-- row -->
		            </div>
		            <div class="col-xs-12 col-sm-6">
		            	<div class="element-title">
		                    <h4>Our Productions</h4>
		                    <p>Percentages destined to our types of productions.</p>
		                </div>

		                <div class="row">

		                    <div class="col-md-12 col-sm-12 col-xs-12 mb-40">
		                        
		                        <div class="pro-bar-container color-midnight-blue">
		                            <div class="pro-bar bar-85 color-kopa-gradient" data-pro-bar-percent="85" data-pro-bar-delay="400">
		                                <div class="">COLLECTIONS</div>
		                                <p>85%</p>
		                            </div>
		                        </div>

		                        <div class="pro-bar-container color-midnight-blue">
		                            <div class="pro-bar bar-75 color-kopa-gradient" data-pro-bar-percent="75" data-pro-bar-delay="500">
		                                <div class="">MAXIS</div>
		                                <p>75%</p>
		                            </div>
		                        </div>

		                        <div class="pro-bar-container color-midnight-blue">
		                            <div class="pro-bar bar-60 color-kopa-gradient" data-pro-bar-percent="60" data-pro-bar-delay="600">
		                                <div class="">EVENTS</div>
		                                <p>60%</p>
		                            </div>
		                        </div>

		                    </div>
		                    <!-- col-md-12 -->
		                    
		                </div>
		                <!-- row -->
		            </div>
				</div>
	            
	        </section>
	        </div>


			<div class="kopa-area kopa-area-4 kopa-parallax kopa-area-dark">
	        <div class="wrapper clearfix">

        	<div id="container" class="mb-40">

                <div id="content" role="main">

	        	<div class="clear"></div>

                    <ul class="products">

                        <li class="product">
                            
                            <div>
                                
                                <div class="product-thumb">
                                	<a href="#">        
	                                    
	                                    <img src="[base_url]theme/theme/images/img/11_1.png" alt="">                                    
	                                </a>
	                                <div class="button-box text-center">
	                                	<a href="#"><i class="fa fa-heart"></i></a>
	                                	
	                                </div>
                                </div>
                                <!-- product-thumb -->
                                 <a href="#">
                                    <h3>Pedro Morales</h3>
                                    <footer>                                    	
                                    	<span class="price"><span class="amount">Executive Producer</span>
                                    	
                                    </footer>                                    
                                </a>

                                <a class="button add_to_cart_button product_type_simple" href="mailto:info@discotecarecords.com">Contact</a>

                            </div>                                    

                        </li>
                        <!-- end:product -->

                        <li class="product">
                            
                            <div>
                                
                                <div class="product-thumb">
                                	<a href="#">        
	                                    
	                                    <img src="[base_url]theme/theme/images/img/11.png" alt="">                                    
	                                </a>
	                                <div class="button-box text-center">
	                                	<a href="#"><i class="fa fa-heart"></i></a>
	                                	
	                                </div>
                                </div>
                                <!-- product-thumb -->
                                <a href="#">
                                    <h3>Rafa Carmona</h3>
                                    <footer>                                    	
                                    	<span class="price"><span class="amount">A&R Director</span>
                                    	
                                    </footer>                                    
                                </a>

                                <a class="button add_to_cart_button product_type_simple clearfix" href="mailto:info@discotecarecords.com">Contact</a>

                            </div>                                    

                        </li>
                        <!-- end:product -->

                        <li class="product">
                            
                            <div>
                                
                                <div class="product-thumb">
                                	<a href="#">
	                                    <img src="[base_url]theme/theme/images/img/11_2.png" alt="">
	                                </a>
	                                <div class="button-box text-center">
	                                	<a href="#"><i class="fa fa-heart"></i></a>
	                                	
	                                </div>
                                </div>
                                <!-- product-thumb -->
                                <a href="#">
                                    <h3>Quim Quer</h3>
                                    <footer>                                    	
                                    	<span class="price"><span class="amount">Studio</span>
                                    	
                                    </footer>                                    
                                </a>
                                <a class="button add_to_cart_button product_type_simple" href="mailto:info@discotecarecords.com">Contact</a>

                            </div>                                    

                        </li>
                        <!-- end:product -->

                        
                        
                    </ul>
                    <!-- end:products -->
                </div>
            </div>
		</div>
		</div>
	[footer]
</div>