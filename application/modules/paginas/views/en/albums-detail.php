<body class="kopa-single-album kopa-subpage woocommerce">
	
	<div id="main-content">
        [menu]
        <header class="page-header text-center pagediskheader">

            <div class="disc-bg"></div>

            <div class="mask"></div>

            <div class="topcd activado" style="">
                <div>  
                    <?php if(!empty($d->disco)): ?>
                         <div class="cd">                  
                            <img src="<?= $d->disco ?>" alt="" style="">
                        </div>
                    <?php endif ?>
                    <?php if(!empty($d->disco2)): ?>
                         <div class="cd">                  
                            <img src="<?= $d->disco2 ?>" alt="" style="">
                        </div>
                    <?php endif ?>
                    <?php if(!empty($d->disco3)): ?>
                         <div class="cd">                  
                            <img src="<?= $d->disco3 ?>" alt="" style="">
                        </div>
                    <?php endif ?>
                    <?php if(!empty($d->disco4)): ?>
                         <div class="cd">                  
                            <img src="<?= $d->disco4 ?>" alt="" style="">
                        </div>
                    <?php endif ?>
                    <div class="sd"><span></span></div>
                    <div class="portada" style="background:url(<?= $d->caratula_grande ?>); background-size:cover; background-repeat:no-repeat;"></div>
                </div>
            </div>

            <div class="page-header-bg-1 page-header-bg" style="background:url([base_url]theme/theme/images/background/page-header-bg_1.jpg)"></div>

            <div class="page-header-inner">

                <div class="wrapper">
                    
                    <h1 class="page-title"><?= $d->titulo ?></h1>

                    <div class="breadcrumb clearfix">                    
                        <span itemtype="http://data-vocabulary.org/Breadcrumb" itemscope="">
                            <a href="<?= base_url() ?>" itemprop="url">
                                <span itemprop="title"><?= $d->artista ?></span>
                            </a>
                        </span>                        
                    </div>
                    <!-- breadcrumb -->

                </div>
                <!-- wrapper -->

            </div>

            <div class="album-icon">
                <div class="icon-inner-1">
                    <span class="icon-inner-2"></span>
                </div>
                <span class="fa fa-music"></span>
            </div>
            <div class="pepino">
                <div class="wrapper">
                    <div class="breadcrumb clearfix">
                        <span>
                            <a href="<?= base_url() ?>" itemprop="url">
                                <span>Inicio</span>
                            </a>
                        </span>
                        <span>&nbsp;/&nbsp;</span>
                        <span class="current-page">
                            <span>Albums</span>
                        </span>
                    </div>
                    <!-- breadcrumb -->
                </div>
            </div>    
        </header>
        <!-- page-header -->

        <section class="kopa-area kopa-area-15">

            <div class="wrapper">
            
                <div class="row">
                
                    <div class="col-md-12 col-sm-12 col-xs-12">

                        <div class="widget kopa-album-page-widget">
                            <div class="clearfix">
                                <div class="col-left">
                                    <div class="album-thumb">
                                        <img src="<?= $d->caratula ?>" alt="">
                                    </div>
                                    <h3><?= $d->titulo ?></h3>
                                    <ul class="album-info">
                                        <li>
                                            <p>Artista: <a href="#"><?= $d->artista ?></a></p>
                                        </li>
                                        <li>
                                            <p>Lanzamiento: <span><?= $d->lanzamiento ?></span></p>
                                        </li>
                                        <li>
                                            <p>Genero: <a href="#"><?= $d->genero ?></a></p>
                                        </li>
                                        <li>
                                            <p>Productor: <a href="#"><?= $d->productor ?></a></p>
                                        </li>
                                        <li>
                                            <p>Precio: <span><?= $d->precio ?></span> - <span style="font-size:10px">VAT INCLUIDED</span></p>
                                        </li>
                                        <?php if(!empty($d->tienda)): ?>
                                            <li>
                                                PVR: <?= $d->pvr ?>
                                            </li>
                                        <?php endif ?>
                                        <li style="margin-top: 20px;">
                                            <p>
                                                <a class="button add_to_cart_button product_type_simple" href="javascript:addToCart(<?= $d->id ?>,1)"><?= l('anadir-al-carro') ?></a> 
                                                <span style="float:right"><a href="javascript:addToFav(<?= $d->id ?>)" class="linkcuadro icon-like fav<?= $d->id ?>"  data-toggle="tooltip" data-placement="top" title="Haz añadido este disco a tu lista de favoritos, tu lista la encontrarás en el menú superior de la página en el símbolo del corazón"><i class="fa fa-heart icon-like fav<?= $d->id ?>"></i></a></span>
                                            </p>
                                        </li>
                                    </ul>
                                    <h3><?= l('comparte-este-album') ?></h3>
                                    <div class="kopa-social-links style2"  style="margin-bottom:50px;">
                                        <ul class="clearfix">
                                            <li><a href="https://www.facebook.com/sharer/sharer.php?p[title]=<?= urlencode($d->titulo) ?>&p[summary]=<?= urlencode('Discoteca records tiene este disco a la venta '.$d->titulo) ?>&p[url]=<?= urlencode(base_url('album/'.toUrl($d->titulo))) ?>&p[images][0]=<?= urlencode(base_url('img/albums/'.$d->caratula)) ?>" class="fa fa-facebook"></a></li>
                                            <li><a href="https://twitter.com/share?text=Discotecarecords+tiene+este+disco+a+la+venta+<?= urlencode($d->titulo) ?>&url=<?= base_url('album/'.toUrl($d->titulo)) ?>" class="fa fa-twitter"></a></li>                                            
                                        </ul>
                                    </div>

                                    <div class="woocommerce-checkout-payment" style="margin-top:14px;background: #9b5e8f;color: #fff;padding: 14px;">
                                        <div class="form-row place-order">
                                            <p><b>Note: </b>Always i ship registered mail , for the returned Ítems, the buyer, always pay the shipping costs and also certified.</p>
                                            <p>
                                                In orders to Australia the buyer should know that a 10% increase must be added to the total purchase + shipping for the tax (GST) that the Australian government imposes.
                                            </p>
                                        </div>
                                    </div>

                                </div>
                                <div class="col-right">



                                    <?php if($d->playlists->num_rows()>0): ?>
                                    <div class="audio-wrap mb-30">
                                        <div class="kopa-jp-jplayer4"></div>
                                        <div class="jp-audio kopa-jp-wrap4" role="application" aria-label="media player">
                                            <div class="jp-type-playlist">
                                                <div class="jp-gui jp-interface">
                                                    <header>
                                                        <div class="jp-controls">
                                                            <button class="jp-play" role="button" tabindex="0"></button>
                                                        </div>
                                                        <div class="current-track">
                                                            <p></p>
                                                        </div>
                                                    </header>
                                                    <footer>
                                                        <div class="jp-progress">
                                                            <div class="jp-seek-bar">
                                                                <div class="jp-play-bar"></div>
                                                            </div>
                                                        </div>
                                                        <div class="jp-time-holder">
                                                            <div class="jp-current-time" role="timer" aria-label="time">&nbsp;</div>
                                                        </div>
                                                        <div class="jp-volume-controls">
                                                            <span class="fa fa-volume-down"></span>
                                                            <div class="jp-volume-bar">
                                                                <div class="jp-volume-bar-value"></div>
                                                            </div>
                                                        </div>
                                                    </footer>
                                                </div>
                                                
                                                    <div class="jp-playlist" data-list = "<?= json_encode($d->playlists->json_list) ?>">
                                                        <ul>
                                                            <li>&nbsp;</li>
                                                        </ul>
                                                    </div>
                                                    <div class="jp-no-solution">
                                                        <span>Update Required</span>
                                                        To play the media you will need to either update your browser to a recent version or update your <a href="http://get.adobe.com/flashplayer/" target="_blank">Flash plugin</a>.
                                                    </div>
                                                
                                            </div>
                                        </div>
                                    </div>

                                    <?php endif ?>
                                    <div class="audio-description mb-30">
                                        <?= $d->descripcion ?>
                                    </div>
                                    <?php if(!empty($d->video)): ?>
                                        <div class="video-wrap mb-30">
                                            <h3>Album Promo Video</h3>
    										<div class="video-wrapper">
    											<iframe height="400" allowfullscreen="" src="https://www.youtube.com/embed/<?= $d->video ?>"></iframe> 
    										</div>
                                        </div>
                                    <?php endif ?>
                                    
                                </div>
                            </div>
                        </div>
                        <!-- widget --> 
                
                    </div>
                    <!-- col-md-12 -->
                
                </div>
                <!-- row --> 

            </div>
            <!-- wrapper -->
            
        </section>
        <!-- kopa-area-15 -->
        
    </div>
    <!-- main-content -->
	[footer]

    <script>
        window.onload = function(){
            setTimeout(function(){$(".topcd").addClass('activado')},600)
        }
    </script>