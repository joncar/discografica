<body class="kopa-single-artist kopa-subpage">
	[menu]
	<div id="main-content">

        <header class="page-header text-center">

            <div class="disc-bg"></div>

            <div class="mask"></div>

            <div class="page-header-bg-4 page-header-bg"></div>

            <div class="page-header-inner page-header-inner-1">

                <div class="wrapper">
                    
                    <h1 class="page-title">Normanç</h1>

                    <div class="breadcrumb clearfix">                    
                        <span itemtype="http://data-vocabulary.org/Breadcrumb" itemscope="">
                            <a href="<?= base_url() ?>" itemprop="url">
                                <span itemprop="title">Home</span>
                            </a>
                        </span>
                        <span>&nbsp;/&nbsp;</span>
                        <span itemtype="http://data-vocabulary.org/Breadcrumb" itemscope="" class="current-page"><span itemprop="title">Single Artist</span></span>
                    </div>
                    <!-- breadcrumb -->

                </div>
                <!-- wrapper -->

            </div>
            <!-- page-header-inner -->

            <div class="artist-avatar"></div>    
            
        </header>
        <!-- page-header -->

        <section class="kopa-area kopa-area-15">

            <div class="wrapper">

            	<div class="single-artist-box">

            		<div class="row">
                
	                    <div class="col-md-4 col-sm-4">

	                        <div class="artist-info">

	                        	<h3>Leona</h3>

	                        	<p><span>Origin: </span>London, UK</p>
	                            <p><span>Link: </span><a href="#">Facebook</a><a href="#">Twitter</a></p>
	                            <p><span>Email: </span><a href="mailto:contact@beatmix.com">contact@beatmix.com</a></p>
	                            <p><span>Genres: </span>Drum and Bass</p>

	                        </div>
	                        <!-- artist-info -->

	                        <h3>Share This Album</h3>

	                        <div class="kopa-social-links style2">
	                            <ul class="clearfix">
	                                <li><a href="#" class="fa fa-facebook"></a></li>
	                                <li><a href="#" class="fa fa-twitter"></a></li>
	                                <li><a href="#" class="fa fa-instagram"></a></li>
	                                <li><a href="#" class="fa fa-rss"></a></li>
	                                <li><a href="#" class="fa fa-google-plus"></a></li>
	                            </ul>
	                        </div>
	                        <!-- kopa-social-links -->
	                
	                    </div>
	                    <!-- col-md-4 -->

						<div class="col-md-8 col-sm-8">
							
							<div class="artist-content">

								<h3 class="artist-title">Heading With Heading Shortcode</h3>

								<p>Nulla vitae elit libero, a pharetra augue. Vestibulum id ligula porta felis euismod semper. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Nullam quis risus eget urna mollis ornare vel eu leo.</p>

								<p>Donec id elit non mi porta gravida at eget metus. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Donec sed odio dui. Curabitur blandit tempus porttitor. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit.</p>

								<br>

								<h3>Track List</h3>

								<div class="audio-wrap">
	                                <div class="kopa-jp-jplayer4"></div>
	                                <div class="jp-audio kopa-jp-wrap4" role="application" aria-label="media player">
	                                    <div class="jp-type-playlist">
	                                        <div class="jp-gui jp-interface">
	                                            <header>
	                                                <div class="jp-controls">
	                                                    <button class="jp-play" role="button" tabindex="0"></button>
	                                                </div>
	                                                <div class="current-track">
	                                                    <p></p>
	                                                </div>
	                                            </header>
	                                            <footer>
	                                                <div class="jp-progress">
	                                                    <div class="jp-seek-bar">
	                                                        <div class="jp-play-bar"></div>
	                                                    </div>
	                                                </div>
	                                                <div class="jp-time-holder">
	                                                    <div class="jp-current-time" role="timer" aria-label="time">&nbsp;</div>
	                                                </div>
	                                                <div class="jp-volume-controls">
	                                                    <span class="fa fa-volume-down"></span>
	                                                    <div class="jp-volume-bar">
	                                                        <div class="jp-volume-bar-value"></div>
	                                                    </div>
	                                                </div>
	                                            </footer>
	                                        </div>
	                                        <div class="jp-playlist">
	                                            <ul>
	                                                <li>&nbsp;</li>
	                                            </ul>
	                                        </div>
	                                        <div class="jp-no-solution">
	                                            <span>Update Required</span>
	                                            To play the media you will need to either update your browser to a recent version or update your <a href="http://get.adobe.com/flashplayer/" target="_blank">Flash plugin</a>.
	                                        </div>
	                                    </div>
	                                </div>
	                            </div>
	                            <div class="video-wrap mb-30">
	                                <h3>Video</h3>
	                                <p>Nulla vitae elit libero, a pharetra augue. Vestibulum id ligula porta felis euismod semper. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Nullam quis risus eget urna mollis ornare vel eu leo.</p>
	                                <br>
									<div class="video-wrapper">
										<iframe height="400" allowfullscreen="" src="http://player.vimeo.com/video/104421565"></iframe> 
									</div>
	                            </div>
	                            <div class="comment-box-1">
	                                <form class="comments-form-1 clearfix" action="processForm.php" method="post" novalidate="novalidate">
	                                    <div class="row">
	                                        <div class="col-md-6 col-sm-6 col-xs-12">
	                                            <p class="input-block">
	                                                <input type="text" value="First Name" onfocus="if(this.value=='First Name')this.value='';" onblur="if(this.value=='')this.value='First Name';" id="comment_name-1" name="name1" class="valid">
	                                            </p>
	                                        </div>
	                                        <!-- col-md-6 -->
	                                        <div class="col-md-6 col-sm-6 col-xs-12">
	                                            <p class="input-block">
	                                                <input type="text" value="Last Name" onfocus="if(this.value=='Last Name')this.value='';" onblur="if(this.value=='')this.value='Last Name';" id="comment_name-2" name="name2" class="valid">
	                                            </p>
	                                        </div>
	                                        <!-- col-md-6 -->
	                                    </div>
	                                    <!-- row --> 
	                                    <div class="row">
	                                        <div class="col-md-6 col-sm-6 col-xs-12">
	                                            <p class="input-block">
	                                                <input type="text" value="Email" onfocus="if(this.value=='Email')this.value='';" onblur="if(this.value=='')this.value='Email';" id="comment_email" name="email" class="valid">
	                                            </p>
	                                        </div>
	                                        <!-- col-md-6 -->
	                                        <div class="col-md-6 col-sm-6 col-xs-12">
	                                            <p class="input-block">
	                                                <input type="text" value="Website" onfocus="if(this.value=='Website')this.value='';" onblur="if(this.value=='')this.value='Website';" id="comment_ws-link" name="website" class="valid">
	                                            </p>
	                                        </div>
	                                        <!-- col-md-6 -->
	                                    </div>
	                                    <p class="textarea-block">  
	                                        <textarea name="message" id="comment_message" cols="88" rows="7"></textarea>
	                                    </p>
	                                    <p class="comment-button clearfix">           
	                                        <span><input type="submit" value="Post Comment" id="submit-comment-1"></span>
	                                    </p>
	                                </form>
	                                <div id="response2"></div>
	                            </div>
	                            <!-- comment-box -->
								
							</div>
							<!-- artist-content -->

						</div>
						<!-- col-md-8 -->
	                
	                </div>
	                <!-- row -->
            		
            	</div>
            	<!-- single-artist-box -->
            


            </div>
            <!-- wrapper -->
            
        </section>
        <!-- kopa-area-15 -->

        <?php $this->load->view('_my_music',array('')); ?>
        
    </div>
    <!-- main-content -->
	[footer]