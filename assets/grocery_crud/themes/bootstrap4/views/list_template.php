<?php	                  
	$this->set_js_lib($this->default_javascript_path.'/'.grocery_CRUD::JQUERY);
	$this->set_js_lib($this->default_javascript_path.'/jquery_plugins/jquery.noty.js');
	$this->set_js_lib($this->default_javascript_path.'/jquery_plugins/config/jquery.noty.config.js');

	if (!$this->is_IE7()) {
		$this->set_js_lib($this->default_javascript_path.'/common/list.js');
	}                  
	$this->set_js($this->default_theme_path.'/bootstrap2/js/cookies.js');
	$this->set_js($this->default_theme_path.'/bootstrap3/js/flexigrid.js');
	$this->set_js($this->default_theme_path.'/bootstrap2/js/jquery.form.js');
	$this->set_js($this->default_javascript_path.'/jquery_plugins/jquery.numeric.min.js');
	$this->set_js($this->default_theme_path.'/bootstrap2/js/jquery.printElement.min.js');                
                  $this->set_js($this->default_theme_path.'/bootstrap2/js/pagination.js');                
	/** Fancybox */
	$this->set_css($this->default_css_path.'/jquery_plugins/fancybox/jquery.fancybox.css');
	$this->set_js($this->default_javascript_path.'/jquery_plugins/jquery.fancybox-1.3.4.js');
	$this->set_js($this->default_javascript_path.'/jquery_plugins/jquery.easing-1.3.pack.js');

	/** Jquery UI */
	$this->load_js_jqueryui();

?>
<script type='text/javascript'>
    var base_url = '<?php echo base_url();?>';
    var subject = '<?php echo $subject?>';
    var ajax_list_info_url = '<?php echo $ajax_list_info_url; ?>';
    var ajax_list = '<?php echo $ajax_list_url; ?>';
    var unique_hash = '<?php echo $unique_hash; ?>';
    var message_alert_delete = "<?php echo $this->l('alert_delete'); ?>";
    var crud_pagin = 1;
    var fragmentos = 1;
    var total_results = <?= $total_results ?>;
</script>


<div id='list-report-error' class='alert alert-danger' style="display:none;"></div>
<div id='list-report-success' class='alert alert-success' <?php if($success_message !== null){?>style="display:block"<?php }else{ ?> style="display:none" <?php }?>><?php
if($success_message !== null){?>
	<p><?php echo $success_message; ?></p>
<?php }
?></div>

<?php echo form_open( $ajax_list_url, 'method="post" id="filtering_form" class="filtering_form" autocomplete = "off" data-ajax-list-info-url="'.$ajax_list_info_url.'" onsubmit="return filterSearchClick(this)"'); ?>
<div class="panel panel-default flexigrid" style='width: 100%;' data-unique-hash="<?php echo $unique_hash; ?>">
    <?= $list_view ?>   
    <!--<div id="pagination">
        <ul class="pagination">
            <li class="prev"><a href="#">Previous Page</a></li>
            <li class="page"><span class="current">1</span></li>
            <li class="page"><a href="#">2</a></li>
            <li class="next"><a href="#">Next Page</a></li>
        </ul>
    </div>-->
    <button style="display: none" title='Refrescar listado'  type="button" class="ajax_refresh_and_loading dt-button buttons-collection buttons-colvis btn btn-white btn-primary btn-bold" tabindex="0" aria-controls="dynamic-table" data-original-title="" title="">
        <span>
            <i class="ace-icon fa fa-refresh bigger-110 grey"></i>
        </span>
    </button>
</div>
<?php echo form_close() ?>
