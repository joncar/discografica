<script src="<?= base_url() ?>theme/theme/js/bootstrap.min.js"></script>
<script src="<?= base_url() ?>theme/theme/js/custom_theme.js" charset="utf-8"></script>
<script src='<?= base_url() ?>js/lightgallery/js/lightgallery.js'></script>
<script src='<?= base_url() ?>js/frame.js'></script>
<script>
	 

    $('#video-gallery').lightGallery({
    	autoplayFirstVideo:true,
    });

    $('.lightgallery').lightGallery({
        
    });



    function addToFav(producto_id){
        <?php if($this->user->log): ?>
            $.post('<?= base_url() ?>tt/addToFav/'+producto_id,{},function(data){
                $(".fav"+producto_id).addClass('active');
                $(".fav"+producto_id).parent().tooltip();
                $(".fav"+producto_id).parent().tooltip('show');
            });
        <?php else: ?>
            document.location.href="<?= base_url('registro/index/add') ?>?redirect=favoritos.html";
        <?php endif ?>
    }

    function addToCart(producto_id,cantidad,type){
        $.post('<?= base_url() ?>tt/addToCart/'+producto_id+'/'+cantidad,{},function(data){
            $(".menubar-cart").html(data);      
            $("#header-cart .header-cart-content").addClass('showed'); 
            $("#header-cart-mobile").addClass('showed');   
            $(".cart-box").addClass('activado');  
        });


    }
    function remToCart(producto_id){
        $.post('<?= base_url() ?>tt/delToCart/'+producto_id,{},function(data){
            $(".cartdetail").html(data);
        }); 
    }

    function remCart(producto_id){
        $.post('<?= base_url() ?>tt/delToCartNav/'+producto_id,{},function(data){
            $(".menubar-cart").html(data);
        });    
    }

    function closeCart(){
        $('.cart-box').removeClass('activado');
    }

    var $navAffix = $("nav");
    $navAffix.affix({
        offset: {
            top: 50/* Change offset form top */
        }
    });
    
</script>

<?php 
if(!empty($js_files)): ?>
<?php foreach($js_files as $file): ?>
<script src="<?= $file ?>"></script>
<?php endforeach; ?>  
<?php endif; ?>


<!-- RS5.0 Core JS Files -->
<script type="text/javascript" src="<?= base_url() ?>theme/theme/css/revolution/js/jquery.themepunch.tools.min.js?rev=5.0"></script>
<script type="text/javascript" src="<?= base_url() ?>theme/theme/css/revolution/js/jquery.themepunch.revolution.min.js?rev=5.0"></script>

<script type="text/javascript" src="<?= base_url() ?>theme/theme/css/revolution/js/extensions/revolution.extension.video.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>theme/theme/css/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>theme/theme/css/revolution/js/extensions/revolution.extension.actions.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>theme/theme/css/revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>theme/theme/css/revolution/js/extensions/revolution.extension.kenburn.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>theme/theme/css/revolution/js/extensions/revolution.extension.navigation.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>theme/theme/css/revolution/js/extensions/revolution.extension.migration.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>theme/theme/css/revolution/js/extensions/revolution.extension.parallax.min.js"></script>
<script type="text/javascript"> 
jQuery(document).ready(function() { 
   var revapi19 = jQuery("#slider1").revolution({      
      delay:6000,
      disableProgressBar:"off",
      spinner:"off",      
      navigation: {
        keyboardNavigation:"off",
        keyboard_direction: "horizontal",
        mouseScrollNavigation:"off",
        onHoverStop:"off",
        arrows: {
            style:"arrow",
            enable:true,
            hide_onmobile:true,
            hide_onleave:false,
            tmp:'',
            left: {
                h_align:"left",
                v_align:"bottom",
                h_offset:110,
                v_offset:57
            },
            right: {
                h_align:"left",
                v_align:"bottom",
                h_offset:150,
                v_offset:57
            }
        }
    }, 
      responsiveLevels: [1240, 1024, 778, 480],
      gridwidth:[1230,1230,1230,1230],
      gridheight:[900, 900, 900, 1500],
      
    });
}); 
</script>

<script>
    window.onload = function(){
        $(".loading").fadeOut(500);
    }
</script>