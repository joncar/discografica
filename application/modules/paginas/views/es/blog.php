<body>
	[menu]
	<div id="main-content">

        <header class="page-header text-center">

        	<div class="disc-bg"></div>

            <div class="mask"></div>

        	<div class="page-header-bg-1 page-header-bg"></div>

        	<div class="page-header-inner page-header-inner-1">

        		<div class="wrapper">
                
	                <h1 class="page-title"><?= l('blog') ?></h1>

	                <div class="breadcrumb clearfix">                    
	                    <span itemtype="http://data-vocabulary.org/Breadcrumb" itemscope="">
	                        <a href="<?= base_url() ?>" itemprop="url">
	                            <span itemprop="title"><?= l('inicio') ?></span>
	                        </a>
	                    </span>
	                    <span>&nbsp;/&nbsp;</span>
	                    <span itemtype="http://data-vocabulary.org/Breadcrumb" itemscope="">
	                        <a href="<?= base_url() ?>" itemprop="url">
	                            <span itemprop="title"><?= l('todas-nuestras-noticias') ?></span>
	                        </a>
	                    </span>
	                    
	                </div>
	                <!-- breadcrumb -->

	            </div>
	            <!-- wrapper -->
        		
        	</div>
        	<!-- page-header-inner -->   
        	<div class="album-icon">
                <div class="icon-inner-1">
                    <span class="icon-inner-2"></span>
                </div>
                <span class="fa fa-music"></span>
            </div>           
            
        </header>
        <!-- page-header -->

        <div class="wrapper clearfix">

        	<div class="widget kopa-masonry-list-1-widget">

                <div class="masonry-list-wrapper">

                    <ul class="clearfix">
                        [foreach:blog]
                        <li class="masonry-item">
                            <article class="entry-item standard-post">
                                <div class="entry-content">
                                    <header><span class="entry-date">[fecha]</span></header>
                                    <h3 class="entry-title"><a href="[link]">[titulo]</a></h3>
                                    <p>[texto]</p>
                                    <!--<ul class="social-links clearfix">
                                        <li><a class="fa fa-facebook" href="#"></a></li>
                                        <li><a class="fa fa-twitter" href="#"></a></li>
                                        <li><a class="fa fa-instagram" href="#"></a></li>
                                        <li><a class="fa fa-youtube" href="#"></a></li>
                                        <li><a class="fa fa-google-plus" href="#"></a></li>
                                    </ul>-->
                                </div>
                                <div class="entry-thumb">
                                    <a href="[link]"><img src="[foto]" alt=""></a>
                                    <div class="mask"><a href="[link]"><i class="fa fa-plus"></i></a></div>
                                </div>
                            </article>
                        </li>
                        [/foreach]
                    </ul>
                    
                </div>
                <!-- masonry-list-wrapper -->
                
            </div>
            <!-- kopa-masonry-list-1-widget -->

        </div>
        <!-- wrapper -->
        
    </div>
    <!-- main-content -->
    [footer]