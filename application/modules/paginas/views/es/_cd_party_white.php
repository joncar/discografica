<article class="entry-item" style="background:white; margin-bottom:30px;">
    <div class="entry-wrap" style="background:transparent;">
        <div class="entry-top">
            <div class="entry-thumb">
                <?php if(!empty($d->precio_tachado)): ?>
                    <span class="onsale"><?= $d->precio_tachado ?></span>
                <?php endif ?>
                <span class="sd"></span>
                <img src="<?= $d->disco ?>" alt="">
                <?php if(!empty($d->disco2)): ?>
                 <img src="<?= $d->disco2 ?>" alt="" style="">
                <?php endif ?>
                <?php if(!empty($d->disco3)): ?>
                 <img src="<?= $d->disco3 ?>" alt="" style="">
                <?php endif ?>
                <?php if(!empty($d->disco4)): ?>
                 <img src="<?= $d->disco4 ?>" alt="" style="">
                <?php endif ?>
                <a href="<?= $d->link ?>">
                        <img src="<?= $d->caratula ?>" alt="">
                    </a>    
                <!--<h4 class="entry-title"><a href="[base_url]albums-rdetail.html">Party cloudy</a></h4>-->
            </div>
        </div>                                                
    </div>
    <div class="entry-content entry-content-white" style="padding:13px; position: relative;">          
            <p class="entry-cat" style="position:absolute; right:18px;font-size: 22px;">
                <?= $d->precio ?>
                <?php if(empty($d->tienda)): ?>
                    <span style="display: block;font-size: 10px;text-align: right;">IVA INC.</span>
                <?php else: ?>
                    <span style="font-size: 10px;text-align: right;">IVA INC.</span>
                    <span style="display:block; font-size: 16px;text-align: right; margin-top: -2px; color:black">PVR: <?= $d->pvr ?></span>
                <?php endif ?>
            </p>
            <h4 class="entry-title"><a href="<?= $d->link ?>"><?= $d->titulo ?></a></h4>
            <p class="entry-cat"><a href="<?= $d->link ?>"><?= $d->artista ?></a></p>
            <footer>
                <ul class="clearfix">
                    <li>
                        <span><?= date("Y",strtotime($d->_lanzamiento)); ?></span>
                    </li>
                    <li>
                        <span><?= $d->canciones ?> tracks</span>
                    </li>
                </ul>
            </footer>
            <div style="float:right; margin-top:-9.4px;">
                <a href="javascript:addToFav(<?= $d->id ?>)" class="linkcuadro" style="top: 61px;" data-toggle="tooltip" data-placement="top" title="Haz añadido este disco a tu lista de favoritos, tu lista la encontrarás en el menú superior de la página en el símbolo del corazón"><i class="fa fa-heart icon-like fav<?= $d->id ?>"></i></a>
                <span><a <?= $d->_precio==0?'opa':'' ?>  class="button add_to_cart_button product_type_simple" href="javascript:addToCart(<?= $d->id ?>,1)">Añadir al carro</a></span>&nbsp;                
            </div>
        </div>
</article>